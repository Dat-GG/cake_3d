﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="FillOutlineData", menuName ="Tools/FillOutlineData")]
public class FillOutlineData : ScriptableObject
{
	public Color StartColor = Color.red, EndColor = Color.red;
	public float Duration = 1;
	public Ease EaseType = Ease.InOutSine;

}
