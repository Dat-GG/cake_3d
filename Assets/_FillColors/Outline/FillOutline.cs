﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FillOutline : MonoBehaviour
{
	[SerializeField] FillOutlineData fillData;

	private Renderer render;

	private void Start()
	{
		render = this.GetComponent<Renderer>();
		render.material.color = fillData.StartColor;
		render.material.DOColor(fillData.EndColor, fillData.Duration).SetEase(fillData.EaseType)
						.SetLoops(-1, LoopType.Yoyo).Play();
	}

}
