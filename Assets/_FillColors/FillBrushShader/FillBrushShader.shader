﻿Shader "Unlit/FillBrushShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		
		_BrushTex ("BrushTex", 2D) = "white" {}
		_BrushSize("BrushSize",Range(0,1)) = 0
		_BrushColor("BrushColor",Color) =(1,1,1,1)
    }
    SubShader
    {
		Tags {"Queue" = "Transparent" }
        LOD 100
		ZWrite On
		Blend SrcAlpha OneMinusSrcAlpha
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float2 uv1 : TEXCOORD1;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
				float2 uv1 : TEXCOORD1;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float4 _BrushTex_ST;
			sampler2D _BrushTex;
			fixed _BrushSize;
			fixed4 _BrushColor;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.uv1 = TRANSFORM_TEX(v.uv1, _BrushTex);
				o.uv1 -= half2(0.5,0.5);
				o.uv1 = o.uv1 / _BrushSize;
				o.uv1 += half2(0.5,0.5);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 mainCol = tex2D(_MainTex, i.uv);
				fixed4 brushCol =  tex2D(_BrushTex, i.uv1);

                return mainCol *  step(0.2,brushCol.a) * _BrushColor;
            }
            ENDCG
        }
    }
}
