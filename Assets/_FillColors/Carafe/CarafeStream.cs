using System.Collections.Generic;
using UnityEngine;

namespace Nama
{
	public class CarafeStream : MonoBehaviour
	{
		private const float MinHeight = -1.8f;
		private const float DropRate = 0.0001f;
		private const float Gravity = -60f;
		private const float GravityHalf = Gravity * 0.5f;

		[SerializeField] private Transform move;
		[SerializeField] private new MeshRenderer renderer;
		[SerializeField] private MeshFilter meshFilter;
		[SerializeField] private float thickness = 0.035f;
		[SerializeField] private Transform pointer;

		private class Particle
		{
			public float X, Y, Height;
			public float StartTime;

			public void Update()
			{
				var t = Time.timeSinceLevelLoad - StartTime;
				Height = GravityHalf * t * t;
			}

			public Vector3 Position => new Vector3(X, Height, Y);
		}

		private readonly LinkedList<Particle> _particles = new LinkedList<Particle>();
		private readonly LinkedList<Particle> _particlePool = new LinkedList<Particle>();
		private bool _pouring;
		private float _time;

		public void SetColor(Color color, Texture texture)
		{
			var material = renderer.material;
			material.color = texture != null ? Color.white : color;
			material.mainTexture = texture;
		}

		private MeshCollider _meshCollider;
		private float _height;
		public void SetMeshCollider(MeshCollider meshCollider)
		{
			_meshCollider = meshCollider;
			_height = transform.position.y;
		}

		public void Begin()
		{
			_pouring = true;
		}

		public bool Pouring { get; private set; }

		public Vector2 PourPosision { get; private set; }
		public Vector2 PourUv { get; private set; }

		public void End()
		{
			_pouring = false;
		}

		private void UpdateParticleCollision()
		{
			for (var node = _particles.First; node != null;)
			{
				var next = node.Next;
				if (node.Value.Height < MinHeight + 0.1)
				{
					PourPosision = new Vector2(node.Value.X, node.Value.Y);
					Pouring = true;
				}
				else
				{
					break;
				}

				node = next;
			}

			for (var node = _particles.First; node != null; )
			{
				var next = node.Next;
				if (node.Value.Height < MinHeight)
				{
					_particlePool.AddLast(node.Value);
					_particles.Remove(node);
				}
				else
				{
					break;
				}
				node = next;
			}
		}

		private void UpdateParticleCollisionWithCollider()
		{
			if (_particles.Count < 2) return;
			var b = _particles.First;
			var a = b.Next;
			var p0 = transform.TransformPoint(a.Value.Position);
			var p1 = transform.TransformPoint(b.Value.Position);
			var v = (p1 - p0).normalized;
			var p = p0 - v * 2.0f;
			var ray = new Ray(p, v);
			var d = Vector3.Distance(p, p1);
			Pouring = _meshCollider.Raycast(ray, out var hit, d);
			if (Pouring)
			{
				PourUv = hit.textureCoord;
			}

			for (var node = _particles.First; node != null; )
			{
				var next = node.Next;
				if (node.Value.Height < -_height)
				{
					_particlePool.AddLast(node.Value);
					_particles.Remove(node);
				}
				else
				{
					break;
				}
				node = next;
			}
		}

		private float _dropTime;
		private float _pourTime;
		private void Update()
		{
			if (_pouring)
			{
				_dropTime -= Time.smoothDeltaTime;
				if (_dropTime <= 0)
				{
					Particle particle;
					if (_particlePool.Count > 0)
					{
						particle = _particlePool.First.Value;
						_particlePool.RemoveFirst();
					}
					else
					{
						particle = new Particle();
					}
					particle.Height = 0;
					particle.X = move.localPosition.x;
					particle.Y = move.localPosition.z;
					particle.StartTime = Time.timeSinceLevelLoad;
					_particles.AddLast(particle);
					_dropTime = DropRate;
				}
			}

			Pouring = false;
			if (_particles.Count > 0)
			{
				foreach (var particle in _particles)
				{
					particle.Update();
				}

				if (_meshCollider)
				{
					UpdateParticleCollisionWithCollider();
				}
				else
				{
					UpdateParticleCollision();
				}
			}

			if (_particles.Count < 2)
			{
				pointer.localPosition = move.localPosition;
				meshFilter.sharedMesh = null;
				return;
			}

			const int nnn = 8;
			var nVertices = (_particles.Count + 1) * nnn;
			var vertices = new Vector3[nVertices];
			var normals = new Vector3[nVertices];
			var uvs = new Vector2[nVertices];

			// Cylinders
			var i = 0;
			var v = 0.0f;
			_pourTime += Time.smoothDeltaTime * 5.0f;
			if (_pourTime > 1.0f) _pourTime -= 1.0f;
			foreach (var particle in _particles)
			{
				var y = -particle.Height / 2.0f - _pourTime;
				v = 1.0f - y + Mathf.Floor(y);
				for (var j = 0; j < nnn; j++)
				{
					var rad = Mathf.PI * 2 * j / nnn;
					var sin = Mathf.Sin(rad);
					var cos = Mathf.Cos(rad);
					vertices[i * nnn + j] = new Vector3(
						particle.X + cos * thickness,
						particle.Height,
						particle.Y + sin * thickness);
					normals[i * nnn + j] = new Vector3(cos, 0, sin);
					uvs[i * nnn + j] = new Vector2((float) j / nnn, v);
				}

				i++;
			}
			var indices = new int[nnn * 6 * (_particles.Count - 1) + (nnn - 2) * 3];
			int iv = 0, ii = 0;
			for (i = 1; i < _particles.Count; i++)
			{
				for (var j = 1; j < nnn; j++)
				{
					indices[ii + 0] = iv + j - 1;
					indices[ii + 1] = iv + nnn + j - 1;
					indices[ii + 2] = iv + j;
					indices[ii + 3] = iv + nnn + j - 1;
					indices[ii + 4] = iv + nnn + j;
					indices[ii + 5] = iv + j;
					ii += 6;
				}

				indices[ii + 0] = iv + nnn - 1;
				indices[ii + 1] = iv + nnn + nnn - 1;
				indices[ii + 2] = iv + 0;
				indices[ii + 3] = iv + nnn + nnn - 1;
				indices[ii + 4] = iv + nnn;
				indices[ii + 5] = iv + 0;
				ii += 6;

				iv += nnn;
			}

			// Cap
			iv += nnn;
			for (var j = 2; j < nnn; j++)
			{
				indices[ii + 0] = iv;
				indices[ii + 2] = iv + j - 1;
				indices[ii + 1] = iv + j;
				ii += 3;
			}

			var last = _particles.Last.Value;
			for (var j = 0; j < nnn; j++)
			{
				var rad = Mathf.PI * 2 * j / nnn;
				var sin = Mathf.Sin(rad);
				var cos = Mathf.Cos(rad);
				vertices[iv] = new Vector3(
					last.X + cos * thickness,
					last.Height,
					last.Y + sin * thickness);
				normals[iv] = new Vector3(0, 1, 0);
				uvs[iv] = new Vector2((float) j / nnn, v);
				iv++;
			}

			var first = _particles.First.Value;
			pointer.localPosition = new Vector3(first.X, 0, first.Y);

			meshFilter.sharedMesh = new Mesh()
			{
				vertices = vertices,
				normals = normals,
				uv = uvs,
				triangles = indices
			};
		}
	}
}