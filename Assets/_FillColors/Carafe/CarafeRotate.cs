﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarafeRotate : MonoBehaviour
{
	public Vector3 StartAngle = Vector3.zero;
	public Vector3 EndAngle = Vector3.zero;
	public Vector3 TableOffset = Vector3.zero;
	public Vector3 RestRotation = Vector3.zero;
}
