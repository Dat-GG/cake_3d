
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

namespace Nama
{
	public class Carafe : MonoBehaviour
	{
		public const float WaitTime = 0.3f;

		private Transform _rotate;
		[SerializeField] private Transform move;
		public Transform CarafeMove => move;
		[SerializeField] private CarafeStream stream;
		[SerializeField] private Pointer pointer;
		[SerializeField] private GameObject completeFX;

		public Pointer Pointer => pointer;

		private bool Finished { get; set; }
		public CarafeStream Stream => stream;
		private Tweener _rotateAnim;
		private Tweener _moveAnim;
		private GameObject _modelSkin;

		[SerializeField] private GameObject[] skins;

		private Color _lastColor = Color.white;
		private CarafeRotate _rotateSetting;
		private bool _endAnimOnStart;
		public bool endAnimOnStart => _endAnimOnStart;
		public void ReloadSkin()
		{
			Init(Profile.Instance.GlazeName);
		}

		public void Init(string skinName)
		{
			SetSkin(skinName);
			completeFX.gameObject.SetActive(false);
		}

		private void SetSkin(string skinName)
		{
			var exist = false;
			foreach (var skin in skins)
			{
				if (skin.name == skinName)
				{
					skin.SetActive(true);
					exist = true;
					_modelSkin = skin;
				}
				else
				{
					skin.SetActive(false);
				}
			}

			if (!exist) // using a default  is the first skin 
			{
				skins[0].SetActive(true);
				_modelSkin = skins[0];
			}

			_rotate = _modelSkin.transform;
			_rotateSetting = _rotate.GetComponent<CarafeRotate>();

			_rotateAnim?.Kill();
			_rotate.localRotation = Quaternion.Euler(_rotateSetting.StartAngle);
		}

		private int _testIndex;
		[ContextMenu("Change Fill")]
		public void TestSkin()
		{
			_testIndex = (_testIndex + 1) % skins.Length;

			SetSkin(skins[_testIndex].name);
			SetColor(_lastColor, null);

		}


		public bool ready;

		public void Press(System.Action begin)
		{
			_rotateAnim?.Kill();
			_rotateAnim = _rotate
				.DOLocalRotate(_rotateSetting.EndAngle, WaitTime)
				.SetEase(Ease.Linear)
				.OnComplete(() =>
				{
					_rotateAnim = null;
					begin();
					ready = true;
					effectPoisonReady = true;
				});

			pointer.Press(Config.SprinkleDelay);
		}

		public void PreUsePointer()
		{
			pointer.Appear();
		}

		public void Release()
		{
			stream.End();
			ready = false;
			effectPoisonReady = false;
			_rotateAnim?.Kill();
			_rotateAnim = _rotate
				.DOLocalRotate(_rotateSetting.StartAngle, WaitTime)
				.SetEase(Ease.Linear)
				.OnComplete(() => _rotateAnim = null);
			pointer.Release();
		}

		public void Show(Transform parent, Vector2 position)
		{
			gameObject.SetActive(true);
			transform.SetParent(parent, true);
			transform.localPosition = Vector3.zero;
			_moveAnim?.Kill();
			Release();
			_moveAnim = move
				.DOLocalMove(new Vector3(position.x, 0, position.y), 0.5f)
				.OnComplete(() =>
				{
					_moveAnim = null;
					_endAnimOnStart = true;
				});

			pointer.Appear();
			Finished = false;
		}

		public void Hide(Transform parent)
		{
			if (Finished)
			{
				return;
			}
			Finished = true;
			Release();
			transform.SetParent(parent, true);
			_moveAnim = null;
			pointer.Disappear();
		}

		public void SetColor(Color color, Texture texture)
		{
			stream.SetColor(color, texture);
			if (!_modelSkin)
				return;
			var wobble = _modelSkin.GetComponentInChildren<Wobble>();
			if (wobble)
			{
				wobble.SetTexture(texture);
				wobble.SetColor(texture != null ? Color.white : color);
			}
			_lastColor = color;
		}

		public void Move(Vector2 pos)
		{
			pos.x = Mathf.Clamp(pos.x, -Statistics.PaintRangeX, Statistics.PaintRangeX);
			pos.y = Mathf.Clamp(pos.y, -Statistics.PaintRangeZ, Statistics.PaintRangeZ);
			move.localPosition = new Vector3(pos.x, 0, pos.y);
		}

		public void Translate(Vector3 v)
		{
			var p = move.localPosition + v;
			p.x = Mathf.Clamp(p.x, -2.0f, 2.0f);
			p.z = Mathf.Clamp(p.z, -2.0f, 2.0f);
			move.localPosition = p;
		}

		public void CompleteFX(System.Action action)
		{
			StartCoroutine(IECompleteFX(action));
		}

		IEnumerator IECompleteFX(System.Action action)
		{
			completeFX.gameObject.SetActive(false);
			yield return new WaitForEndOfFrame();
			completeFX.gameObject.SetActive(true);
			var newParent = Gameplay.Instance.CarafeParent;
			yield return new WaitForSeconds(0.5f);
			move.transform.DOJump(newParent.transform.position + _rotateSetting.TableOffset, 1, 1, 1);
			transform.DORotate(newParent.transform.eulerAngles, 1).OnComplete(delegate
			{
				transform.SetParent(newParent);
			});

			_rotateSetting.transform.DOLocalRotate(_rotateSetting.RestRotation, 1);
			
			yield return new WaitForSeconds(1.5f);
			action?.Invoke();
		}

		private bool effectPoisonReady;
		private GameObject effectPoison;
		private List<GameObject> effectPoisons = new List<GameObject>();
		private float time;
		private float timeBornEff => Gameplay.Instance.bigDecorConfig.DataConfig.timeToBornEffPoison;
		public void EffectFreeStyle()
		{
			if (Gameplay.Level.LevelStype != LevelStyle.FreeStyle)
				return;
			if(!effectPoisonReady)
				return;
			time -= Time.deltaTime;
			if(time<=0)
			{
				if (effectPoisons.Count <= 4)
				{
					var go = Instantiate(Gameplay.Instance.bigDecorConfig.DataConfig.effectPoison,
						pointer.transform.position,
						Quaternion.identity, Gameplay.Level.transform);
					effectPoisons.Add(go);
				}

				time =timeBornEff;
			}
		}

		public void ClearEffectFreeStyle()
		{
			foreach (var go in effectPoisons)
			{
				Destroy(go);
			}
			effectPoisons.Clear();
		}
	}
}