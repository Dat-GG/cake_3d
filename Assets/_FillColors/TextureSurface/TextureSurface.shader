﻿Shader "Unlit/TextureSurface"
{
    Properties
    {
        _Tint("Tint", Color) = (1,1,1,1)
        _MainTex("Texture", 2D) = "white" {}
        _FillAmount("Fill Amount", Range(-10,10)) = 0.0
        [HideInInspector] _WobbleX("WobbleX", Range(-1,1)) = 0.0
        [HideInInspector] _WobbleZ("WobbleZ", Range(-1,1)) = 0.0

        _Scale("Scale",Range(0, 3)) = 0.0
        _Offset("Offset", Vector) = (2,2,2,2)
        _FoamRange("FoamRange", Range(0, 0.5)) = 0.02
        [HideInInspector] _FoamSin("FoamSin", Range(0,0.1)) = 0.0
    }

        SubShader
        {
            Tags {"Queue" = "Transparent"  "DisableBatching" = "True" }

                    Pass
            {
             Zwrite Off
             Blend SrcAlpha OneMinusSrcAlpha
             Cull Off // we want the front and back faces
             AlphaToMask Off // transparency
             Offset -0.1, -1

             CGPROGRAM


             #pragma vertex vert
             #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
              float4 vertex : POSITION;
              float2 uv : TEXCOORD0;
              float3 normal : NORMAL;
            };

            struct v2f
            {
               float2 uv : TEXCOORD0;
               UNITY_FOG_COORDS(1)
               float4 vertex : SV_POSITION;
               float3 viewDir : COLOR;
               float3 normal : COLOR2;
               float fillEdge : TEXCOORD2;
               float2 surfaceUV : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _FillAmount, _WobbleX, _WobbleZ;
            float4  _Tint;
            float _Scale;
            float2 _Offset;
            float _FoamRange;
            float _FoamSin;

            float4 RotateAroundYInDegrees(float4 vertex, float degrees)
            {
               float alpha = degrees * UNITY_PI / 180;
               float sina, cosa;
               sincos(alpha, sina, cosa);
               float2x2 m = float2x2(cosa, sina, -sina, cosa);
               return float4(vertex.yz , mul(m, vertex.xz)).xzyw;
            }


            v2f vert(appdata v)
            {
               v2f o;

               o.vertex = UnityObjectToClipPos(v.vertex);
               o.uv = TRANSFORM_TEX(v.uv, _MainTex);
               UNITY_TRANSFER_FOG(o,o.vertex);
               // get world position of the vertex
               float3 worldPos = mul(unity_ObjectToWorld, v.vertex.xyz);
               // rotate it around XY
               float3 worldPosX = RotateAroundYInDegrees(float4(worldPos,0),360);
               // rotate around XZ
               float3 worldPosZ = float3 (worldPosX.y, worldPosX.z, worldPosX.x);
               // combine rotations with worldPos, based on sine wave from script
               float3 worldPosAdjusted = worldPos + (worldPosX * _WobbleX) + (worldPosZ * _WobbleZ);
               // how high up the liquid is
               o.fillEdge = worldPosAdjusted.y + _FillAmount;

               o.viewDir = normalize(ObjSpaceViewDir(v.vertex));
               o.normal = v.normal;

               o.surfaceUV = float2(worldPos.x, worldPos.z) / _Scale + float2(_Offset.x, _Offset.y);
               return o;
            }

            fixed4 frag(v2f i, fixed facing : VFACE) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.surfaceUV) * _Tint;
            // apply fog
            UNITY_APPLY_FOG(i.fogCoord, col);

            // rest of the liquid
            float4 result = step(i.fillEdge, 0.5) ;

            float4 resultColored = result * col;

            float _Rim = _FoamRange + _FoamSin; 

            float4 foam = (step(i.fillEdge, 0.5) - step(i.fillEdge, (0.5 - _Rim)));
            float4 foamColored = foam * float4(1,1,1,1) * 0.9;

            return resultColored + foamColored;

          }
          ENDCG
         }

        }
}