﻿// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Simplified Diffuse shader. Differences from regular Diffuse one:
// - no Main Color
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "Custom/FillHint" {
    Properties {
        _MainTex("Base (RGB)", 2D) = "black" {}
        _Color("Main Color", COLOR) = (0.3,0.3,0.3,1.0)
    }

    SubShader{
        Tags { "RenderType" = "Transparent" }
        LOD 150
        CGPROGRAM
        #pragma surface surf Lambert noforwardadd

        #include "UnityCG.cginc"

        sampler2D _MainTex;
        float4 _Color;

        struct Input {
            float2 uv_MainTex;
        };

        void surf(Input IN, inout SurfaceOutput o) {
            fixed4 color = tex2D(_MainTex, IN.uv_MainTex);
            fixed a = color.r * color.a;
            clip(a-0.9);
            o.Albedo = _Color.rgb;
            o.Alpha = 1.0;
        }
        ENDCG
    } // SubShader
}