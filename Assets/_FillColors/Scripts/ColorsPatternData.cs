﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace  Nama
{
	[CreateAssetMenu( fileName ="ColosrPatternData", menuName = "Tools/ColosrPatternData")]
	public class ColorsPatternData : ScriptableObject
	{
		public Color[] colors = { Color.white };
	} 
}
