﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Nama
{
	public class FillOptionSelectorEditor : EditorWindow
	{
		public static FillOptionSelectorEditor Instance;

		private FillTextureOptionData[] datas;
		private static FillManualOption refOption;
		private ColorsPatternData[] colorsPatterns;

		private void OnEnable()
		{
			datas = Resources.LoadAll<FillTextureOptionData>("FillPatterns");
			colorsPatterns = Resources.LoadAll<ColorsPatternData>("ColorPatterns");
			Instance = this;
		}

		private void OnDisable()
		{
			Instance = null;
			refOption = null;
		}


		public static void ShowMe(FillManualOption option)
		{
			if(refOption!= null)
			{
				refOption = null;
			}

			refOption = option;

			tab = refOption.PaintText == null ? 0 : 1;
			FillOptionSelectorEditor window = EditorWindow.GetWindow<FillOptionSelectorEditor>();
			window.minSize = new Vector2(400, 200);
			window.Show();
		}

		static int tab = 0;
		private void OnGUI()
		{

			if (refOption == null)
				return;

			tab = GUILayout.Toolbar(tab, new string[] { "Color", "Pattern" });
			switch (tab)
			{
				case 0:
					refOption.BlendColor = EditorGUILayout.ColorField(refOption.BlendColor, GUILayout.Width(100), GUILayout.Height(100));
					GUILayout.Space(30);

					GUILayout.Label(" Mau Co San");

					foreach (var pattern in colorsPatterns)
					{
						GUILayout.BeginHorizontal();

						foreach (var color in pattern.colors)
						{
							Color guiNormalColor = GUI.backgroundColor;
							GUI.backgroundColor = color;
							if (GUILayout.Button("", GUILayout.Width(50), GUILayout.Height(20)))
							{
								refOption.BlendColor = color;

							}
							GUI.backgroundColor = guiNormalColor;
						}

						GUILayout.EndHorizontal();
						GUILayout.Space(10);
					}

					refOption.PaintText = null;
					refOption.Icon = null;
					break;
				case 1:
					GUILayout.BeginHorizontal();
					for (int i = 0; i < datas.Length; i++)
					{
						var style = new GUIStyle(GUI.skin.button);
						style.normal.background = datas[i].Option.PaintText;
						if (GUILayout.Button("", style, GUILayout.Width(50), GUILayout.Height(50)))
						{
							refOption.PaintText = datas[i].Option.PaintText;
							refOption.Icon = datas[i].Option.Icon;
							refOption.BlendColor = datas[i].Option.BlendColor;
						}
					}
					GUILayout.EndHorizontal();
					break;
				default:
					break;
			}


		}
	}

}