﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Nama
{
	[CustomEditor(typeof(FillColorSurface))]
	public class FillColorSurfaceEditor : Editor
	{
		private FillColorSurface surface;

		private void Awake()
		{
			surface = target as FillColorSurface;
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
		}

	}
}
