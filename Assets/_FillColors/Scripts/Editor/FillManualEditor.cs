﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;

namespace Nama
{
	[CustomEditor(typeof(FillManual))]
	public class FillManualEditor : Editor
	{
		public FillManual fillManual => target as FillManual;

		public override void OnInspectorGUI()
		{
			EditorGUI.BeginChangeCheck();
			serializedObject.Update();
			fillManual.drawTexture =
				(Texture2D) EditorGUILayout.ObjectField(fillManual.drawTexture, typeof(Texture2D), false, GUILayout.Width(100), GUILayout.Height(100));

			GUILayout.Space(20);

			GUILayout.BeginHorizontal();
			if (GUILayout.Button("-",GUILayout.Width(50),GUILayout.Height(30)))
			{
				if (fillManual.options.Count > 1)
				{
					fillManual.options.RemoveAt(fillManual.options.Count - 1);
				}
			}
			if (GUILayout.Button("+", GUILayout.Width(50), GUILayout.Height(30)))
			{
				if(fillManual.options.Count < 5)
				{
					fillManual.options.Add(new FillManualOption());
				}
			}

			GUILayout.EndHorizontal();

			GUILayout.Space(20);

			GUILayout.BeginHorizontal();
			for (int i = 0; i < fillManual.options.Count; i++)
			{
				var style = new GUIStyle(GUI.skin.button);
				style.normal.background = fillManual.options[i].PaintText;

				Color guiNormalColor = GUI.backgroundColor;
				if (fillManual.options[i].PaintText == null)
				{
					style = new GUIStyle(GUI.skin.button);
					GUI.backgroundColor = fillManual.options[i].BlendColor;
				}

				if (GUILayout.Button("",style, GUILayout.Width(50), GUILayout.Height(50)))
				{
					FillOptionSelectorEditor.ShowMe(fillManual.options[i]);
				}
				GUI.backgroundColor = guiNormalColor;
			}

			GUILayout.EndHorizontal();

			GUILayout.Space(20);

			fillManual.correctIndex = EditorGUILayout.IntSlider(fillManual.correctIndex, 0, fillManual.options.Count - 1);
			GUILayout.Space(20);

			fillManual.loangOption =(LoangOption) EditorGUILayout.EnumPopup("Toc Do Loang:", fillManual.loangOption);

			GUILayout.Space(20);

			fillManual.patternScale = EditorGUILayout.FloatField("PatternScale", fillManual.patternScale);
			GUILayout.Space(20);
			
			fillManual.showHint = EditorGUILayout.Toggle("Show Hint", fillManual.showHint);
			GUILayout.Space(20);

			fillManual.showAreaFill = EditorGUILayout.Toggle("Show Area Fill", fillManual.showAreaFill);
			GUILayout.Space(20);
			fillManual.optionAds = EditorGUILayout.IntField("Option Ads", fillManual.optionAds);
			GUILayout.Space(20);

			serializedObject.ApplyModifiedProperties();

			if (GUILayout.Button("RefreshPreview"))
			{
				fillManual.RefreshPreview();
			}

			if (EditorGUI.EndChangeCheck())
			{
				fillManual.EditorPreviewRefresh();
				var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
				if (prefabStage != null)
				{
					EditorSceneManager.MarkSceneDirty(prefabStage.scene);
				}
			}
		}

	}

}