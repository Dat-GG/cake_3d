﻿using UnityEngine;

namespace Nama
{
	internal struct GlazeElementInfo
	{
		internal Vector2 Position;
		internal float Radius;
		internal float Lifetime;
	}
	
	public class FillBrushElement : MonoBehaviour
	{
		private bool _isPreview;
		private const float PhotoshopScale = 0.8828125f;
		private bool _stop;
		public float LifeTime { get; private set; }

		private float _stopTime;
		private float _radius;
		private float _startRadius;
		private const float Acceleration = 0f;

		private Material _myMaterial;

		public float Radius => _radius / _fillManual.transformScale;
		public Vector2 Position { get; private set; }

		private FillManual _fillManual;
		private static readonly int BrushTex = Shader.PropertyToID("_BrushTex");
		private static readonly int BrushSize = Shader.PropertyToID("_BrushSize");
		private static readonly int MainTex = Shader.PropertyToID("_MainTex");
		private static readonly int BrushColor1 = Shader.PropertyToID("_BrushColor");

		public bool Active { get; private set; }

		public void Init(FillManual fillManual, Material material)
		{
			_fillManual = fillManual;
			GetComponent<Renderer>().material = material;
			_myMaterial = material;
			Active = false;
		}

		public void Reset()
		{
			_stop = false;
			LifeTime = 0;
			_stopTime = 0;
			_startRadius = BrushConfig.Instance.FillRadius;
			_radius = _startRadius;
			SetSize(_radius);
			Active = true;
		}


		public void SetPostion(Vector2 hitPos)
		{
			Position = hitPos / (FillColorSurface.CakeSize * _fillManual.TextureMaxScale * _fillManual.transformScale);
			var drawPos = -hitPos / _fillManual.Size / _fillManual.patternScale;
			_myMaterial.SetTextureOffset(BrushTex, drawPos);
		}

		public void SetSize(float radius)
		{
			var value = radius / (PhotoshopScale * transform.localScale.x * _fillManual.TextureMaxScale) ;
			_myMaterial.SetFloat(BrushSize,1.45f* value);
		}

		private void LateUpdate()
		{
			if (_isPreview)
				return;

			_radius = _startRadius + LifeTime * _fillManual.FillTuning.LoangSpeed + 0.5f * LifeTime * LifeTime * Acceleration;
			SetSize(_radius);

			LifeTime += Time.smoothDeltaTime;

			if (!_stop) return;
			_stopTime += Time.smoothDeltaTime;
			if (_stopTime <= _fillManual.FillTuning.LoangTime) return;
			gameObject.SetActive(false);
			Active = false;
		}

		public void StopScale()
		{
			_stop = true;
		}

		public void SetOption(FillManualOption option)
		{
			if (option.PaintText == null)
			{
				_myMaterial.SetTexture(MainTex, null);
				_myMaterial.SetColor(BrushColor1, option.BlendColor);
			}
			else
			{
				_myMaterial.SetTexture(MainTex, option.PaintText);
				_myMaterial.SetColor(BrushColor1, Color.white);
			}

		}

		public void RequestMaterial()
		{
			if (_myMaterial == null)
			{
				_myMaterial = this.GetComponent<Renderer>().material;
			}
		}

		public void MaxBrushSize()
		{
			_myMaterial.SetFloat(BrushSize, 2f);
		}

		public void MakePreview()
		{
			_isPreview = true;
		}
	}

}