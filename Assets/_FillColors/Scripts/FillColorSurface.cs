﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Nama
{
	public class FillColorSurface : MonoBehaviour
	{
		[DllImport(Funzilla.Plugin.Name)] private static extern void initGlazeIndices(
			int xCount, int yCount, int[] indices);

		[DllImport(Funzilla.Plugin.Name)] private static extern void initGlazeVertices(
			int xCount,
			int yCount,
			float cellSize,
			Vector3[] vertices,
			Vector3[] normals,
			Vector2[] uvs,
			Color32[] pixels,
			int imgW,
			int imgH,
			int textureSize,
			float depth,
			float[] heights);
		
		[DllImport(Funzilla.Plugin.Name)] internal static extern void updateGlaze(
			Vector3[] vertices,
			float[] targetHeights,
			int nVertices,
			GlazeElementInfo[] elements,
			int nElements,
			float fillSize,
			float maxWaterFallingTime,
			float gravitySpeed);

		[DllImport(Funzilla.Plugin.Name)] internal static extern void resetGlaze(
			Vector3[] vertices, float[] targetHeights, int count);
		
		[DllImport(Funzilla.Plugin.Name)] private static extern void previewGlaze(
			Vector3[] vertices, float[] targetHeights, int count);

		internal const int MAXTextureSize = 1024;
		internal const float CakeSize = 3.735f;
		private float _maxWaterFallingTime;
		internal const float FillArvgHeight = 0.651f;
		private MeshFilter _filter;
		private Material _normalMaterial;

		private float[] _targetHeights;
		private Texture2D _drawTexture;
		private RenderCamera _renderCamera;

		private Vector3[] _vertices;

		private bool _isPreview;
		private FillManual _fillManual;
		private int _vetexSize;
		private int _textureSize;
		private float _cellSize;
		public float FillSize { get; private set; }

		private Material _specialMaterial;

		internal static Mesh CreateGlazeMesh(
			int resolution,
			float cellSize,
			Texture2D drawTexture,
			int textureSize,
			Vector3[] vertices,
			float[] targetHeights,
			float unfillDepth)
		{
			Funzilla.Plugin.Validate();
			var nVertices = resolution * resolution;
			var normals = new Vector3[nVertices];
			var uvs = new Vector2[nVertices];
			initGlazeVertices(
				resolution, 
				resolution, 
				cellSize, 
				vertices, 
				normals,
				uvs,
				drawTexture.GetPixels32(0),
				drawTexture.width,
				drawTexture.height,
				textureSize,
				unfillDepth,
				targetHeights);
			var indices = new int[(resolution - 1) * (resolution - 1) * 6];
			initGlazeIndices(resolution, resolution, indices);
			return new Mesh()
			{
				vertices = vertices,
				normals = normals,
				uv = uvs,
				triangles = indices
			};
		}

		public void Init(RenderCamera renderCamera, Texture2D drawTexture, bool preview,
			FillManual fillManual)
		{
			_renderCamera = renderCamera;
			_fillManual = fillManual;
			_drawTexture = drawTexture;
			_filter = GetComponent<MeshFilter>();
			_cellSize = CakeSize / BrushConfig.Instance.VertexSize;
			_textureSize = Mathf.Max(fillManual.drawTexture.width, fillManual.drawTexture.height);
			_vetexSize = BrushConfig.Instance.VertexSize * _textureSize / MAXTextureSize;
			FillSize = (_vetexSize - 1) * _cellSize;

			var nVertices = _vetexSize * _vetexSize;
			_vertices = new Vector3[nVertices];
			_targetHeights = new float[nVertices];
			_filter.sharedMesh = CreateGlazeMesh(
				_vetexSize,
				_cellSize,
				_drawTexture,
				_textureSize,
				_vertices,
				_targetHeights,
				-2000f);

			_maxWaterFallingTime =
				BrushConfig.Instance.FillHeight * (1 - FillArvgHeight) /
				fillManual.FillTuning.GravitySpeed;
			_specialMaterial = preview
				? _normalMaterial
				: Instantiate(AssetManager.GetGlitterFillMaterial());
		}

		private Material _surfaceMaterial;

		public void SetSufaceMaterial(bool special)
		{
			_surfaceMaterial = special ? _specialMaterial : _normalMaterial;
		}

		public void UpdateSurfaceMaterial()
		{
			if (_surfaceMaterial == null)
			{
				return;
			}

			var render = GetComponent<MeshRenderer>();
			render.sharedMaterial = _surfaceMaterial;
			render.sharedMaterial.mainTexture = _renderCamera.RenderTexture;
		}

		private void Start()
		{
			var render = GetComponent<MeshRenderer>();
			var material = render.material;
			material.mainTexture = _renderCamera.RenderTexture;
			_normalMaterial = material;
		}


		private void Update()
		{
			if (_isPreview)
				return;
			if (_renderCamera.ElementActiveNumber == 0)
				return;

			UpdateElements();
		}

		private readonly List<GlazeElementInfo> _activeElements = new List<GlazeElementInfo>(10);
		private void UpdateElements()
		{
			_activeElements.Clear();
			foreach (var element in _renderCamera.elements.Where(element => element.gameObject.activeSelf))
			{
				_activeElements.Add(new GlazeElementInfo()
				{
					Position = element.Position,
					Radius = element.Radius,
					Lifetime = element.LifeTime
				});
			}

			if (_activeElements.Count <= 0) return;

			updateGlaze(
				_vertices,
				_targetHeights,
				_vertices.Length,
				_activeElements.ToArray(),
				_activeElements.Count,
				FillSize,
				_maxWaterFallingTime,
				_fillManual.FillTuning.GravitySpeed);
			var m = _filter.sharedMesh;
			m.vertices = _vertices;
			m.RecalculateNormals();
		}

		public void ShowPreview()
		{
			_isPreview = true;
			previewGlaze(_vertices, _targetHeights, _vertices.Length);
			var m = _filter.sharedMesh;
			m.vertices = _vertices;
			m.RecalculateNormals();
			_renderCamera.MakePreview();
		}

		internal void Undo()
		{
			resetGlaze(_vertices, _targetHeights, _vertices.Length);
			_filter.sharedMesh.vertices = _vertices;
		}

		#region CHECK_INSIZE

		public bool InSizeShape(Vector2 worldPosition)
		{
			var index = GetIndex(worldPosition / _fillManual.transformScale);
			if (index == -1)
				return false;

			return _targetHeights[index] > 0;
		}

		private int GetIndex(Vector2 worldPosition)
		{
			if (Mathf.Abs(worldPosition.x) > FillSize * 0.5f)
				return -1;
			if (Mathf.Abs(worldPosition.y) > FillSize * 0.5f)
				return -1;

			var cellSize = FillSize / _vetexSize;

			var x = (int) ((worldPosition.x + FillSize * 0.5f) / cellSize);
			x = Mathf.Clamp(x, 0, _vetexSize);

			var y = (int) ((worldPosition.y + FillSize * 0.5f) / cellSize);
			y = Mathf.Clamp(y, 0, _vetexSize);

			return x + y * _vetexSize;
		}

		#endregion
	}
}