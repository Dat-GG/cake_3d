﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Nama
{
	public class RenderCamera : MonoBehaviour
	{
		[SerializeField] private FillBrushElement fillElementPrefab;

		[SerializeField] private Camera renderCamera;
		public Camera Camera => renderCamera;

		private const int DrawElementMax = 15;
		public List<FillBrushElement> elements = new List<FillBrushElement>(DrawElementMax);
		public RenderTexture RenderTexture => renderCamera.targetTexture;

		private int _drawCount;
		private FillManual _fillManual;

		public void Init(FillManual fillManual)
		{

#if UNITY_EDITOR
			if (!UnityEditor.EditorApplication.isPlaying)

				return; // Not Create Elements Pooling below
#endif

			var texture = new RenderTexture(
				1024,
				1024,
				0,
				RenderTextureFormat.ARGB32,
				RenderTextureReadWrite.Default)
			{
				wrapMode = TextureWrapMode.Repeat,
				filterMode = FilterMode.Bilinear,
				autoGenerateMips = false,
				useMipMap = false,
				anisoLevel = 0
			};
			renderCamera.targetTexture = texture;

			_fillManual = fillManual;
			MakeElementPooling();
		}

		private void MakeElementPooling()
		{
			for (var i = renderCamera.transform.childCount - 1; i >= 0; i--)
			{
				DestroyImmediate(transform.GetChild(i).gameObject);
			}
			elements.Clear();
			for (var i = 0; i < DrawElementMax; i++)
			{
				var element = CreateAFillElement();
				element.SetActive(false);
				var elementComponent = element.GetComponent<FillBrushElement>();

				var prefabMaterial = fillElementPrefab.GetComponent<Renderer>().sharedMaterial;
				var newMaterial = new Material(prefabMaterial);
				elementComponent.Init(this._fillManual, newMaterial);
				elements.Add(elementComponent);
			}
		}

		public void Undo()
		{
			_drawCount = 0;
			foreach (var e in elements)
			{
				e.Reset();
				e.gameObject.SetActive(false);
			}
			renderCamera.backgroundColor = Color.black;
			renderCamera.clearFlags = CameraClearFlags.SolidColor;
			renderCamera.Render();
			renderCamera.clearFlags = CameraClearFlags.Depth;
		}

		public void SetCullingMask(int layer)
		{
			renderCamera.cullingMask = layer;
		}


		public void CreateDrawElement(Vector2 hitPos, FillManualOption option)
		{
			if (_drawCount > 0)
			{
				var lastIndex = (_drawCount - 1) % DrawElementMax;
				elements[lastIndex].StopScale();
			}

			var element = elements[_drawCount % DrawElementMax];
			element.gameObject.SetActive(false);
			element.Reset();
			element.SetPostion(hitPos) ;

			// sort order
			var count = 0;
			for (var i = _drawCount; i >= 0; i--)
			{
				var index = i % DrawElementMax;
				var elementPos = new Vector3(0, 0, 1 + 0.2f * count);
				elements[index].transform.localPosition = elementPos;
				count++;

				if (count >= DrawElementMax)
				{
					break;
				}
			}

			element.SetOption(option);
			element.gameObject.SetActive(true);
			_drawCount++;
		}

		internal int ElementActiveNumber => elements.Count(element => element.Active);

		private GameObject CreateAFillElement()
		{
			var fillElement = Instantiate(fillElementPrefab.gameObject, transform, true);
			fillElement.transform.localRotation = Quaternion.identity;
			fillElement.transform.localPosition = Vector3.zero;
			fillElement.transform.localScale = Vector3.one * _fillManual.patternScale / _fillManual.transformScale;
			return fillElement;
		}

		public void MakePreview()
		{
			var fillManual = transform.parent.GetComponent<FillManual>();
			var option = fillManual.CorrectOption;

			var previewElement = CreateAFillElement().GetComponent<FillBrushElement>();
			previewElement.gameObject.SetActive(true);
			previewElement.RequestMaterial();
			previewElement.SetOption(option);
			previewElement.MaxBrushSize();
			previewElement.MakePreview();
			previewElement.transform.localPosition = new Vector3(0, 0, 5);
			previewElement.gameObject.layer = Layers.SpinklesPreview;
			var currentRT = RenderTexture.active;
			RenderTexture.active = renderCamera.targetTexture;
			renderCamera.gameObject.SetActive(true);
			renderCamera.Render();
			renderCamera.gameObject.SetActive(false);
			RenderTexture.active = currentRT;
		}

		public void StopDraw()
		{
			if (_drawCount <= 0) return;
			var lastIndex = (_drawCount - 1) % DrawElementMax;
			elements[lastIndex].StopScale();
		}

		public void StopScale()
		{
			foreach(var e in elements)
			{
				e.SetSize(0);
			}
		}
	}

}