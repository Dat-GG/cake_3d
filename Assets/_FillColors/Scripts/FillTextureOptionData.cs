﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nama
{
	[CreateAssetMenu(fileName ="FillOptionTextureData", menuName = "Tools/FillOptionTextureData")]
	public class FillTextureOptionData : ScriptableObject
	{
		public FillManualOption Option;

	} 
}
