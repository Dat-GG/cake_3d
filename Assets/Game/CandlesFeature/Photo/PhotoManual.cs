using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Nama;
using UnityEngine;
using UnityEngine.UI;

public class PhotoManual : DecorationStep
{
    private Vector3 _touchPos;
    private bool _touched;
    public GameObject _cameraTFX;
    [SerializeField] private GameObject _cameraTFY;
    [SerializeField] private float _speed;
    [SerializeField] private Camera _cam;
    [SerializeField] private Vector3 _target;
    [SerializeField] private float maxX,minX;
    [SerializeField]private Level _level;
    private GameObject _imageEffect;
    private GameObject _photoBttEff;
    private GameObject ui;
    private GameObject _preview;
    [SerializeField]private Vector3 offset = new Vector3(0, 0.5f, 0);
    public override void Init(bool preview)
    {
        _cameraTFY=GetComponentInParent<Level>().gameObject;
        var layer = _cameraTFY.GetComponentInParent<Level>().Layers;
        if (layer.Length>=2)
            _target = layer[layer.Length-2].transform.position+offset;
        else
        {
            _target = layer[0].transform.position+offset;
        }
        _level = GetComponentInParent<Level>();
        _cam = _cameraTFX.GetComponentInChildren<Camera>();
        _cameraTFX.SetActive(false);
    }
    
    public override void Begin()
    {
        Gameplay.Instance.isPhoto = true;
        Gameplay.Instance.FinishStep = Finish;
        Gameplay.Instance.Camera.gameObject.SetActive(false);
        _cameraTFX.SetActive(true);
        Gameplay.Instance.ShowPhotoManual(out _imageEffect,out _photoBttEff,out ui,out _preview);
        TutorialSign.Instance.StartTutorial();
        _cameraTFX.transform.SetParent(Gameplay.Instance.transform);
        _preview.SetActive(false);
        Gameplay.Instance.opBg.SetActive(false);
        //SceneManager.Instance.CloseScene(SceneID.Home);
    }
    public float ControlSensivity = 10f;
    public override void DoFrame()
    {
        // if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
        // {
        //     return;
        // }
        if (Input.GetMouseButtonDown(0))
        {
                
            _touched = true;
            _touchPos = Input.mousePosition;
            TutorialSign.Instance.EndTutorial();
        
        }
        
        if (Input.GetMouseButtonUp(0))
        {
            _touched = false;
        }
        
        if (!_touched) return;
        var v = Input.mousePosition - _touchPos;
        _touchPos = Input.mousePosition;
        if(_cameraTFX==null||_cameraTFY==null)
            return;
        var r = _cameraTFX.transform.localEulerAngles;
        var r1 = _cameraTFY.transform.localEulerAngles;
        var vel = new Vector3(v.x, 0, v.y);
        //if(Mathf.Abs(vel.z *  Screen.width / 1080.0f * ControlSensivity)>1f)
            r.x -= vel.z *  Screen.width / 1080.0f * ControlSensivity*Time.deltaTime;
        if (r.x < 1)
            r.x = 1;
        if (r.x > 85)
            r.x = 85;
        _cameraTFX.transform.localEulerAngles =Vector3.Lerp( _cameraTFX.transform.localEulerAngles,new Vector3(r.x, _cameraTFX.transform.localEulerAngles.y, 0),0.5f);
        //if(Mathf.Abs(vel.x * Screen.width / 1080.0f * ControlSensivity * Time.deltaTime) >1f)
            r1.y -= vel.x *  Screen.width / 1080.0f * ControlSensivity*Time.deltaTime;
        _cameraTFY.transform.localEulerAngles =Vector3.Lerp(_cameraTFY.transform.localEulerAngles,new Vector3(_cameraTFY.transform.localEulerAngles.x, r1.y, 0),0.5f);
        _cam.transform.LookAt(_target);
        //RotateView();
    }

    public override float GetScore()
    {
        return 1;
    }
    private void Finish()
    {
        GetScore();
        _photoBttEff.SetActive(true);
        DOVirtual.DelayedCall(0.2f, () => _photoBttEff.SetActive(false));
        _imageEffect.SetActive(true);
        _imageEffect.GetComponent<Image>().DOColor(new Vector4(1, 1, 1, 0), 1f).OnComplete(()=>
        {
            _imageEffect.SetActive(false);
            Gameplay.Instance.GetTextureForResultPhotoManual(_cam);
            _cam.fieldOfView = 75.0f;
            _cam.DOFieldOfView(60.0f, 1.0f);
            _cam.gameObject.SetActive(true);
            ui.SetActive(false);
            if (Gameplay.Level.LevelStype == LevelStyle.FreeStyle)
                return;
            _preview.SetActive(true);
        });
        Finished = true;
    }
    public override void CompleteFX(Action onFinished)
    {
        DOVirtual.DelayedCall(0.1f, () => onFinished?.Invoke());
    }
    
}
