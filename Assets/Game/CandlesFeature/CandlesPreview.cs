using System;
using System.Collections;
using System.Collections.Generic;
using Nama;
using UnityEngine;

public class CandlesPreview : MonoBehaviour
{
    // Start is called before the first frame update
    private MiniDecorManual _miniDecorManual;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Init(MiniDecorManual miniDecorManual)
    {
        _miniDecorManual = miniDecorManual;
        var box = GetComponent<BoxCollider>();
        box.enabled = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("HighLightCandles"))
        {
            transform.position = other.transform.position;
            _miniDecorManual.BornCandle(true);
            //_miniDecorManual._snap = true;
            other.GetComponent<HighLight>().ChangeToTrue();
            _miniDecorManual.candleAngle = other.GetComponent<HighLight>().localAngle;
            Debug.Log("???????");
        }
    }
}
