using System.Collections;
using System.Collections.Generic;
using Nama;
using UnityEngine;

public class CandlesFireType : MonoBehaviour
{
    public TypeFire TypeFire;
}
public enum TypeFire
{
    CandlesLightRed,
    CandlesLightBlue,
    FireworkRed,
    FireworkBlue
}