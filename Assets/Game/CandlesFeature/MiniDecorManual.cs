using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DG.Tweening;
using Nama;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

namespace Nama
{
    public class MiniDecorManual : DecorationStep
    {
        public List<MiniDecorObj> options;
        public Transform instantiatePos;
        int iSelected; // Currently selected option
        public int correctIndex = 0;
        private GameObject _gameObjPreview;
        private Vector3 _touchPos;
        private bool _touched;
        public GameObject miniDecor;
        private List<MiniDecorObj> listMiniDecor;
        [SerializeField]private bool _isCandle;
        private Vector3 posToScore;
        private bool lightTheCandles;
        private int _candleCount;
        private int _candlePlaceCount;
        private float _delayTimeMini => Gameplay.Instance.bigDecorConfig.DataConfig.delayTimeMiniDecor;
        private GameObject _batluaPref => Gameplay.Instance.bigDecorConfig.DataConfig.batlua;
        private GameObject _batlua;
        public int _candleCoundMax;
        [SerializeField] private LayerMask _layerMask;
        [SerializeField]private List<Vector3> _points;
        private float _minDrawDistance=1f;
        //private Text _countCandlesText;
        private List<MiniDecorObj> ListForScore=new List<MiniDecorObj>();
        private GameObject _highlightObjCandles=>Gameplay.Instance.bigDecorConfig.DataConfig.hightlightObjCandles;
        private List<GameObject> _listhighlight=new List<GameObject>();
        private int _countCandlesToLight;
        private GameObject previewPic;
        public Vector3 candleAngle;
        public TypeFire TypeFire;
        public LevelStyle LevelStyle;
        private float _yPosPreview;
        public int optionAds;
        public override void Init(bool preview)
        {
            CreateDisplay("Display of MiniDecor");
            SetUpMiniDecor();
             if (preview)
             {
                 foreach (var decor in listMiniDecor)
                 {
                     decor.Preview(Display.transform);
                 }
             }
            // else
            // {
            //     InitHint();
            // }
        }

        private void InitHint()
        {
#if UNITY_EDITOR
            if (!EditorApplication.isPlayingOrWillChangePlaymode)
            {
                return;
            }
#endif
            // hint = Instantiate(AssetManager.Instance.FillHint, transform);
            // hint.gameObject.SetActive(false);
            // var scale  = new Vector3(
            //     (FillColorSurface.CakeSize) * drawTexture.width / 1024.0f,
            //     (FillColorSurface.CakeSize) * drawTexture.height / 1024.0f,
            //     1);
            // hint.transform.localScale = scale;
            // hint.material.mainTexture = drawTexture;
            // if(ShowHint)
            // {
            //     var dashedHint = hint.transform.GetChild(0).GetComponent<DashedHint>();
            //     dashedHint.CreateDasedHint(drawTexture);
            // }
            // hint.GetComponent<Renderer>().enabled = ShowAreaFill;
        }

        private void SetUpMiniDecor()
        {
            listMiniDecor = miniDecor.GetComponentsInChildren<MiniDecorObj>().ToList();
            foreach (var m in listMiniDecor)
            {
                m.Init(this);
                
                m.ActivePointer();
                m.gameObject.SetActive(false);
            }

            _candleCoundMax = listMiniDecor.Count;
        }
        public override void Begin()
        {
            LevelStyle = GetComponentInParent<Level>().LevelStype;
            Gameplay.Instance.OnOptionSelected = OnOptionSelected;
            Gameplay.Instance.ShowMiniDecorManualOptions(options,_candleCoundMax,out previewPic,optionAds);
            previewPic.SetActive(false);
            Gameplay.Instance.FinishStep = Finish;
            //_countCandlesText = Gameplay.Instance.countCandlesText;
            if (LevelStyle == LevelStyle.FreeStyle)
            {
                RaycastHit hit;
                if (Physics.Raycast(Gameplay.Instance.Camera.transform.position,
                    Gameplay.Instance.Camera.transform.forward, out hit, 100, _layerMask))
                {
                    _yPosPreview = hit.point.y;
                }

                var instantiatePosPosition = instantiatePos.position;
                instantiatePosPosition.y = _yPosPreview;
                instantiatePos.position = instantiatePosPosition;
                var transformPosition = Display.transform.position;
                transformPosition.y = _yPosPreview;
                Display.transform.position = transformPosition;
            }
            _gameObjPreview=Instantiate(options[iSelected]._preview,instantiatePos.position,Quaternion.identity,Display.transform);
            _gameObjPreview.GetComponent<CandlesPreview>().Init(this);
            Gameplay.Instance.FlowTool = null;
            Gameplay.Instance.Camera.GetComponent<CameraController>().toolX = 0;
            TutorialSign.Instance.StartTutorial();
            if (LevelStyle==LevelStyle.FreeStyle)
            {
                return;
            }
            for (var index = 0; index < listMiniDecor.Count; index++)
            {
                var m = listMiniDecor[index];
                var go = Instantiate(_highlightObjCandles, m.transform.position + new Vector3(0, 0.05f, 0),
                    Quaternion.identity, Display.transform);
                go.transform.localEulerAngles = new Vector3(0, 0, 0);
                go.GetComponent<HighLight>().localAngle = listMiniDecor[index].transform.localEulerAngles;
                //go.GetComponent<HighLight>().TypeFire = listMiniDecor[index].TypeFire;
                _listhighlight.Add(go);
            }

            // for (int i = 0; i < listMiniDecor.Count; i++)
            // {
            //     if (listMiniDecor[i].typeMini == TypeMini.Candle)
            //     {
            //         _candleCoundMax++;
            //     }
            // }
        }

        private void OnOptionSelected(int index) 
        {
            iSelected = index;
            if (_gameObjPreview != null)
            {
                Destroy(_gameObjPreview);
                _gameObjPreview = Instantiate(options[iSelected]._preview, instantiatePos.position,
                    Quaternion.identity, Display.transform);
                _gameObjPreview.GetComponent<CandlesPreview>().Init(this);
                foreach (var t in Gameplay.Instance.listCandleCountText)
                {
                    t.text = (_candleCoundMax - _candlePlaceCount).ToString();
                }
            }
        }
        internal const float ControlSensivity = 0.005f;
        private bool _endtut;
        public bool _snap;
        public override void DoFrame()
        {
            if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }

            if (_gameObjPreview == null /*||_bigDecorPreview==null*/)
                return;
            if (!lightTheCandles)
            {
                var eventSystem = UnityEngine.EventSystems.EventSystem.current;
                if (Input.GetMouseButtonDown(0)&& !eventSystem.IsPointerOverGameObject())
                {
                    TutorialSign.Instance.EndTutorial();
                    Gameplay.Instance.HideUnSelectedOption();
                    _endtut=true;
                    _gameObjPreview.GetComponent<BoxCollider>().enabled = true;
                }
                if(!_endtut)
                    return;
                RaycastHit hit;
                Ray ray = Gameplay.Instance.Camera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, _layerMask))
                {
                    if (_candlePlaceCount >= _candleCoundMax)
                        return;
                    if (!_snap)
                    {
                        _gameObjPreview.transform.position = Vector3.Lerp(_gameObjPreview.transform.position,
                            new Vector3(hit.point.x, _gameObjPreview.transform.position.y, hit.point.z) +
                            new Vector3(0, 0, 0.5f), 0.1f);
                    }
                    if (Input.GetMouseButtonUp(0) && !eventSystem.IsPointerOverGameObject())
                    {
                        BornCandle(false);
                        _snap = false;
                    }
                }
                
            }
            else
            {
                if (Input.GetMouseButtonDown(0))
                {
                    _touched = true;
                    _touchPos = Input.mousePosition;
                    TutorialSign.Instance.EndTutorial();
                }

                if (Input.GetMouseButtonUp(0))
                {
                    _touched = false;
                }

                if (!_touched) return;
                var v = Input.mousePosition - _touchPos;
                _touchPos = Input.mousePosition;
                var pos = _batlua.transform.localPosition;
                _batlua.transform.localPosition = new Vector3(
                    pos.x + v.x * Screen.width / 1080.0f * ControlSensivity,
                    pos.y + v.y * Screen.width / 1080.0f * ControlSensivity, pos.z);
            }
        }

        private float DistanceToLastPoint(Vector3 point,List<Vector3> p)
        {
            return !p.Any() ? Mathf.Infinity : Vector3.Distance(p.Last(), point);
        }
        public override float GetScore()
        {
            Funzilla.DailyQuestManager.UpdateDay();
            Profile.Instance.LitTheCandlesCurrentTime++;
            float score=0;
            float scoreCorrectIndex = 0;
            float scoreCorrectPos=0;
            if (LevelStyle == LevelStyle.FreeStyle)
            {
                if (iSelected == correctIndex||iSelected==optionAds)
                {
                    scoreCorrectIndex = 1f;
                    score = scoreCorrectIndex;
                }
            }
            else
            {
                if (iSelected == correctIndex)
                    scoreCorrectIndex = 0.5f;
                foreach (var t in ListForScore)
                {
                    scoreCorrectPos += t.score;
                }

                var score2 = (scoreCorrectPos / ListForScore.Count) / 2;
                score = score2 + scoreCorrectIndex;
            }

            Debug.Log("????????"+score);
            return score;
        }

        private void Finish()
        {
        
        }

        public void CoutingCandles()
        {
            _candleCount++;
            if (_candleCount >= _countCandlesToLight)
            {
                _batlua.transform.GetChild(1).gameObject.SetActive(true);
                DOVirtual.DelayedCall(1.5f,()=>
                {
                    _batlua.SetActive(false);
                    Finished = true;
                    previewPic.SetActive(true);
                    GetScore();
                    Gameplay.Instance.HideCompleteButton();
                });
            }
        }
        public override void CompleteFX(Action onFinished)
        {
            onFinished?.Invoke();
        }

        public void BornCandle(bool correct)
        {
            var pos = _gameObjPreview.transform.position;
            var go = Instantiate(options[iSelected], pos+new Vector3(0,0.1f,0), _gameObjPreview.transform.rotation,Display.transform);
            go.gameObject.SetLayerRecursively(LayerMask.NameToLayer("NoCollideSprinkle"));
            _candlePlaceCount++;
            foreach (var t in Gameplay.Instance.listCandleCountText)
            {
                t.text=(_candleCoundMax - _candlePlaceCount).ToString();
            }
            _endtut = false;
            Utils.Instance.VibrateMedium();
            _gameObjPreview.transform.position = instantiatePos.position;
            ListForScore.Add(go);
            go.Init(this);
            go.InitFireType();
            go.CalculateScrore(listMiniDecor);
            posToScore = pos;
            if (go.CheckCorrect())
            {
                go.CorrectSituation();
                go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y,
                    go.transform.localPosition.z - 1f);
                go.transform.DOLocalMoveZ(0, 0.5f).SetEase(Ease.InOutBack).OnComplete(() =>
                {
                    if(correct)
                        go.transform.DOLocalRotate(candleAngle, 0.5f);
                });
                _countCandlesToLight++;
                Debug.Log(_countCandlesToLight);
            }
            else
            {
                go.FailSituation();
            }
            if (_candlePlaceCount >= _candleCoundMax)
            {
                foreach (var t in Gameplay.Instance.listCandleCountText)
                {
                    t.transform.parent.parent.gameObject.SetActive(false);
                }
                TutorialSign.Instance.StartTutorial();
                _gameObjPreview.SetActive(false);
                _batlua = Instantiate(_batluaPref, instantiatePos.position, Quaternion.identity,
                    transform);
                _batlua.transform.localPosition = new Vector3(0.05f, -1.22f, -1.3f);
                lightTheCandles = true;
                foreach (var hl in _listhighlight)
                {
                    if (hl.activeInHierarchy)
                        hl.SetActive(false);
                }

                if (_countCandlesToLight == 0)
                {
                    _batlua.transform.GetChild(1).gameObject.SetActive(true);
                    DOVirtual.DelayedCall(1.5f,()=>
                    {
                        _batlua.SetActive(false);
                        Finished = true;
                        GetScore();
                        Gameplay.Instance.HideCompleteButton();
                    });
                }
            }
            
        }

        internal override void Undo()
        {
            for (var index = 0; index < _listhighlight.Count; index++)
            {
                var go = _listhighlight[index];
                Destroy(go);
            }
            _listhighlight.Clear();

            for (var index = 0; index < ListForScore.Count; index++)
            {
                var go = ListForScore[index];
                Destroy(go.gameObject);
            }
            ListForScore.Clear();
            if(_batlua!=null)
                _batlua.SetActive(false);
            if(_gameObjPreview!=null)
                Destroy(_gameObjPreview);
            _countCandlesToLight = 0;
            lightTheCandles = false;
            _candlePlaceCount = 0;
            _candleCount = 0;
            Begin();
            Gameplay.Instance.ShowOptionOnUndo(options);
            Debug.Log("????undocandles");
        }
    }
}

