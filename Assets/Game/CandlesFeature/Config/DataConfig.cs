using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DataConfig
{
    public float delayTimeMainDecor;
    public float delayTimeMiniDecor;
    public GameObject batlua;
    public GameObject vfx;
    public float distanceScoreMiniDecor;
    public GameObject hightlightObjCandles;
    public GameObject photoManual;
    public GameObject camphotoManual;
    [Header("FreeStyle")] 
    public GameObject effectPoison;

    public float timeToBornEffPoison;
    public float maxTotalDistance;
    public float minDistance;
}
