using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DG.Tweening;
using Nama;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

namespace Nama
{
    public class BigDecorManual : DecorationStep
    {
        public List<BigDecor> options;
        public Transform instantiatePos;
        int iSelected; // Currently selected option
        public int correctIndex = 0;
        public Transform correctPos;
        private GameObject _gameObjPreview;
        private Vector3 _touchPos;
        private bool _touched;
        public List<MiniDecorObj> listMiniDecor;
        //private BigDecorPreview _bigDecorPreview;
        [SerializeField]private bool _isCandle;
        private Vector3 posToScore;
        private bool lightTheCandles;
        private int _candleCount;
        private float _delayTimeMain => Gameplay.Instance.bigDecorConfig.DataConfig.delayTimeMainDecor;
        public override void Init(bool preview)
        {
            CreateDisplay("Display of BigDecor");
            if (preview)
            {
                var go = Instantiate(options[correctIndex].Model,Vector3.zero, Quaternion.identity,correctPos);
                go.transform.localPosition = Vector3.zero;
                go.transform.localEulerAngles=new Vector3(-90,0,0);
                go.SetLayerRecursively(Layers.SpinklesPreview);
                // foreach (var decor in listMiniDecor)
                // {
                //     decor.Preview(Display.transform);
                // }
            }
            else
            {
                InitHint();
            }
        }

        private void InitHint()
        {
#if UNITY_EDITOR
            if (!EditorApplication.isPlayingOrWillChangePlaymode)
            {
                return;
            }
#endif
            // hint = Instantiate(AssetManager.Instance.FillHint, transform);
            // hint.gameObject.SetActive(false);
            // var scale  = new Vector3(
            //     (FillColorSurface.CakeSize) * drawTexture.width / 1024.0f,
            //     (FillColorSurface.CakeSize) * drawTexture.height / 1024.0f,
            //     1);
            // hint.transform.localScale = scale;
            // hint.material.mainTexture = drawTexture;
            // if(ShowHint)
            // {
            //     var dashedHint = hint.transform.GetChild(0).GetComponent<DashedHint>();
            //     dashedHint.CreateDasedHint(drawTexture);
            // }
            // hint.GetComponent<Renderer>().enabled = ShowAreaFill;
        }
        public override void Begin()
        {
            //hint.transform.localPosition -= new Vector3(0, 0, 0.11f);
            TutorialSign.Instance.StartTutorial();
            Gameplay.Instance.OnOptionSelected = OnOptionSelected;
            Gameplay.Instance.ShowBigDecorManualOptions(options);
            Gameplay.Instance.FinishStep = Finish;
            _gameObjPreview=Instantiate(options[iSelected].ModelPreview,instantiatePos.position,correctPos.rotation,Display.transform);
            //_bigDecorPreview = _gameObjPreview.GetComponent<BigDecorPreview>();
            //hint.gameObject.SetActive(true);
            //Gameplay.Instance.FlowTool = null;
            Gameplay.Instance.Camera.GetComponent<CameraController>().toolX = 0;

        }

        private void OnOptionSelected(int index) 
        {
            iSelected = index;
            if (_gameObjPreview != null)
            {
                Destroy(_gameObjPreview);
                _gameObjPreview = Instantiate(options[iSelected].ModelPreview,instantiatePos.position,correctPos.rotation,Display.transform);
                //_bigDecorPreview = _gameObjPreview.GetComponent<BigDecorPreview>();

            }
        }
        public override void DoFrame()
        {
            if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }
            if (Input.GetMouseButtonDown(0))
            {
                
                _touched = true;
                _touchPos = Input.mousePosition;
                TutorialSign.Instance.EndTutorial();

            }

            if (Input.GetMouseButtonUp(0))
            {
                _touched = false;
                Gameplay.Instance.ShowCompleteButton();
            }

            if (!_touched) return;
            var v = Input.mousePosition - _touchPos;
            _touchPos = Input.mousePosition;
            
            if(_gameObjPreview==null)
                return;
            var p = _gameObjPreview.transform.localPosition;
            var vel = new Vector3(v.x, 0, v.y);
            _gameObjPreview.GetComponent<NavMeshAgent>().Move(vel.normalized*0.05f);
            

        }

        public override float GetScore()
        {
            float score=0;
            if (iSelected == correctIndex)
                score += 0.5f;
            score += 0.5f *Mathf.Clamp(0.5f/(Vector3.Distance(correctPos.position, posToScore)),0,1);
            Debug.Log(score);
            return score;
        }

        private void Finish()
        {
            var pos = _gameObjPreview.transform.position;
            var go = Instantiate(options[iSelected].Model, pos, _gameObjPreview.transform.rotation,Display.transform);
            posToScore = pos;
            go.SetLayerRecursively(LayerMask.NameToLayer("NoCollideSprinkle"));
            go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y,
                go.transform.localPosition.z - 1f);
            go.transform.DOLocalMoveZ(0, 1f);
            GetScore();
            Finished = true;
            //hint.gameObject.SetActive(false);
            Gameplay.Instance.HideCompleteButton();
            Destroy(_gameObjPreview);
        }
        public override void CompleteFX(Action onFinished)
        {
			onFinished?.Invoke();
        }
    }
}

