using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckGround : MonoBehaviour
{
    public bool groundCheck;

    public LayerMask LayerMask;
   
    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(transform.position, -transform.up, out hit, 1f, LayerMask))
        {
            groundCheck = true;
        }
        else
        {
            groundCheck = false;
        }
    }
}
