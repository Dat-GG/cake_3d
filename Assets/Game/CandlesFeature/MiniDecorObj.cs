using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Nama;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.UI;
using Random = System.Random;

namespace Nama
{
    

    public class MiniDecorObj : MonoBehaviour
    {
        public Transform baseTf;
        public TypeMini typeMini;
        [SerializeField] private Vector3 basePos;
        [SerializeField] private Vector3 baseRot;
        private MiniDecorManual _miniDecorManual;
        private bool isFire;
        [SerializeField] private GameObject _pointer;
        //[SerializeField] private GameObject _fire;
        public GameObject _preview;
        public Sprite Icon;
        [SerializeField] private bool _isScrored;
        [SerializeField] private float _score;
        public float score => _score;
        //[SerializeField] private Image _loading;
        private float distanceScore => Gameplay.Instance.bigDecorConfig.DataConfig.distanceScoreMiniDecor;

        private Rigidbody _rb;

        [SerializeField] private LayerMask _layerMask;

        public GameObject PreviewUI;
        [SerializeField]private TypeFire _typeFire;
        public Texture texture;
        public TypeFire TypeFire => _typeFire;
        [SerializeField] private GameObject _fireParent;

        private List<CandlesFireType> _listFire;

        [SerializeField] private Vector3 _localScaleOnPreview;

        [SerializeField] private Vector3 _offset;
        // Start is called before the first frame update
        public void Init(MiniDecorManual miniDecorManual)
        {
            baseTf = transform;
            basePos = transform.localPosition;
            baseRot = transform.localEulerAngles;
            _rb = GetComponent<Rigidbody>();
            _miniDecorManual = miniDecorManual;
            SetTypeOfFire(_miniDecorManual.TypeFire);
            //_loading = GetComponentInChildren<Image>();
        }

        public void InitFireType()
        {
            for (int i = 0; i < _fireParent.transform.childCount; i++)
            {
                _fireParent.transform.GetChild(i).gameObject.SetActive(true);
            }
            _listFire = _fireParent.GetComponentsInChildren<CandlesFireType>().ToList();
            foreach (var t in _listFire)
            {
                t.gameObject.SetActive(false);
            }
        }

        public void ActivePointer()
        {
            _pointer.SetActive(false);
            InitFireType();
            var t = _listFire.FirstOrDefault(t => t.TypeFire == TypeFire);
            if (t!=null)
            {
                t.gameObject.SetActive(true);
            }
            _isScrored = false;
        }

        public void Setup(Transform parent)
        {
            transform.parent = parent;
            transform.localPosition = basePos + new Vector3(0, 0, -1);
            transform.DOLocalMoveZ(basePos.z, 0.2f);
        }

        public void Preview(Transform parent)
        {
            var go = Instantiate(gameObject, parent);
            go.transform.localPosition = basePos;
            go.transform.localEulerAngles = baseRot;
            go.SetLayerRecursively(Layers.SpinklesPreview);
            go.SetActive(true);
        }

        private void SetTypeOfFire(TypeFire typeFire)
        {
            _typeFire = typeFire;
        }
        public void CandlesActive()
        {
            if (isFire)
                return;
            var t = _listFire.FirstOrDefault(t => t.TypeFire == _typeFire);
            if (t!=null)
            {
                t.gameObject.SetActive(true);
            }
            GetComponent<BoxCollider>().enabled = false;
            _miniDecorManual.CoutingCandles();
            _pointer.SetActive(false);
            Utils.Instance.VibrateMedium();
            Instantiate(Gameplay.Instance.bigDecorConfig.DataConfig.vfx, _fireParent.transform.position, Quaternion.identity,_miniDecorManual.transform);
            //_loading.gameObject.SetActive(false);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Batlua"))
            {
                CandlesActive();
            }

            if (other.CompareTag("HighLightCandles"))
            {
                SetTypeOfFire(_miniDecorManual.TypeFire);
                other.gameObject.SetActive(false);
            }
        }

        // private void OnTriggerStay(Collider other)
        // {
        //     if (other.CompareTag("Batlua"))
        //     {
        //         LoadFire();
        //     }
        // }

        private void OnTriggerExit(Collider other)
        {
            //_loading.fillAmount = 0;
        }

        public void PreviewObj()
        {
            _pointer.gameObject.SetActive(false);
            GetComponent<BoxCollider>().enabled = false;
        }

        public void CalculateScrore(List<MiniDecorObj> list)
        {
            var l1 = list;
            List<MiniDecorObj> l2 = new List<MiniDecorObj>();
            foreach (var m in l1)
            {
                if (!m._isScrored)
                    l2.Add(m);
            }

            if (l2.Count != 0)
            {
                var l = l2.OrderBy(m => Vector3.Distance(transform.position, m.transform.position)).ToList();
                _score = Vector3.Distance(l[0].transform.position, transform.position) < distanceScore ? 1 : 0;
                l[0]._isScrored = true;
            }
            else
            {
                _score = 0;
            }

        }

        public void FailSituation()
        {
            _pointer.SetActive(false);
            _rb.useGravity = true;
            var torque1 = UnityEngine.Random.Range(-20, -5);
            var torque2 = UnityEngine.Random.Range(5, 20);
            var i = UnityEngine.Random.Range(0, 2);
            _rb.AddTorque(i == 0 ? new Vector3(0, 0, torque1) : new Vector3(0, 0, torque2));
            GetComponent<MiniDecorObj>().enabled = false;
        }

        public void CorrectSituation()
        {
            _rb.isKinematic = true;
        }

        bool b;
        public bool CheckCorrect()
        {
            RaycastHit hit;
            // Does the ray intersect any objects excluding the player layer
            if (Physics.Raycast(transform.position, -transform.up, out hit, 1f, _layerMask))
            {
                b = hit.collider.name == "Cake";
            }
            return b;
        }

        private void OnDrawGizmos()
        {
            Debug.DrawRay(transform.position, -transform.up, Color.red);
        }
        public void MakePreview(Camera orthographicCamera ,float camerasize = 2f)
        {
            PreviewUI = Instantiate(gameObject);
            var m =PreviewUI.GetComponent<MiniDecorObj>();
            m.enabled = false;
            m.GetComponent<Rigidbody>().isKinematic = true;
            m.GetComponent<BoxCollider>().enabled = false;
            PreviewUI.layer = Layers.MiniDecorPreview;
            PreviewUI.transform.SetParent(orthographicCamera.transform);
            PreviewUI.transform.localPosition = Vector3.zero + Vector3.forward * 2;
            PreviewUI.SetLayerRecursively(Layers.MiniDecorPreview);
            PreviewUI.transform.localPosition += _offset;
            PreviewUI.transform.localScale = _localScaleOnPreview;
            // Fit camera
            // var _bounds = new Bounds(PreviewUI.transform.position, Vector3.one);
            // foreach (var r in PreviewUI.GetComponentsInChildren<Renderer>())
            // {
            //     _bounds.Encapsulate(r.bounds);
            // }
            //
            // var max = Mathf.Max(_bounds.size.x, _bounds.size.y);
            //
            // orthographicCamera.orthographicSize = (max / orthographicCamera.aspect) / camerasize;
            // var localPosition = PreviewUI.transform.localPosition;
            // localPosition -= _bounds.center;
            // localPosition += Vector3.forward * 2;
            // PreviewUI.transform.localScale = previewScale;
            // localPosition += previewOffset;
            // PreviewUI.transform.localPosition = localPosition;
        }
        
        // public void LoadFire()
        // {
        //     _loading.fillAmount += Time.deltaTime;
        //     if (_loading.fillAmount >= 0.9f)
        //     {
        //         CandlesActive();
        //     }
        // }
    }
    
}

    public enum TypeMini
    {
        Normal,
        Candle
    }
