using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BigDecorPreview : MonoBehaviour
{
    private bool _isGround;
    [SerializeField] private List<CheckGround> _listCheckGround;
    public bool isGround => _isGround;

    private void Awake()
    {
        _listCheckGround = GetComponentsInChildren<CheckGround>().ToList();
    }

    // Update is called once per frame
    void Update()
    {
        _isGround = _listCheckGround.TrueForAll(c => c.groundCheck);
    }
}
