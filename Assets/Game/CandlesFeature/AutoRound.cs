﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AutoRound : MonoBehaviour
{
    public bool isRound;
    public float speed;
    public Vector3 velocity = new Vector3(0, -1, 0);
    void Update()
    {
        if (isRound)
        {
            transform.Rotate(velocity * speed * Time.deltaTime);
        }
    }
	
	
}
