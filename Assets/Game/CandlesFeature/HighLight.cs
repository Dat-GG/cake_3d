using System.Collections;
using System.Collections.Generic;
using Nama;
using UnityEngine;

public class HighLight : MonoBehaviour
{
    [SerializeField] private Sprite _hightlightTrue;

    [SerializeField] private SpriteRenderer _spriteRenderer;

    public Vector3 localAngle;

    //public TypeFire TypeFire;
    // Start is called before the first frame update
    public void ChangeToTrue()
    {
        _spriteRenderer.sprite = _hightlightTrue;
    }
}
