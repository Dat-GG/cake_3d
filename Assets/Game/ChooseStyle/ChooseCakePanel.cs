using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Nama
{
	public class ChooseCakePanel : MonoBehaviour
	{
		[SerializeField] private List<Sprite> listBackground;
		[SerializeField] private Image background;
		[SerializeField] private GameObject imgLeft;
		[SerializeField] private GameObject imgRight;
		[SerializeField] private RawImage rawA;
		[SerializeField] private RawImage rawB;
		[SerializeField] private Image borderA;
		[SerializeField] private Image borderB;
		[SerializeField] private Text chooseText;
		[SerializeField] private GameObject conffeti;
		[SerializeField] private GameObject blockClick;

		private void Start()
		{
			SetBackground();
			EffectOne();
			EffectTwo();
			DOVirtual.DelayedCall(.5f,EffectThree);
		}

		private void SetBackground()
		{
			int i = (Profile.Instance.Level - 1 )/ 6;
			background.sprite = listBackground[Mathf.Clamp(i,0,listBackground.Count-1)];
		}

		#region Scale Effect

		private void EffectOne()
		{
			DOVirtual.DelayedCall(4f,
				() =>
				{
					chooseText.DOColor( new Color(1,1,1,0),.5f)
					.OnComplete(()=>
						{
							chooseText.DOColor(new Color(1, 1, 1, 1), .5f)
							.OnComplete(EffectOne);
						});
				});
		}
		private void EffectTwo()
		{
			DOVirtual.DelayedCall(2f,
				() =>
				{
					imgLeft.transform.DOPunchScale(Vector3.one * .02f, .5f, 1,.1f)
					.OnComplete(EffectTwo);
				});
		}
		private void EffectThree()
		{
			DOVirtual.DelayedCall(2f,
				() =>
				{
					imgRight.transform.DOPunchScale(Vector3.one * .02f, .5f, 1,.1f)
					.OnComplete(EffectThree);
				});
		}

		#endregion

		public void ShowCoffeti(int option)
		{
			var p = conffeti.transform.localPosition;
			if (option == 0)
			{
				conffeti.transform.localPosition = new Vector3(-250f,p.y,p.z);
			}
			else
			{
				conffeti.transform.localPosition = new Vector3(250f,p.y,p.z);
			}
			conffeti.SetActive(true);
			blockClick.SetActive(true);
		}

		public void HightLightOptionChosen(int option)
		{
			var color = new Color(152/255f,152 /255f,152 /255f,152 /255f);
			if (option == 0)
			{
				rawB.color = color;
				borderB.color = color;
			}
			else
			{
				rawA.color = color;
				borderA.color = color;
			}
		}
		
	}
}

