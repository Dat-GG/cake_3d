﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Nama
{
	class PetManager : Singleton<PetManager>
	{
		[SerializeField]
		private PetDataHolder petDataHolder;
		public List<PetData> Pets => petDataHolder.Pets;

#if UNITY_EDITOR
		private void Awake()
		{
			if (petDataHolder == null)
			{
				petDataHolder = AssetDatabase.LoadAssetAtPath<PetDataHolder>("Assets/Game/PetShop/PetDataHolder.prefab");
			}
		}
#endif

		public PetData GetPet(string id)
		{
			foreach (PetData item in Pets)
			{
				if (item.id.Equals(id))
				{
					return item;
				}
			}
			return Pets.Count > 0 ? Pets[0] : null;
		}

		public PetData RandomPetSkin(string notId = null, string notId1 = null)
		{
			List<PetData> avaibleList = Pets.FindAll(
				e => !Profile.Instance.PetOwned(e.id) &&
				e.id != notId && e.id != notId1
			);

			return avaibleList.GetRandomElement();
		}

		public bool HasPet(string itemId)
		{
			var pet =  GetPet(itemId);
			return pet != null && pet.id == itemId;
		}

		public bool AllUnlocked
		{
			get
			{
				for (int i = 0; i < Pets.Count; i++)
				{
					if (!Profile.Instance.PetOwned(Pets[i].id))
					{
						return false;
					}
				}
				return true;
			}
		}

		public bool Unlockable
		{
			get
			{
				return Profile.Instance.CoinAmount >= Profile.Instance.PetPrice;
			}
		}

	}

}