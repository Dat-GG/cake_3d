﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetController : MonoBehaviour
{
	[SerializeField]
	private Animation ani;
	[SerializeField] SkinnedMeshRenderer bodyMesh;
	[SerializeField] MeshRenderer faceMesh;

	[SerializeField] Texture[] faceTextureArray = new Texture[9];
	[SerializeField] Texture[] bodyTextureArray = new Texture[4];
	[SerializeField] GameObject[] effPrefabArray = new GameObject[9];

	private GameObject currentEffect;

	private void Awake()
	{
		foreach (AnimationState state in ani)
		{
			state.speed = 0.5f;
		}
	}

	public float Idle()
	{
		EffectClear();
		ani.wrapMode = WrapMode.Loop;
		ani.CrossFade("Idle");
		faceMesh.materials[0].SetTexture("_MainTex", faceTextureArray[0]);
		return ani.GetClip("Idle").length;
	}

	public float Stand()
	{
		EffectClear();
		ani.wrapMode = WrapMode.Loop;
		ani.CrossFade("Stand");
		faceMesh.materials[0].SetTexture("_MainTex", faceTextureArray[5]);
		currentEffect = GameObject.Instantiate(effPrefabArray[6]);
		currentEffect.transform.SetParent(this.transform);
		currentEffect.transform.localPosition = Vector3.zero;
		return ani.GetClip("Stand").length;
	}

	public float Attack()
	{
		EffectClear();
		ani.wrapMode = WrapMode.Once;
		ani.CrossFade("Attack");
		faceMesh.materials[0].SetTexture("_MainTex", faceTextureArray[3]);
		currentEffect = GameObject.Instantiate(effPrefabArray[0]);
		currentEffect.transform.SetParent(this.transform);
		currentEffect.transform.localPosition = Vector3.zero;
		return ani.GetClip("Attack").length;
	}

	public float Walk()
	{
		EffectClear();
		ani.wrapMode = WrapMode.Loop;
		ani.CrossFade("Walk");
		faceMesh.materials[0].SetTexture("_MainTex", faceTextureArray[0]);

		return ani.GetClip("Walk").length;
	}

	public float Run()
	{
		EffectClear();
		ani.wrapMode = WrapMode.Loop;
		ani.CrossFade("Run");
		faceMesh.materials[0].SetTexture("_MainTex", faceTextureArray[6]);
		currentEffect = GameObject.Instantiate(effPrefabArray[5]);
		currentEffect.transform.SetParent(transform);
		currentEffect.transform.localPosition = Vector3.zero;
		return ani.GetClip("Run").length;
	}

	public float Eat()
	{
		EffectClear();
		ani.wrapMode = WrapMode.Loop;
		ani.CrossFade("Eat");
		faceMesh.materials[0].SetTexture("_MainTex", faceTextureArray[5]);
		currentEffect = Instantiate(effPrefabArray[7]);
		currentEffect.transform.SetParent(transform);
		currentEffect.transform.localPosition = Vector3.zero;
		return ani.GetClip("Eat").length;
	}

	public float Sleep()
	{
		EffectClear();
		ani.wrapMode = WrapMode.Loop;
		ani.CrossFade("Sleep");
		faceMesh.materials[0].SetTexture("_MainTex", faceTextureArray[4]);
		currentEffect = Instantiate(effPrefabArray[1]);
		currentEffect.transform.SetParent(transform);
		currentEffect.transform.localPosition = Vector3.zero;
		return ani.GetClip("Sleep").length;
	}
	void EffectClear()
	{
		if (currentEffect != null)
		{
			DestroyImmediate(currentEffect);
		}
	}
}
