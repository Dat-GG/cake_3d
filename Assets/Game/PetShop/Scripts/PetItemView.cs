﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nama
{
	class PetItemView : ShopItemView
	{
		protected override bool Owned(string itemId)
		{
			return Profile.Instance.PetOwned(itemId);
		}
		protected override bool Active(string itemId)
		{
			return Profile.Instance.PetName.Equals(itemId);
		}

		protected override void Buy(string itemId)
		{
			Profile.Instance.AddPet(itemId);
			Select();
			Profile.Instance.PetBuyCount++;
			Analytics.Instance.LogEvent(string.Format("Shop_{0}_Unlock_{1}", GiftItemType.Pet.ToString(), Profile.Instance.PetBuyCount));
			UIShop.Instance.RefreshPetUnlockButton();
		}

		protected override void SetActive(string itemId)
		{
			Profile.Instance.PetName = itemId;
			UIShop.Instance.ChangePet(this);
		}

		public override void Buy()
		{
			if (Profile.Instance.CoinAmount < Profile.Instance.PetPrice)
			{
				return;
			}

			selectButton.SetActive(false);
			selectedIcon.gameObject.SetActive(true);
			Profile.Instance.CoinAmount -= Profile.Instance.PetPrice;
			Buy(data.id);
			Refresh();
		}

		protected override Sprite GetSprite(ShopItemData data)
		{
			return Resources.Load<Sprite>("Sprites/Pets/" + data.image);
		}
	}
}