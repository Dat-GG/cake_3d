﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class OpacityModel : MonoBehaviour
{
	private void Start()
	{
		this.GetComponent<Renderer>().material.DOFade(0, 2f);
	}
}
