﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nama
{
	[System.Serializable]
	class PetData : ShopItemData
	{
		[SerializeField] string name;
		public string Name => name;
	}
}
