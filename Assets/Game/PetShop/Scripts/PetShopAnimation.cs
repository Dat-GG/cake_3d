﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nama
{
	[RequireComponent(typeof(PetController))]
	public class PetShopAnimation : MonoBehaviour
	{
		private int Infinity = 1000;
		[SerializeField] PetController pet;
		private List<System.Func<float>> functions = new List<Func<float>>();
		private void Awake()
		{
			functions.Add(pet.Idle);
			functions.Add(pet.Stand);
			functions.Add(pet.Sleep);
			functions.Add(pet.Eat);
		}

		private float AniDuration = 0;
		public void StartNewAnimamtion()
		{
			Func<float> randomFunction = functions.GetRandomElement();
			AniDuration = randomFunction.Invoke();
		}

		private void Update()
		{
			if(AniDuration  < 0)
			{
				StartNewAnimamtion();
			}
			else
			{
				AniDuration -= Time.deltaTime;
			}
		}

		public void UnlockAnimation()
		{
			StartCoroutine(IE_Collect());
		}

		private IEnumerator IE_Collect()
		{
			AniDuration = Mathf.Infinity;
			float duration = pet.Attack();
			yield return new WaitForSeconds(duration);
			StartNewAnimamtion();
		}

	}
}
