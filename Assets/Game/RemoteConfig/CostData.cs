﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nama
{
	[System.Serializable]
	public class CostData
	{
		[System.Serializable]
		public class ToolShopData
		{
			public List<int> PastryBagCosts;
			public List<int> BottleCosts;
			public List<int> GlazeCosts;
			public List<int> PetCosts;
		}

		[System.Serializable]
		public class BackgroundData
		{
			public List<int> UpgradeCosts;
		}

		[System.Serializable]
		public class UpgradeWallData
		{
			public List<int> UpgradeWallCosts;
		}

		[System.Serializable]
		public class UpgradeFloorData
		{
			public List<int> UpgradeFloorCosts;
		}

		public ToolShopData ToolShop = new ToolShopData();
		public BackgroundData Background = new BackgroundData();
		public UpgradeWallData Wall = new UpgradeWallData();
		public UpgradeFloorData Floor = new UpgradeFloorData();
		public string ToJson()
		{
			return JsonUtility.ToJson(this);
		}

		public static CostData FromJson(string json)
		{
			return JsonUtility.FromJson<CostData>(json);
		}
		
		public static CostData Default => new CostData()
		{
			ToolShop = new ToolShopData()
			{
				PastryBagCosts = new List<int>() { 200, 500, 1000, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500 },
                BottleCosts = new List<int>() { 200, 500, 1000, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500 },
                GlazeCosts = new List<int> { 200, 500, 1000, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500 },
                PetCosts =  new List<int> { 500, 1000, 2000,4000,6000,10000}
			},

			Background = new BackgroundData()
			{
				UpgradeCosts = new List<int>() {50, 100, 200, 500}
			},

			Wall = new UpgradeWallData()
            {
				UpgradeWallCosts = new List<int>() {50, 100, 200, 500}
            },

			Floor = new UpgradeFloorData()
			{
				UpgradeFloorCosts = new List<int>() {50, 100, 200, 500}
			},
		};
	}
}