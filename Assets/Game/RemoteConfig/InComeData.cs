﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nama
{
	[System.Serializable]
	public class InComeData
	{
		public int TopPrizeGem;

		[System.Serializable]
		public class OpenChestData
		{
			public int Gem1;
			public int Gem2;
			public int Gem3;

			public List<RewardItemDataList> TopPrizes;

			public int TopPrize3FirstKeyPercent;
			public int TopPrize3AfterKeyPercent;

		}

		[System.Serializable]
		public class KeyData
		{
			public int StartLevel;
			public int SpawnRate;
		}

		[System.Serializable]
		public class GiftProgessData
		{
			public int StartLevel;
			public int Rate;
			public List<RewardItemDataList> TopPrizes;
		}

		public OpenChestData OpenChest;
		public KeyData Key;
		public GiftProgessData GiftProgess;

		public string ToJson()
		{
			return JsonUtility.ToJson(this);
		}

		public static InComeData FromJson(string json)
		{
			return JsonUtility.FromJson<InComeData>(json);
		}

		public static InComeData Default => new InComeData()
		{

			TopPrizeGem = 300,
			OpenChest = new OpenChestData()
			{
				Gem1 = 10,
				Gem2 = 20,
				Gem3 = 50,

				TopPrizes = new List<RewardItemDataList>()
				{
					new RewardItemDataList()
					{
						 Prizes = new List<GiftItemData>(){
							 new GiftItemData(100,GiftItemType.IcingBag,"random")
						 }
					},
					new RewardItemDataList()
					{
						 Prizes = new List<GiftItemData>(){
							 new GiftItemData(100,GiftItemType.GlazeCup,"random"),
						 }
					},
					new RewardItemDataList()
					{
						Prizes = new List<GiftItemData>(){
							 new GiftItemData(80,GiftItemType.Random,"random"),
							  new GiftItemData(20,GiftItemType.Gem,300),
						 }
					}
				},

				TopPrize3FirstKeyPercent = 0,
				TopPrize3AfterKeyPercent = 5
			},

			Key = new KeyData()
			{
				StartLevel = 3,
				SpawnRate = 2,
			},
			GiftProgess = new GiftProgessData()
			{
				StartLevel = 1,
				Rate = 6,
				TopPrizes = new List<RewardItemDataList>()
				{
					new RewardItemDataList()
					{
						 Prizes = new List<GiftItemData>(){
							 new GiftItemData(100,GiftItemType.IcingBag,"random")
						 }
					},
					new RewardItemDataList()
					{
						 Prizes = new List<GiftItemData>(){
							 new GiftItemData(100,GiftItemType.SprinkleCup,"random")
						 }
					},
					new RewardItemDataList()
					{
						 Prizes = new List<GiftItemData>(){
							 new GiftItemData(80,GiftItemType.Random,"random"),
							  new GiftItemData(20,GiftItemType.Gem,300)
						 }
					}
				},
			}
		};
	}
}



