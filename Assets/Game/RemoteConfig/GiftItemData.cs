﻿namespace Nama
{
	public enum GiftItemType
	{
		Gem = 0,
		Pet = 1,
		SprinkleCup = 2,
		IcingBag = 3,
		GlazeCup  = 4,
		Random = 5
	}

	[System.Serializable]
	public class GiftItemData
	{
		public int Percent;
		public GiftItem Gift;

		public GiftItemData(int percent, GiftItemType type, string id)
		{
			Percent = percent;
			Gift = new GiftItem(type, id);
		}

		public GiftItemData(int percent, GiftItemType type, int gem)
		{
			Percent = percent;
			Gift = new GiftItem(type, gem);
		}

		public bool IsGem => Gift.type == GiftItemType.Gem;
	}


}