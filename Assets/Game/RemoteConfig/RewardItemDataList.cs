﻿using System.Collections.Generic;
using UnityEngine;

namespace Nama
{
	[System.Serializable]
	public class RewardItemDataList
	{
		public List<GiftItemData> Prizes;

		public GiftItemData GetRandomReward()
		{
			if (Prizes == null || Prizes.Count == 0)
				return null;

			List<int> indexs = new List<int>();

			for (int j = 0; j < Prizes.Count; j++)
			{
				for (int k = 0; k < Prizes[j].Percent; k++)
				{
					indexs.Add(j);
				}
			}

			if (indexs.Count == 0)
				return null;

			int randomIndex = Random.Range(0, indexs.Count);

			return Prizes[indexs[randomIndex]];
		}
	}
}