using UnityEngine;
using UnityEngine.UI;

namespace Nama
{
	public class StencilButton : MonoBehaviour
	{
		[SerializeField] private Button button;
		[SerializeField] private Image background;
		[SerializeField] private Image sprite;
		[SerializeField] private Image checkIcon;

		public Button Button => button;

		public void Init(int index, StencilOption option, Color backgroundColor)
		{
			button.onClick.AddListener(() => StencilUI.Instance.OnOptionSelected(index));
			background.color = backgroundColor;
			sprite.color = option.color;
			sprite.sprite = option.mask;
		}

		public void SetActive(bool active)
		{
			button.interactable = !active;
			checkIcon.gameObject.SetActive(active);
		}
	}

}
