﻿Shader "Nama/StencilMask" {
		
		Properties{
			_MainTex("Mask", 2D) = "black" {}
			_Color("Main Color", COLOR) = (1,1,1,1)
			_Fill("Fill", Range(0, 1)) = 0.5
			_Diameter("Diameter", Range(0, 50)) = 5
		}

		SubShader{
			Tags { "RenderType" = "Transparent" }
			Blend SrcAlpha OneMinusSrcAlpha
			LOD 150

			CGPROGRAM
			#pragma surface surf Lambert noforwardadd alpha:fade vertex:vert

			sampler2D _MainTex;
			float4 _Color;
			float _Diameter;

			float _Fill;
			struct Input {
				float placeHolder;
				float2 uv_MainTex;
			};

			void vert (inout appdata_full v) {
				v.vertex.xyz += v.normal * 0.005;
			}

			void surf(Input IN, inout SurfaceOutput o)
			{
				float2 uv = IN.uv_MainTex;
				uv.x = uv.x * _Diameter;
				uv.y = (uv.y - 0.5) * 2;
				fixed a = 1.0 - tex2D(_MainTex, uv).a;
				fixed4 black = fixed4(0.2f, 0.2f, 0.2f, a);
				fixed4 color = _Color;
				float t = step(IN.uv_MainTex.x, _Fill);
				color = color * t + black * (1.0 - t);

				o.Albedo = color.rgb * t + black.rgb * (1.0 - t);
				o.Alpha = color.a * step(0.0, uv.y);
			}
			ENDCG
		}

			Fallback "Mobile/VertexLit"
}