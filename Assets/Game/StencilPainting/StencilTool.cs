using System;
using DG.Tweening;
using Funzilla;
using UnityEngine;

namespace Nama
{
	public class StencilTool : MonoBehaviour
	{
		[SerializeField] private MeshRenderer icing;
		[SerializeField] private ParticleSystem fx;

		private enum State
		{
			Init,
			Begin,
			Paint,
			Finish
		}

		private State _state = State.Init;

		public void ChangeColor(Color color)
		{
			icing.materials[2].DOKill();
			icing.materials[2].DOColor(color * 1.2f, 0.3f).OnComplete(() =>
			{
				icing.materials[2].DOColor(color, 0.3f);
			});
			fx.GetComponent<ParticleSystemRenderer>().material.color = color;
		}

		public void Begin(Vector3 position, Action onFinished)
		{
			_state = State.Begin;
			gameObject.SetActive(true);
			transform.DOKill();
			transform.DOLocalRotate(Vector3.zero, 1);
			transform.DOLocalMove(position, 0.5f).OnComplete(() => onFinished?.Invoke());
		}

		public void Paint(int amount)
		{
			_state = State.Paint;
			var emission = fx.emission;
			emission.rateOverTime = amount;
		}

		public void Stop()
		{
			var emission = fx.emission;
			emission.rateOverTime = 0;
		}

		public void Finish(Action onFinished)
		{
			Stop();
			if (_state != State.Paint) return;
			transform.DOKill();
			transform.DOMove(new Vector3(0, 4, 0), 0.8f).OnComplete(() =>
			{
				transform.DOMove(new Vector3(2.2420001f,0,4.23600006f), 1.5f);
				transform.DORotate(new Vector3(84.0980682f,34.8100891f,305.84552f), 1.5f).OnComplete(() =>
				{
					onFinished?.Invoke();
				});
			});
		}

		private void Update()
		{
			switch (_state)
			{
				case State.Init:
				case State.Begin:
				case State.Finish:
					break;
				case State.Paint:
				
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}

