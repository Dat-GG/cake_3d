using DG.Tweening;
using UnityEngine;

namespace Nama
{
	public class StencilUI : MonoBehaviour
	{
		[SerializeField] private StencilButton[] buttons;
		[SerializeField] private CanvasGroup canvasGroup;

		private static StencilUI _instance;
		public static StencilUI Instance {
			get
			{
				if (_instance) return _instance;
				_instance = FindObjectOfType<StencilUI>();
				return _instance;
			}
		}

		private void Awake()
		{
			_instance = this;
			enabled = false;
		}

		private int SelectIndex { get; set; }
		private int optionAds;
		public void Show(StencilOption[] options, Color backgroundColor, int selectIndex,int optionAd)
		{
			if (enabled) return;
			if (options == null || options.Length < 2) return;
			Gameplay.Instance.ShowOptionsBackground();
			Gameplay.Instance.ShowStencilUI();
			enabled = true;
			gameObject.SetActive(true);
			canvasGroup.DOFade(1.0f, 0.3f);
			for (var i = 0; i < options.Length; i++)
			{
				buttons[i].Init(i, options[i], backgroundColor);
				buttons[i].gameObject.SetActive(true);
				buttons[i].SetActive(false);
			}

			SelectIndex = selectIndex;
			buttons[selectIndex].SetActive(true);

			for (var i = options.Length; i < buttons.Length; i++)
			{
				buttons[i].gameObject.SetActive(false);
			}
			if (Gameplay.Level.LevelStype != LevelStyle.FreeStyle)
				return;
			if(wathchedReward)
				return;
			optionAds = optionAd;
			buttons[optionAd].transform.GetChild(3).gameObject.SetActive(true);
		}

		public void Hide()
		{
			if (!enabled) return;
			enabled = false;
			Gameplay.Instance.HideOptionsBackground();
			foreach (var button in buttons) button.Button.interactable = false;
			canvasGroup.DOFade(0, 0.3f).OnComplete(() => gameObject.SetActive(false));
		}
		private bool wathchedReward;
		public void OnOptionSelected(int index)
		{
			buttons[SelectIndex].SetActive(false);
			SelectIndex = index;
			if (Gameplay.Level.LevelStype == LevelStyle.FreeStyle)
			{
				if (index == optionAds&&!wathchedReward)
				{
					Ads.ShowRewardedVideo("Stencil_optionAds", (result) =>
					{
						if (result != RewardedVideoState.Watched)
						{
							return;
						}
						buttons[SelectIndex].transform.GetChild(3).gameObject.SetActive(false);
						buttons[SelectIndex].SetActive(true);
						Gameplay.Level.CurrentLayer.CurrentSurface.CurrentStep.OnOptionSelected(index);
						wathchedReward = true;
					});
				}
				else
				{
					Gameplay.Level.CurrentLayer.CurrentSurface.CurrentStep.OnOptionSelected(index);
					buttons[SelectIndex].SetActive(true);
				}
			}
			else
			{
				Gameplay.Level.CurrentLayer.CurrentSurface.CurrentStep.OnOptionSelected(index);
				buttons[SelectIndex].SetActive(true);
			}
		}
	}
}

