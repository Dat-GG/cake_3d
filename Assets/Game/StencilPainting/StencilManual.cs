using System;
using DG.Tweening;
using UnityEngine;

namespace Nama
{
	[Serializable]
	public class StencilOption
	{
		public Sprite mask;
		public Color color;
	}

	public class StencilManual : DecorationStep
	{
		[SerializeField] private Color cakeColor; 
		[SerializeField] private StencilOption[] options;
		[SerializeField] private int correctOption;

		private StencilOption CorrectOption => options[correctOption];
		private static readonly int Fill = Shader.PropertyToID("_Fill");
		private static readonly int PaintColor = Shader.PropertyToID("_Paint");

		private enum State
		{
			None,
			Begin,
			Idle,
			Paint,
			Finish
		}

		private State _state = State.None;
		private CakeRenderer _cakeRenderer;
		private Cake _cake;
		private int _iSelected;
		private float _angle;

		public override int SelectedIndex => _iSelected;
		private Material _cakeMaterial, _maskMaterial;
		public int optionAds;
		public override void Init(bool preview)
		{
			_cake = GetComponentInParent<Cake>();
			_cakeRenderer = _cake.CakeRenderer;
			_cakeMaterial = _cakeRenderer.Cake.sharedMaterial;
			_cakeMaterial.color = cakeColor;
			_maskMaterial = _cakeRenderer.Mask.sharedMaterial;
			if (preview)
			{
				_cakeMaterial.mainTexture = CorrectOption.mask.texture;
				_cakeMaterial.SetColor(PaintColor, CorrectOption.color);
				_cakeMaterial.SetFloat(Fill, 1.0f);
				_cakeRenderer.Mask.gameObject.SetActive(false);
			}
			else
			{
				_cakeMaterial.SetFloat(Fill, 0.0f);
				_maskMaterial.SetFloat(Fill, 0.0f);
			}
		}

		public override float GetScore()
		{
			if (Gameplay.Level.LevelStype != LevelStyle.FreeStyle)
				return _iSelected == correctOption ? 1.0f : 0.0f;
			if (_iSelected == correctOption || _iSelected == optionAds)
				return 1.0f;
			return 0;
		}

		public override void Begin()
		{
			StencilUI.Instance.Show(options, cakeColor, _iSelected,optionAds);
			_state = State.Begin;
			ShowMask();
			_angle = 0;
			_cake.transform
				.DOLocalRotate(new Vector3(0, _cake.InitialAngle, 0), 0.5f)
				.OnComplete(() =>
				{
					var hit = CheckHit();
					if (hit.collider == null) return;
					var p = hit.point + new Vector3(0, 0, -0.1f);
					Gameplay.Instance.StencilTool.Begin(p, () => _state = State.Idle);
				});
			Gameplay.Instance.HideCompleteButton();
			Gameplay.Instance.HideUndoButton();
			TutorialSign.Instance.StartTutorial();
		}

		private Vector3 _touchPos;
		private void OnTouchDown(Vector3 screenPos)
		{
			switch (_state)
			{
				case State.None:
				case State.Begin:
				case State.Idle:
					_state = State.Paint;
					StencilUI.Instance.Hide();
					_touchPos = screenPos;
					TutorialSign.Instance.EndTutorial();
					Gameplay.Instance.ShowUndoButton();
					_cakeRenderer.Edge.gameObject.SetActive(true);
					_cakeRenderer.Edge.material.color = options[_iSelected].color;
					break;
				case State.Paint:
				case State.Finish:
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private void OnTouchUp(Vector3 screenPos)
		{
			switch (_state)
			{
				case State.None:
				case State.Begin:
				case State.Idle:
				case State.Paint:
					_state = State.Idle;
					Gameplay.Instance.StencilTool.Stop();
					break;
				case State.Finish:
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private RaycastHit CheckHit()
		{
			var t = _cake.transform;
			var p = t.position + new Vector3(0, .5f, -5);
			Physics.Raycast(p, Vector3.forward, out var hit, 100, Layers.CakeSurfaceMask);
			return hit;
		}

		private void OnTouchHold(Vector3 screenPos)
		{
			switch (_state)
			{
				case State.None:
				case State.Begin:
				case State.Idle:
				case State.Finish:
					break;
				case State.Paint:
				{
					var v = screenPos - _touchPos;
					var l = v.magnitude * 4.0f / Screen.width;
					if (l <= 0) return;
					_touchPos = screenPos;
					_angle += l * Time.smoothDeltaTime * 3000;
					Gameplay.Instance.StencilTool.Paint(Mathf.FloorToInt(l * 4000));
					var t = _cake.transform;
					t.localEulerAngles = new Vector3(0, _cake.InitialAngle + _angle, 0);
					t = Gameplay.Instance.StencilTool.transform;
					var fill = Mathf.Clamp01(_angle / 360.0f);
					_cakeMaterial.SetFloat(Fill, fill);
					_maskMaterial.SetFloat(Fill, fill);
					if (_angle >= 360)
					{
						Finish();
						return;
					}
					var hit = CheckHit();
					if (hit.collider == null) return;
					t.position = hit.point + new Vector3(0, 0, -0.1f);
					t.forward = hit.normal;
					break;
				}
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private void Finish()
		{
			_state = State.Finish;
			Gameplay.Instance.StencilTool.Finish(null);
			_cakeRenderer.Edge.gameObject.SetActive(false);
			HideMask(() =>
			{
				_cakeRenderer.Mask.gameObject.SetActive(false);
				_cake.transform.DOLocalRotate(Vector3.zero, 0.5f).OnComplete(() => Finished = true);
			});
		}

		private bool _touched;
		public override void DoFrame()
		{
			var eventSystem = UnityEngine.EventSystems.EventSystem.current;
			if (_state == State.None) return;
#if UNITY_EDITOR
			if (Input.GetMouseButtonDown(0) && !eventSystem.IsPointerOverGameObject())
			{
				_touched = true;
				OnTouchDown(Input.mousePosition);
			}

			if (Input.GetMouseButtonUp(0) && _touched)
			{
				_touched = false;
				OnTouchUp(Input.mousePosition);
			}

			if (Input.GetMouseButton(0) && _touched)
			{
				OnTouchHold(Input.mousePosition);
			}
#else
			if (Input.touchCount <= 0)
			{
				return;
			}

			switch (Input.touches[0].phase)
			{
				case TouchPhase.Began:
					if (!eventSystem.IsPointerOverGameObject(Input.touches[0].fingerId))
					{
						_touched = true;
						OnTouchDown(Input.touches[0].position);
					}
					break;

				case TouchPhase.Stationary:
				case TouchPhase.Moved:
					if (_touched)
					{
						OnTouchHold(Input.touches[0].position);
					}
					break;
				case TouchPhase.Ended:
				case TouchPhase.Canceled:
					if (_touched)
					{
						_touched = false;
						OnTouchUp(Input.touches[0].position);
					}
					break;
			}
#endif
		}

		internal override void Undo()
		{
			_cakeMaterial.SetFloat(Fill, 0.0f);
			_maskMaterial.SetFloat(Fill, 0.0f);
			_cakeRenderer.Edge.gameObject.SetActive(false);
			Begin();
		}

		private void HideMask(Action onFinished)
		{
			var t = _cakeRenderer.Mask.transform; 
			t.DOKill();
			t.DOLocalMove(new Vector3(0, 7, 0), 0.6f).OnComplete(() => onFinished?.Invoke());
		}

		private void ShowMask()
		{
			_cakeRenderer.Mask.gameObject.SetActive(true);
			var t = _cakeRenderer.Mask.transform; 
			t.DOKill();
			t.localPosition = new Vector3(0, 7, 0);
			t.DOLocalMove(Vector3.zero, 0.6f).OnComplete(() =>
			{
				_state = State.Idle;
			});
			UpdateColor();
		}

		private void UpdateColor()
		{
			var option = options[_iSelected];
			Gameplay.Instance.StencilTool.ChangeColor(option.color);
			_cakeMaterial.SetColor(PaintColor, option.color);
			_cakeMaterial.mainTexture = option.mask.texture;
			_maskMaterial.color = option.color;
			_maskMaterial.mainTexture = option.mask.texture;
		}

		public override void OnOptionSelected(int index)
		{
			if (_iSelected.Equals(index)) return;
			var previousmask = options[_iSelected].mask;
			_iSelected = index;
			if (previousmask == options[index].mask)
			{
				UpdateColor();
			}
			else
			{
				_state = State.None;
				HideMask(ShowMask);
			}
		}

		public override bool IsOptionActive(int index)
		{
			return _iSelected == index;
		}

		public override bool IsOptionCorrect(int index)
		{
			return index == correctOption;
		}

		public override int OptionCount => options.Length;

		public override void RefreshTool()
		{
			
		}

		public override void CompleteFX(Action onFinished)
		{
			DOVirtual.DelayedCall(0.1f, () => onFinished?.Invoke());
		}
	}
}

