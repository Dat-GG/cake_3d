﻿Shader "Nama/StencilPainted" {
		
		Properties{
			_MainTex("Mask", 2D) = "black" {}
			_Color("Main Color", COLOR) = (1,1,1,1)
			_Paint("Paint Color", COLOR) = (1,1,1,1)
			_Fill("Fill", Range(0, 1)) = 0.5
			_Diameter("Diameter", Range(0, 50)) = 5
		}

		SubShader{
			Tags { "RenderType" = "Opaque" }
			LOD 150

			CGPROGRAM
			#pragma surface surf Lambert noforwardadd

			sampler2D _MainTex;
			float _Diameter;

			float4 _Color;
			float4 _Paint;
			float _Fill;
			struct Input {
				float placeHolder;
				float2 uv_MainTex;
			};

			void surf(Input IN, inout SurfaceOutput o) 
			{
				float2 uv = IN.uv_MainTex;
				uv.x = uv.x * _Diameter;
				uv.y = (clamp(uv.y, 0.5, 1.0) - 0.5) * 2;
				float a = tex2D(_MainTex, uv).a;
				float t = step(IN.uv_MainTex.x, _Fill) * a;
				fixed4 color = _Paint * t + _Color * (1.0 - t);

				o.Albedo = color.rgb;
				o.Alpha = 1.0;
			}
			ENDCG
		}

			Fallback "Mobile/VertexLit"
}