﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RenderCameaShopPreview : MonoBehaviour
{
	[SerializeField] private FillElementShopPreview fillElementPrefab;

	[SerializeField] private Camera renderCamera;

	private const int DrawElementMax = 15;
	public List<FillElementShopPreview> elements = new List<FillElementShopPreview>(DrawElementMax);
	public RenderTexture RenderTexture => renderCamera.targetTexture;

	private int _drawCount;
	private Color _color;
	public void Init(FillManualShopPreview fillManual, Color color)
	{

#if UNITY_EDITOR
		if (!UnityEditor.EditorApplication.isPlaying)

			return; // Not Create Elements Pooling below
#endif
		var texture = new RenderTexture(
			1024,
			1024,
			0,
			RenderTextureFormat.ARGB32,
			RenderTextureReadWrite.Default)
		{
			wrapMode = TextureWrapMode.Repeat,
			filterMode = FilterMode.Bilinear,
			autoGenerateMips = false,
			useMipMap = false,
			anisoLevel = 0
		};
		renderCamera.targetTexture = texture;
		MakeElementPooling(fillManual);
		_color = color;
	}

	private void MakeElementPooling(FillManualShopPreview fillManual)
	{
		for (var i = renderCamera.transform.childCount - 1; i >= 0; i--)
		{
			DestroyImmediate(this.transform.GetChild(i).gameObject);
		}
		elements.Clear();
		for (var i = 0; i < DrawElementMax; i++)
		{
			var element = CreateAFillElement();
			element.SetActive(false);
			var elementComponent = element.GetComponent<FillElementShopPreview>();

			var prefabMaterial = fillElementPrefab.GetComponent<Renderer>().sharedMaterial;
			var newMaterial = new Material(prefabMaterial);
			elementComponent.Init(newMaterial, fillManual);
			elements.Add(elementComponent);
		}
	}


	public void CreateDrawElement(Vector2 hitPos)
	{
		if (_drawCount > 0)
		{
			var lastIndex = (_drawCount - 1) % DrawElementMax;
			elements[lastIndex].StopScale();
		}

		var element = elements[_drawCount % DrawElementMax];


		element.gameObject.SetActive(false);
		element.Reset();
		element.SetPostion(hitPos);

		// sort order
		var count = 0;
		for (var i = _drawCount; i >= 0; i--)
		{
			var index = i % DrawElementMax;
			var elementPos = new Vector3(0, 0, 1 + 0.2f * count);
			elements[index].transform.localPosition = elementPos;
			count++;

			if (count >= DrawElementMax)
			{
				break;
			}
		}

		element.SetOption(_color);
		element.gameObject.SetActive(true);
		_drawCount++;
	}

	public int ElementActiveNumber => elements.Count(element => element.Active);

	private GameObject CreateAFillElement()
	{
		var fillElement = Instantiate(fillElementPrefab.gameObject, transform, true);
		fillElement.transform.localRotation = Quaternion.identity;
		fillElement.transform.localPosition = Vector3.zero;
		fillElement.transform.localScale = Vector3.one;
		return fillElement;
	}

	public void Stop()
	{
		foreach (var element in elements)
		{
			element.StopImmediate();
		}
	}

	[ContextMenu("fill")]
	public void CreatTest()
	{
		CreateDrawElement(Vector2.zero);
	}
}
