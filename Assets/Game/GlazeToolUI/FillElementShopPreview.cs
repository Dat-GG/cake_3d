﻿using Nama;
using UnityEngine;

public class FillElementShopPreview : MonoBehaviour
{

	private const float SpreadTime = 1.5f;
	private const float SpreadSpeed =1f;
	private bool _stop;
	public float LifeTime { get; private set; }

	private float _stopTime;
	private float _startRadius;

	private Material _myMaterial;

	public float Radius { get; private set; }

	public Vector2 Position { get; private set; }

	public bool Active { get; private set; }
	private FillManualShopPreview _fillManual;
	private static readonly int BrushTex = Shader.PropertyToID("_BrushTex");
	private static readonly int BrushSize = Shader.PropertyToID("_BrushSize");
	private static readonly int MainTex = Shader.PropertyToID("_MainTex");
	private static readonly int BrushColor1 = Shader.PropertyToID("_BrushColor");

	public void Init(Material material, FillManualShopPreview fillManual)
	{
		GetComponent<Renderer>().material = material;
		_myMaterial = material;
		Active = false;
		_fillManual = fillManual;
	}

	public void Reset()
	{
		_stop = false;
		LifeTime = 0;
		_stopTime = 0;
		_startRadius = BrushConfig.Instance.FillRadius;
		Radius = _startRadius;
		SetSize();
		Active = true;
	}


	public void SetPostion(Vector2 hitPos)
	{
		this.Position = hitPos / (FillColorSurface.CakeSize * _fillManual.DrawTexture.width / 1024 );
		var drawPos = -hitPos / _fillManual.FillSize ;

		_myMaterial.SetTextureOffset(BrushTex, drawPos);
	}

	private void SetSize()
	{
		_myMaterial.SetFloat(BrushSize, 10f );
	}

	private void LateUpdate()
	{

		Radius = _startRadius + LifeTime * SpreadSpeed;
		SetSize();

		LifeTime += Time.smoothDeltaTime;

		if (!_stop) return;
		_stopTime += Time.smoothDeltaTime;
		if (_stopTime > SpreadTime)
		{
			StopImmediate();
		}

	}

	public void StopImmediate()
	{
		gameObject.SetActive(false);
		Active = false;
	}

	public void StopScale()
	{
		_stop = true;
	}

	public void SetOption(Color color)
	{
		_myMaterial.SetTexture(MainTex, null);
		_myMaterial.SetColor(BrushColor1, color);
	}
}
