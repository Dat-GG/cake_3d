﻿using Nama;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;
using System;

public class GlazeToolPreview : MonoBehaviour
{
	[SerializeField] GameObject cake;
	[SerializeField] FillManualShopPreview fillAreaPreview;
	[SerializeField] RenderCameaShopPreview renderCamera;
	[SerializeField] Carafe carafe;

	[SerializeField] Transform[] moveTransformPoints;

	[SerializeField] Transform visualTransform;
	[SerializeField] float moveDuration = 3;
	private Vector3[] movePoints;

	private float createRateTime = 0.25f;
	private float lastCreateTime = 0;

	[SerializeField] Color color = Color.yellow;


	private void Awake()
	{
		fillAreaPreview.Init();
		renderCamera.Init(fillAreaPreview, color);
		movePoints = moveTransformPoints.Select(t => t.localPosition).ToArray();
	}

	public void Show()
	{
		cake.SetActive(true);
		fillAreaPreview.gameObject.SetActive(true);
		renderCamera.gameObject.SetActive(true);
		carafe.gameObject.SetActive(true);
		visualTransform.transform.localPosition = movePoints[0];
		Vector2 startPos = new Vector2(visualTransform.localPosition.x, visualTransform.localPosition.z);
		fillAreaPreview.Clear();

		carafe.Init(Profile.Instance.GlazeName);
		carafe.SetColor(Color.yellow, null);
		carafe.Move(startPos);
		visualTransform.DOKill();
		carafe.Press(() =>
		{
			carafe.Stream.Begin();
			visualTransform.DOPath(movePoints, moveDuration).Play().OnUpdate(UpdateCarafePoint).OnComplete(CarafeMoveComplete);
		});



	}
	public void ShowWithId(string name)
	{
		cake.SetActive(true);
		fillAreaPreview.gameObject.SetActive(true);
		renderCamera.gameObject.SetActive(true);
		carafe.gameObject.SetActive(true);
		visualTransform.transform.localPosition = movePoints[0];
		Vector2 startPos = new Vector2(visualTransform.localPosition.x, visualTransform.localPosition.z);
		fillAreaPreview.Clear();

		carafe.Init(name);
		carafe.SetColor(Color.yellow, null);
		carafe.Move(startPos);
		visualTransform.DOKill();
		carafe.Press(() =>
		{
			carafe.Stream.Begin();
			visualTransform.DOPath(movePoints, moveDuration).Play().OnUpdate(UpdateCarafePoint).OnComplete(CarafeMoveComplete);
		});



	}
	private void CarafeMoveComplete()
	{
		carafe.Release();
	}

	private void UpdateCarafePoint()
	{
		var pos = new Vector2(visualTransform.localPosition.x, visualTransform.localPosition.z);
		carafe.Move(pos);

		if (!carafe.Stream.Pouring) return;
		if (lastCreateTime + createRateTime >= Time.time) return;
		lastCreateTime = Time.time;
		renderCamera.CreateDrawElement(pos);
	}

	public void Hide()
	{

		visualTransform.DOKill();
		cake.SetActive(false);
		fillAreaPreview.gameObject.SetActive(false);
		renderCamera.gameObject.SetActive(false);
		carafe.gameObject.SetActive(false);
	}
}
