﻿using System.Collections.Generic;
using System.Linq;
using Nama;
using UnityEngine;

public class FillManualShopPreview : MonoBehaviour
{

	[SerializeField] private Texture2D drawTexture;
	public Texture2D DrawTexture => drawTexture;
	private float _maxWaterFallingTime;
	private const float Gravity = 0.08f;
	private MeshFilter _filter;

	private float[] _targetHeights;
	[SerializeField] private RenderCameaShopPreview renderCamera;

	private Vector3[] _vertices;

	private int _vetexSize;
	private int _textureSize;
	private float _cellSize;

	public FillManualShopPreview()
	{
		_textureSize = 0;
		FillSize = 0;
	}

	public float FillSize { get; private set; }

	public void Init()
	{
		_filter = GetComponent<MeshFilter>();
		_cellSize = FillColorSurface.CakeSize / BrushConfig.Instance.VertexSize;
		_textureSize = Mathf.Max(drawTexture.width, drawTexture.height);
		_vetexSize = BrushConfig.Instance.VertexSize * _textureSize / FillColorSurface.MAXTextureSize;
		FillSize = (_vetexSize - 1) * _cellSize;

		var nVertices = _vetexSize * _vetexSize;
		_vertices = new Vector3[nVertices];
		_targetHeights = new float[nVertices];
		_filter.sharedMesh = FillColorSurface.CreateGlazeMesh(
			_vetexSize,
			_cellSize,
			drawTexture,
			_textureSize,
			_vertices,
			_targetHeights,
			-.1f);

		_maxWaterFallingTime = (BrushConfig.Instance.FillHeight * (1 - FillColorSurface.FillArvgHeight)) / Gravity;

	}

	private void Start()
	{
		var render = this.GetComponent<MeshRenderer>();
		render.material.mainTexture = renderCamera.RenderTexture;
	}

	private void Update()
	{

		if (renderCamera.ElementActiveNumber == 0)
			return;

		UpdateElements();
	}

	public void Clear()
	{
		renderCamera.Stop();
		FillColorSurface.resetGlaze(_vertices, _targetHeights, _vertices.Length);
		_filter.sharedMesh.vertices = _vertices;
	}

	private readonly List<GlazeElementInfo> _activeElements = new List<GlazeElementInfo>(10);
	private void UpdateElements()
	{
		_activeElements.Clear();
		foreach (var element in renderCamera.elements.Where(element => element.gameObject.activeSelf))
		{
			_activeElements.Add(new GlazeElementInfo()
			{
				Position = element.Position,
				Radius = element.Radius,
				Lifetime = element.LifeTime
			});
		}

		FillColorSurface.updateGlaze(
			_vertices,
			_targetHeights,
			_vertices.Length,
			_activeElements.ToArray(),
			_activeElements.Count,
			FillSize,
			_maxWaterFallingTime,
			Gravity);
		var m = _filter.sharedMesh;
		m.vertices = _vertices;
		m.RecalculateNormals();
	}
}
