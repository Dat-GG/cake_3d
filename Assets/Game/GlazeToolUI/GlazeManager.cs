﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nama
{
	class GlazeManager : Singleton<GlazeManager>
	{
		[SerializeField]
		private GlazeDataHolder glazeData;
		public List<GlazeData> Glazes => glazeData.Glazes;

#if UNITY_EDITOR
		private void Awake()
		{
			if (glazeData == null)
			{
				glazeData = UnityEditor.AssetDatabase.LoadAssetAtPath<GlazeDataHolder>("Assets/Game/GlazeToolUI/GlazeDataHolder.prefab");
			}
		}
#endif

		public GlazeData GetGlazeSkin(string id)
		{
			foreach (var item in Glazes)
			{
				if (item.id.Equals(id))
				{
					return item;
				}
			}
			return Glazes.Count > 0 ? Glazes[0] : null;
		}

		public GlazeData RandomGalzeSkin(string notId = null, string notId1 = null)
		{
			List<GlazeData> avaibleList = Glazes.FindAll(
				e => !Profile.Instance.GlazeSkinOwned(e.id) &&
				e.id != notId && e.id != notId1
			);
			if (avaibleList.Count <= 0)
			{
				return null;
			}
			return Glazes.Count > 0 ? avaibleList[Random.Range(0, avaibleList.Count)] : null;
		}
		
		public bool HasGlazeSkin(string itemId)
		{
			var glaze =  GetGlazeSkin(itemId);

			return glaze != null && glaze.id == itemId;
		}

		public bool AllUnlocked
		{
			get
			{
				for (int i = 0; i < Glazes.Count; i++)
				{
					if (!Profile.Instance.GlazeSkinOwned(Glazes[i].id))
					{
						return false;
					}
				}
				return true;
			}
		}

		public bool Unlockable
		{
			get
			{
				return Profile.Instance.CoinAmount >= Profile.Instance.GlazePrice;
			}
		}


	}
}
