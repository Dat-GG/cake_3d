﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Funzilla;

namespace Nama
{
	class GlazeToolItemView : ShopItemView
	{
		protected override bool Owned(string itemId)
		{
			return Profile.Instance.GlazeSkinOwned(itemId);
		}
		protected override bool Active(string itemId)
		{
			return Profile.Instance.GlazeName.Equals(itemId);
		}

		protected override void Buy(string itemId)
		{
			Profile.Instance.AddGlazeSkin(itemId);
			Select();
			Profile.Instance.GlazeBuyCount++;
			Analytics.Instance.LogEvent(string.Format("Shop_{0}_Unlock_{1}", GiftItemType.GlazeCup.ToString(), Profile.Instance.GlazeBuyCount));
			UIShop.Instance.RefreshGlazeUnlockButton();
		}

		protected override void SetActive(string itemId)
		{
			Profile.Instance.GlazeName = itemId;
			UIShop.Instance.ChangeGlaze(this);
		}

		public override void Buy()
		{
			if (Profile.Instance.CoinAmount < Profile.Instance.GlazePrice)
			{
				return;
			}

			selectButton.SetActive(false);
			selectedIcon.gameObject.SetActive(true);
			PlayFocusEff();
			Profile.Instance.CoinAmount -= Profile.Instance.GlazePrice;
			DailyQuestManager.UpdateDay();
			Profile.Instance.MoneyCurrentSpendOnShop += Profile.Instance.GlazePrice;
			Buy(data.id);
			Refresh();
		}

		protected override Sprite GetSprite(ShopItemData data)
		{
			return Resources.Load<Sprite>("Sprites/GlazeTools/" + data.image);
		}
	}
}