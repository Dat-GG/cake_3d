using System.Collections;
using Nama;
using UnityEngine;
using UnityEngine.UI;

namespace Funzilla
{
	internal class LevelPreview : MonoBehaviour
	{
		[SerializeField] private MeshRenderer meshRenderer;
		[SerializeField] private Text levelText;
		private static int _level = 1;
		private void Start()
		{
			try
			{
				System.IO.Directory.CreateDirectory("Preview");
			}
			catch
			{
				// ignored
			}

			levelText.text = $"Level {_level}";
			var levelName = AssetManager.LevelList[_level-1];
			var prefab = Resources.Load<Level>(levelName);
			var level = Instantiate(prefab, transform);
			level.Init(true);
			meshRenderer.material.color = level.BackgroundColor;
			StartCoroutine(TakeScreenshot());
		}

		private static IEnumerator TakeScreenshot()
		{
			yield return new WaitForEndOfFrame();
			SceneManager.Instance.HideLoading();
			SceneManager.Instance.HideSplash();
			QualitySettings.shadows = ShadowQuality.Disable;
#if true
			ScreenCapture.CaptureScreenshot("Preview/Level_" + _level.ToString("000") + ".png");
			_level++;
			if (_level - 1 >= AssetManager.LevelList.Count) yield break;
			UnityEngine.SceneManagement.SceneManager.LoadScene("LevelPreview");
#endif
		}
	}
}
