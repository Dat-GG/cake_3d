﻿using DG.Tweening;
using UnityEngine;
using Nama;
using UnityEngine.UI;
using System.Collections;
using Funzilla;

public class UIResult : Scene
{
	private const int Multiplier = 3;
	[SerializeField] private Text complementTextOld, coinText, xCoinText;
	[SerializeField] private Transform gemReward;
	[SerializeField] private GameObject diamondPrefab;
	[SerializeField] private Image dark;
	[SerializeField] private Transform coinTarget;

	[SerializeField] private CanvasGroup boxProgress;
	[SerializeField] private Transform boxProgressDots;

	[SerializeField] private Button nextButton;
	[SerializeField] private Button restartButton;
	[SerializeField] private Button xcoinButton;
	[SerializeField] private CanvasGroup canvasGroup;
	[SerializeField] private RectTransform leftFrame, rightFrameOld;
	[SerializeField] private RectTransform freeStyleFrame;

	[SerializeField] private RawImage leftImage;
	[SerializeField] private RawImage rightImageOld;
	[SerializeField] private RawImage freeStyleImage;

	[SerializeField] private Image[] starsOld;
	
	private int _bonusCoin;
	[SerializeField] private new Camera camera;
	[SerializeField] private GameObject specialEarnCoinFX;

	private bool _canClick = true;
	public static bool Replayed;

	[SerializeField] private Text scoreRateText;
	[SerializeField] private Image completeBar;
	public Animator buttonClaimRvAnim;
	public GameObject buttonEffect;
	public Text notAd;

	public override void Init(object data) // data bool: won
	{
		completeBar.fillAmount = 0;
		completeBar.DOFillAmount(Gameplay.Level.Match, 1.0f).OnUpdate(() =>
		{
			var score = (int) (completeBar.fillAmount * 100);
			scoreRateText.text = score + "%";
			if (score >= 80)
			{
				starsOld[1].gameObject.SetActive(true);
				starsOld[2].gameObject.SetActive(true);
			}
			else if (score >= 40)
			{
				starsOld[1].gameObject.SetActive(true);
			}
		});

		// Reset bonus score
		Level.BonusScore = 0;

		UIGlobal.ShowCoinInfo();
		var match = Gameplay.Level.Match;
		_bonusCoin = (int)Mathf.Clamp(match * 100, 5, 100);
		// Show math
		var leftPos = leftFrame.transform.position;
		leftPos.z -= 1;
		var rightPos = rightFrameOld.transform.position;
		rightPos.z -= 1;

		//Gameplay.Instance.GetTextureForResult();
		camera.backgroundColor = Gameplay.Level.BackgroundColor;
		leftImage.texture = Gameplay.CorrectTexture;
		rightImageOld.texture = Gameplay.Instance.ResultTexture;
		freeStyleFrame.gameObject.SetActive(false);
		if (Gameplay.Level.LevelStype == LevelStyle.FreeStyle)
		{
			leftFrame.gameObject.SetActive(false);
			rightFrameOld.gameObject.SetActive(false);
			freeStyleFrame.gameObject.SetActive(true);
			freeStyleImage.texture = Gameplay.Instance.ResultTexture;
		}
		Gameplay.Instance.HideUI();
		//reset
		nextButton.gameObject.SetActive(false);
		restartButton.gameObject.SetActive(false);
		xcoinButton.gameObject.SetActive(false);
		complementTextOld.transform.localScale = Vector3.zero;
		coinText.transform.parent.gameObject.SetActive(false);
		coinText.text = "0";
		xCoinText.text = (_bonusCoin * Multiplier).ToString();
		starsOld[0].transform.parent.gameObject.SetActive(false);
		specialEarnCoinFX.gameObject.SetActive(false);
		starsOld[1].gameObject.SetActive(false);
		starsOld[2].gameObject.SetActive(false);
		SetDotColor();
		ShowBoxProgress();
	}

	private IEnumerator IE_PopupAnimation()
	{
		var match = (int) (Gameplay.Level.Match * 100);
		match = Mathf.Clamp(match, 0, 101);

		Analytics.Instance.LogEvent($"level_{Profile.Instance.Level - 1}_star_{match / 30}");

		// Stars Animation
		starsOld[0].transform.parent.gameObject.SetActive(true);

		yield return new WaitForSeconds(0.2f);
		//yield return StartCoroutine(IE_FillStars(match, 0.4f));
		if (Preference.Instance.VibrationOn)
		{
			Handheld.Vibrate();
		}

		// complement animation
		var complementText = complementTextOld;
		yield return new WaitForSeconds(0.2f);
		complementText.text = GetComplementText(match);
		var textAnimation = complementText.transform.DOScale(1, 0.4f).Play();
		yield return textAnimation.WaitForCompletion();

		// coin animation
		yield return StartCoroutine(IE_CoinAnimation(_bonusCoin, 0.4f));
		yield return new WaitForSeconds(0.2f);
		// ad button appear
		xcoinButton.transform.localScale = Vector3.zero;
		Ads.SendRvImpression("x3money");
		xcoinButton.gameObject.SetActive(true);
		xcoinButton.transform.DOScale(1, 0.2f).SetEase(Ease.InOutSine).Play();

		// Replay and Next
		restartButton.gameObject.SetActive(true);
		nextButton.gameObject.SetActive(true);
		restartButton.transform.localScale = Vector3.zero;
		nextButton.transform.localScale = Vector3.zero;
		yield return new WaitForSeconds(Config.Instance.DelayDisplaySkipButtonTime);
		restartButton.transform.DOScale(1, 0.2f).SetEase(Ease.InOutSine).Play();
		nextButton.transform.DOScale(1, 0.2f).SetEase(Ease.InOutSine).Play();
	}

	private string GetComplementText(float match)
	{
		string[] oneStarCompliments = {"TRY HARDER", "TRY BETTER"};
		string[] twoStarCompliments = {"COOL", "THAT WAS GOOD"};
		string[] threeStarCompliments = {"AWESOME", "PERFECT", "MARVELOUS"};

		if (match <= 34)
		{
			return oneStarCompliments.GetRandomElement();
		}
		return match <= 67 ? twoStarCompliments.GetRandomElement() : threeStarCompliments.GetRandomElement();
	}

	private IEnumerator IE_CoinAnimation(int target, float duration)
	{
		coinText.transform.parent.gameObject.SetActive(true);
		var amount = 0;
		const int amoutSize = 5;

		var waitForSeconds = new WaitForSeconds(Time.deltaTime * duration / (amoutSize));
		while (amount < target)
		{
			amount += amoutSize;
			if (amount > target)
			{
				amount = target;
			}

			coinText.text = amount.ToString();
			yield return waitForSeconds;
		}

		yield return new WaitForSeconds(0.5f);
		var hasBonusCoin = Config.Instance.BonusAndSpecialOption && Gameplay.Instance.SpecialCoinEarned > 0;
		if (hasBonusCoin)
		{
			_bonusCoin += Gameplay.Instance.SpecialCoinEarned;
			specialEarnCoinFX.gameObject.SetActive(true);
		}

		coinText.text = "+" + _bonusCoin;
		xCoinText.text = "+" + _bonusCoin * Multiplier;
		if (!hasBonusCoin) yield break;
		yield return new WaitForSeconds(2.0f);
		specialEarnCoinFX.gameObject.SetActive(false);
	}

	private float _startTime;

	public override void AnimateIn()
	{
		canvasGroup.alpha = 0;
		canvasGroup.DOFade(1, .1f).OnComplete(delegate
		{
			base.AnimateIn();
			SceneManager.Instance.OnSceneAnimatedIn(this);

			StartCoroutine(IE_PopupAnimation());
		});
		_startTime = Time.realtimeSinceStartup;
		Analytics.Instance.LogEvent("Result_open");
	}

	public override void AnimateOut()
	{
		canvasGroup.DOFade(0, .1f).OnComplete(delegate
		{
			base.AnimateOut();
			SceneManager.Instance.OnSceneAnimatedOut(this);
		});
		Analytics.Instance.LogEvent("Result_close", "duration", Time.realtimeSinceStartup - _startTime);
	}

	private void Next()
	{
		Gameplay.ResetOwnedOption();
		HideBoxProgress();
		Gameplay.Instance.HideCompleteButton();
		Replayed = false;
		var objects = Gameplay.Level.GetComponentsInChildren<SprinkleObject>();
		foreach (var obj in objects)
		{
			if (!obj.OnSurface)
			{
				Destroy(obj.gameObject);
			}
		}
		var n = (Profile.Instance.Level - 2) % Config.Instance.InComeData.GiftProgess.Rate;
		if (n == (Config.Instance.InComeData.GiftProgess.Rate - 1))
		{
			Profile.Instance.GiftProgress = 0;
			SceneManager.Instance.OpenScene(SceneID.Gift);
			SceneManager.Instance.CloseScene(SceneID.Result);
			SceneManager.Instance.CloseScene(SceneID.Gameplay);
			return;
		}

		if (Profile.Instance.KeyAmount >= 3)
		{
			SceneManager.Instance.ShowLoading(false, 1.0f, () =>
			{
				SceneManager.Instance.OpenScene(SceneID.ChestRoom);
				SceneManager.Instance.CloseScene(SceneID.Result);
				SceneManager.Instance.CloseScene(SceneID.Gameplay);
			});
			return;
		}
		PlayNextLevel();
	}

	private static void PlayNextLevel()
	{
		SceneManager.Instance.ShowPopupShield();
		Ads.ShowInterstitial(() =>
		{
			SceneManager.Instance.HidePopupShield();
			var condition =
				ShopManager.Instance.PastryBagsUnlockable && !ShopManager.Instance.AllPastryBagsUnlocked ||
				ShopManager.Instance.SprinkleBottleUnlockable && !ShopManager.Instance.AllSprinklBottleUnlocked ||
				GlazeManager.Instance.Unlockable && !GlazeManager.Instance.AllUnlocked;
		if (condition && Profile.Instance.FirstTwoTimeGoStore < 2)
		{
			SceneManager.Instance.ShowLoading(false, 1.0f, () =>
			{
				SceneManager.Instance.HidePopupShield();
				SceneManager.Instance.CloseScene(SceneID.Home);
				SceneManager.Instance.CloseScene(SceneID.Gameplay);
				SceneManager.Instance.CloseScene(SceneID.Result);
				SceneManager.Instance.OpenScene(SceneID.Shop);
			});
			Profile.Instance.FirstTwoTimeGoStore++;
		}
		else
		{
			SceneManager.Instance.ShowLoading(false, 1.0f, () => 
			{
				SceneManager.Instance.CloseScene(SceneID.Result);
				SceneManager.Instance.CloseScene(SceneID.Gameplay);
				SceneManager.Instance.OpenScene(SceneID.UIBakery, false);
			});
		}
		}, "Result");
	}

	public void Replay()
	{
		if (!_canClick) return;
		_canClick = false;
		if (Gameplay.Level.CakeType == CakeType.Vip)
		{
			Profile.Instance.VipLevel--;
		}
		else
		{
			Profile.Instance.Level--;
		}
		HideBoxProgress();
		Gameplay.Instance.HideCompleteButton();
		Level.BonusScore = 0.1f;
		Ads.ShowInterstitial(delegate
		{
			SceneManager.Instance.ShowLoading(false, 1.0f, () =>
			{
				Destroy(Gameplay.Level.gameObject);
				Gameplay.Level = null;
				SceneManager.Instance.CloseScene(SceneID.Result);
				SceneManager.Instance.ReloadScene(SceneID.Gameplay);
				SceneManager.Instance.OpenScene(SceneID.Home);
			});
			
		}, "Result");

		var replayEvent = $"level_{Profile.Instance.Level}_replay";
		Replayed = true;
		Analytics.Instance.LogEvent(replayEvent);
	}

	public void OnClickAds()
	{
		Analytics.Instance.LogEvent("x3money_clicked");
		Ads.SendRvClicked("x3money");
		Ads.ShowRewardedVideo("x3money", (result) =>
		{
			if (result != RewardedVideoState.Watched)
			{
				return;
			}

			Ads.SendRvWatched("x3money");
			Analytics.Instance.LogEvent("x3money_watched");
			StartCoroutine(RewardGems(_bonusCoin * Multiplier));
		});
	}

	private IEnumerator RewardGems(int amount, float timeScale = 1f)
	{
		dark.gameObject.SetActive(true);
		gemReward.gameObject.SetActive(true);
		for (var i = 0; i < 20; i++)
		{
			var diamond = Instantiate(diamondPrefab, gemReward).transform;
			diamond.localRotation = Random.rotation;
			diamond.DOLocalMove(Random.insideUnitSphere, Random.Range(0.3f, 0.7f) / timeScale).SetEase(Ease.OutCubic);
			diamond.DOLocalRotate(Random.rotation.eulerAngles, 10).SetEase(Ease.Linear);
		}

		yield return new WaitForSeconds(0.7f / timeScale);
		UIGlobal.ShowCoinInfo();

		var counter = 0;
		var gemCount = gemReward.childCount;
		var n = amount / gemCount;
		var a = amount % gemCount;
		for (var i = 0; i < gemReward.childCount; i++)
		{
			var diamond = gemReward.GetChild(i);
			diamond.DOMove(coinTarget.transform.position, 0.5f / timeScale)
				.SetEase(Ease.InCubic)
				.SetDelay(i * 0.05f)
				.OnComplete(() =>
				{
					diamond.gameObject.SetActive(false);
					counter += n + (a > 0 ? 1 : 0);
					a--;
					if (counter <= amount)
					{
						UIGlobal.ShowCoinAmount(Profile.Instance.CoinAmount + counter);
					}
				});
		}

		yield return new WaitForSeconds(gemReward.childCount * 0.05f + 0.6f / timeScale);
		Profile.Instance.CoinAmount += amount;
		DailyQuestManager.UpdateDay();
		Profile.Instance.MoneyCurrentEarn += amount;
		UIGlobal.ShowCoinAmount(Profile.Instance.CoinAmount);
		Next();
	}

	public void OnClickNext()
	{
		if (!_canClick) return;
		_canClick = false;
		Analytics.Instance.LogEvent("Result_skip");
		StartCoroutine(RewardGems(_bonusCoin, 1.5f));
	}

	private void HideBoxProgress()
	{
		boxProgress.DOFade(0, 0.3f).OnComplete(
			() => boxProgress.gameObject.SetActive(false));
	}

	private void ShowBoxProgress()
	{
		boxProgress.gameObject.SetActive(true);
		boxProgress.alpha = 0;
		boxProgress.DOFade(1, 0.3f).SetDelay(.4f);
	}

	private void SetDotColor()
	{
		var level = Profile.Instance.Level - 2;
		var p = AssetManager.Instance.CurrentProgress(Mathf.Max(level, 1));
		var n = level % 4;
		if (!Config.Instance.DisableProgress && p != null)
		{
			n = level - p.levelStart + 1;
		}

		for (var i = 0; i < 4; i++)
		{
			var green = i <= n;
			boxProgressDots.GetChild(i).GetChild(0).gameObject.SetActive(!green);
			boxProgressDots.GetChild(i).GetChild(1).gameObject.SetActive(green);
			boxProgressDots.GetChild(i).GetChild(2).gameObject.SetActive(i == n);
			boxProgressDots.GetChild(i).gameObject.SetActive((p != null && i <= p.levelEnd - p.levelStart) || Config.Instance.DisableProgress);
		}
	}
}