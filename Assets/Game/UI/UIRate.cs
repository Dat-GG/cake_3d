
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Nama
{
	class UIRate : Popup
	{
		[SerializeField] Button thankButton;
		[SerializeField] GameObject[] buttons;

		private void OnEnable()
		{
			thankButton.gameObject.SetActive(false);
			buttons[0].SetActive(true);
			buttons[1].SetActive(true);
			buttons[2].SetActive(true);
		}

		public void Close()
		{
			SceneManager.Instance.ClosePopup();
		}

		public override void OnBackButtonPressed()
		{
			SceneManager.Instance.ClosePopup();
		}

		public void Rate(int count)
		{
			Analytics.Instance.LogEvent(string.Format("rate_select_{0}", count));
			if (count >= 4)
			{
				Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
			}

			thankButton.gameObject.SetActive(true);
			buttons[0].SetActive(false);
			buttons[1].SetActive(false);
			buttons[2].SetActive(false);
			StartCoroutine(DelayClose());
		}

		IEnumerator DelayClose()
		{
			yield return new WaitForSeconds(1);
			SceneManager.Instance.ClosePopup();
		}
	}
}
