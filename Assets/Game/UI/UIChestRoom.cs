﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Nama
{
	public class UIChestRoom : Scene
	{
		[SerializeField] private ChestItemView[] chestArray;
		[SerializeField] private GameObject[] keyArray;
		[SerializeField] private GameObject keyGroup;
		[SerializeField] private GameObject watchAdsGroup;
		[SerializeField] private GameObject closeGroup;
		[SerializeField] private GameObject bestCoin;
		[SerializeField] private Image bestSkin;
		[SerializeField] private Text cointext;
		[SerializeField] private ParticleSystem confetti;
		[SerializeField] private Button noThankButton;
		private int _keyAmount = 3;
		private int _openCount;

		private GiftItem _topReward;
		private readonly List<int> _gems = new List<int>();
		private Sprite _topSkinSprite;
		private int _topPrizeIndex;
		private float _startTime;

		private void OnEnable()
		{
			_startTime = Time.realtimeSinceStartup;
			Analytics.Instance.LogEvent("ChestRoom_open");

			UIGlobal.ShowCoinInfo();
			noThankButton.gameObject.SetActive(false);
			_gems.Add(Config.Instance.InComeData.OpenChest.Gem1);
			_gems.Add(Config.Instance.InComeData.OpenChest.Gem2);
			_gems.Add(Config.Instance.InComeData.OpenChest.Gem3);
			_topPrizeIndex = GetTopPrizeIndex();
			_topReward = new GiftItem(GiftItemType.Gem, "");

			var openChest = Config.Instance.InComeData.OpenChest;
			var topPrizes = openChest.TopPrizes;
			var rewardIndex = Mathf.Clamp(Profile.Instance.OpenChestCount, 0, topPrizes.Count - 1);

			var currentRewardList = topPrizes[rewardIndex];
			var remoteReward = currentRewardList.GetRandomReward();

			var rewardGift = new GiftItem(GiftItemType.Gem, Config.Instance.InComeData.TopPrizeGem);
			if (remoteReward != null)
			{
				rewardGift = remoteReward.Gift;
			}

			switch (rewardGift.type)
			{
				case GiftItemType.Gem:

					_topReward = new GiftItem(GiftItemType.Gem, rewardGift.gem);
					break;

				case GiftItemType.Pet:

					var avaiablePets = Profile.Instance.GetAvailablePets();
					var petData = avaiablePets.Find(p => p.id == rewardGift.id);

					if (petData != null)
					{
						_topReward = new GiftItem(GiftItemType.Pet, petData.id);
						var skin = PetManager.Instance.GetPet(_topReward.id);
						_topSkinSprite = Resources.Load<Sprite>("Sprites/Pets/" + skin.image);

						break;
					}
					else
					{
						if (avaiablePets.Count > 0)
						{
							var randomPet = avaiablePets.GetRandomElement();

							_topReward = new GiftItem(GiftItemType.Pet, randomPet.id);
							var skin = PetManager.Instance.GetPet(_topReward.id);
							_topSkinSprite = Resources.Load<Sprite>("Sprites/Pets/" + skin.image);

							break;
						}

						goto default;
					}


				case GiftItemType.SprinkleCup:
					List<StorageSkin> avaiableSprinkleBottles = Profile.Instance.GetAvailableSprinkleBottles();
					StorageSkin sprinkleBottle = avaiableSprinkleBottles.Find(t => t.id == rewardGift.id);
					if (sprinkleBottle != null)
					{
						_topReward = new GiftItem(GiftItemType.SprinkleCup, sprinkleBottle.id);
						var skin = ShopManager.Instance.GetSprinkleBottle(_topReward.id);
						_topSkinSprite = Resources.Load<Sprite>("Sprites/Storages/" + skin.image);
						break;
					}
					else
					{
						if (avaiableSprinkleBottles.Count > 0)
						{
							var randomBottle = avaiableSprinkleBottles.GetRandomElement();

							_topReward = new GiftItem(GiftItemType.SprinkleCup, randomBottle.id);
							var skin = ShopManager.Instance.GetSprinkleBottle(_topReward.id);
							_topSkinSprite = Resources.Load<Sprite>("Sprites/Storages/" + skin.image);
							break;
						}

						goto default;
					}

				case GiftItemType.IcingBag:
					var avaiablePastrybags = Profile.Instance.GetAvailablePastryBags();
					var pastrybagSkin = avaiablePastrybags.Find(t => t.id == rewardGift.id);
					if (pastrybagSkin != null)
					{
						_topReward = new GiftItem(GiftItemType.IcingBag, pastrybagSkin.id);

						var skin = ShopManager.Instance.GetPastryBag(_topReward.id);
						_topSkinSprite = Resources.Load<Sprite>("Sprites/Characters/" + skin.image);
						break;
					}
					else
					{
						if (avaiablePastrybags.Count > 0)
						{
							var randomPastrybag = avaiablePastrybags.GetRandomElement();

							_topReward = new GiftItem(GiftItemType.IcingBag, randomPastrybag.id);
							var skin = ShopManager.Instance.GetPastryBag(_topReward.id);
							_topSkinSprite = Resources.Load<Sprite>("Sprites/Characters/" + skin.image);
							break;
						}

						goto default;
					}

				case GiftItemType.GlazeCup:
					var avaiableGlazes = Profile.Instance.GetAvailableGlazes();
					var galzeData = avaiableGlazes.Find(t => t.id == rewardGift.id);
					if (galzeData != null)
					{
						_topReward = new GiftItem(GiftItemType.GlazeCup, galzeData.id);
						var skin = GlazeManager.Instance.GetGlazeSkin(_topReward.id);
						_topSkinSprite = Resources.Load<Sprite>("Sprites/GlazeTools/" + skin.image);
						break;
					}
					else
					{
						if (avaiableGlazes.Count > 0)
						{
							var randomGlaze = avaiableGlazes.GetRandomElement();
							_topReward = new GiftItem(GiftItemType.GlazeCup, randomGlaze.id);
							GlazeData skin = GlazeManager.Instance.GetGlazeSkin(_topReward.id);
							_topSkinSprite = Resources.Load<Sprite>("Sprites/GlazeTools/" + skin.image);
							break;
						}

						goto default;
					}

				case GiftItemType.Random:
					var gifts = Profile.Instance.GetAvaibleGifts();
					if (gifts.Count > 0)
					{
						rewardGift = gifts.GetRandomElement();

						if (rewardGift.type == GiftItemType.GlazeCup)
						{
							goto case GiftItemType.GlazeCup;
						}

						if (rewardGift.type == GiftItemType.IcingBag)
						{
							goto case GiftItemType.IcingBag;
						}

						if (rewardGift.type == GiftItemType.Pet)
						{
							goto case GiftItemType.Pet;
						}

						if (rewardGift.type == GiftItemType.SprinkleCup)
						{
							goto case GiftItemType.SprinkleCup;
						}

						goto default;
					}
					else
					{
						goto default;
					}

				default:
					_topReward = new GiftItem(GiftItemType.Gem, Config.Instance.InComeData.TopPrizeGem);
					break;
			}

			Analytics.Instance.LogEvent(_topReward.type == GiftItemType.Gem ?
				$"Chest_{_topReward.type}" : $"Chest_{_topReward.type}_{_topReward.id}");

			if (_topReward.type == GiftItemType.Gem)
			{
				bestCoin.transform.localScale = Vector3.zero;
				bestCoin.SetActive(true);
				bestCoin.transform.DOScale(Vector3.one, 0.5f);
				bestSkin.gameObject.SetActive(false);
				cointext.text = _topReward.gem.ToString();
			}
			else
			{
				bestCoin.SetActive(false);
				bestSkin.gameObject.SetActive(true);
				bestSkin.transform.localScale = Vector3.zero;
				bestSkin.transform.DOScale(Vector3.one, 0.5f);

				if (_topSkinSprite != null)
					bestSkin.sprite = _topSkinSprite;
			}

			Profile.Instance.OpenChestCount++;

			try
			{
				SceneManager.Instance.HideLoading();
			}
			catch
			{
				Analytics.Instance.LogEvent("exception_1");
			}

			try
			{
				Refresh();
			}
			catch
			{
				Analytics.Instance.LogEvent("exception_4");
			}

		}

		private void Start()
		{
			foreach (ChestItemView chest in chestArray)
			{
				chest.Init(OpenChest);
			}

			ChestWaveEffect();
		}

		private int GetTopPrizeIndex()
		{
			var firstRate = Config.Instance.InComeData.OpenChest.TopPrize3FirstKeyPercent / 100f;
			var afterRate = Config.Instance.InComeData.OpenChest.TopPrize3AfterKeyPercent / 100f;
			if (Random.value < firstRate)
			{
				return Random.Range(0, 3);
			}

			return Random.value < afterRate ? Random.Range(3, 6) : Random.Range(6, 9);
		}

		private void OpenChest(ChestItemView chest)
		{
			if (_keyAmount < 1)
			{
				return;
			}

			var isBest = false;
			var item = new GiftItem(GiftItemType.Gem, _gems.GetRandomElement());

			if (_openCount == _topPrizeIndex)
			{
				item = _topReward;
				isBest = true;
			}


			_openCount++;
			_keyAmount--;

			Profile.Instance.KeyAmount = _keyAmount;

			chest.SetItem(item, isBest, _topSkinSprite);
			CollectRewardItem(item, isBest);

			chest.OpenEffect();
			Refresh();

			_mySequence.Pause();
		}

		private void CollectRewardItem(GiftItem item, bool isBest)
		{
			if (item.type == GiftItemType.Gem)
			{
				Profile.Instance.CoinAmount += item.gem;
			}
			else
			{

				Profile.Instance.CollectGift(item);
			}

			if (isBest)
			{
				confetti.Play();
			}
		}

		public void GetKey()
		{
			Analytics.Instance.LogEvent("ChestRoom_ad_clicked");
			Ads.SendRvClicked("get_key");
			Ads.ShowRewardedVideo("get_key", (state) =>
			{
				if (state != RewardedVideoState.Watched) return;
				Ads.SendRvWatched("get_key");
				Analytics.Instance.LogEvent("ChestRoom_ad_watched");
				Analytics.Instance.LogEvent(_openCount >= 6
					? $"Chest_{_topReward.type}_{_topReward.id}_RW_2"
					: $"Chest_{_topReward.type}_{_topReward.id}_RW_1");
				_keyAmount = 3;
				Refresh();
			});
		}

		private void Refresh()
		{
			if (_openCount >= 9)
			{
				closeGroup.SetActive(true);
				keyGroup.SetActive(false);
				watchAdsGroup.SetActive(false);
				return;
			}
			if (_keyAmount > 0)
			{
				keyGroup.SetActive(true);
				watchAdsGroup.SetActive(false);
				closeGroup.SetActive(false);
				for (var i = 0; i < keyArray.Length; i++)
				{
					keyArray[i].SetActive(i < _keyAmount);
				}
			}
			else
			{
				keyGroup.SetActive(false);
				closeGroup.SetActive(false);
				watchAdsGroup.SetActive(true);
				Ads.SendRvImpression("get_key");
				DOVirtual.DelayedCall(Config.Instance.DelayDisplaySkipButtonTime, delegate
				{
					noThankButton.gameObject.SetActive(true);
				});
			}
		}

		public void Close()
		{
			if (_openCount <= 3)
			{
				Analytics.Instance.LogEvent("ChestRoom_skip");
				Analytics.Instance.LogEvent($"Chest_{_topReward.type}_{_topReward.id}_Skip");
			}
			else if (_openCount <= 6)
			{
				Analytics.Instance.LogEvent($"Chest_{_topReward.type}_{_topReward.id}_Skip1");
			}
			else
			{
				Analytics.Instance.LogEvent($"Chest_{_topReward.type}_{_topReward.id}_Skip2");
			}

			Analytics.Instance.LogEvent("ChestRoom_close", "duration", Time.realtimeSinceStartup - _startTime);
			SceneManager.Instance.ShowPopupShield();
			Ads.ShowInterstitial(() =>
			{
				SceneManager.Instance.HidePopupShield();
				SceneManager.Instance.ShowLoading(false, 1.0f, () =>
				{
					//SceneManager.Instance.OpenScene(SceneID.Gameplay);
					SceneManager.Instance.CloseScene(SceneID.ChestRoom);
					SceneManager.Instance.OpenScene(SceneID.UIBakery, false);
				});
			}, "chestroom");
		}

		private Sequence _mySequence;

		private void ChestWaveEffect()
		{
			_mySequence = DOTween.Sequence();
			foreach (var t in chestArray)
			{
				_mySequence.Append(t.transform.DOScale(1.1f, .1f));
				_mySequence.Append(t.transform.DOScale(1f, .03f).SetDelay(.1f));
			}
			_mySequence.SetDelay(1f).SetLoops(-1);
		}
	}

	[System.Serializable]
	public class GiftItem
	{
		public GiftItemType type;
		public int gem;
		public string id;

		public GiftItem(GiftItemType type, int gem = 0)
		{
			this.type = type;
			this.gem = gem;
		}

		public GiftItem(GiftItemType type, string id = "")
		{
			this.type = type;
			this.id = id;
		}
	}

}
