using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Nama
{
    class UIExit : Popup
    {
        public void Back()
        {
            SceneManager.Instance.ClosePopup();
        }

        public void Exit()
        {
            Application.Quit();
        }

        public override void OnBackButtonPressed()
        {
            Back();
        }
    }
}
