﻿using DG.Tweening;
using Tabtale.TTPlugins;
using UnityEngine;
using UnityEngine.UI;

namespace Nama
{
	internal class UISettings : Popup
	{
		[SerializeField] private CustomToggle sfxToggle;
		[SerializeField] private CustomToggle vibrateToggle;
		[SerializeField] private Button privacySettingsButton;
		[SerializeField] private Button privacyPolicyButton;
		[SerializeField] private Button termOfUseButton;

		private void OnEnable()
		{
			var gdpr = ShouldShouldPrivacySettings();
			privacySettingsButton.gameObject.SetActive(gdpr);
			privacyPolicyButton.gameObject.SetActive(!gdpr);
			termOfUseButton.gameObject.SetActive(!gdpr);
			privacySettingsButton.onClick.AddListener(() =>
			{
				TTPPrivacySettings.ShowPrivacySettings(null);
			});
			privacyPolicyButton.onClick.AddListener(() =>
			{
				Application.OpenURL("https://www.crazylabs.com/apps-privacy-policy/");
			});
			termOfUseButton.onClick.AddListener(() =>
			{
				Application.OpenURL("https://www.crazylabs.com/terms-of-use/");
			});
			sfxToggle.Set(Preference.Instance.SfxOn);
			vibrateToggle.Set(Preference.Instance.VibrationOn);
		}

		private bool vibrateCheck = true;
		public void ToogleVibration()
		{
			Preference.Instance.VibrationOn = !Preference.Instance.VibrationOn;
			vibrateToggle.Set(Preference.Instance.VibrationOn);
			if (!vibrateCheck) return;
			vibrateCheck = false;
			Analytics.Instance.LogEvent(
				$"settings_vibrate_{(Preference.Instance.VibrationOn ? "on" : "off")}");
			DOVirtual.DelayedCall(2, delegate
			{
				vibrateCheck = true;
			});
		}

		private bool sfxCheck = true;
		public void ToggleSfx()
		{
			Preference.Instance.SfxOn = !Preference.Instance.SfxOn;
			sfxToggle.Set(Preference.Instance.SfxOn);
			if (!sfxCheck) return;
			sfxCheck = false;
			Analytics.Instance.LogEvent(
				$"settings_sound_{(Preference.Instance.SfxOn ? "on" : "off")}");
			DOVirtual.DelayedCall(2, delegate
			{
				sfxCheck = true;
			});
		}

		public void Close()
		{
			SceneManager.Instance.ClosePopup();
		}

		private static bool ShouldShouldPrivacySettings()
		{
			var currentConsent = TTPPrivacySettings.CustomConsentGetConsent();
			return
				currentConsent == TTPPrivacySettings.ConsentType.NPA ||
				currentConsent == TTPPrivacySettings.ConsentType.PA;
		}
		
		public override void OnBackButtonPressed()
		{
			SceneManager.Instance.ClosePopup();
		}
	}
}