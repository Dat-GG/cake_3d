using DG.Tweening;
using Nama;
using UnityEngine;
using UnityEngine.UI;

internal class UIHome : Scene
{
	[SerializeField] private CanvasGroup canvasGroup;
	[SerializeField] private Text levelText;
	[SerializeField] private GameObject settingsGO;

	//[Space] [Header("Shop")] [SerializeField]
	//private Button shopButton;

	//[SerializeField] private GameObject shopNotification;
	//[SerializeField] private Animator shopButtonAnim;

	private void OnEnable()
	{
		levelText.text = "LEVEL " + Profile.Instance.Level;
		settingsGO.SetActive(true);
		//ShowShopButton();
	}

	//public override void AnimateIn()
	//{
	//	shopButton.interactable = true;
	//	canvasGroup.alpha = 0;
	//	canvasGroup.DOFade(1f, 0.5f).OnComplete(delegate { SceneManager.Instance.OnSceneAnimatedIn(this); });
	//}

	//public override void AnimateOut()
	//{
	//	shopButton.interactable = false;
	//	canvasGroup.DOFade(0, 0.3f).OnComplete(delegate { SceneManager.Instance.OnSceneAnimatedOut(this); });
	//}

	//private void ShowShopButton()
	//{
	//	// Skin button
	//	shopButton.interactable = true;
	//	var isNew =
	//		(ShopManager.Instance.PastryBagsUnlockable && !ShopManager.Instance.AllPastryBagsUnlocked) ||
	//		(ShopManager.Instance.SprinkleBottleUnlockable && !ShopManager.Instance.AllSprinklBottleUnlocked) ||
	//		(PetManager.Instance.Unlockable && !PetManager.Instance.AllUnlocked) ||
	//		(GlazeManager.Instance.Unlockable && !GlazeManager.Instance.AllUnlocked);

	//	shopButton.gameObject.SetActive(
	//		ShopManager.Instance.PastryBagsUnlockable ||
	//		Profile.Instance.PastryBagAmount > 1 ||
	//		ShopManager.Instance.SprinkleBottleUnlockable ||
	//		Profile.Instance.SprinkleBottleAmount > 1 ||
	//		PetManager.Instance.Unlockable ||
	//		Profile.Instance.PetAmount > 0 ||
	//		GlazeManager.Instance.Unlockable ||
	//		Profile.Instance.GlazeAmount > 1);

	//	shopButtonAnim.enabled = false;
	//	if ( /*!Profile.Instance.ShopNotified &&*/ isNew)
	//	{
	//		shopButtonAnim.enabled = true;
	//		Profile.Instance.ShopNotified = true;
	//	}

	//	shopNotification.SetActive(shopButtonAnim.enabled);
	//	if (!shopButtonAnim.enabled)
	//	{
	//		shopButtonAnim.transform.localEulerAngles = Vector3.zero;
	//	}
	//}

	//public void OnClickShop()
	//{
	//	SceneManager.Instance.CloseScene(SceneID.Home);
	//	SceneManager.Instance.ShowLoading(false, 1.0f, () =>
	//	{
	//		SceneManager.Instance.HidePopupShield();
	//		SceneManager.Instance.OpenScene(SceneID.Shop);
	//	});
	//}

	public void Settings()
	{
		SceneManager.Instance.OpenPopup(SceneID.Settings);
	}

	public override void OnBackButtonPressed()
	{
		SceneManager.Instance.OpenPopup(SceneID.Exit);
	}
}