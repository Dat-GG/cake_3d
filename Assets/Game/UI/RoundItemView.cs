﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class RoundItemView : MonoBehaviour
{
	[SerializeField] Text nameText;
	[SerializeField] RectTransform frameRT;
	[SerializeField] RectTransform checkRT;
	[SerializeField] GameObject checkOn;
	[SerializeField] GameObject checkOff;
	[SerializeField] Image lineImage;
	[SerializeField] GameObject winObject;
	[SerializeField] GameObject loseObject;
	[SerializeField] Image nextObject;

	float animationTime = .7f;

	public void SetDefault(string roundName)
	{
		nameText.text = roundName;
		frameRT.localScale = Vector3.zero;
		checkRT.localScale = Vector3.zero;
		lineImage.fillAmount = 0;
	}

	public void Win(bool playAnimation = false)
	{
		bool win = true;
		checkOn.SetActive(win);
		checkOff.SetActive(!win);
		winObject.SetActive(win);
		loseObject.SetActive(!win);
		nextObject.gameObject.SetActive(false);

		if (playAnimation)
		{
			AnimationIn(win);
			lineImage.fillAmount = 0;
			lineImage.DOFillAmount(1, animationTime);
		} else
		{
			lineImage.fillAmount = 1;
		}
	}

	public void Lose(bool playAnimation = false)
	{
		bool win = false;
		checkOn.SetActive(win);
		checkOff.SetActive(!win);
		winObject.SetActive(win);
		loseObject.SetActive(!win);
		nextObject.gameObject.SetActive(false);
		lineImage.fillAmount = 0;

		if (playAnimation)
		{
			AnimationIn(win);
		}
	}

	public void SetNext()
	{
		checkOn.SetActive(false);
		checkOff.SetActive(false);
		winObject.SetActive(false);
		loseObject.SetActive(false);
		nextObject.gameObject.SetActive(true);
		lineImage.fillAmount = 0;
		//frameRT.DOScale(Vector3.one, animationTime).SetEase(Ease.OutBack);
		frameRT.localScale = Vector3.one;
		nextObject.color = new Color(1, 1, 1, 0);
		nextObject.DOFade(1, .3f);
	}

	void AnimationIn(bool win)
	{
		frameRT.DOScale(Vector3.one, animationTime).SetEase(Ease.OutBack);
		checkRT.DOScale(Vector3.one, animationTime).SetEase(Ease.OutBack);
		nameText.DOText(win ? "WIN" : "FAIL", 0f).SetDelay(.6f).OnComplete(delegate
		{
			nameText.transform.DOScale(Vector3.one * 1.15f, .3f).SetEase(Ease.InBack);
		});
	}
}
