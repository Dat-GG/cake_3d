﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using DG.Tweening;

namespace Nama
{
	public class ChestItemView : MonoBehaviour
	{
		[SerializeField] Text coinText;
		[SerializeField] Image skinImage;
		[SerializeField] GameObject OpenedGO;
		[SerializeField] GameObject DefaultGO;
		[SerializeField] ParticleSystem particle;
		[SerializeField] GameObject bestGroup;

		bool opened = false;
		Action<ChestItemView> OpenAction;

		public void SetItem(GiftItem item, bool isBest, Sprite skinSprite)
		{
			OpenedGO.SetActive(false);
			DefaultGO.SetActive(true);
			bestGroup.SetActive(isBest);
			if (isBest)
			{

				Utils.Instance.VibrateHeavy();
			}

			if (item.type == GiftItemType.Gem)
			{
				coinText.text = item.gem.ToString();
				coinText.gameObject.SetActive(true);
				skinImage.gameObject.SetActive(false);

			}
			else
			{
				coinText.gameObject.SetActive(false);
				skinImage.gameObject.SetActive(true);
				skinImage.sprite = skinSprite;
			}
		}

		public void Open()
		{
			if (opened)
			{
				return;
			}
			OpenAction?.Invoke(this);
		}

		public void OpenEffect()
		{
			OpenedGO.SetActive(true);
			DefaultGO.SetActive(false);
			OpenedGO.transform.localEulerAngles = new Vector3(0, 70, 0);
			OpenedGO.transform.DORotate(Vector3.zero, .5f).SetEase(Ease.OutBack);
			particle.Play();
			opened = true;
			SoundManager.Instance.PlaySFX("choose_award");
			Utils.Instance.VibrateLight();
		}

		public void Init(Action<ChestItemView> openAction)
		{
			OpenAction = openAction;
		}
	}
}