using Nama;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Funzilla
{
    public class UnlockItem : Scene
    {
        [SerializeField] private Text UnlockRateText;
        [SerializeField] private Image completeBar;
        private bool _active;

        public override void Init(object data)
        {
            completeBar.DOFillAmount(0, 1f).OnUpdate(() =>
            {
                var rate = (int)(completeBar.fillAmount * 100);
                UnlockRateText.text = rate + "% Unlocked";
            });
        }

        private void Awake()
        {

        }

        private void Start()
        {
            SceneManager.Instance.HideSplash();
            SceneManager.Instance.HideLoading();
        }


        public void Close()
        {
            SceneManager.Instance.ShowLoading(false, 1.0f, () =>
            {
                SceneManager.Instance.CloseScene(SceneID.UnlockItem);
                SceneManager.Instance.OpenScene(SceneID.UIBakery, false);
            });
        }

        public override void OnBackButtonPressed()
        {
            Close();
        }

        public void Collect()
        {

        }
    }
}
