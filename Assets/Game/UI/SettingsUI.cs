﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class SettingsUI : MonoBehaviour
{
	[SerializeField] RectTransform[] items;
	[SerializeField] RectTransform slider;
	Button a;
	Toggle b;

	float transitionTime = .4f;
	bool value = false;

	private void OnEnable()
	{
		Close();
	}

	public void Toggle()
	{
		value = !value;
		if(value)
		{
			Open();
		}else
		{
			Close();
		}
	}

	void Open()
	{
		slider.DOAnchorPosY(slider.sizeDelta.y * -1f, transitionTime);

		DOVirtual.DelayedCall(transitionTime, delegate {
			foreach(var item in items)
			{
				Selectable s = item.GetComponent<Selectable>();
				s.interactable = true;
			}
		});
	}

	void Close()
	{
		slider.DOAnchorPosY(0, transitionTime);

		DOVirtual.DelayedCall(transitionTime, delegate {
			foreach (var item in items)
			{
				Selectable s = item.GetComponent<Selectable>();
				s.interactable = false;
			}
		});
	}
}
