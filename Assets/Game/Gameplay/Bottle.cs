using DG.Tweening;
using System.Collections;
using UnityEngine;

namespace Nama
{
	public class Bottle : SprinkleCharacterPart
	{
		[SerializeField] private MeshFilter meshFilter;
		[SerializeField] private Transform spawnTrans;
		[SerializeField] private Vector3 scale = Vector3.one;
		[SerializeField] private float rangeX = 0.3f;
		[SerializeField] private float rangeY = 0.4f;
		[SerializeField] private float rangeZ = 0.3f;
		[SerializeField] private Vector3 startPosition = new Vector3(0.3f,0,-0.1f);
		[SerializeField] private Vector3 startRotation = new Vector3(-75,40,0);
		[SerializeField] private Vector3 targetRotation = new Vector3(-75,130,0);
		[SerializeField] private GameObject completeFX;
		[SerializeField] private Vector3 tableOffset;
		[SerializeField] private Vector3 restRoatation;

		public Transform SpawnTrans => spawnTrans;
		public MeshFilter MeshFilter => meshFilter;
		public float RangeX => rangeX;
		public float RangeY=> rangeY;
		public float RangeZ => rangeZ;
		public Vector3 StartPosition => startPosition;
		public Vector3 StartRotation => startRotation;

		public bool Ready => rotateTween == null;
		Tweener rotateTween;
		
		public Vector3 Scale => scale;

		public override void Disappear()
		{
			gameObject.SetActive(false);
		}
		
		public override void Appear()
		{
			gameObject.SetActive(true);
		}

		float animDuration;
		public override void Press(float duration)
		{
			animDuration = duration;
			rotateTween = transform
				.DOLocalRotate(targetRotation, duration)
				.OnComplete(() => rotateTween = null);
		}

		public override void Release()
		{
			transform.DOLocalRotate(startRotation, animDuration / 2);
		}

		public void SetCompleteFX(GameObject go)
		{
			completeFX = go;
		}

		public void CompleteFX(System.Action action)
		{
			StartCoroutine(IECompleteFX(action));
		}

		IEnumerator IECompleteFX(System.Action action)
		{
			Gameplay.Instance.Pointer.gameObject.SetActive(false);
			completeFX.transform.SetParent(transform);
			completeFX.transform.localPosition = Vector3.zero;
			completeFX.gameObject.SetActive(false);
			yield return new WaitForEndOfFrame();
			completeFX.gameObject.SetActive(true);
			var newParent = Gameplay.Instance.BottleParent;
			yield return new WaitForSeconds(0.5f);
			transform.SetParent(newParent);
			transform.DOJump(newParent.transform.position + tableOffset, 1, 1, 1);
			transform.DOLocalRotate(restRoatation, 1f);
			yield return new WaitForSeconds(1.5f);
			action?.Invoke();
		}
	}
}