﻿using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using System.Linq;

namespace Nama
{
	public class StrokeAuto : DecorationStep, IPolylineSectionDistribution
	{
		[SerializeField] private Path path;
		[SerializeField] private StrokeSection[] sections;
		[SerializeField] private Color[] options = { Color.red, Color.yellow, Color.green, Color.cyan, Color.magenta };
		[SerializeField] private Brush brush;
		[SerializeField] private Material material;
		private Material _specialMaterial;

		private Polyline _poly;

		private bool[] _active;
		private readonly List<Color> _activeColors = new List<Color>();

		private PolylinePosition _currentPosition;

		public int SectionCount => sections.Length;

		public PolylineSectionByWeight GetSection(int index) => sections[index];

		private Guideline _guideline;
		private StrokeDisplay _stroke;
		private bool _hasTwist;
		private readonly List<StrokeDisplay> _strokes = new List<StrokeDisplay>(1);
		private int _specialOption;

		public override void Init(bool preview)
		{
			CreateDisplay("Display of StrokeAuto");
			_poly = new Polyline(path, transform);
			_poly.Distribute(this);
			brush.Init();

			if (!preview)
			{
				if(Config.Instance.BonusAndSpecialOption && Gameplay.Level.SpecialStroke)
				{
					// Add special option 
					foreach (var step in Gameplay.Level.CurrentLayer.CurrentSurface.Steps)
					{
						if (step is StrokeAuto strokeAuto) _specialOption = strokeAuto.sections[0].colors[0];
						break;
					}

					var listOption = options.ToList();
					listOption.Add(options[_specialOption]);
					options = listOption.ToArray();
				}
			}

			_specialMaterial = AssetManager.GetGlitterStrokeMaterial();

			_hasTwist = false;
			foreach (var section in sections)
			{
				if (section.colors.Length <= 1) continue;
				_hasTwist = true;
				break;
			}

			_active = new bool[options.Length];
			for (var i = 0; i < _active.Length; i++)
			{
				_active[i] = false;
			}

			if (preview)
			{
				var parent = transform.parent;
				var grandParent = parent.parent;
				var level = parent != null && grandParent != null ?
					grandParent.GetComponentInParent<Level>() : null;
				var specialActived = level != null && level.GlitterStroke; 
				foreach (var section in sections)
				{
					_stroke = StrokeDisplay.Create(Display.transform);
					_stroke.Init(
						brush,
						_poly,
						section.Start,
						Surface.Radius,
						section.colors.Select(color => options[color]).ToArray(),
						null,
						specialActived ? _specialMaterial : material);
					_stroke.ShowPreview(section.Length);
				}
			}
			else
			{
				_guideline = AssetManager.Instance.NewGuideline(
					Display.transform, _poly, Surface.Radius);
			}


		}

		#region Scoring

		private readonly List<int[]> _userOptions = new List<int[]>();
		public List<float> userSections = new List<float>();
		public override float GetScore()
		{
			var splitLength = .3f;
			float totalMatch = 0;
			var sectionCount = 0;
			if (splitLength >= userSections[userSections.Count - 1])
			{
				splitLength = userSections[userSections.Count - 1] / 2.0f;
			}
			for (var i = 1; i < userSections.Count; i++)
			{
				var count = Mathf.RoundToInt((userSections[i] - userSections[i - 1]) / splitLength);
				if (count <= 0) continue;
				var score = 1.0f / count;
				for (var j = 0; j < count; j++)
				{
					var p = Mathf.Lerp(userSections[i - 1], userSections [i], (float)j / count);
					var anwserOptions = GetOptions(p);
					if(Config.Instance.BonusAndSpecialOption && Gameplay.Level.SpecialStroke)
					{
						for (var x = 0; x < _userOptions[i - 1].Length; x++)
						{
							if (_userOptions[i - 1][x] == options.Length - 1)
							{
								_userOptions[i - 1][x] = _specialOption;
							}
						}
					}
						
					var matchPercent = CalculateMatch(_userOptions[i - 1], anwserOptions);
					totalMatch += score * matchPercent;
				}
				sectionCount++;
			}
			sectionCount = Mathf.Max(1, sectionCount);
			var match = Mathf.Clamp01(totalMatch / sectionCount);
			return match;
		}

		private int[] GetOptions(float position)
		{
			for (var i = sections.Length - 1; i >= 0; i--)
			{
				if (position <= 0.8f) continue;
				if (sections[i].Start.Offset <= position)
				{
					return sections[i].colors;
				}
			}
			return sections[0].colors;
		}

		private int[] GetActiveOptions()
		{
			var activeOptions = new List<int>(1);
			for (var i = 0; i < _active.Length; i++)
			{
				if (_active[i])
					activeOptions.Add(i);
			}
			return activeOptions.ToArray();
		}

		private void SaveSectionData()
		{
			userSections.Add(_currentPosition.Offset);
			_userOptions.Add(GetActiveOptions());
		}
		#endregion

		private void StartOver()
		{
			_currentPosition = _poly.StartPosition;
			_activeColors.Clear();
			for (var i = 0; i < _active.Length; i++)
			{
				_active[i] = false;
			}
		}

		private bool CanKeepGoing()
		{
			if (Surface.Previous == null)
			{
				return false;
			}

			if (Surface.Previous.GetType() != typeof(StrokeAuto))
			{
				return false;
			}

			if (!(Surface.Previous is StrokeAuto previous) || previous.OptionCount != OptionCount)
			{
				return false;
			}

			var keepGoing = false;
			for (var i = 0; i < options.Length; i++)
			{
				if (!previous._active[i]) continue;
				OptionOn(i);
				keepGoing = true;
			}

			if (keepGoing)
			{
				CreateNewStroke();
			}

			return keepGoing;
		}

		public override void Begin()
		{
			_guideline.gameObject.SetActive(true);

			StartOver();
			_stroke = null;
			Gameplay.Instance.OptionOn = OptionOn;
			Gameplay.Instance.OptionOff = OptionOff;

			var lastColor = Gameplay.Instance.PastryBag.Color;
			var colorIndex = 0;
			for (var i = 0; i < options.Length; i++)
			{
				if (options[i] != lastColor) continue;
				colorIndex = i;
				break;

			}

			Surface.PastryBag.Begin(
					Display.transform,
					_currentPosition.Pos,
					options[colorIndex],
					Surface.Radius,
					CanKeepGoing());
			Gameplay.Instance.ShowAutoOptions(options);
			Gameplay.Instance.SetCurrentTutorial(_hasTwist ? 3 : 0);
			Gameplay.Instance.SetupTutorial(_hasTwist);
			Gameplay.Instance.ActiveTutorial();
		}

		public override void DoFrame()
		{
			if (_activeColors.Count <= 0)
			{
				return;
			}
			if (Surface.PastryBag.Moving)
			{
				return;
			}
			if (_currentPosition.Offset > _poly.Length)
			{
				return;
			}
			if (_stroke == null)
			{
				CreateNewStroke();
			}

			var swipeLength = Config.DrawSpeed * Time.smoothDeltaTime;
			Advance(swipeLength);
		}

		public override bool IsOptionActive(int index)
		{
			return _active[index];
		}

		public override int OptionCount => options.Length;

		public override bool IsOptionCorrect(int index)
		{
			return sections[0].colors.Any(color => color == index);
		}

		private void OptionOn(int index)
		{
			if (index < 0 || index >= _active.Length || _activeColors.Count >= 2)
			{
				return;
			}

			CancelInvoke();
			StopAllCoroutines();
			Gameplay.Instance.HideTutorial();

			_active[index] = true;
			if (_stroke == null)
			{
				Surface.PastryBag.Press();
			}

			UpdateActiveColors();
		}

		private void OptionOff(int index)
		{
			if (index < 0 || index >= _active.Length)
			{
				return;
			}
			_active[index] = false;
			UpdateActiveColors();

			Gameplay.Instance.ActiveTutorial();

			if (_activeColors.Count <= 0)
			{
				Surface.PastryBag.Release(true);
				_stroke = null;
			}
			else if (_stroke != null)
			{
				CreateNewStroke();
			}
		}

		private void UpdateActiveColors()
		{
			_activeColors.Clear();
			for (var i = 0; i < _active.Length; i++)
			{
				if (_active[i])
				{
					_activeColors.Add(options[i]);
				}
			}

			RefreshTool();
		}

		public override void RefreshTool()
		{
			if (_activeColors.Count > 0)
			{
				Surface.PastryBag.SetColor(_activeColors[0], _stroke == null);
			}
		}

		/// <summary>
		/// Finish this step
		/// </summary>
		private void Finish()
		{
			if (_userOptions.Any(userOption => userOption.Length > 1))
			{
				Analytics.Instance.LogEvent($"level_{Profile.Instance.Level}_twist_stroke");
			}
			SaveSectionData();
			Finished = true;
			_guideline.gameObject.SetActive(false);
			Gameplay.Instance.OptionOn = null;
			Gameplay.Instance.OptionOff = null;
			Surface.PastryBag.Finish(Gameplay.Instance.transform);
			_stroke.UnderGuideLine();
		}

		private void Advance(float delta)
		{
			VibrateLight();
			_currentPosition = _poly.Advance(delta, _currentPosition);
			_stroke.Advance(delta);
			Surface.PastryBag.Move(_currentPosition.Pos, Surface.Radius);
			if (_currentPosition.Offset < _poly.Length) return;
			Utils.Instance.VibrateMedium();
			Finish();
		}

		private void CreateNewStroke()
		{
			_stroke = StrokeDisplay.Create(Display.transform);
			var specialActived =
				Config.Instance.BonusAndSpecialOption &&
				Gameplay.Level.SpecialStroke &&
				_active[_active.Length - 1];
			specialActived = specialActived || Gameplay.Level.GlitterStroke;
			_stroke.Init(
				brush,
				_poly,
				_currentPosition,
				Surface.Radius,
				_activeColors.ToArray(),
				null,
				specialActived ? _specialMaterial : material);
			_stroke.OverGuideline();
			SaveSectionData();
			_strokes.Add(_stroke);
		}

		public override void CompleteFX(System.Action action)
		{
			Gameplay.Level.CurrentLayer.PrevSurface.FlickStroke();
			Gameplay.Instance.PastryBag.CompleteFX(action);
		}

		public void FlickColor()
		{
			foreach (var stroke in _strokes)
			{
				stroke.MeshRenderer.sharedMaterial.DOFloat(0.5f, "_Bright", 0.11f).SetLoops(4, LoopType.Yoyo).SetEase(Ease.Linear);
			}
		}

#if UNITY_EDITOR
		public StrokeStepData GetStrokeStepData()
		{
			return new StrokeStepData(path);
		}

		public Color[] Options => options;

		public IEnumerable<StrokeSection> Sections => sections;
#endif
	}
}
