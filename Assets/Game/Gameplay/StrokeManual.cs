﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;

namespace Nama
{
	[System.Serializable]
	public struct ColorOption
	{
		public Color[] colors;
		public bool gradient;

		private Texture2D _gradientTexture;
		public Texture2D GradientTexture
		{
			get
			{
				if (!gradient) return null;
				if (_gradientTexture) return _gradientTexture;
				_gradientTexture = new Texture2D(colors.Length, 1, TextureFormat.RGB24, false)
				{
					anisoLevel = 0,
					wrapMode = TextureWrapMode.Clamp
				};
				_gradientTexture.SetPixels(colors);
				_gradientTexture.Apply();
				return _gradientTexture;
			}
		}
	}

	public class StrokeManual : DecorationStep, IPolylineSectionDistribution
	{
		[SerializeField] private Path path;
		[SerializeField] private StrokeSection[] sections;
		[SerializeField] private ColorOption[] options;
		[SerializeField] private Brush brush;
		[SerializeField] private Material material;
		private Material _specialMaterial;

		public Transform KeyParent => Display.transform;

		public Polyline Poly { get; private set; }

		private PolylinePosition _currentPosition;

		public int SectionCount => sections.Length;

		public PolylineSectionByWeight GetSection(int index) => sections[index];

		private Guideline _guideline;
		private Guideline _guidelineExtend;
		private StrokeDisplay _stroke;
		private readonly List<StrokeDisplay> _strokes = new List<StrokeDisplay>(1);
		private int _specialOption;
		private Vector3 _startPoint;
		private Vector3 _startDirection;
		private static bool CorrectOptionHightlight => Profile.Instance.Level <= 6 && Profile.Instance.Level != 3;

		public override void Init(bool preview)
		{
			CreateDisplay("Display of StrokeAuto");
			Poly = new Polyline(path, transform);
			Poly.Distribute(this);
			brush.Init();

			if (!preview)
			{
				if (Config.Instance.BonusAndSpecialOption && Gameplay.Level.SpecialStroke)
				{
					// Add special option 
					foreach (var step in Gameplay.Level.CurrentLayer.CurrentSurface.Steps)
					{
						if (step.GetType() != typeof(StrokeManual)) continue;
						if (step is StrokeManual strokeAuto) _specialOption = strokeAuto.sections[0].colors[0];
						break;
					}

					var listOption = options.ToList();
					listOption.Add(options[_specialOption]);
					options = listOption.ToArray();
				}
			}

			_specialMaterial = AssetManager.GetGlitterStrokeMaterial();

			if (preview)
			{
				var parent = transform.parent;
				var level = parent != null && parent.parent != null ? parent.parent.GetComponentInParent<Level>() : null;
				var specialActived = level != null && level.GlitterStroke;
				foreach (var section in sections)
				{
					_stroke = StrokeDisplay.Create(Display.transform);
					var option = options[section.colors[0]];
					_stroke.Init(
						brush,
						Poly,
						section.Start,
						Surface.Radius,
						option.colors,
						option.GradientTexture,
						specialActived ? _specialMaterial : material);
					_stroke.ShowPreview(section.Length);
				}
#if UNITY_EDITOR
				if (EditorApplication.isPlayingOrWillChangePlaymode)
				{
#endif
					if (Gameplay.Instance != null)
					{
						Gameplay.Instance.SetStrokePreview(this);
					}
#if UNITY_EDITOR
				}
#endif
			}
			else
			{
				_guideline = AssetManager.Instance.NewGuideline(
					Display.transform, Poly, Surface.Radius);
				_guideline.transform.localPosition = Vector3.zero;
			}

			_startPoint = (Poly.Points != null && Poly.Points.Count > 0) ? new Vector3(Poly.Points[0].x, 0, Poly.Points[0].y) : Vector3.zero;
			var secondPoint = (Poly.Points != null && Poly.Points.Count > 1) ? new Vector3(Poly.Points[1].x, 0, Poly.Points[1].y) : Vector3.zero;
			_startDirection = (secondPoint - _startPoint).normalized;
			_startPoint.y = transform.position.y + 0.2f;
		}

		#region Scoring

		private readonly List<int[]> _userOptions = new List<int[]>();
		private readonly List<float> _userSections = new List<float>();

		public override float GetScore()
		{
			var splitLength = .3f;
			float totalMatch = 0;
			var sectionCount = 0;
			if (splitLength >= _userSections[_userSections.Count - 1])
			{
				splitLength = _userSections[_userSections.Count - 1] / 2.0f;
			}

			for (var i = 1; i < _userSections.Count; i++)
			{
				var count = Mathf.RoundToInt((_userSections[i] - _userSections[i - 1]) / splitLength);
				if (count <= 0) continue;
				var score = 1.0f / count;
				for (var j = 0; j < count; j++)
				{
					var p = Mathf.Lerp(_userSections[i - 1], _userSections[i], (float) j / count);
					var anwserOptions = GetOptions(p);
					if (Config.Instance.BonusAndSpecialOption && Gameplay.Level.SpecialStroke)
					{
						for (var x = 0; x < _userOptions[i - 1].Length; x++)
						{
							if (_userOptions[i - 1][x] == options.Length - 1)
							{
								_userOptions[i - 1][x] = _specialOption;
							}
						}
					}

					var matchPercent = CalculateMatch(_userOptions[i - 1], anwserOptions);
					totalMatch += score * matchPercent;
				}

				sectionCount++;
			}

			sectionCount = Mathf.Max(1, sectionCount);
			var match = Mathf.Clamp01(totalMatch / sectionCount);
			return Mathf.Clamp01(match/0.99f);
		}

		private int[] GetOptions(float position)
		{
			for (var i = sections.Length - 1; i >= 0; i--)
			{
				if (position <= 0.8f) continue;
				if (sections[i].Start.Offset <= position)
				{
					return sections[i].colors;
				}
			}

			return sections[0].colors;
		}

		private void SaveSectionData()
		{
			_userSections.Add(_currentPosition.Offset);
			_userOptions.Add(new[] {_iSelected});
		}

		#endregion

		private void StartOver()
		{
			_currentPosition = Poly.StartPosition;
		}

		private bool CanKeepGoing()
		{
			if (Surface.Previous == null ||
				!(Surface.Previous is StrokeManual previous) ||
				previous.OptionCount != OptionCount)
			{
				return false;
			}

			OptionOn(Surface.PastryBag.IndexOption);
			CreateNewStroke();
			return true;
		}

		private float _delay;
		private bool _instancetiateGuideline;

		public override void Begin()
		{
			Gameplay.Instance.HideUndoButton();
			TutorialSign.Instance.StartTutorial();
			// guideline colors
			foreach (var t in sections)
			{
				var gs = new GuideLineSection
				{
					Length = t.Length / Poly.Length * (Poly.Length / 2 + 1)
				};
				foreach (var optionIndex in t.colors)
				{
					var colors = Options[optionIndex].colors;
					foreach (var color in colors)
					{
						gs.Colors.Add(color);
					}
				}

				_guideline.Sections.Add(gs);
			}

			// if twist
			if (_guideline.Sections[0].Colors.Count > 1 && !_instancetiateGuideline)
			{
				_instancetiateGuideline = true;
				_guidelineExtend = AssetManager.Instance.NewGuideline(
					Display.transform, Poly, Surface.Radius);
				_guidelineExtend.SetColor(_guideline.Sections[0].Colors[1]);
				_guidelineExtend.gameObject.SetActive(true);
				_guidelineExtend.T = .5f;
				_guidelineExtend.transform.localPosition = Vector3.zero;
			}

			_guideline.gameObject.SetActive(true);

			//
			StartOver();
			_stroke = null;
			_canKeepGoing = CanKeepGoing();
			Gameplay.Instance.OptionOn = OptionOn;

			var lastColor = Surface.PastryBag.IndexOption;
			if (lastColor >= 0 && lastColor < options.Length && !EqualOption(sections[0].colors, options[lastColor].colors))
			{
				_delay = Config.DELAY_TIME;
			}

			lastColor = (lastColor >= options.Length || lastColor < 0) ? 0 : lastColor;
			Surface.PastryBag.Begin(
				Display.transform,
				_currentPosition.Pos,
				options[lastColor].colors.Length > 0 ? options[lastColor].colors[0] : Color.white,
				Surface.Radius,
				_canKeepGoing);

			Gameplay.Instance.ShowStrokeManualOptions(options);
			OptionOn(lastColor);

			if (Gameplay.Instance.CameraFlowEnable)
				Gameplay.Instance.CameraController.ZoomIn();

			Gameplay.Instance.ONOptionsSetup = () =>
			{
				Gameplay.Instance.SetCurrentTutorial(4);
				Gameplay.Instance.ActiveTutorial();
				_tutorialVisible = true;
			};
			Gameplay.Instance.ONOptionsSetup?.Invoke();
			Surface.PastryBag.Rotate();

			if (Profile.Instance.VipLevel > 1 &&
				Profile.Instance.Level == 2 &&
				Config.Instance.StrokeManualEnabled &&
				!Profile.Instance.StrokeManualSelectOptionTutorialActived &&
				Gameplay.Level.CurrentLayer.CurrentSurface != null)
			{
				switch (Gameplay.Level.CurrentLayer.CurrentSurface.StepIndex)
				{
					case 0:
						Gameplay.Instance.ShowOnlyOneOption();
						break;
					case 1:
						Gameplay.Instance.TutorialStrokeOption(1);
						Surface.PastryBag.Release(false);
						_canKeepGoing = false;
						_touched = false;
						_delay = 0;
						Profile.Instance.StrokeManualSelectOptionTutorialActived = true;
						break;
				}
			}

			Gameplay.Instance.UndoStep = Undo;
			int correctOption;
			for (correctOption = 0; correctOption < options.Length; correctOption++)
			{
				if (IsOptionCorrect(correctOption)) break;
			}
			if (CorrectOptionHightlight && _iSelected != correctOption)
			{
				Gameplay.Instance.ShowCorrectStrokeManualOptionFx(correctOption);
			}
		}

		internal void Clear()
		{
			Finished = false;
			_stroke = null;
			foreach (var s in _strokes)
			{
				Destroy(s.gameObject);
			}
			_strokes.Clear();
			_guideline.gameObject.SetActive(false);
		}

		private bool EqualOption(IReadOnlyCollection<int> a, IReadOnlyCollection<Color> b)
		{
			if (a.Count != b.Count || a.Count < 1 || b.Count < 1)
			{
				return false;
			}

			return a.SelectMany(t => options[Mathf.Clamp(t, 0, options.Length - 1)].colors).All(b.Contains);
		}


		private Vector2 _touchPos;
		private bool _touched;
		private bool _canKeepGoing;
		private const float VibrateRate = 0.2f;
		private float _vibrateTime;
		private bool _tutorialVisible;
		private bool _vfxPositiveActivated;

		private float _strokeLengthFactor, _maxSwipeLength, _frameTime;

		public override void DoFrame()
		{
			if (Input.GetMouseButtonDown(0) || _canKeepGoing)
			{
				TutorialSign.Instance.EndTutorial();
				if (!Utils.NonUIHold())
				{
					return;
				}
				Surface.PastryBag.Press();

				_touchPos = Input.mousePosition;
				_touched = true;
				_canKeepGoing = false;

				if (_tutorialVisible && Gameplay.Instance.Tutorial != null)
				{
					Gameplay.Instance.Tutorial.gameObject.SetActive(false);
					_tutorialVisible = false;
					Profile.Instance.TutorialActived[4] = true;
				}
			}

			if (Input.GetMouseButtonUp(0))
			{
				_touched = false;
				var surface = Surface;
				if (surface)
				{
					// Don't know why we have null exception here
					surface.PastryBag.Release(true);
				}

				_delay = 0;
				return;
			}

			Vector2 p = Input.mousePosition;
			if (!_touched)
			{
				return;
			}

			var swipeDirection = p - _touchPos;
			var swipeLength = 0f;
			if (swipeDirection.magnitude > 0)
			{
				_touchPos = p;
				swipeDirection.y *= 1.2f;
				swipeLength = swipeDirection.magnitude;
				var scale = 4.0f / Screen.width;
				swipeLength *= scale;
			}

			if (Profile.Instance.Level != 2)
			{
				Gameplay.Instance.ShowUndoButton();
			}

			if (Surface.PastryBag.Moving)
			{
				return;
			}

			if (_currentPosition.Offset > Poly.Length)
			{
				return;
			}

			_delay -= Time.deltaTime;
			if (_delay > 0)
			{
				return;
			}

			if (_stroke == null)
			{
				if (!Surface.PastryBag.Ready) return;
				CreateNewStroke();
			}

			_strokeLengthFactor = Mathf.Clamp(Poly.Length, Config.MIN_LENGTH_FACTOR, Config.MAX_LENGTH_FACTOR);
			_frameTime = 1 / Time.deltaTime;
			_maxSwipeLength = swipeLength > Config.DRAW_SPEED_POINT / _frameTime ? Config.MAX_DRAW_SPEED_2 / _frameTime : Config.MAX_DRAW_SPEED_1 / _frameTime;
			swipeLength = Mathf.Min(swipeLength * _strokeLengthFactor * Config.DRAW_SPEED_MULTIPLY, _maxSwipeLength);
			Advance(swipeLength);

			if (Time.realtimeSinceStartup - _vibrateTime > VibrateRate)
			{
				Utils.Instance.VibrateMedium();
				_vibrateTime = Time.realtimeSinceStartup;
			}

			if (_vfxPositiveActivated) return;
			if (!IsOptionCorrect(_iSelected)) return;
			var previousStep = Surface.Previous as StrokeManual;
			if (previousStep != null && previousStep.IsOptionCorrect(_iSelected)) return;
			Gameplay.Instance.PlayPositiveFX();
			_vfxPositiveActivated = true;
		}


		public override bool IsOptionActive(int index)
		{
			return _iSelected == index;
		}

		public override int OptionCount => options.Length;

		public override bool IsOptionCorrect(int index)
		{
			return sections[0].colors.Any(color => color == index);
		}

		private int _iSelected;

		private void OptionOn(int index)
		{
			if (index < 0 || index >= options.Length)
			{
				return;
			}

			if (IsOptionCorrect(index))
			{
				Gameplay.Instance.FocusTutorial.Hide();
				if (CorrectOptionHightlight)
				{
					Gameplay.Instance.HideCorrectStrokeManualOptionFx();
				}
			}

			CancelInvoke();
			StopAllCoroutines();
			Gameplay.Instance.HideTutorial();

			if (index != _iSelected)
			{
				_iSelected = index;
				_stroke = null;
			}
			if (_stroke == null)
			{
				CreateNewStroke();
			}
			RefreshTool();
			Surface.PastryBag.IndexOption = index;
		}

		public override void RefreshTool()
		{
			Surface.PastryBag.SetColor(options[_iSelected].colors[0], _stroke == null);
		}

		/// <summary>
		/// Finish this step
		/// </summary>
		private void Finish()
		{
			if (_userOptions.Any(userOption => userOption.Length > 1))
			{
				Analytics.Instance.LogEvent($"level_{Profile.Instance.Level}_twist_stroke");
			}

			SaveSectionData();
			Finished = true;
			_guideline.gameObject.SetActive(false);
			if (_guidelineExtend != null)
			{
				_guidelineExtend.gameObject.SetActive(false);
			}

			Gameplay.Instance.OptionOn = null;
			Gameplay.Instance.OptionOff = null;
			Surface.PastryBag.Finish(Gameplay.Instance.transform);
			_stroke.UnderGuideLine();
			Gameplay.Instance.CameraController.ZoomOut();
			if (CorrectOptionHightlight)
			{
				Gameplay.Instance.HideCorrectStrokeManualOptionFx();
			}
		}

		private void Advance(float delta)
		{
			_currentPosition = Poly.Advance(delta, _currentPosition);
			_stroke.Advance(delta);
			Surface.PastryBag.Move(_currentPosition.Pos, Surface.Radius);
			if (_currentPosition.Offset < Poly.Length) return;
			Utils.Instance.VibrateMedium();
			Finish();
		}

		private void CreateNewStroke()
		{
			_stroke = StrokeDisplay.Create(Display.transform);
			var specialActived =
				Config.Instance.BonusAndSpecialOption &&
				Gameplay.Level.SpecialStroke &&
				_iSelected == options.Length - 1;
			specialActived = specialActived || Gameplay.Level.GlitterStroke;

			var option = options[_iSelected];
			_stroke.Init(
				brush,
				Poly,
				_currentPosition,
				Surface.Radius,
				option.colors,
				option.GradientTexture,
				specialActived ? _specialMaterial : material);
			_stroke.OverGuideline();
			SaveSectionData();
			_strokes.Add(_stroke);
		}

		public override void CompleteFX(System.Action action)
		{
			Gameplay.Instance.PastryBag.CompleteFX(action);
		}

		public ColorOption[] Options
		{
			get => options;
			set => options = value;
		}
#if UNITY_EDITOR

		public void SetStrokeStepData(StrokeStepData data)
		{
			path = data.Path;
		}

		private void OnDrawGizmos()
		{
			if (Poly?.Points == null) return;
			Gizmos.color = Color.yellow;
			Handles.color = Handles.zAxisColor;
			Handles.ConeHandleCap(
				GetInstanceID(),
				_startPoint + transform.localPosition,
				Quaternion.LookRotation(_startDirection.normalized * 0.2f, Vector3.up),
				0.1f,
				UnityEngine.EventType.Repaint
			);
#if DEBUG_ENABLE || false
			for (int i = 0; i < Poly.Points.Count; i++)
			{
				UnityEditor.Handles.Label(new Vector3(Poly.Points[i].x, 1.2f, Poly.Points[i].y) , $"{i}");
			}
#endif
		}
#endif
	}
}