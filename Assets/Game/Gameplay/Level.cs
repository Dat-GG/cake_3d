using System.Linq;
using UnityEngine;
using DG.Tweening;

namespace Nama
{
	public class Level : MonoBehaviour
	{
		[SerializeField] private Cake[] layers;
		[SerializeField] private Color backgroundColor = Utils.ColorFromUint(0xF1E18E);
		[SerializeField] private bool glitterStroke;
		[SerializeField] private bool glitterCake;
		[SerializeField] private bool newPreview = true;

		[SerializeField] private float strokeRate = .5f;
		[SerializeField] private float sprinkleRate = .5f;
		[SerializeField] private Color diskColor = Utils.ColorFromUint(0xE2ACC7);
		[SerializeField] private CakeType type = CakeType.Origin;
		[SerializeField] private Transform previewCameraTransform;
		[SerializeField] private float previewCameraSize = 2.0f;
		[SerializeField] private Transform finishCameraTransform;
		[SerializeField] private float previewScale = 1.0f;
		[SerializeField] private bool specialStroke;
		[SerializeField] private bool specialFill;
		[SerializeField] private LevelStyle style = LevelStyle.Normal;
		public RenderTexture PreviewImage { get; set; }
		public Texture2D FreeStyleTexture;
		public Color BackgroundColor => backgroundColor;
		public Color DiskColor => diskColor;
		public bool HasKey { get; private set; }
		public bool GlitterStroke => glitterStroke;
		public bool SpecialStroke => specialStroke;
		public bool SpecialFill => specialFill;
		public float PreviewScale => previewScale;
		public float PreviewCameraSize => previewCameraSize;
		public Transform PreviewCameraTransform => previewCameraTransform;
		public bool Finished => _iLayer >= layers.Length;
		public float StrokeRate => strokeRate;

		public float SprinkleRate => sprinkleRate;
		public bool GlitterCake => glitterCake;
		
		public static float BonusScore = 0;

		private int _iLayer;
		public Cake CurrentLayer => layers.Length <= 0 && _iLayer < 0 || _iLayer >= layers.Length ?
			null : layers[_iLayer];

		public Cake[] Layers => layers;
		public int LayerIndex => _iLayer;

		private PutLayer _putLayer;

		public void Init(bool preview)
		{
			if (preview)
			{
				foreach (var layer in layers)
				{
					layer.Init(true, type);
					layer.gameObject.SetActive(true);
				}
			}
			else
			{
				CakeSurface.StepId = 1;
				foreach (var layer in layers)
				{
					layer.Init(false, type);
					layer.gameObject.SetActive(false);
					if (layer.HasKey) HasKey = true;
				}
				_iLayer = 0;
				layers[_iLayer].gameObject.SetActive(true);
			}
		}

		public void Begin()
		{
			layers[_iLayer].Begin();
		}

		public void DoFrame()
		{
			if (_iLayer < 0 || _iLayer >= layers.Length)
			{
				return;
			}

			if (_putLayer == null)
			{
				var layer = layers[_iLayer];
				layer.DoFrame();

				if (!layer.Finished)
				{
					return;
				}

				while (_iLayer < layers.Length && layers[_iLayer].Finished)
				{
					_iLayer++;
				}

				if (_iLayer >= layers.Length)
				{
					Switch(
						finishCameraTransform, Gameplay.Instance.LightRotation, 2.0f);
					Gameplay.Instance.HideUndoButton();
					SceneManager.Instance.CloseScene(SceneID.Home);

					DOVirtual.DelayedCall(2f, delegate
					{
						Gameplay.Instance.Win();
					});
				}
				else
				{
					_putLayer = new PutLayer(layers[_iLayer]);
				}
			}
			else
			{
				_putLayer.DoFrame();
				if (!_putLayer.Finished) return;
				var layer = _putLayer.Layer;
				_putLayer = null;
				layer.Begin();
			}
		}

		public float Match { get; private set; }

		public void CalculateMatch()
		{
			Match = BonusScore;
			var count = 0;
			foreach (var layer in layers)
			{
				if (!layer.CalculateMatch()) continue;
				count++;
				Match += layer.Match;
			}

			// Accept 1% difference as nothing
			Match = count > 0 ? Mathf.Clamp(Match / count / 0.99f, 0.0f, 1.0f) : 0.0f;
		}

		private static void Switch(Transform cameraTransform, Vector3 lightRotation, float duration = 1.0f)
		{
			if (Gameplay.Instance.isPhoto)
				return;
			Gameplay.Instance.CameraController.Move(cameraTransform, duration);
			Gameplay.Instance.Light.transform.DOLocalRotate(lightRotation, 0.5f).SetEase(Ease.InOutCubic);
		}

		public CakeType CakeType => type;
		public LevelStyle LevelStype => style;
		public void AddPhotoManual()
		{
			var layer = layers[layers.Length - 1];
			CakeSurface cakeSurface = null;
			for (int i = layer.Surfaces.Length-1; i >= 0; i--)
			{
				if (layer.Surfaces[i].gameObject.activeInHierarchy)
				{
					cakeSurface = layer.Surfaces[i];
				}
				break;
			}
			var cam = Instantiate(Gameplay.Instance.bigDecorConfig.DataConfig.camphotoManual, transform);
			var photomanual = Instantiate(Gameplay.Instance.bigDecorConfig.DataConfig.photoManual, cakeSurface.gameObject.transform);
			photomanual.GetComponent<PhotoManual>()._cameraTFX = cam;
		}
	}
}

public enum LevelStyle
{
	Normal,
	FreeStyle
}
