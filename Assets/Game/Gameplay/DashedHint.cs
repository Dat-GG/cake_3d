﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DashedHint : MonoBehaviour
{
    [SerializeField] LineRenderer lineRenderer;
	[SerializeField] Color normalColor;
	[SerializeField] Color brightColor;

	private Tween idleTween = null;

	private void Awake()
	{
		var spriteRenderer = GetComponent<SpriteRenderer>();
		lineRenderer.gameObject.SetActive(false);
		spriteRenderer.enabled = false;
	}

	public void CreateDasedHint(Texture2D drawTexture)
	{
		var spriteRenderer = GetComponent<SpriteRenderer>();
		drawTexture = RemoveBlack(drawTexture);
		var sprite = Sprite.Create(drawTexture, new Rect(0, 0, drawTexture.width, drawTexture.height), new Vector2(0.5f, 0.5f), 1024);
		spriteRenderer.sprite = sprite;
		var polygonCollider2D = gameObject.AddComponent<PolygonCollider2D>();
		Vector3[] points = new Vector3[polygonCollider2D.points.Length];
		for(int i =0; i < polygonCollider2D.points.Length;i++)
		{
			points[i] = polygonCollider2D.points[i];
		}
		lineRenderer.positionCount = points.Length;
		lineRenderer.SetPositions(points);

		lineRenderer.gameObject.SetActive(true);
		polygonCollider2D.enabled = false;
		spriteRenderer.enabled = false;

		lineRenderer.sharedMaterial.color = normalColor;
		StartIdle();
	}

	private void Update()
	{
		if(Input.GetMouseButtonDown(0))
		{
			StopIdle();
		}

		if (Input.GetMouseButtonUp(0))
		{
			StartIdle();
		}
	}

	void StartIdle()
	{
		lineRenderer.sharedMaterial.DOKill();
		var lightColor = Color.Lerp(normalColor, Color.white, 0.5f);
		var darkColor = Color.Lerp(normalColor, Color.black, 0.2f);
		var delayTime = 0.5f;
		if(idleTween != null)
		{
			idleTween.Kill();
		}
		idleTween = DOVirtual.DelayedCall(4f, delegate
		{
			if(lineRenderer != null)
			{
				lineRenderer.sharedMaterial.DOColor(lightColor, delayTime);
				lineRenderer.sharedMaterial.DOColor(normalColor, delayTime).SetDelay(delayTime);
				lineRenderer.sharedMaterial.DOColor(darkColor, delayTime).SetDelay(delayTime * 2);
				lineRenderer.sharedMaterial.DOColor(normalColor, delayTime).SetDelay(delayTime * 3);
			} else
			{
				if (idleTween != null)
				{
					idleTween.Kill();
				}
			}
		}).SetLoops(-1);
	}

	void StopIdle()
	{
		if (idleTween != null)
		{
			idleTween.Kill();
		}
	}

	Texture2D RemoveBlack(Texture2D myTex)
	{
		var newTex = new Texture2D(myTex.width, myTex.height);
		var colors = myTex.GetPixels();
		for (int i = 0; i < colors.Length; i++)
			if (colors[i].r < 1)
				colors[i].a = 0;
		newTex.SetPixels(colors);
		newTex.Apply();
		return newTex;
	}
}
