using UnityEngine;
using System.IO;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

namespace Nama
{
	public class GameplayScene : Scene
	{
		[SerializeField] Gameplay gameplay;
		[SerializeField] Button abButton;
		[SerializeField] Text abText;

		static string[] abTexts;
		private void Awake()
		{
			abTexts = new string[Config.EXPERIMENT_COUNT];
			for (var i = 0; i < Config.EXPERIMENT_COUNT; i++)
			{
				abTexts[i] = i.ToString();
			}

			abText.text = abTexts[Config.Instance.Experiment];
			abButton.onClick.AddListener(() =>
			{
				Config.Instance.Experiment = (Config.Instance.Experiment + 1) % Config.EXPERIMENT_COUNT;
				AssetManager.ReloadLevelList();
				PlayerPrefs.SetInt(Config.EXPERIMENT_NAME, Config.Instance.Experiment);
				PlayerPrefs.Save();
				abText.text = abTexts[Config.Instance.Experiment];
			});
		}

		public override void Init(object data)
		{

		}

		public override void OnBackButtonPressed()
		{
			SceneManager.Instance.OpenPopup(SceneID.Exit);
		}

		private void Start()
		{
#if UNITY_EDITOR && false
			AutoScreenShot();
#endif
		}

		private void AutoScreenShot()
		{
			if (Profile.Instance.Level > Nama.AssetManager.LevelList.Count + 1) return;
			Debug.LogError(Profile.Instance.Level);
			if (Profile.Instance.Level == 1)
			{
				DOVirtual.DelayedCall(.5f, delegate
				{
					StartCoroutine(AutoScreenshot());
				});
			}
			else
			{
				StartCoroutine(AutoScreenshot());
			}

		}

		public void Restart(int i)
		{
			Profile.Instance.Level += i;
			SceneManager.Instance.ReloadScene(SceneID.Gameplay);
			Gameplay.ResetOwnedOption();
		}

		public void Skip()
		{
			Analytics.Instance.LogEvent(string.Format("level_{0}_skip", Profile.Instance.Level));
			Ads.SendRvClicked("gameplay_skip");
			Ads.ShowRewardedVideo("gameplay_skip", (result) =>
			{
				if (result != RewardedVideoState.Watched)
				{
					return;
				}
				Ads.SendRvWatched("gameplay_skip");
				Next();
				
			});
		}

		public void Next()
		{
			SceneManager.Instance.ReloadScene(SceneID.Gameplay);
			Gameplay.ResetOwnedOption();
		}

		public void Prev()
		{
			SceneManager.Instance.ReloadScene(SceneID.Gameplay);
			Gameplay.ResetOwnedOption();
		}

		public void TakeScreenShot()
		{
#if UNITY_EDITOR
			try
			{
				Directory.CreateDirectory("Preview");
			}
			catch { }
			ScreenCapture.CaptureScreenshot(string.Format("Preview/{0}.{1}.png", Profile.Instance.Level, Gameplay.Level.name));
#endif
		}

		int resWidth = 1920;
		int resHeight = 1080;

		private bool takeHiResShot = false;

		public static string ScreenShotName(int width, int height)
		{
			return string.Format("{0}/screenshots/screen_{1}x{2}_{3}.png",
								 Application.dataPath,
								 width, height,
								 System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
		}

		[SerializeField] Camera camera;

		private void LateUpdate()
		{
#if UNITY_EDITOR
			takeHiResShot |= Input.GetKeyDown(KeyCode.F9);
			if (!takeHiResShot) return;
			resWidth = camera.pixelWidth;
			resHeight = camera.pixelHeight;
			var rt = new RenderTexture(resWidth, resHeight, 24);
			camera.targetTexture = rt;
			var screenShot = new Texture2D(resWidth, resHeight, TextureFormat.ARGB32, false);
			camera.Render();
			RenderTexture.active = rt;
			screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
			camera.targetTexture = null;
			RenderTexture.active = null; // JC: added to avoid errors
			Destroy(rt);
			var bytes = screenShot.EncodeToPNG();
			File.WriteAllBytes("Preview/" + Gameplay.Level.name + ".png", bytes);
			Debug.Log(string.Format("Took screenshot"));
			takeHiResShot = false;
#endif
		}

		private IEnumerator AutoScreenshot()
		{
			yield return 0;
			TakeScreenShot();
			Profile.Instance.Level++;
			SceneManager.Instance.ReloadScene(SceneID.Gameplay);
		}
	}

}
