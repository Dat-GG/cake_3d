using System.Collections.Generic;
using UnityEngine;

namespace Nama
{
	/// <summary>
	/// Information of a position on a polyline
	/// </summary>
	public struct PolylinePosition
	{
		/// <summary>
		/// Index of the segment
		/// </summary>
		public int Seg;

		/// <summary>
		/// Distance along the segment
		/// </summary>
		public float SegOffset;

		/// <summary>
		/// Distance along the polyline from its begin
		/// </summary>
		public float Offset;

		/// <summary>
		/// Specific 2D position in local space
		/// </summary>
		public Vector2 Pos;
	}

	/// <summary>
	/// Information of a section along a polyline
	/// </summary>
	public struct PolylineSection
	{
		/// <summary>
		/// The start position
		/// </summary>
		public PolylinePosition Start;

		/// <summary>
		/// Distance along the polyline from the its begin
		/// </summary>
		public float Finish;
	}

	/// <summary>
	/// Polyline
	/// </summary>
	public class Polyline
	{
		public float Length { get; private set; }
		public List<Vector2> Points { get; } = new List<Vector2>();

		public Polyline() { }

		public Polyline(Path path, Transform transformation)
		{
			if (path == null)
			{
				return;
			}
			Points = path.GeneratePolyline(transformation);
			Length = 0;
			Points[0] = new Vector3(Points[0].x, Points[0].y, 0);
			for (var i = 1; i < Points.Count; i++)
			{ 
				Length += Vector2.Distance(Points[i], Points[i - 1]);
				Points[i] = new Vector3(Points[i].x, Points[i].y, Length);
			}
		}

		/// <summary>
		///  Get the first point, use it care fully
		/// </summary>
		public Vector3 FirstPoint => Points[0];

		/// <summary>
		/// Gets the last point, use it carefully
		/// </summary>
		public Vector3 LastPoint => Points[Points.Count - 1];

		public PolylinePosition StartPosition
		{
			get
			{
				PolylinePosition p;
				p.Seg = 0;
				p.SegOffset = 0;
				if (Points != null && Points.Count > 0)
				{
					p.Pos = Points[0];
				}
				else
				{
					p.Pos = Vector2.zero;
				}
				p.Offset = 0;
				return p;
			}
		}

		public PolylinePosition LastPosition
		{
			get
			{
				PolylinePosition p;
				p.Seg = Points.Count - 1;
				p.SegOffset = 0;
				if (Points != null && Points.Count > 0)
				{
					p.Pos = Points[Points.Count - 1];
				}
				else
				{
					p.Pos = Vector2.zero;
				}
				p.Offset = Length;
				return p;
			}
		}

		public PolylinePosition Advance(float delta, PolylinePosition position)
		{
			var result = position;
			result.Offset += delta;

			var nSegments = Points.Count - 1;
			if (result.Offset >= Length)
			{
				result.Seg = nSegments;
				result.SegOffset = 0;
				result.Pos = Points[nSegments];
				result.Offset = Length;
			}

			for (; result.Seg < nSegments && delta > 0;)
			{
				var segDirection = Points[result.Seg + 1] - Points[result.Seg];
				var segLength = segDirection.magnitude;
				segDirection /= segLength;

				var segRemainLength = segLength - result.SegOffset;
				if (delta > segRemainLength)
				{
					delta -= segRemainLength;
					result.Pos = Points[result.Seg + 1];
					result.SegOffset = 0;
					result.Seg++;
					if (result.Seg >= nSegments)
					{
						break;
					}
				}
				else
				{
					result.Pos.x += delta * segDirection.x;
					result.Pos.y += delta * segDirection.y;
					result.SegOffset += delta;
					break;
				}
			}
			return result;
		}

		public void AddPoint(Vector3 point)
		{
			Points.Add(point);
			var n = Points.Count - 2;
			if (n >= 0)
			{
				Length += Vector2.Distance(Points[n], Points[n + 1]);
			}
		}

		public void Clear()
		{
			Points.Clear();
			Length = 0;
		}

		public void Distribute(IPolylineSectionDistribution distribution)
		{
			var nSections = distribution.SectionCount;
			float w = 0;
			for (var i = 0; i < nSections; i++)
			{
				w += distribution.GetSection(i).Weight;
			}

			var start = StartPosition;
			for (var i = 0; i < nSections; i++)
			{
				var section = distribution.GetSection(i);
				var portion = w > 0 ? section.Weight / w : 1.0f / nSections;
				var sectionLength = Length * portion;

				var finish = Advance(sectionLength, start);
				section.Init(start, finish, sectionLength);
				start = finish;
			} // for
		}
	}
}
