using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Funzilla;
using Tabtale.TTPlugins;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

namespace Nama
{
	public class Gameplay : MonoBehaviour
	{
		[SerializeField] private Level levelTest;

		[Space] [Header("Gameplay")] [SerializeField]
		private ParticleEmitter[] emitters;

		[SerializeField] private Button completeButton;
		[SerializeField] private Button undoButton;
		[SerializeField] private GameObject optionHub;
		[SerializeField] private Image optionsBG;
		[SerializeField] private Transform pastrybagParent;
		public Transform PastrybagParent => pastrybagParent;
		[SerializeField] private Transform carafeParent;
		public Transform CarafeParent => carafeParent;
		[SerializeField] private Transform bottleParent;
		public Transform BottleParent => bottleParent;
		[SerializeField] private new Camera camera;
		[SerializeField] private CameraController cameraController;
		[SerializeField] private GameObject winFx;
		[SerializeField] private Text levelName;
		[SerializeField] private new Light light;
		[SerializeField] private Vector3 lightRotation;
		[SerializeField] private PastryBag pastryBag;
		[SerializeField] private Carafe carafe;
		[SerializeField] private StencilTool stencilTool;
		[SerializeField] private Renderer table;
		[SerializeField] private Renderer disk;
		[SerializeField] private GameObject ui;
		[SerializeField] private KeyGroup keyGroup;

		[SerializeField] private RawImage previewImage;
		[SerializeField] private Camera previewCamera;
		[SerializeField] private Camera resultCamera;
		[SerializeField] private Camera sprinklesPreviewCamera;
		[SerializeField] private Camera miniDecorPreviewCamera;
		[SerializeField] private Camera selectSurfacePreviewCamera;
		[SerializeField] private Transform sideCameraTransform;
		[SerializeField] private Transform sideLightTransform;
		[SerializeField] private GameObject putLayerTutorial;
		[SerializeField] private GameObject background;
		public GameObject PutLayerTutorial => putLayerTutorial;
		
		private RectTransform _previewRT;
		private Vector2 _previewPosition;
		public static RenderTexture CorrectTexture { get; set; }
		public RenderTexture ResultTexture { get; private set; }
		private ShadowQuality _shadowQuality = ShadowQuality.All;
		public Tutorial Tutorial { get; private set; }
		[SerializeField] private CanvasGroup boxProgress;

		[Space] [Header("Tutorial")] [SerializeField]
		private List<Tutorial> listTut;

		[SerializeField] private Transform textSuggest;
		private GameObject _hand1;
		private GameObject _hand2;
		private const string TutorialHide = "hide";
		public Action ONOptionsSetup;
		private Sequence _anim;
		[SerializeField] private FocusTutorial focusTutorial;
		public FocusTutorial FocusTutorial => focusTutorial;

		[Space] [Header("Sprinkle")] [SerializeField]
		private Transform bottleTrans;

		[SerializeField] private Pointer pointer;
		[SerializeField] private GameObject bottleCompleteFX;

		[Header("Level Selection")] [SerializeField]
		private LevelSelect levelSelect;

		[SerializeField] private Button showLevelSelect;

		public Pointer Pointer => pointer;
		public Bottle Bottle { get; private set; }
		public int SpecialCoinEarned { get; private set; }

		public Camera Camera => camera;
		public Light Light => light;
		public Vector3 LightRotation => lightRotation;
		public CameraController CameraController => cameraController;
		public static Level Level { get; set; }
		public static Level PreviewLevel { get; set; }
		public PastryBag PastryBag => pastryBag;
		public Carafe Carafe => carafe;
		public StencilTool StencilTool => stencilTool;
		public GameObject PreviewImage => previewImage.transform.parent.gameObject;
		public int KeyAmount { get; set; }

		private readonly GameStateManager _stateManager = new GameStateManager();
		private readonly InitState _initState = new InitState();
		private readonly PlayState _playState = new PlayState();
		private readonly WinState _winState = new WinState();
		private NavMeshSurface _navMeshSurface;
		[SerializeField] private GameObject choseOne;
		private enum PlayType
		{
			StrokeAuto,
			SprinkleAuto,
			FillManual,
			SprinkleManual,
			StrokeManual,
			BigDecorManual,
			StencilManual,
			MiniDecorManual,
			PhotoManual,
			MirrorGlaze,
			SurfaceSelect
		}

		private const int ToolCount = 11;

		private PlayType _currentType = PlayType.SprinkleAuto;

		private void Play()
		{
			if (Playing || Finished)
			{
				return;
			}

			UIGlobal.HideCoinInfo();
			_stateManager.ChangeState(_playState);
			KeyGroup.gameObject.SetActive(Level.HasKey && Statistics.Instance.KeyCount > 0);
			KeyAmount = 0;
			keyGroup.UpdateKey(false);
			Level.Begin();
			if (Level.LevelStype == LevelStyle.FreeStyle)
			{
				ShowBoxLevelFreeProgress();
			}
			else
			{
				HideBoxLevelFreeProgress();
			}
		}

		public void PlayPositiveFX()
		{
			foreach (var e in emitters)
			{
				e.Emit();
			}
		}

		private float _startTime;

		public void Win()
		{
			var timePlayed = Time.realtimeSinceStartup - _startTime;
			Analytics.Instance.LogEvent(
				$"level_{Profile.Instance.Level}_complete",
				"duration", timePlayed);
			Ads.SendMissionCompleted();
			foreach (var s in GetComponentsInChildren<SprinkleObject>())
			{
				s.Rigidbody.isKinematic = true;
			}

			boxProgress.gameObject.SetActive(true);
			boxProgress.DOFade(1, 0.3f);

			Level.CalculateMatch();
			Level.transform.SetParent(AssetManager.Instance.transform);

			var playIndex = Profile.Instance.PlayCount + 1;
			Analytics.Instance.LogEvent($"gameplay_{playIndex}_complete");
			Analytics.Instance.LogEvent($"level_{Profile.Instance.Level}_complete", "duration",
				timePlayed);

			string time;
			if (timePlayed < 20) time = "20";
			else if (timePlayed < 40) time = "2040";
			else if (timePlayed < 60) time = "4060";
			else if (timePlayed < 99) time = "6099";
			else time = "100";

			Analytics.Instance.LogEvent($"gameplay_{playIndex}_complete_time_{time}");
			Analytics.Instance.LogEvent($"level_{Profile.Instance.Level}_complete_time_{time}");


			_stateManager.ChangeState(_winState);
			KeyGroup.gameObject.SetActive(false);
			PastryBag.Finish(transform);

			if (Tutorial != null)
				HideTutorial();

			optionHub.SetActive(false);
		}

		public void GetTextureForResult()
		{
			pastryBag.gameObject.SetActive(false);
			var objects = Level.GetComponentsInChildren<SprinkleObject>();
			foreach (var obj in objects)
			{
				if (obj.OnTable) DestroyImmediate(obj.gameObject);
			}

			SetupForPreview(Level, previewCamera, table);
			ResultTexture = RenderTexture2D(previewCamera, 512, Level.PreviewCameraTransform);
			ResetAfterPreview(Level, table);
			Level.gameObject.SetLayerRecursively(Layers.Background);
		}

		private static ShadowQuality _saveShadowQuality;

		public static void SetupForPreview(Level level, Camera camera, Renderer table)
		{
			level.gameObject.SetLayerRecursively(Layers.SpinklesPreview);
			camera.orthographicSize = level.PreviewCameraSize + 0.2f;
			camera.backgroundColor = level.DiskColor;
			table.material.color = level.DiskColor;
			table.gameObject.layer = Layers.SpinklesPreview;
			table.transform.localPosition = Vector3.zero;
			_saveShadowQuality = QualitySettings.shadows;
			QualitySettings.shadows = ShadowQuality.Disable;
		}

		public static void ResetAfterPreview(Level level, Renderer table)
		{
			QualitySettings.shadows = _saveShadowQuality;
			table.material.color = level.BackgroundColor;
			table.gameObject.layer = Layers.Background;
			table.transform.localPosition = new Vector3(0, -0.25f, 0);
		}

		public bool Playing => _stateManager.CurrentState == _playState;
		private bool Finished => _stateManager.CurrentState == _winState;
		public GameObject FlowTool { set; get; }
		public float StepWidth { set; get; } = 1;
		public bool StrokeManualDecorating { set; get; }
		public float DistanceLastStroke { set; get; }
		public bool CameraFlowEnable { get; private set; } = true;

		public void InitPreview()
		{
			_shadowQuality = QualitySettings.shadows;
			Level prefab;

#if UNITY_EDITOR
			var testLevel = PlayerPrefs.GetString("TestLevel");
			if (!string.IsNullOrEmpty(testLevel))
			{
				prefab = AssetDatabase.LoadAssetAtPath<Level>($"{testLevel}");
				PlayerPrefs.SetString("TestLevel", string.Empty);
			}
			else
			{
				prefab = levelTest != null ? levelTest : AssetManager.GetLevel();
			}
#else
			prefab = AssetManager.GetLevel();
#endif
			if (CorrectTexture != null)
			{
				previewImage.texture = CorrectTexture;
				Init(prefab);
				return;
			}

			StartCoroutine(PreviewCake(prefab, (level, texture) =>
			{
				previewImage.texture = CorrectTexture = texture;
				Init(prefab);
			}));
		}

		private void Init(Level prefab)
		{
#if UNITY_EDITOR
			if (!GameManager.Initialized)
			{
				GameManager.Initialized = true;
				TTPCore.Setup();
			}
#endif
			_startTime = Time.realtimeSinceStartup;
			Analytics.Instance.LogEvent($"level_{Profile.Instance.Level}_start");
			if (Profile.Instance.Level == 20)
			{
				Debug.Log("#####Rateus");
				TTPRateUs.Popup();
			}
			SceneManager.Instance.HideLoading();

			ReloadToolSkins();
#if PREVIEW
			PastryBag.gameObject.SetActive(true);
#endif
			QualitySettings.shadowDistance = 20f;
			QualitySettings.shadows = _shadowQuality;
			Level = Instantiate(prefab, transform);
			Level.AddPhotoManual();
			Level.gameObject.name = prefab.name;
			Level.Init(false);
			Ads.SendMissionStart();
			table.material.color = Level.BackgroundColor;
			disk.material.color = Level.DiskColor;
			levelName.text = Profile.Instance.Level + "\n" + prefab.name;
			PastryBag.gameObject.SetActive(true);

			previewImage.transform.parent.localScale = Level.PreviewScale * Vector3.one;

			CameraFlowEnable =
				!Level.name.Contains("fill_tra_011") && !Level.name.Contains("hai_006");

			ShowBoxProgress();

			KeyGroup.gameObject.SetActive(false);

			//  tool tip 
			foreach (var tut in listTut)
			{
				tut.gameObject.SetActive(false);
			}

			_previewRT = previewImage.transform.parent.GetComponent<RectTransform>();
			_previewPosition = _previewRT.localPosition;

			// play
			_strokePreviewId = 0;
			UIGlobal.HideCoinInfo();
			Play();
		}


		private static int _strokePreviewId;
		private const float ZoomPreviewTime = 0.5f;

		public void OnClickPreview()
		{
			var opened = _previewRT.localScale.x > 1;
			if (opened)
			{
				_previewRT.DOLocalMove(_previewPosition, ZoomPreviewTime);
				_previewRT.DOScale(1f, ZoomPreviewTime + 0.1f).OnComplete(delegate
				{
					_previewRT.transform.GetChild(1).gameObject.SetActive(true);
				});
			}
			else
			{
				_previewRT.DOLocalMove(new Vector2(0, 300), ZoomPreviewTime);
				_previewRT.DOScale(2.5f, ZoomPreviewTime + 0.1f);
				_previewRT.transform.GetChild(1).gameObject.SetActive(false);
			}
		}

		private void LoadBottleSkin()
		{
			Pointer.transform.SetParent(transform);
			Pointer.gameObject.SetActive(false);
			if (Bottle)
			{
				Destroy(Bottle.gameObject);
			}

			var bottleName = Profile.Instance.SprinkleBottle.name;
			if (string.IsNullOrEmpty(bottleName))
			{
				bottleName = "jar_bottle";
			}

			if (bottleTrans.childCount > 0)
			{
				for (var i = 0; i < bottleTrans.childCount; i++)
				{
					Destroy(bottleTrans.GetChild(i).gameObject);
				}
			}

			Bottle = AssetManager.GetBottle(bottleName);
			Bottle.gameObject.SetActive(false);
			Bottle.SetCompleteFX(bottleCompleteFX);
			Bottle.transform.SetParent(transform);
		}

		public void HideUI()
		{
			ui.SetActive(false);
		}

		private IEnumerator PreviewCake(Level prefab, Action<Level, RenderTexture> onComplete)
		{
			var level = Instantiate(prefab, AssetManager.Instance.transform);
			level.Init(true);
			PreviewLevel = level;
			QualitySettings.shadows = ShadowQuality.Disable;
			PastryBag.gameObject.SetActive(false);
			yield return new WaitForEndOfFrame();
			SetupForPreview(level, previewCamera, table);
			var texture = RenderTexture2D(previewCamera, 256, level.PreviewCameraTransform);
			ResetAfterPreview(level, table);
			level.gameObject.SetActive(false);
			onComplete?.Invoke(level, texture);
		}

		public static RenderTexture RenderTexture2D(Camera previewCamera, int size,
			Transform transform = null)
		{
			if (transform != null)
			{
				var t = previewCamera.transform;
				t.position = transform.position + new Vector3(0, 0, 0.1f);
				t.rotation = transform.rotation;
			}

			// Render
			previewCamera.gameObject.SetActive(true);
			var texture = new RenderTexture(
				size,
				size,
				24,
				RenderTextureFormat.Default,
				RenderTextureReadWrite.Default)
			{
				wrapMode = TextureWrapMode.Repeat,
				filterMode = FilterMode.Bilinear,
				autoGenerateMips = false,
				useMipMap = false
			};
			previewCamera.targetTexture = texture;
			previewCamera.Render();
			previewCamera.targetTexture = null;
			previewCamera.gameObject.SetActive(false);
			return texture;
		}

		public void ShowWinFx()
		{
			PastryBag.Release(false);
			winFx.gameObject.SetActive(false);
			DOVirtual.DelayedCall(0.5f, delegate
			{
				winFx.gameObject.SetActive(true);
				winFx.transform.localPosition = new Vector3(0, 1.2f, 0);
			});
		}

		public void HideWinFx()
		{
			winFx.transform.localPosition = new Vector3(0, -10, 0);
		}

		private void Start()
		{
			_stateManager.ChangeState(_initState);
			levelSelect.gameObject.SetActive(false);
			showLevelSelect.gameObject.SetActive(false);
			showLevelSelect.onClick.AddListener(() =>
			{
				showLevelSelect.gameObject.SetActive(false);
				levelSelect.gameObject.SetActive(true);
			});
		}

		public static Gameplay Instance { get; private set; }

		public KeyGroup KeyGroup
		{
			get => keyGroup;
			set => keyGroup = value;
		}

		private void Awake()
		{
			Instance = this;
		}

		private void OnDestroy()
		{
			Instance = null;
		}

		private void Update()
		{
			_stateManager.Update();

			if (Input.GetKeyDown(KeyCode.Alpha1))
			{
				OptionOn?.Invoke(0);
			}

			if (Input.GetKeyDown(KeyCode.Alpha2))
			{
				OptionOn?.Invoke(1);
			}

			if (Input.GetKeyDown(KeyCode.Alpha3))
			{
				OptionOn?.Invoke(2);
			}

			if (Input.GetKeyDown(KeyCode.Alpha4))
			{
				OptionOn?.Invoke(3);
			}

			if (Input.GetKeyDown(KeyCode.Alpha4))
			{
				OptionOn?.Invoke(4);
			}

			if (Input.GetKeyUp(KeyCode.Alpha1))
			{
				OptionOff?.Invoke(0);
			}

			if (Input.GetKeyUp(KeyCode.Alpha2))
			{
				OptionOff?.Invoke(1);
			}

			if (Input.GetKeyUp(KeyCode.Alpha3))
			{
				OptionOff?.Invoke(2);
			}

			if (Input.GetKeyUp(KeyCode.Alpha4))
			{
				OptionOff?.Invoke(3);
			}

			if (Input.GetKeyUp(KeyCode.Alpha5))
			{
				OptionOff?.Invoke(4);
			}

			if (Input.GetKeyDown(KeyCode.H))
			{
				SelectManualOption(0);
			}

			if (Input.GetKeyDown(KeyCode.J))
			{
				SelectManualOption(1);
			}

			if (Input.GetKeyDown(KeyCode.K))
			{
				SelectManualOption(2);
			}
		}

		public Action FinishStep;
		public Action UndoStep;
		public Action<int> OnOptionSelected;
		private static List<string> _ownedSprinkles = new List<string>(2);

		string GetSprinkleOptionId(int index)
		{
			return $"{levelName}_{Level.CurrentLayer.CurrentSurface.StepIndex}_{index}";
		}

		void UnlockSprinkle(int index)
		{
			_ownedSprinkles.Add(GetSprinkleOptionId(index));
		}

		public bool OwnedSprinkle(int index)
		{
			return _ownedSprinkles.Contains(GetSprinkleOptionId(index));
		}

		bool wathchedSprinklesReward;
		public void SelectManualOption(int index)
		{
			var f1 = optionHub.transform.GetChild((int) _currentType);
			// Special option
			if (Level.LevelStype != LevelStyle.FreeStyle)
			{
				if (Config.Instance.BonusAndSpecialOption &&
				    _sprinklesOptions[index].Special &&
				    !OwnedSprinkle(index))
				{
					Ads.SendRvClicked("special_sprinkle");
					Ads.ShowRewardedVideo("special_sprinkle", state =>
					{
						if (state != RewardedVideoState.Watched) return;
						Ads.SendRvWatched("special_sprinkle");
						UnlockSprinkle(index);
						f1.GetChild(index).GetChild(2).gameObject.SetActive(false);
						f1.GetChild(index).GetComponent<Animation>().enabled = false;
						f1.GetChild(index).GetChild(3).gameObject.SetActive(true);
						SpecialCoinEarned += Config.SPECIAL_COIN_EANRED;
						var sprinkleManual =
							Level.CurrentLayer.CurrentSurface.CurrentStep as SprinkleManual;
						if (sprinkleManual != null)
						{
							Analytics.Instance.LogEvent(sprinkleManual.Bonus
								? $"level_{Profile.Instance.Level}_rw_deco"
								: $"level_{Profile.Instance.Level}_rw_sprinkle");
						}

						Analytics.Instance.LogEvent($"level_{Profile.Instance.Level}_rw_playing");

						OnOptionSelected?.Invoke(index);
						for (var i = 0; i < 5; i++)
						{
							f1.GetChild(i).GetChild(1).gameObject.SetActive(i == index);
						}
					});
				}

				Play();
				OnOptionSelected?.Invoke(index);
				for (var i = 0; i < 5; i++)
				{
					f1.GetChild(i).GetChild(1).gameObject.SetActive(i == index);
				}
			}
			else
			{
				if (index == optionAds)
				{
					Ads.ShowRewardedVideo("sprinkles_optionAds", (result) =>
					{
						if (result != RewardedVideoState.Watched)
						{
							return;
						}
						f1.GetChild(index).GetChild(4).gameObject.SetActive(false);
						OnOptionSelected?.Invoke(index);
						optionAds = -1;
						for (var i = 0; i < 5; i++)
						{
							f1.GetChild(i).GetChild(1).gameObject.SetActive(i == index);
						}
						wathchedSprinklesReward = true;
					});
				}
				else
				{
					OnOptionSelected?.Invoke(index);
					for (var i = 0; i < 5; i++)
					{
						f1.GetChild(i).GetChild(1).gameObject.SetActive(i == index);
					}
				}
				Play();
			}
		}

		private bool wathchedFillManualReward;
		public void SelectFillManualOption(int index)
		{
			var f1 = optionHub.transform.GetChild((int) _currentType);
			// Special option
			if (Level.LevelStype != LevelStyle.FreeStyle)
			{
				if (Config.Instance.BonusAndSpecialOption &&
				    Level.SpecialFill &&
				    index == _fillOptions.Count - 1 &&
				    !_specialFillUnlocked)
				{
					Ads.SendRvClicked("special_fill");
					Ads.ShowRewardedVideo("special_fill", state =>
					{
						if (state != RewardedVideoState.Watched) return;
						Ads.SendRvWatched("special_fill");
						f1.GetChild(index).GetChild(2).gameObject.SetActive(false);
						f1.GetChild(index).GetChild(3).gameObject.SetActive(true);
						f1.GetChild(index).GetChild(4).gameObject.SetActive(true);
						SpecialCoinEarned += Config.SPECIAL_COIN_EANRED;
						Analytics.Instance.LogEvent($"level_{Profile.Instance.Level}_rw_fill");
						Analytics.Instance.LogEvent($"level_{Profile.Instance.Level}_rw_playing");

						OnOptionSelected?.Invoke(index);

						for (var i = 0; i < 5; i++)
						{
							var f2 = f1.GetChild(i);
							f2.GetChild(1).GetChild(0).gameObject.SetActive(i == index);
						}
					});
				}

				Play();
				OnOptionSelected?.Invoke(index);

				for (var i = 0; i < 5; i++)
				{
					var f2 = f1.GetChild(i);
					f2.GetChild(1).GetChild(0).gameObject.SetActive(i == index);
				}
			}
			else
			{
				if (index == optionAds)
				{
					Ads.ShowRewardedVideo("fillmanual_optionAds", (result) =>
					{
						if (result != RewardedVideoState.Watched)
						{
							return;
						}
						f1.GetChild(index).GetChild(5).gameObject.SetActive(false);
						OnOptionSelected?.Invoke(index);
						optionAds = -1;
						for (var i = 0; i < 5; i++)
						{
							f1.GetChild(i).GetChild(1).GetChild(0).gameObject.SetActive(i == index);
						}
						wathchedFillManualReward = true;
					});
				}
				else
				{
					OnOptionSelected?.Invoke(index);
					for (var i = 0; i < 5; i++)
					{
						f1.GetChild(i).GetChild(1).GetChild(0).gameObject.SetActive(i == index);
					}
				}
				Play();
			}
		}

		public void FinishManual()
		{
			HideUndoButton();
			FinishStep?.Invoke();
		}

		public void Undo()
		{
			Level.CurrentLayer.CurrentSurface.Undo();
		}

		public Action<int> OptionOn;
		public Action<int> OptionOff;

		public void StartAuto(int index)
		{
			var f1 = optionHub.transform.GetChild((int) _currentType);

			// Special option
			if (Config.Instance.BonusAndSpecialOption &&
			    Level.SpecialStroke &&
			    index == _colors.Length - 1 &&
			    !_specialColorUnlocked)
			{
				Ads.SendRvClicked("special_color");
				Ads.ShowRewardedVideo("special_color", state =>
				{
					if (state == RewardedVideoState.Watched)
					{
						Ads.SendRvWatched("special_color");
						f1.GetChild(index).GetChild(2).gameObject.SetActive(false);
						f1.GetChild(index).GetChild(3).gameObject.SetActive(true);
						f1.GetChild(index).GetChild(4).gameObject.SetActive(true);
						SpecialCoinEarned += Config.SPECIAL_COIN_EANRED;
						Analytics.Instance.LogEvent($"level_{Profile.Instance.Level}_rw_stroke");
						Analytics.Instance.LogEvent($"level_{Profile.Instance.Level}_rw_playing");
					}
					else
					{
						StopAuto(index);
					}
				});
			}

			if (Tutorial != null)
				Tutorial.Hide();

			Play();
			OptionOn?.Invoke(index);
			f1.GetChild(index).GetChild(0).gameObject.SetActive(false);
			f1.GetChild(index).GetChild(1).gameObject.SetActive(true);
		}

		private void StopAuto(int index)
		{
			Play();
			OptionOff?.Invoke(index);
			var f1 = optionHub.transform.GetChild((int) _currentType);
			f1.GetChild(index).GetChild(0).gameObject.SetActive(true);
			f1.GetChild(index).GetChild(1).gameObject.SetActive(false);
		}

		private Transform GetOptionType(PlayType type)
		{
			return optionHub.transform.GetChild((int) _currentType);
		}

		private Transform SetCurrentType(PlayType type)
		{
			_currentType = type;
			if (_currentType == PlayType.MiniDecorManual)
			{
				optionsBG.gameObject.SetActive(false);
			}

			for (var i = 0; i < ToolCount; i++)
			{
				optionHub.transform.GetChild(i).gameObject.SetActive(i == (int) type);
			}

			return optionHub.transform.GetChild((int) _currentType);
		}

		public void HideOptionsUI()
		{
			HideOptionsBackground();
			for (var i = 0; i < ToolCount; i++)
			{
				optionHub.transform.GetChild(i).gameObject.SetActive(false);
			}
		}

		private List<int> GetTutorialOptionIndex()
		{
			var list = new List<int>(5);
			var step = Level.CurrentLayer.CurrentSurface.CurrentStep;
			for (var i = 0; i < step.OptionCount; i++)
			{
				if (step.IsOptionCorrect(i))
					list.Add(i);
			}

			return list;
		}

		private Color[] _colors;
		private static bool _specialColorUnlocked;

		public void ShowAutoOptions(Color[] colors)
		{
			_colors = colors;
			var f1 = SetCurrentType(PlayType.StrokeAuto);
			var optionCount = colors.Length;
			for (var i = optionCount; i < 5; i++)
			{
				var f2 = f1.GetChild(i);
				f2.gameObject.SetActive(false);
			}

			var step = Level.CurrentLayer.CurrentSurface.CurrentStep;
			for (var i = 0; i < optionCount; i++)
			{
				var active = step.IsOptionActive(i);
				var f2 = f1.GetChild(i);
				f2.GetChild(0).gameObject.SetActive(!active);
				f2.GetChild(0).GetChild(0).GetComponent<Image>().color = colors[i];
				f2.GetChild(1).gameObject.SetActive(active);
				f2.GetChild(1).GetChild(0).GetComponent<Image>().color = colors[i];

				if (!Level.SpecialStroke) continue;
				// AD group
				f2.GetChild(2).gameObject.SetActive(
					Config.Instance.BonusAndSpecialOption &&
					i == colors.Length - 1 && !_specialColorUnlocked);
				// Particle image
				f2.GetChild(3).gameObject
					.SetActive(Config.Instance.BonusAndSpecialOption && i == colors.Length - 1);
			}

			FinishStep = null;
			completeButton.gameObject.SetActive(false);
			Canvas.ForceUpdateCanvases();
			_hand1 = f1.GetChild(GetTutorialOptionIndex()[0]).gameObject;
			if (GetTutorialOptionIndex().Count > 1)
				_hand2 = f1.GetChild(GetTutorialOptionIndex()[1]).gameObject;
		}

		public void ShowOnlyOneOption()
		{
			var f1 = GetOptionType(PlayType.StrokeManual);
			f1.gameObject.SetActive(false);
			HideOptionsBackground();
		}

		public void ShowStencilUI()
		{
			SetCurrentType(PlayType.StencilManual);
		}

		public void ShowMirrorGlazeUI()
		{
			SetCurrentType(PlayType.MirrorGlaze);
		}

		public void TutorialStrokeOption(int index)
		{
			var f1 = SetCurrentType(PlayType.StrokeManual);
			ShowOptionsBackground();
			Transform correctOption = null;
			for (var i = 0; i < 5; i++)
			{
				var f2 = f1.GetChild(i);
				var button = f2.GetComponent<Button>();
				button.interactable = i == index;
				f2.GetChild(4).gameObject.SetActive(i == index);
				if (i == index)
				{
					correctOption = f2;
				}
			}

			if (correctOption == null) return;
			Debug.LogError("focus");
			focusTutorial.gameObject.SetActive(true);
			focusTutorial.Focus(correctOption.transform.position + new Vector3(0, 0.3f, 0));
			TutorialSign.Instance.EndTutorial();
		}

		private ColorOption[] _colorOptions;

		public void ShowStrokeManualOptions(ColorOption[] colorOptions)
		{
			_colorOptions = colorOptions;
			var f1 = SetCurrentType(PlayType.StrokeManual);
			if (_colorOptions == null || _colorOptions.Length < 2)
			{
				f1.gameObject.SetActive(false);
				HideOptionsBackground();
			}
			else
			{
				ShowOptionsBackground();
			}
			SelectStrokeManualOption(Math.Max(0, PastryBag.IndexOption));
		}


		private StrokeManual StrokeManualFirst { get; set; }
		private StrokeManual StrokeManualSecond { get; set; }

		public void SetStrokePreview(StrokeManual strokeManual)
		{
			if (_strokePreviewId == 0)
			{
				_strokePreviewId++;
				StrokeManualFirst = strokeManual;
			}
			else
			{
				StrokeManualSecond = strokeManual;
				_strokePreviewId = 0;
			}
		}

		public void ShowOptionsBackground()
		{
			optionsBG.gameObject.SetActive(true);
		}

		public void HideOptionsBackground()
		{
			optionsBG.gameObject.SetActive(false);
		}

		public void HideCorrectStrokeManualOptionFx()
		{
			var f1 = GetOptionType(PlayType.StrokeManual);
			for (var i = 0; i < f1.childCount; i++)
			{
				var f2 = f1.GetChild(i);
				f2.GetChild(5).gameObject.SetActive(false);
			}
		}

		public void ShowCorrectStrokeManualOptionFx(int index)
		{
			var f1 = GetOptionType(PlayType.StrokeManual);
			for (var i = 0; i < 5; i++)
			{
				f1.GetChild(i).GetChild(5).gameObject.SetActive(i == index);
			}
		}

		public void SelectStrokeManualOption(int index)
		{
			var f1 = GetOptionType(PlayType.StrokeManual);
			var optionCount = _colorOptions.Length;
			OptionOn?.Invoke(index);
			for (var i = 0; i < 5; i++)
			{
				var f2 = f1.GetChild(i);
				f2.GetComponent<Button>().interactable = true;
				f2.gameObject.SetActive(i < optionCount);
				if (i >= optionCount)
				{
					continue;
				}

				var active = i == index;
				var image = f2.GetChild(0).GetComponent<RawImage>();
				if (image)
				{
					if (_colorOptions[i].gradient)
					{
						image.color = Color.white;
						image.texture = _colorOptions[i].GradientTexture;
					}
					else
					{
						image.color = _colorOptions[i].colors[0];
					}
				}

				if (_colorOptions[i].colors.Length > 1 && !_colorOptions[i].gradient)
				{
					var img = f2.GetChild(0).GetChild(0).GetComponent<Image>();
					if (img)
					{
						img.color = _colorOptions[i].colors[1];
					}

					f2.GetChild(0).GetChild(0).gameObject.SetActive(true);
				}
				else
				{
					f2.GetChild(0).GetChild(0).gameObject.SetActive(false);
				}

				f2.GetChild(1).gameObject.SetActive(active);
				f2.GetChild(4).gameObject.SetActive(false);
			}
			FinishStep = null;
			completeButton.gameObject.SetActive(false);
			Canvas.ForceUpdateCanvases();
		}

		public void ShowAutoOptions(Sprinkles[] options)
		{
			SetCurrentType(PlayType.SprinkleAuto);
			StartCoroutine(SetOptionImages(options));
			FinishStep = null;
			completeButton.gameObject.SetActive(false);
		}

		private static bool _specialFillUnlocked;
		private List<FillManualOption> _fillOptions;

		public void ShowFillManualOptions(List<FillManualOption> options,int optionAd)
		{
			_fillOptions = options;
			OptionOn = null;
			OptionOff = null;

			var f1 = SetCurrentType(PlayType.FillManual);
			if (options == null || options.Count < 2)
			{
				f1.gameObject.SetActive(false);
				HideOptionsBackground();
				return;
			}
			ShowOptionsBackground();
			var optionCount = _fillOptions.Count;
			for (var i = 0; i < 5; i++)
			{
				var f2 = f1.GetChild(i);
				if (i >= optionCount)
				{
					f2.gameObject.SetActive(false);
					continue;
				}

				var iconImage = f2.GetChild(0).GetComponent<Image>();
				if (options[i].PaintText == null)
				{
					iconImage.color = options[i].BlendColor;
				}
				else
				{
					iconImage.sprite = options[i].Icon;
					iconImage.color = Color.white;
				}

				var select = f2.GetChild(1).GetChild(0).gameObject;
				select.SetActive(true);
				select.gameObject.SetActive(i == 0);
				if (Level.LevelStype == LevelStyle.FreeStyle)
				{
					if(wathchedFillManualReward)
						return;
					optionAds = optionAd;
					if (i == optionAd)
					{
						f2.GetChild(5).gameObject.SetActive(true);
					}
				}
				if (!Level.SpecialFill) continue;
				// AD group
				f2.GetChild(2).gameObject.SetActive(
					Config.Instance.BonusAndSpecialOption &&
					i == _fillOptions.Count - 1 &&
					!_specialFillUnlocked);
				// Particle image
				f2.GetChild(3).gameObject.SetActive(
					Config.Instance.BonusAndSpecialOption && i == _fillOptions.Count - 1);
				
			}
		}

		public void ResetFillOptionSelected()
		{
			var f1 = SetCurrentType(PlayType.FillManual);
			for (var i = 0; i < 5; i++)
			{
				var f2 = f1.GetChild(i);
				var select = f2.GetChild(1).GetChild(0).gameObject;
				select.SetActive(false);
			}
		}

		private Sprinkles[] _sprinklesOptions;

		public void ShowManualOptions(Sprinkles[] options,int optionAd)
		{
			_sprinklesOptions = options;
			OptionOn = null;
			OptionOff = null;

			var f1 = SetCurrentType(PlayType.SprinkleManual);
			if (options == null || options.Length < 2)
			{
				f1.gameObject.SetActive(false);
				HideOptionsBackground();
				return;
			}
			
			ShowOptionsBackground();
			StartCoroutine(SetOptionImages(options));
			if (Level.LevelStype != LevelStyle.FreeStyle)
				return;
			for (int i = 0; i < 5; i++)
			{
				var f2 = f1.GetChild(i);
				if (i >= options.Length)
				{
					f2.gameObject.SetActive(false);
					continue;
				}
				if(wathchedSprinklesReward)
					return;
				optionAds = optionAd;
				if (i == optionAd)
				{
					f2.GetChild(4).gameObject.SetActive(true);
				}
			}
		}

		public static void ResetOwnedOption()
		{
			_specialFillUnlocked = false;
			_specialColorUnlocked = false;
			_ownedSprinkles = new List<string>(2);
		}

		public void ShowCompleteButton()
		{
			if (!Profile.Instance.DoneTutorialActived)
			{
				Profile.Instance.DoneTutorialActived = true;
				var doneFx = DOTween.Sequence();
				doneFx.Append(completeButton.transform.DOScale(Vector3.one * 1.1f, 0.5f));
				doneFx.Append(completeButton.transform.DOScale(Vector3.one, 0.5f));
				doneFx.SetLoops(-1).Play();
			}

			completeButton.gameObject.SetActive(true);
		}

		public void HideCompleteButton()
		{
			completeButton.gameObject.SetActive(false);
		}

		public void HideUndoButton()
		{
			undoButton.gameObject.SetActive(false);
		}

		public void ShowUndoButton()
		{
			undoButton.gameObject.SetActive(true);
		}

		public void ShowBonusUI()
		{
			_previewRT.gameObject.SetActive(false);
		}

		public void ActiveTutorial()
		{
			if (!Profile.Instance.TutorialActived[listTut.IndexOf(Tutorial)])
			{
				Tutorial.gameObject.SetActive(true);
			}
		}

		public void HideTutorial()
		{
			if (Tutorial == null || !Tutorial.gameObject.activeInHierarchy) return;
			Tutorial.ControlAnim.Play(TutorialHide);
			if (_anim == null) return;
			_anim.Kill();
			_anim = null;
		}

		public void SetupTutorial(bool doubleHand)
		{
			if (doubleHand)
			{
				var position = _hand1.transform.position;
				Tutorial.SetPosButton1(position);
				Tutorial.SetPosHand1(position);

				position = _hand2.transform.position;
				Tutorial.SetPosButton2(position);
				Tutorial.SetPosHand2(position);

				Tutorial.SetSpriteButton1(_hand1);
				Tutorial.SetSpriteButton2(_hand2);
			}
			else
			{
				var position = _hand1.transform.position;
				Tutorial.SetPosButton1(position);
				Tutorial.SetPosHand1(position);
				Tutorial.SetSpriteButton1(_hand1);
			}

			Tutorial.SetPosText(textSuggest.transform.position);
		}

		public void SetCurrentTutorial(int index)
		{
			Tutorial = listTut[index];
		}

		public List<SprinkleObject> SprinkleObjectsFirst { get; set; } = new List<SprinkleObject>();

		public List<SprinkleObject> SprinkleObjectsSecond { get; set; } = new List<SprinkleObject>();

		static Gameplay()
		{
			_specialColorUnlocked = false;
		}

		private float DistanceFirst { get; set; } = -1f;
		private float DistanceSecond { get; set; } = -1f;

		public void SetSprinklePreview(Sprinkles[] options)
		{
			var textures = new Texture[options.Length];
			for (var i = 0; i < options.Length; i++)
			{
				options[i].MakePreview(sprinklesPreviewCamera, 4);
				options[i].Preview.SetActive(true);

				var texture = RenderTexture2D(sprinklesPreviewCamera, 128);
				options[i].Preview.SetActive(false);
				textures[i] = texture;
			}

			if (SprinkleObjectsFirst.Count > 0 && StrokeManualFirst != null)
			{
				if (DistanceFirst < 0)
				{
					foreach (var sprinkleObject in SprinkleObjectsFirst)
					{
						DistanceFirst +=
							Vector3.Distance(StrokeManualFirst.transform.position,
								sprinkleObject.transform.position);
					}
				}
			}

			if (SprinkleObjectsSecond.Count <= 0 || StrokeManualSecond == null) return;
			if (DistanceSecond >= 0) return;
			foreach (var sprinkleObject in SprinkleObjectsSecond)
			{
				DistanceSecond +=
					Vector3.Distance(StrokeManualSecond.transform.position,
						sprinkleObject.transform.position);
			}
		}

		public static Texture2D ToTexture2D(RenderTexture texture)
		{
			// Convert render texture to 2d texture
			var texture2d =
				new Texture2D(texture.width, texture.height, TextureFormat.ARGB32, false);
			RenderTexture.active = texture;
			texture2d.ReadPixels(new Rect(0, 0, texture.width, texture.height), 0, 0);
			texture2d.Apply();
			RenderTexture.active = null; // JC: added to avoid errors
			return texture2d;
		}

		private IEnumerator SetOptionImages(IReadOnlyList<Sprinkles> options)
		{
			var sprites = new Sprite[options.Count];
			sprinklesPreviewCamera.aspect = 1;

			var f1 = optionHub.transform.GetChild((int) _currentType);
			// Sprinkle manual
			for (var i = 0; i < options.Count; i++)
			{
				options[i].MakePreview(sprinklesPreviewCamera);
				options[i].Preview.SetActive(true);

				yield return new WaitForEndOfFrame();
				var texture = RenderTexture2D(sprinklesPreviewCamera, 128);
				options[i].Preview.SetActive(false);
				sprites[i] = Sprite.Create(ToTexture2D(texture),
					new Rect(0, 0, texture.width, texture.height), Vector2.zero);

				f1.GetChild(i).gameObject.SetActive(true);
				f1.GetChild(i).GetChild(2).gameObject
					.SetActive(options[i].Special && !OwnedSprinkle(i));
				var sprinkleManual =
					Level.CurrentLayer.CurrentSurface.CurrentStep as SprinkleManual;
				f1.GetChild(i).GetComponent<Animation>().enabled = sprinkleManual != null &&
					sprinkleManual.Bonus && options[i].Special;
			}

			for (var i = options.Count; i < 5; i++)
			{
				var f2 = f1.GetChild(i);
				f2.gameObject.SetActive(false);
			}

			if (_currentType == PlayType.SprinkleAuto)
			{
				var step = Level.CurrentLayer.CurrentSurface.CurrentStep;
				for (var i = 0; i < options.Count; i++)
				{
					var active = step.IsOptionActive(i);
					var f2 = f1.GetChild(i);
					f2.GetChild(0).gameObject.SetActive(!active);
					f2.GetChild(0).GetChild(1).GetComponent<Image>().sprite = sprites[i];
					f2.GetChild(1).gameObject.SetActive(active);
					f2.GetChild(1).GetChild(1).GetComponent<Image>().sprite = sprites[i];
				}
			}
			else
			{
				// Sprinkle manual
				for (var i = 0; i < options.Count; i++)
				{
					var f2 = f1.GetChild(i);
					f2.GetChild(1).gameObject
						.SetActive(i == 0 && (!options[0].Special || OwnedSprinkle(i)));
					f2.GetChild(0).GetComponent<Image>().sprite = sprites[i];
				}
			}

			Canvas.ForceUpdateCanvases();

			_hand1 = f1.GetChild(GetTutorialOptionIndex()[0]).gameObject;
			if (GetTutorialOptionIndex().Count > 1)
				_hand2 = f1.GetChild(GetTutorialOptionIndex()[1]).gameObject;
			ONOptionsSetup?.Invoke();
		}

		public void ReloadToolSkins()
		{
			Carafe.ReloadSkin();
			PastryBag.ReloadSkin();
			LoadBottleSkin();
		}

		public void SwitchToSide()
		{
			cameraController.Move(sideCameraTransform, 1.0f);
			light.transform.DOLocalRotate(sideLightTransform.localEulerAngles, 0.5f);
		}

		#region CandlesFeature

		private List<BigDecor> _bigDecorsOptions;
		public DataConfigSO bigDecorConfig;

		public void SelectBigDecorManualOption(int index)
		{
			var f1 = GetOptionType(PlayType.BigDecorManual);
			Play();
			OnOptionSelected?.Invoke(index);
			for (var i = 0; i < 5; i++)
			{
				f1.GetChild(i).GetChild(1).GetChild(0).gameObject.SetActive(i == index);
			}
		}

		public void ShowBigDecorManualOptions(List<BigDecor> options)
		{
			_navMeshSurface = GetComponent<NavMeshSurface>();
			_navMeshSurface.BuildNavMesh();
			this._bigDecorsOptions = options;
			var f1 = SetCurrentType(PlayType.BigDecorManual);
			var optionCount = _bigDecorsOptions.Count;
			for (int i = 0; i < 5; i++)
			{
				var f2 = f1.GetChild(i);
				if (i >= optionCount)
				{
					f2.gameObject.SetActive(false);
					continue;
				}

				Image iconImage = f2.GetChild(0).GetComponent<Image>();
				if (options[i].Icon == null)
				{
					iconImage.color = Color.white;
				}
				else
				{
					iconImage.sprite = options[i].Icon;
					iconImage.color = Color.white;
				}

				var select = f2.GetChild(1).GetChild(0).gameObject;
				select.SetActive(true);
				select.gameObject.SetActive(i == 0);
			}

			OptionOn = null;
			OptionOff = null;
		}

		List<MiniDecorObj> _miniDecorsOptions;
		//public Text countCandlesText;
		public List<Text> listCandleCountText;
		private int indexDecorOption;
		private int optionAds;
		public void SelectMiniDecorManualOption(int index)
		{
			var f1 = GetOptionType(PlayType.MiniDecorManual);
			Play();
			if (Level.LevelStype == LevelStyle.FreeStyle)
			{
				if (index == optionAds)
				{
					Ads.ShowRewardedVideo("minidecor_optionAds", (result) =>
					{
						if (result != RewardedVideoState.Watched)
						{
							return;
						}
						f1.GetChild(index).GetChild(2).gameObject.SetActive(false);
						OnOptionSelected?.Invoke(index);
						optionAds = -1;
						for (var i = 0; i < _miniDecorsOptions.Count; i++)
						{
							f1.GetChild(i).GetChild(1).GetChild(0).gameObject.SetActive(i == index);
						}
					});
				}
				else
				{
					OnOptionSelected?.Invoke(index);
					for (var i = 0; i < _miniDecorsOptions.Count; i++)
					{
						f1.GetChild(i).GetChild(1).GetChild(0).gameObject.SetActive(i == index);
					}
				}
			}
			else
			{
				OnOptionSelected?.Invoke(index);
				for (var i = 0; i < _miniDecorsOptions.Count; i++)
				{
					f1.GetChild(i).GetChild(1).GetChild(0).gameObject.SetActive(i == index);
				}
			}
			indexDecorOption = index;
		}

		public void HideUnSelectedOption()
		{
			var f1 = GetOptionType(PlayType.MiniDecorManual);
			Play();
			OnOptionSelected?.Invoke(indexDecorOption);
			for (var i = 0; i < _miniDecorsOptions.Count; i++)
			{
				f1.GetChild(i).gameObject.SetActive(i == indexDecorOption);
			}

		}
		public List<RenderTexture> _listSprite;
		private  void SetCandlesPreview(List<MiniDecorObj> options)
		{
			miniDecorPreviewCamera.aspect = 1;
			for (var i = 0; i < options.Count; i++)
			{
				//miniDecorPreviewCamera.backgroundColor = new Vector4(1, 1, 0.81f, 0);
				options[i].MakePreview(miniDecorPreviewCamera, 4);
				options[i].PreviewUI.SetActive(true);
				var texture = RenderTexture2D(miniDecorPreviewCamera, 128);
				options[i].PreviewUI.SetActive(false);
				_listSprite[i] = texture;
			}
		}
		public void ShowMiniDecorManualOptions(List<MiniDecorObj> options, int count,out GameObject previewPic,int optionAd)
		{ 
			previewPic = previewImage.transform.parent.gameObject;
			this._miniDecorsOptions = options;
			var f1 = SetCurrentType(PlayType.MiniDecorManual);
			var optionCount = _miniDecorsOptions.Count;
			SetCandlesPreview(options);
			optionAds = optionAd;
			for (int i = 0; i < 5; i++)
			{
				var f2 = f1.GetChild(i);
				if (i >= optionCount)
				{
					f2.gameObject.SetActive(false);
					continue;
				}

				// Image iconImage = f2.GetChild(0).GetComponent<Image>();
				// if (options[i].Icon == null)
				// {
				// 	iconImage.color = Color.white;
				// }
				// else
				// {
				// 	//iconImage.sprite = _listSprite[i];
				// 	iconImage.color = Color.white;
				// }
				RawImage icon=f2.GetChild(0).GetComponent<RawImage>();
				icon.texture = _listSprite[i];
				f2.GetComponent<MiniDecorOptions>().countCandles.text = count.ToString();
				GameObject select = f2.GetChild(1).GetChild(0).gameObject;
				select.SetActive(true);
				select.gameObject.SetActive(i == 0);
				listCandleCountText.Add(f1.GetChild(i).GetComponent<MiniDecorOptions>().countCandles);
				if (Level.LevelStype != LevelStyle.FreeStyle)
					return;
				if (i == optionAd)
				{
					f2.GetChild(2).gameObject.SetActive(true);
				}
			}

			OptionOn = null;
			OptionOff = null;
		}

		public void ShowOptionOnUndo(List<MiniDecorObj> options)
		{
			var f1 = SetCurrentType(PlayType.MiniDecorManual);
			var optionCount = _miniDecorsOptions.Count;
			SetCandlesPreview(options);
			for (int i = 0; i < 5; i++)
			{
				var f2 = f1.GetChild(i);
				if (i >= optionCount)
				{
					f2.gameObject.SetActive(false);
					continue;
				}
				f2.gameObject.SetActive(true);
			}
		}
		#endregion

		#region Photo
		public bool isPhoto;
		public GameObject opBg;

		public void ShowPhotoManual(out GameObject effect, out GameObject buttonPhoto, out GameObject ui,
			out GameObject previewPic)
		{
			var f1 = SetCurrentType(PlayType.PhotoManual);
			ui = f1.gameObject;
			effect = f1.GetChild(0).gameObject;
			buttonPhoto = f1.GetChild(2).GetChild(0).gameObject;
			previewPic = previewImage.transform.parent.gameObject;
		}
		public void GetTextureForResultPhotoManual(Camera cam)
		{
			var objects = Level.GetComponentsInChildren<SprinkleObject>();
			foreach (var obj in objects)
			{
				if (obj.OnTable) DestroyImmediate(obj.gameObject);
			}

			resultCamera.fieldOfView = cam.fieldOfView = 40;
			ResultTexture = RenderTexture2D(cam, 512, cam.transform);

			table.gameObject.layer = Layers.SpinklesPreview;
			disk.gameObject.layer = Layers.SpinklesPreview;
			pastryBag.gameObject.SetLayerRecursively(Layers.SpinklesPreview);
			Bottle.gameObject.SetLayerRecursively(Layers.SpinklesPreview);
			Carafe.gameObject.SetLayerRecursively(Layers.SpinklesPreview);
			StencilTool.gameObject.SetLayerRecursively(Layers.SpinklesPreview);
			Level.gameObject.SetActive(false);
			PreviewLevel.gameObject.SetActive(true);
			background.SetLayerRecursively(Layers.SpinklesPreview);
			PreviewLevel.transform.localPosition = Level.transform.localPosition;
			PreviewLevel.transform.localRotation = Level.transform.localRotation;
			CorrectTexture = RenderTexture2D(resultCamera, 512, cam.transform);
			Level.gameObject.SetActive(true);
			PreviewLevel.gameObject.SetActive(false);
			background.SetLayerRecursively(Layers.Background);
			table.gameObject.layer = Layers.Background;
			disk.gameObject.layer = Layers.CakeSurface;
			pastryBag.gameObject.SetLayerRecursively(Layers.Gameplay);
			Bottle.gameObject.SetLayerRecursively(Layers.Gameplay);
			Carafe.gameObject.SetLayerRecursively(Layers.Gameplay);
			StencilTool.gameObject.SetLayerRecursively(Layers.Gameplay);
		}

		#endregion
		#region RewardProgress

		public void HideBoxProgress()
		{
			boxProgress.DOFade(0, 0.3f).OnComplete(
				() => boxProgress.gameObject.SetActive(false));
		}

		private void ShowBoxProgress()
		{
			boxProgress.gameObject.SetActive(true);
			boxProgress.alpha = 0;
			boxProgress.DOFade(1, 0.3f).SetDelay(.4f);
		}

		#endregion

		#region SelectCakeSurface
		List<Cake> _surfacesOptions;
		private int indexSurfaceOption;
		public List<RenderTexture> _listSpriteSurface;
		[SerializeField] private CanvasGroup boxProgressLevelFree;
		[SerializeField] private Transform boxProgressDots;
		[SerializeField] private GameObject levelFreeProgress;
		private  void SetSurfaceCakePreview(List<Cake> options,Texture caketexture,bool mirror,bool stencil,bool glitter)
		{
			for (var i = 0; i < options.Count; i++)
			{
				var go = Instantiate(options[i]);
				var m = go.gameObject.AddComponent<MakePreviewSurface>();
				m.MakePreview(selectSurfacePreviewCamera,Level.CakeType,go.gameObject ,caketexture,mirror,stencil,glitter);
				go.gameObject.SetActive(true);
				var texture = RenderTexture2D(selectSurfacePreviewCamera, 128);
				go.gameObject.SetActive(false);
				_listSpriteSurface[i] = texture;
			}
		}
		public void SelectCakeSurfaceSelectManualOption(int index)
		{
			var f1 = GetOptionType(PlayType.SurfaceSelect);
			Play();
			if (Level.LevelStype == LevelStyle.FreeStyle)
			{
				if (index == optionAds)
				{
					Ads.ShowRewardedVideo("SelectSurfaceCake_optionAds", (result) =>
					{
						if (result != RewardedVideoState.Watched)
						{
							return;
						}
						f1.GetChild(index).GetChild(2).gameObject.SetActive(false);
						OnOptionSelected?.Invoke(index);
						for (var i = 0; i < _surfacesOptions.Count; i++)
						{
							f1.GetChild(i).GetChild(1).GetChild(0).gameObject.SetActive(i == index);
						}
						optionAds = -1;
					});
				}
				else
				{
					OnOptionSelected?.Invoke(index);
					for (var i = 0; i < _surfacesOptions.Count; i++)
					{
						f1.GetChild(i).GetChild(1).GetChild(0).gameObject.SetActive(i == index);
					}
				}
			}
			else
			{
				OnOptionSelected?.Invoke(index);
				for (var i = 0; i < _surfacesOptions.Count; i++)
				{
					f1.GetChild(i).GetChild(1).GetChild(0).gameObject.SetActive(i == index);
				}
			}
			choseOne.gameObject.SetActive(false);
			indexSurfaceOption = index;
		}
		public void ShowCakeSurfaceManualOptions(List<Cake> options,Texture texture,bool mirror,bool stencil,bool glitter,int optionAd)
		{
			choseOne.gameObject.SetActive(true);
			_surfacesOptions = options;
			var f1 = SetCurrentType(PlayType.SurfaceSelect);
			var optionCount = _surfacesOptions.Count;
			optionAds = optionAd;
			SetSurfaceCakePreview(options,texture,mirror,stencil,glitter);
			for (int i = 0; i < 5; i++)
			{
				var f2 = f1.GetChild(i);
				if (i >= optionCount)
				{
					f2.gameObject.SetActive(false);
					continue;
				}
				//Image iconImage = f2.GetChild(0).GetComponent<Image>();
				// if (options[i].Icon == null)
				// {
				// 	iconImage.color = Color.white;
				// }
				// else
				// {
				// 	//iconImage.sprite = _listSprite[i];
				// 	iconImage.color = Color.white;
				// }
				RawImage icon=f2.GetChild(0).GetComponent<RawImage>();
				icon.texture = _listSpriteSurface[i];
				if (Level.LevelStype != LevelStyle.FreeStyle)
					return;
				if (i == optionAd)
				{
					f2.GetChild(2).gameObject.SetActive(true);
				}
			}

			OptionOn = null;
			OptionOff = null;
		}

		private int count = 0;
		private void ShowBoxLevelFreeProgress()
		{
			boxProgressLevelFree.gameObject.SetActive(true);
			boxProgressLevelFree.alpha = 0;
			boxProgressLevelFree.DOFade(1, 0.3f).SetDelay(.4f);
			for (int i = 0; i < Level.Layers.Length; i++)
			{
				for (int j = 0; j < Level.Layers[i].Surfaces.Length; j++)
				{
					count += Level.Layers[i].Surfaces[j].TrueCountStep();
				}
			}
			for (int j = 0; j < boxProgressDots.childCount; j++)
			{
				boxProgressDots.GetChild(j).gameObject.SetActive(j<count);
			}
		}

		private void HideBoxLevelFreeProgress()
		{
			boxProgressLevelFree.gameObject.SetActive(false);
		}

		private int n = 0;
		public void SetDotColorLevelFree()
		{
			for (var i = 0; i < count; i++)
			{
				var green = i <= n;
				boxProgressDots.GetChild(i).GetChild(0).gameObject.SetActive(!green);
				boxProgressDots.GetChild(i).GetChild(1).gameObject.SetActive(green);
				boxProgressDots.GetChild(i).GetChild(2).gameObject.SetActive(i == n);
			}
			n++;
		}

		public void ShowLevelFreeProgress()
		{
			levelFreeProgress.SetActive(true);
			levelFreeProgress.transform.GetChild(0).GetComponent<Image>().fillAmount = 0;
			levelFreeProgress.transform.GetChild(0).GetChild(0).GetComponent<Image>().color = new Vector4(0.7f,0.7f,0.7f,0.9f);
		}

		public void HideLevelFreeProgress()
		{
			levelFreeProgress.SetActive(false);
			levelFreeProgress.transform.GetChild(0).GetComponent<Image>().fillAmount = 0;
			levelFreeProgress.transform.GetChild(0).GetChild(0).GetComponent<Image>().color = new Vector4(0.7f,0.7f,0.7f,0.9f);
		}

		public void SetFillAmountFreeProgress(float percent)
		{
			levelFreeProgress.transform.GetChild(0).GetComponent<Image>().fillAmount = Mathf.Clamp(percent,0,1);
			if (levelFreeProgress.transform.GetChild(0).GetComponent<Image>().fillAmount >= 0.9f)
			{
				levelFreeProgress.transform.GetChild(0).GetChild(0).GetComponent<Image>().DOColor( new Vector4(1f,1f,1f,1f),0.5f);
			}
		}
		#endregion
	}
}