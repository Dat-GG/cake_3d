using System.Collections.Generic;
using UnityEngine;

namespace Nama
{
	/// <summary>
	/// 2D Path abstract. Currently, there is only one specific path supported
	/// which is BezierPath
	/// </summary>
	public class Path : MonoBehaviour
	{
		/// <summary>
		/// Generate a 2D polyline specified by an array of 2D points
		/// </summary>
		/// <returns>An array of 2D points on the polyline</returns>
		public virtual List<Vector2> GeneratePolyline(Transform transformation)
		{
			return null;
		}
	}
}