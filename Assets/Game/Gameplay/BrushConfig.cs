﻿using Nama;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BrushConfig", menuName = "Tools/BrushConfig")]
public class BrushConfig : ScriptableObject
{
	public float GuideLineWidth = 0.1f;
	[Header("Fall Height")]
	public float BrushHeight = 0.5f;
	public float FallRange = 0.5f;
	public float FallTime = 0.3f;
	public AnimationCurve Cuver;

	[Header("Scale Line")] [Range(0, 1)]
	public float StartWidthPercent = 0.5f;
	public float ScaleTime = 0.3f;
	public float ScaleRange = 1;
	public AnimationCurve ScaleCuver;

	[Header("Fill Color")] public AnimationCurve fillColorCurver;
	public int VertexSize = 100;
	public float FillHeight = 0.3f;
	public float FillRadius = 0.26f;

	[Header("Fill Tuning Options")]
	public List<FillTuning> fillTunings;

	private static BrushConfig _instance;
	public static BrushConfig Instance
	{
		get
		{
			if (_instance == null)
				_instance = Resources.Load<BrushConfig>("BrushConfig");

			return _instance;
		}
	}
}