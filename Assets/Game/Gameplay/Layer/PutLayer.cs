using DG.Tweening;
using UnityEngine;

namespace Nama
{
	public class PutLayer
	{
		public Cake Layer { get; }
		private readonly float _minY, _maxY;

		public PutLayer(Cake layer)
		{
			Layer = layer;
			Layer.gameObject.SetActive(true);

			var t = layer.transform;
			var p = t.localPosition;
			_minY = p.y;
			t.localPosition = p + Vector3.up * 5;
			p += Vector3.up * 2;
			t.DOLocalMove(p, 1.0f);
			_maxY = p.y;
			CakeSurface.SendStepEvent("miniLevelStarted", "layer", CakeSurface.StepId);
			Gameplay.Instance.SwitchToSide();
			Gameplay.Instance.PutLayerTutorial.SetActive(true);
			Gameplay.Instance.PreviewImage.SetActive(false);
			Gameplay.Instance.HideOptionsUI();
		}

		public bool Finished { get; private set; }
		
		private Vector3 _touchPos;
		private void OnTouchDown(Vector3 screenPos)
		{
			_touchPos = screenPos;
			Gameplay.Instance.PutLayerTutorial.SetActive(false);
		}

		private void OnTouchUp(Vector3 screenPos)
		{
		}

		private void OnTouchHold(Vector3 screenPos)
		{
			var dy = (screenPos - _touchPos).y / Screen.height * 5;
			_touchPos = screenPos;
			var t = Layer.transform;
			var y = t.localPosition.y + dy;
			if (y <= _minY)
			{
				y = _minY;
				Finish();
			}
			else if (y > _maxY)
			{
				y = _maxY;
			}
			
			var p = t.localPosition;
			p.y = y;
			t.localPosition = p;
		}

		private bool _touched;
		public void DoFrame()
		{
			if (Finished) return;
			var eventSystem = UnityEngine.EventSystems.EventSystem.current;
#if UNITY_EDITOR
			if (Input.GetMouseButtonDown(0) && !eventSystem.IsPointerOverGameObject())
			{
				_touched = true;
				OnTouchDown(Input.mousePosition);
			}

			if (Input.GetMouseButtonUp(0) && _touched)
			{
				_touched = false;
				OnTouchUp(Input.mousePosition);
			}

			if (Input.GetMouseButton(0) && _touched)
			{
				OnTouchHold(Input.mousePosition);
			}
#else
			if (Input.touchCount <= 0)
			{
				return;
			}

			switch (Input.touches[0].phase)
			{
				case TouchPhase.Began:
					if (!eventSystem.IsPointerOverGameObject(Input.touches[0].fingerId))
					{
						_touched = true;
						OnTouchDown(Input.touches[0].position);
					}
					break;

				case TouchPhase.Stationary:
				case TouchPhase.Moved:
					if (_touched)
					{
						OnTouchHold(Input.touches[0].position);
					}
					break;
				case TouchPhase.Ended:
				case TouchPhase.Canceled:
					if (_touched)
					{
						_touched = false;
						OnTouchUp(Input.touches[0].position);
					}
					break;
			}
#endif
		}

		private void Finish()
		{
			CakeSurface.SendStepEvent("miniLevelCompleted", "layer", CakeSurface.StepId);
			CakeSurface.StepId++;
			Gameplay.Instance.PreviewImage.SetActive(true);
			Ads.ShowInterstitial(() => Finished = true, "layer");
		}
	}
}

