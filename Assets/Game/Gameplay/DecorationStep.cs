﻿using UnityEngine;
using UnityEditor;
using System;
using System.Linq;

namespace Nama
{
	/// <summary>
	/// Decoration step
	/// </summary>
	[ExecuteInEditMode]
	public class DecorationStep : MonoBehaviour
	{
		public bool Finished { get; protected set; }
		protected GameObject Display { get; private set; }
		private float _timeEachVibrate = 1f;
		/// <summary>
		/// Called once in either level loading
		/// </summary>
		public virtual void Init(bool preview)
		{
			
		}

		/// <summary>
		/// Called when this step turns to the current one
		/// </summary>
		public virtual void Begin()
		{

		}

		/// <summary>
		/// Called every frame if this is current step
		/// </summary>
		public virtual void DoFrame()
		{

		}

		internal virtual void Undo()
		{

		}

		/// <summary>
		/// Get the score achieved in this steps
		/// </summary>
		/// <returns>The score achieved in this steps</returns>
		public virtual float GetScore()
		{
			return 0;
		}

		public virtual bool IsOptionActive(int index)
		{
			return false;
		}

		public virtual void CompleteFX(Action action)
		{

		}

		public virtual int OptionCount => 0;
		public virtual int SelectedIndex => 0;

		public virtual bool IsOptionCorrect(int index)
		{
			return false;
		}

		protected void VibrateLight()
		{
			_timeEachVibrate += Time.deltaTime;
			if (_timeEachVibrate <= 0.1f) return;
			_timeEachVibrate = 0;
			Utils.Instance.VibrateLight();
		}

		public void DestroyDisplay()
		{
			if (Display == null) return;
#if UNITY_EDITOR
			if (EditorApplication.isPlayingOrWillChangePlaymode)
			{
				Destroy(Display);
			}
			else
			{
				DestroyImmediate(Display);
			}
#else
			Destroy(Display);
#endif
			Display = null;
		}

#if UNITY_EDITOR
		private void OnDestroy()
		{
			DestroyImmediate(Display);
		}
#endif

		/// <summary>
		/// Refresh the preview
		/// </summary>
		public virtual void RefreshPreview()
		{
#if UNITY_EDITOR
			_refreshCount++;
#endif
			DestroyDisplay();
			Init(true);
		}

		public virtual void RefreshTool()
		{
			
		}

		public virtual void OnOptionSelected(int index)
		{
			
		}

		protected void CreateDisplay(string displayName)
		{
			Display = new GameObject(displayName);
#if UNITY_EDITOR
			Display.hideFlags = EditorApplication.isPlayingOrWillChangePlaymode ? HideFlags.DontSave : HideFlags.HideAndDontSave;
#endif
			Display.transform.SetParent(Surface.transform, false);
			if (Surface.Radius > 0)
			{
				var position = transform.localPosition;
				var x = position.x;
				var y = position.y;
				var a = x / Surface.Radius * 0.5f;
				var s = Mathf.Sin(a);
				var c = Mathf.Cos(a);
				Display.transform.localPosition = new Vector3(
					Surface.Radius * s,
					y,
					-Surface.Radius * c + Surface.Radius);
				Display.transform.localEulerAngles = new Vector3(0, -a * Mathf.Rad2Deg, 0);
			}
			else
			{
				Display.transform.localPosition = transform.localPosition;
				Display.transform.localRotation = Quaternion.identity;
			}
		}

		protected static float CalculateMatch(int[] a, int[] b)
		{
			var tempA = a.Length <= b.Length ? a : b;
			var tempB = a.Length > b.Length ? a : b;
			var match = tempA.Count(t => Array.IndexOf(tempB, t) >= 0);
			return (float)match / tempB.Length;
		}

#if UNITY_EDITOR

		protected CakeSurface Surface => GetComponentInParent<CakeSurface>();
		private ThirdParty.Outline _outline;

		public void ShowHighlight(bool active)
		{
			switch (active)
			{
				case false when _outline != null:
					DestroyImmediate(_outline);
					_outline = null;
					break;
				case true when Display != null && _outline == null:
					_outline = Display.AddComponent<ThirdParty.Outline>();
					break;
			}
		}

		private void Awake()
		{
			if (EditorApplication.isPlayingOrWillChangePlaymode)
			{
				return;
			}
			EditorApplication.delayCall += RefreshPreview;
		}

		private int _refreshCount;
		private void OnValidate()
		{
			if (_refreshCount <= 0)
			{
				return;
			}
			Awake();
		}

		public static bool LeftMouseButtonDown = false;
		private Vector3 _localPosition = Vector3.zero;
		private Quaternion _localRotation = Quaternion.identity;
		private Vector3 _localScale = Vector3.one;

		public bool CheckTransformChange()
		{
			if (_localPosition == transform.localPosition &&
				_localRotation == transform.localRotation &&
				_localScale == transform.localScale)
			{
				return false;
			}

			UpdateTransform();
			return true;
		}

		private void UpdateTransform()
		{
			var t = transform;
			_localPosition = t.localPosition;
			_localRotation = t.localRotation;
			_localScale = t.localScale;
		}

		private void Update()
		{
			if (!EditorApplication.isPlayingOrWillChangePlaymode &&
				!LeftMouseButtonDown &&
				CheckTransformChange())
			{
				RefreshPreview();
			}
		}
#else
		public CakeSurface Surface { get; set; }
#endif
	}

}
