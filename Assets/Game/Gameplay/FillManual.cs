﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
namespace Nama
{
	[ExecuteInEditMode]
	public class FillManual : DecorationStep
	{
		private const float MINSwipeToMove = 0.001f;
		private const float MINSpawnDistance = 0.1515f;
		public Texture2D drawTexture;

		public List<FillManualOption> options =
			new List<FillManualOption>()
			{
				new FillManualOption(), new FillManualOption(),
				new FillManualOption(), new FillManualOption(), new FillManualOption()
			};

		private int _selectIndex;
		public int correctIndex;

		[SerializeField] private FillColorSurface fillSurface;
		[SerializeField] private RenderCamera renderCamera;
		[SerializeField] private SpriteRenderer previewEditor;
		public FillManualOption CorrectOption => options[correctIndex];
		private const int FillOrder = 0;
		private MeshRenderer _hint;
		private bool _preview;
		private bool _tooltipShowing;
		[SerializeField] private GameObject tooltipHand;

		private float _fillScore;
		private bool _pressed;

		private bool _brushUping;
		public LoangOption loangOption = LoangOption.Normal;
		public float TextureMaxScale { get; private set; }
		public float transformScale = 1;
		public float patternScale = 1;
		public bool showHint = true;
		public bool showAreaFill = true;
		private int _specialOption;

		public float Size => fillSurface.FillSize * transform.localScale.x;
		private  float _minPathDistance=>Gameplay.Instance.bigDecorConfig.DataConfig.minDistance;
		private float totalDistance;
		private float maxDistance=>Gameplay.Instance.bigDecorConfig.DataConfig.maxTotalDistance;
		private List<Vector3> _pointsToCountProgress = new List<Vector3>();
		public int optionAds;
		public override void Init(bool preview)
		{
			transformScale = transform.localScale.x;
			TextureMaxScale = Mathf.Max(drawTexture.width, drawTexture.height) / 1024f;

			FillTuning = loangOption switch
			{
				LoangOption.Slow => BrushConfig.Instance.fillTunings[0],
				LoangOption.Normal => BrushConfig.Instance.fillTunings[1],
				LoangOption.Quick => BrushConfig.Instance.fillTunings[2],
				_ => BrushConfig.Instance.fillTunings[1]
			};

			_preview = preview;

			if (!preview)
			{
				if (Config.Instance.BonusAndSpecialOption && Gameplay.Level.SpecialFill)
				{
					// Add special option 
					foreach (var step in Gameplay.Level.CurrentLayer.CurrentSurface.Steps)
					{
						if (step.GetType() != typeof(FillManual)) continue;
						if (step is FillManual fillManual) _specialOption = fillManual.correctIndex;
					}

					options.Add(options[_specialOption]);
				}
			}

			CreateDisplay("Display of FillManual");
			fillSurface.gameObject.SetActive(true);
			fillSurface.Init(renderCamera, drawTexture, preview, this);
			var spriteMask = CreateSpriteFromTexture(drawTexture);
			renderCamera.Init(this);
			renderCamera.gameObject.SetActive(false);
			renderCamera.SetCullingMask(preview
				? Layers.SpinklesPreviewMask
				: Layers.BrushTextureMask);
			previewEditor.gameObject.SetActive(false);
#if UNITY_EDITOR
			if (!UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode)
			{
				previewEditor.gameObject.SetActive(true);
				previewEditor.sprite = spriteMask;
				return;
			}

			if (preview)
			{
				fillSurface.ShowPreview();
				return;
			}
#else
			if (preview)
			{
				fillSurface.ShowPreview();
				return;
			}
#endif

			_hint = Instantiate(AssetManager.Instance.FillHint, transform);
			_hint.gameObject.SetActive(false);
			var scale = new Vector3(
				(FillColorSurface.CakeSize) * drawTexture.width / 1024.0f,
				(FillColorSurface.CakeSize) * drawTexture.height / 1024.0f,
				1);
			_hint.transform.localScale = scale * .9f;
			_hint.material.mainTexture = drawTexture;
			if (showHint)
			{
				var dashedHint = _hint.transform.GetChild(0).GetComponent<DashedHint>();
				dashedHint.CreateDasedHint(drawTexture);
			}

			_hint.GetComponent<Renderer>().enabled = showAreaFill;
		}

		public override void Begin()
		{
			// Hai Ngo: Fix hint z-fighting issue on iOS
			_hint.transform.localPosition -= new Vector3(0, 0, 0.01f);
			renderCamera.gameObject.SetActive(true);
			_currentBrushPosition = Vector3.zero;
			Gameplay.Instance.OnOptionSelected = OnOptionSelect;
			Gameplay.Instance.FinishStep = Finish;
			Gameplay.Instance.UndoStep = Undo;
			Gameplay.Instance.ShowFillManualOptions(options,optionAds);
			Surface.Carafe.Show(
				Display.transform,
				_currentBrushPosition);
			Surface.Carafe.SetColor(options[0].BlendColor, options[0].PaintText);
			_hint.gameObject.SetActive(true);

			if (!Profile.Instance.FillTutorialActived)
			{
				Profile.Instance.FillTutorialActived = true;
				Profile.Instance.RequestSave();
			}

			Gameplay.Instance.HideCompleteButton();
			Gameplay.Instance.HideUndoButton();
			TutorialSign.Instance.StartTutorial();
			Surface.Carafe.PreUsePointer();
			//Gameplay.Instance.ShowLevelFreeProgress();
			if (Gameplay.Level.LevelStype == LevelStyle.FreeStyle)
			{
				Gameplay.Instance.ShowLevelFreeProgress();
				totalDistance = 0;
				_pointsToCountProgress.Clear();
			}
		}

		private void HideToolTip()
		{
			if (!_tooltipShowing)
				return;

			tooltipHand.SetActive(false);
			_tooltipShowing = false;
		}

		private Vector3 _touchPos = Vector3.zero;

		private bool _optionSelecting;
		private Tween _optionSelectTwen;

		private void OnOptionSelect(int index)
		{
			renderCamera.StopScale();
			_lastCreatePos = new Vector2(float.PositiveInfinity, float.PositiveInfinity);
			_selectIndex = index;
			RefreshTool();

			_optionSelectTwen?.Kill();
			_optionSelecting = true;
			_optionSelectTwen = DOVirtual.DelayedCall(Carafe.WaitTime, () => { _optionSelecting = false; });

			var specialActivated =
				Config.Instance.BonusAndSpecialOption &&
				Gameplay.Level.SpecialFill &&
				index == options.Count - 1;
			fillSurface.SetSufaceMaterial(specialActivated);
		}

		public override void RefreshTool()
		{
			Surface.Carafe.SetColor(options[_selectIndex].BlendColor, options[_selectIndex].PaintText);
		}

		private Vector2 _currentBrushPosition = Vector2.zero;
		private Vector2 _lastHandPosition = Vector2.zero;

		private bool _vfxPositiveActivated;

		public override void DoFrame()
		{

			if (Finished)
				return;
#if UNITY_EDITOR

			_touchPos = Input.mousePosition;
			if (Input.GetMouseButtonDown(0))
			{
				TutorialSign.Instance.EndTutorial();
				if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
				{
					return;
				}

				BrushDown(_touchPos);
			}

			if (Input.GetMouseButtonUp(0))
			{
				BrushUp();
				Surface.Carafe.Stream.End();
				Gameplay.Instance.ShowUndoButton();
			}
#else
			if (Input.touches.Length > 0)
			{

				switch (Input.touches[0].phase)
				{
					case TouchPhase.Began:
						if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(Input.touches[0].fingerId))
						{
							_touchPos = Input.touches[0].position;
							BrushDown(_touchPos);
						}
						break;

					case TouchPhase.Stationary:
						break;
					case TouchPhase.Moved:
						TutorialSign.Instance.EndTutorial();
						break;

					case TouchPhase.Ended:
					case TouchPhase.Canceled:
						BrushUp();
						Surface.Carafe.Stream.End();
						Gameplay.Instance.ShowUndoButton();
						break;
				}

				_touchPos = Input.touches[0].position;
			}

#endif
			if(Gameplay.Level.LevelStype==LevelStyle.FreeStyle)
				SetPercentProgress();
			if (_pressed)
			{
				var ray = Gameplay.Instance.Camera.ScreenPointToRay(_touchPos);
				if (Physics.Raycast(ray, out var hit, 100) && hit.collider != null)
				{
					var surfacePos = new Vector2(hit.point.x, hit.point.z);
					var swipe = surfacePos - _lastHandPosition;
					if (_brushUping)
						swipe = Vector2.zero;
					var newBrushPoint = _currentBrushPosition + swipe * Config.SWIPE_SPEED;

					if (swipe.magnitude >= MINSwipeToMove)
					{
						_currentBrushPosition = newBrushPoint;
						MoveBrush();
						Surface.Carafe.Move(_currentBrushPosition);

						if (fillSurface.InSizeShape(_currentBrushPosition))
						{
							if (!Surface.Carafe.Pointer.gameObject.activeInHierarchy)
							{
								Surface.Carafe.Pointer.gameObject.SetActive(true);
							}

							if (Surface.Carafe.ready)
							{
								_drawTime = SpawnRateTime;
								Surface.Carafe.Stream.Begin();
							}

							Surface.Carafe.Pointer.HideX();
						}
						else
						{
							Surface.Carafe.Pointer.ShowX();
							Surface.Carafe.Stream.End();
						}
					}

					_lastHandPosition = surfacePos;
				}
			}

			_drawTime += Time.smoothDeltaTime;
			var surface = Surface;
			if (surface.Carafe.Stream.Pouring)
			{
				Paint(Surface.Carafe.Stream.PourPosision);
				if (_vfxPositiveActivated) return;
				if (IsOptionCorrect(_selectIndex))
				{
					var previousStep = Surface.Previous as FillManual;
					if (previousStep == null || !previousStep.IsOptionCorrect(_selectIndex))
					{
						Gameplay.Instance.PlayPositiveFX();
						_vfxPositiveActivated = true;
					}
				}
			}

			if (!Surface.Carafe.Stream.Pouring)
			{
				renderCamera.StopDraw();
			}
		}

		internal override void Undo()
		{
			renderCamera.Undo();
			fillSurface.Undo();
			Gameplay.Instance.ResetFillOptionSelected();
			_selectIndex = 0;
			Begin();
		}

		private void BrushUp()
		{
			if (!_pressed)
				return;
			_brushUping = true;

			StopPaint();
		}

		private void StopPaint()
		{
			_pressed = false;
			Gameplay.Instance.ShowCompleteButton();
			_brushUping = false;
			Surface.Carafe.Release();
			_lastCreatePos = new Vector2(float.PositiveInfinity, float.PositiveInfinity);
		}

		private void BrushDown(Vector3 position)
		{
			if (_pressed)
				return;

			var ray = Gameplay.Instance.Camera.ScreenPointToRay(position);
			if (!Physics.Raycast(ray, out var hit, 100) || hit.collider == null) return;
			_drawTime = SpawnRateTime;

			_lastHandPosition = new Vector2(hit.point.x, hit.point.z);
			MoveBrush();
			_pressed = true;
			Surface.Carafe.Press(() =>
			{
				fillSurface.UpdateSurfaceMaterial();
				if (!fillSurface.InSizeShape(_currentBrushPosition)) return;
				Surface.Carafe.Stream.Begin();
				_lastCreatePos = new Vector2(float.PositiveInfinity, float.PositiveInfinity);
			});
		}

		private float _drawTime;
		private const float SpawnRateTime = 0.01f;

		private Vector2 _lastCreatePos = new Vector2(float.PositiveInfinity, float.PositiveInfinity);

		private void Paint(Vector2 pos)
		{
			if (_optionSelecting)
				return;

			if (_drawTime < SpawnRateTime || Vector2.Distance(_lastCreatePos, pos) <= MINSpawnDistance) return;
			_lastCreatePos = pos;
			renderCamera.CreateDrawElement(pos, options[_selectIndex]);
			_drawTime = 0;
		}

		private void MoveBrush()
		{
			VibrateLight();
			if (_tooltipShowing)
			{
				HideToolTip();
			}
		}

		private float GetFillScore()
		{
			var level = Gameplay.Level;
			var previewStep = Gameplay.PreviewLevel.Layers[level.LayerIndex]
				.Surfaces[level.CurrentLayer.SurfaceIndex]
				.Steps[level.CurrentLayer.CurrentSurface.StepIndex] as FillManual;
			if (previewStep == null) return 0.0f;
			var previewTexture = ToTexture2D(previewStep.renderCamera.RenderTexture);
			var cam = renderCamera.Camera;
			var texture2d = ToTexture2D(cam.targetTexture);
			var drawWidth = drawTexture.width;
			var drawHeight = drawTexture.height;

			float sum = 0;
			float correct = 0;

			var startX = (512 - drawWidth / 2);
			var endX = 512 + drawWidth / 2;
			var startY = 512 - drawHeight / 2;
			var endY = 512 + drawHeight / 2;

			var offsetX = drawWidth / 2 - 512;
			var offsetY = drawHeight / 2 - 512;

			const int step = 10;

			for (var i = startX; i < endX; i += step)
			{
				for (var j = startY; j < endY; j += step)
				{
					var color = texture2d.GetPixel(i, j);
					var x1 = i + offsetX;
					var y1 = j + offsetY;
					if (x1 < 0 || x1 >= drawWidth)
						continue;
					if (y1 < 0 || y1 >= drawHeight)
						continue;

					if (drawTexture.GetPixel(x1, y1).a == 0)
						continue;
					sum++;
					var correctColor = previewTexture.GetPixel(i, j);
					if (SameColor(color, correctColor))
						correct++;
				}
			}

			var result = correct / sum + 0.1f;
			result = Mathf.Clamp01(result);
			return result;
		}

		private static bool SameColor(Color c1, Color c2)
		{
			var r1 = (int) (c1.r * 50);
			var g1 = (int) (c1.g * 50);
			var b1 = (int) (c1.b * 50);

			var r2 = (int) (c2.r * 50);
			var g2 = (int) (c2.g * 50);
			var b2 = (int) (c2.b * 50);

			return Mathf.Abs(r1 - r2) < 10 && Mathf.Abs(g1 - g2) < 10 && Mathf.Abs(b1 - b2) < 10;
		}

		public override float GetScore()
		{
			return _fillScore;
		}

		public override bool IsOptionActive(int index)
		{
			return _selectIndex == index;
		}

		public override int OptionCount => options.Count;

		public override bool IsOptionCorrect(int index)
		{
			return Gameplay.Level.LevelStype switch
			{
				LevelStyle.FreeStyle when correctIndex == index || optionAds == index => true,
				_ => correctIndex == index
			};
		}

		public FillTuning FillTuning { get; private set; }

		private void Finish()
		{
			HideToolTip();
			Utils.Instance.VibrateMedium();
			Finished = true;
			_fillScore = GetFillScore();

			if (_preview)
			{
				renderCamera.gameObject.SetActive(false);
			}
			else
			{
				DOVirtual.DelayedCall(FillTuning.LoangTime, () => { renderCamera.gameObject.SetActive(false); });
			}

			fillSurface.GetComponent<Collider>().enabled = false;
			Surface.Carafe.Hide(Gameplay.Instance.transform);
			_hint.gameObject.SetActive(false);
			Gameplay.Instance.HideCompleteButton();
			if (Gameplay.Level.LevelStype == LevelStyle.FreeStyle)
			{
				Gameplay.Instance.HideLevelFreeProgress();
				totalDistance = 0;
				_pointsToCountProgress.Clear();
				if(_selectIndex==optionAds)
					_fillScore = 1;
			}
		}

		private static Sprite CreateSpriteFromTexture(Texture2D texture)
		{
			var sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f), 1024);
			return sprite;
		}

		public void EditorPreviewRefresh()
		{
			previewEditor.sprite = CreateSpriteFromTexture(drawTexture);
			previewEditor.color = CorrectOption.BlendColor;
		}

		private void OnValidate()
		{
#if UNITY_EDITOR
			UnityEditor.EditorApplication.delayCall += () =>
			{
				if (this == null || gameObject == null || UnityEditor.EditorApplication.isPlaying || drawTexture == null)
					return;
				previewEditor.color = CorrectOption.BlendColor;
				previewEditor.sprite = CreateSpriteFromTexture(drawTexture);
				previewEditor.sortingOrder = FillOrder;
			};
#endif
		}

		private static Texture2D ToTexture2D(RenderTexture rTex)
		{
			var tex = new Texture2D(1024, 1024, TextureFormat.ARGB32, false);
			RenderTexture.active = rTex;
			tex.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0, 0);
			tex.Apply();
			RenderTexture.active = null;
			return tex;
		}

		public override void CompleteFX(Action action)
		{
			Gameplay.Instance.Carafe.CompleteFX(action);
		}
		private float DistanceToLastPoint(Vector3 point,List<Vector3> p)
		{
			return !p.Any() ? Mathf.Infinity : Vector3.Distance(p.Last(), point);
		}
		private void SetPercentProgress()
		{
			if (!Surface.Carafe.endAnimOnStart)
				return;
			if (totalDistance >= maxDistance)
				return;
			if (DistanceToLastPoint(Gameplay.Instance.Carafe.transform.GetChild(0).position, _pointsToCountProgress) > _minPathDistance)
			{
				if (DistanceToLastPoint(Gameplay.Instance.Carafe.transform.GetChild(0).position, _pointsToCountProgress) != Mathf.Infinity)
					totalDistance += DistanceToLastPoint(Gameplay.Instance.Carafe.transform.GetChild(0).position, _pointsToCountProgress);
				_pointsToCountProgress.Add(Gameplay.Instance.Carafe.transform.GetChild(0).position);
			}
			Gameplay.Instance.SetFillAmountFreeProgress(totalDistance / maxDistance);
		}
	}
}