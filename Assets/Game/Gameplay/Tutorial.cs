﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Nama;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
	[SerializeField] private Text textDetail;
	[SerializeField] private GameObject buttonDemo1;
	[SerializeField] private GameObject buttonDemo2;
	[SerializeField] private Image hand1;
	[SerializeField] private Image hand2;
	[SerializeField] int indexTutorial;
	
	public static bool isShowed = false;
	private Animator controlAnim;

	public Animator ControlAnim => gameObject.GetComponent<Animator>();
	
	private void OnEnable()
	{
		var anim = DOTween.Sequence();
		anim.Append(textDetail.transform.DOScale(Vector3.one * 1.02f, 0.5f));
		anim.Append(textDetail.transform.DOScale(Vector3.one, 0.5f));
		anim.SetLoops(-1).Play();
	}

	public void Hide()
	{
		gameObject.transform.localScale = Vector3.zero;
		Profile.Instance.TutorialActived[indexTutorial] = true;
	}
	
	public void SetSpriteButton1(GameObject target)
	{
		buttonDemo1.transform.GetComponent<Image>().sprite = target.transform.GetChild(0).GetComponent<Image>().sprite;
		buttonDemo1.transform.GetComponent<Image>().SetNativeSize();

		if (target.transform.GetChild(0).childCount > 0)
		{
			buttonDemo1.transform.GetChild(0).GetComponent<Image>().sprite =
				target.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite;
			buttonDemo1.transform.GetChild(0).GetComponent<Image>().color =
				target.transform.GetChild(0).GetChild(0).GetComponent<Image>().color;
			if (target.transform.GetChild(0).childCount>1)
			{
				buttonDemo1.transform.GetChild(1).GetComponent<Image>().sprite =
					target.transform.GetChild(0).GetChild(1).GetComponent<Image>().sprite;
				buttonDemo1.transform.GetChild(1).GetComponent<Image>().color =
					target.transform.GetChild(0).GetChild(1).GetComponent<Image>().color;
			}
		}
	}

	public void SetSpriteButton2(GameObject target)
	{
		buttonDemo2.transform.GetComponent<Image>().sprite = target.transform.GetChild(0).GetComponent<Image>().sprite;
		buttonDemo2.transform.GetComponent<Image>().SetNativeSize();

		buttonDemo2.transform.GetChild(0).GetComponent<Image>().sprite =
			target.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite;
		buttonDemo2.transform.GetChild(0).GetComponent<Image>().color =
			target.transform.GetChild(0).GetChild(0).GetComponent<Image>().color;
		if (target.transform.GetChild(0).childCount>1)
		{
			buttonDemo2.transform.GetChild(1).GetComponent<Image>().sprite =
				target.transform.GetChild(0).GetChild(1).GetComponent<Image>().sprite;
			buttonDemo2.transform.GetChild(1).GetComponent<Image>().color =
				target.transform.GetChild(0).GetChild(1).GetComponent<Image>().color;
		}
	}

	public void SetPosButton1(Vector3 pos)
	{
		buttonDemo1.transform.position = pos;
	}
	
	public void SetPosButton2(Vector3 pos)
	{
		buttonDemo2.transform.position = pos;
	}
	
	public void SetPosHand1(Vector3 pos)
	{
		hand1.transform.position = pos;
	}
	
	public void SetPosHand2(Vector3 pos)
	{
		hand2.transform.position = pos;
	}

	public void DisableOption2()
	{
		hand2.gameObject.SetActive(false);
		buttonDemo2.gameObject.SetActive(false);
	}

	public void ActiveOption2()
	{
		hand2.gameObject.SetActive(true);
		buttonDemo2.gameObject.SetActive(true);
	}

	public void SetPosText( Vector3 pos)
	{
		textDetail.transform.position = pos;
	}
}
