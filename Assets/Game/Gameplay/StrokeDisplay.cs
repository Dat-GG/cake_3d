
using UnityEngine;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEditor;

namespace Nama
{
	public class StrokeDisplay : MonoBehaviour
	{
		[DllImport(Funzilla.Plugin.Name)]
		private static extern int makeStrokeMesh(
			Vector2[] points,
			int nPoints,
			float maxLength,
			int startSeg,
			float startSegOffset,
			float startOffset,
			float startX,
			float startY,
			float surfaceRadius,
			float[] widths,
			int nWidths,
			Vector2[] tipPoints,
			Vector2[] tipNormals,
			float[] tipUvs,
			int tipVertexCount,
			int[] tipSegs,
			int tipSegCount,
			float tipSize,
			float twist,
			float twistUv,
			float current,
			float fallTime,
			float scaleTime);

		[DllImport(Funzilla.Plugin.Name)] private static extern int getMeshIndexCount();
		[DllImport(Funzilla.Plugin.Name)] private static extern void getMesh(
			Vector3[] positions,
			Vector3[] normals,
			Vector2[] uvs,
			int[] indices);

		[SerializeField] private MeshFilter meshFilter;
		[SerializeField] private MeshRenderer meshRenderer;
		public MeshRenderer MeshRenderer => meshRenderer;
		private float _current;
		private float _fallTime;
		private float _scaleTime;
		private bool _effectEnabled = true;
		
		private int _tipVertexCount;
		private float _tipSize;
		private int[] _tipSegs;
		private Vector2[] _tipPoints;
		private Vector2[] _tipNormals;
		private float[] _tipUvs;
		private float[] _widths;
		private float _twist;
		private float _twistUv;
		private PolylinePosition _startPosition;
		private float _surfaceRadius;
		private Brush _brush;
		private Polyline _poly;
		private Vector2[] _points;

		public void Init(
			Brush brush,
			Polyline poly,
			PolylinePosition start,
			float surfaceRadius,
			Color[] colors,
			Texture2D gradient,
			Material material)
		{
			meshFilter.sharedMesh = null;
			_brush = brush;
			_poly = poly;
			_points = poly.Points.ToArray();
			_current = start.Offset;
			_twist = brush.Twist;
			_twistUv = _twist == 0 ? 2.0f : 0.0f;
			_startPosition = start;
			_surfaceRadius = surfaceRadius;

			meshRenderer.sharedMaterial = new Material(material)
			{
				mainTexture = gradient ? gradient : BrushTexture.Instance.Bake(colors)
			};
			if (gradient) meshRenderer.sharedMaterial.mainTextureScale = new Vector2(1.0f / _poly.Length, 1.0f);
			_tipSize = -100;
			_tipVertexCount = 0;
			_tipSegs = new int[brush.Tip.segments.Length * 2];
			var tipPoints = new List<Vector2>(100);
			var tipNormals = new List<Vector2>(100);
			var tipUvs = new List<float>(100);
			for (var i = 0; i < brush.Tip.segments.Length; i++)
			{
				var segment = brush.Tip.segments[i];
				_tipSegs[2 * i + 0] = _tipVertexCount;
				_tipSegs[2 * i + 1] = segment.points.Length;
				_tipVertexCount += segment.points.Length;
				for (var j = 0; j < segment.points.Length; j++)
				{
					tipPoints.Add(segment.points[j]);
					tipNormals.Add(segment.normals[j]);
					tipUvs.Add(segment.uvs[j]);
					if (_tipSize < segment.points[j].y) _tipSize = segment.points[j].y;
				}
			}
			_tipSize *= 0.5f;

			_tipPoints = tipPoints.ToArray();
			_tipNormals = tipNormals.ToArray();
			_tipUvs = tipUvs.ToArray();

			Funzilla.Plugin.Validate();

			_widths = brush.Widths;
		}

		public void Advance(float delta)
		{
			_current += delta;
			enabled = true;
			if (!_effectEnabled) return;
			_fallTime = BrushConfig.Instance.FallTime;
			_scaleTime = BrushConfig.Instance.ScaleTime;
		}

		public void OverGuideline()
		{
			meshRenderer.sharedMaterial.renderQueue = 4000;
		}

		public void UnderGuideLine()
		{
			meshRenderer.sharedMaterial.renderQueue = 2000;
		}

		private void UpdateMesh()
		{
			meshFilter.sharedMesh = MakeMesh();
		}

		private void EnableEffect(bool active)
		{
			_effectEnabled = active;
		}

		public void ShowPreview(float length)
		{
			enabled = false;
			_fallTime = 0;
			_scaleTime = 0;
			EnableEffect(false);
			Advance(length);
			UpdateMesh();
		}

		public void EnableShadow(bool active)
		{
			meshRenderer.shadowCastingMode = active ?
				UnityEngine.Rendering.ShadowCastingMode.On :
				UnityEngine.Rendering.ShadowCastingMode.Off;
		}

		private void Update()
		{
			if (_poly == null) return;

			if (_fallTime > 0)
			{
				_fallTime -= Time.smoothDeltaTime;
				if (_fallTime < 0)
				{
					_fallTime = 0;
				}
			}

			if (_scaleTime > 0)
			{
				_scaleTime -= Time.smoothDeltaTime;
				if (_scaleTime < 0)
				{
					_scaleTime = 0;
				}
			}

			if (_scaleTime == 0 && _fallTime == 0)
			{
				enabled = false;
			}

			UpdateMesh();
		}

		private Mesh MakeMesh()
		{
			var nVertices = 
				makeStrokeMesh(
					_points,
					_points.Length,
					_poly.Length,
					_startPosition.Seg,
					_startPosition.SegOffset,
					_startPosition.Offset,
					_startPosition.Pos.x,
					_startPosition.Pos.y,
					_surfaceRadius,
					_widths,
					_widths.Length,
					_tipPoints,
					_tipNormals,
					_tipUvs,
					_tipVertexCount,
					_tipSegs,
					_brush.Tip.segments.Length,
					_tipSize,
					_twist,
					_twistUv,
					_current,
					_fallTime,
					_scaleTime);
			return GetMesh(nVertices);
		}

		public static StrokeDisplay Create(Transform transform)
		{
#if UNITY_EDITOR
			var prefab = AssetDatabase.LoadAssetAtPath<StrokeDisplay>(
				"Assets/Game/Gameplay/StrokeDisplay.prefab");

			var stroke = Instantiate(prefab, transform);
#else
			var stroke = AssetManager.Instance.NewStrokeDisplay(transform);
#endif
			return stroke;
		}

		internal static Mesh GetMesh(int nVertices)
		{
			if (nVertices <= 0) return null;
			var positions = new Vector3[nVertices];
			var normals = new Vector3[nVertices];
			var uvs = new Vector2[nVertices];
			var nIndices = getMeshIndexCount();
			var indices = new int[nIndices];
			getMesh(positions, normals, uvs, indices);
			var mesh = new Mesh()
			{
				vertices = positions,
				normals = normals,
				triangles = indices,
				uv = uvs,
			};
			return mesh;
		}

		
	}
}