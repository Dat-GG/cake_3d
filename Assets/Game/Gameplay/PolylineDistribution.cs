﻿using UnityEngine;
using System;

namespace Nama
{
	/// <summary>
	/// Polyline section whose start position and length will be determined by
	/// its predefined weight
	/// </summary>
	[Serializable] public class PolylineSectionByWeight
	{
		[Range(0.1f, 1.0f)] [SerializeField] private float weight = 1.0f;
		public PolylinePosition Start { get; private set; }
		public PolylinePosition Finish { get; private set; }
		public float Length { get; private set; }
		public float Weight => weight;

		public void Init(PolylinePosition start, PolylinePosition finish, float length)
		{
			Start = start;
			Finish = finish;
			Length = length;
		}
	}

	/// <summary>
	/// Abstract of polyline section distribution
	/// </summary>
	public interface IPolylineSectionDistribution
	{
		PolylineSectionByWeight GetSection(int index);
		int SectionCount { get; }
	} // class
} // namespace