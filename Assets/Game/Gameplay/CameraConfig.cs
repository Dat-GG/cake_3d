﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CameraConfig", menuName = "Tools/CameraConfig")]
public class CameraConfig : ScriptableObject
{
	private static CameraConfig _instance;
	public static CameraConfig Instance
	{
		get
		{
			if (_instance == null)
				_instance = Resources.Load<CameraConfig>("CameraConfig");

			return _instance;
		}
	}


	public float cameraRange = 0.5f;
	[Range(0.1f, 10)]
	public float cameraMoveVelocity = 2f;
	public float resetCameraSpeed = 10;
	public float minAreaRange = 1f;
	public float areaRange = 1.8f;
	public float BlockRangeX = 2.8f;
	public float BlockRangeZ = 2.8f;
}
