using System;
using DG.Tweening;
using Funzilla;
using UnityEngine;
using UnityEditor;
#if UNITY_EDITOR
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;
#endif

namespace Nama
{
	[ExecuteInEditMode]
	public class Cake : CakeBase
	{
		[SerializeField] private CakeSurface[] surfaces;
		[SerializeField] private float size = 3.0f;

		[SerializeField] private Mesh cakeMesh;
		[SerializeField] private Mesh colliderMesh;
		[SerializeField] private Mesh edgeMesh;
		[SerializeField] private Mesh mirrorGlazeMesh;
		[SerializeField] private Vector2 cakeTextureScale = new Vector2(1, 1);
		[SerializeField] private Vector2 cakeTextureOffset = new Vector2(0, 0);
		[SerializeField] private Vector2 toppingTextureScale = new Vector2(6, 1);
		[SerializeField] private Vector2 toppingTextureOffset = new Vector2(0, 0);
		[SerializeField] private Texture cakeTexture;
		[SerializeField] private Color cakeColor = Color.white;
		[SerializeField] private Texture toppingTexture;
		[SerializeField] private Color toppingColor = Utils.ColorFromUint(0xE5E5E5);

		[Space] [SerializeField] private Texture normalCakeTexture;
		[SerializeField] private Color normalCakeColor = Color.white;
		[SerializeField] private Texture normalToppingTexture;
		[SerializeField] private Color normalToppingColor = Color.white;
		[SerializeField] private Transform layerTransform;
		[SerializeField] private float diameter = 5;
		[SerializeField] private float initialAngle;
		[SerializeField] private bool sprinklePhysics;
		[SerializeField] private TextAsset velocityMap;

		public CakeSurface[] Surfaces => surfaces;
		public float InitialAngle => initialAngle;
		public Mesh MirrorGlazeMesh => mirrorGlazeMesh;

		public float Size => size;

		private int _strokeCount;
		private int _sprinkleCount;
		private int _fillCount;
		private int _stencilCount;
		private int _mirrorCount;
		private static readonly int CakeTex = Shader.PropertyToID("_CakeTex");
		private static readonly int CakeColor = Shader.PropertyToID("_CakeColor");
		private static readonly int ToppingTex = Shader.PropertyToID("_ToppingTex");
		private static readonly int ToppingColor = Shader.PropertyToID("_ToppingColor");
		private static readonly int Diameter = Shader.PropertyToID("_Diameter");

		public CakeRenderer CakeRenderer { get; private set; }

		public float Match { get; private set; }

		public bool CalculateMatch()
		{
			if (surfaces.Length <= 0) return false;
			float totalMatch = 0;

			foreach (var surface in surfaces)
			{
				foreach (var step in surface.Steps)
				{
					switch (step)
					{
						case StrokeManual _:
							_strokeCount++;
							break;
						case SprinkleManual _:
							_sprinkleCount++;
							break;
						case FillManual _:
							_fillCount++;
							break;
						case StencilManual _:
							_stencilCount++;
							break;
						case MirrorGlazeManual _:
							_mirrorCount++;
							break;
					}
				}
			}

			var typeCount = 0;
			if (_strokeCount > 0) typeCount++;
			if (_sprinkleCount > 0) typeCount++;
			if (_fillCount > 0) typeCount++;
			if (_stencilCount > 0) typeCount++;
			if (_mirrorCount > 0) typeCount++;

			foreach (var surface in surfaces)
			{
				foreach (var step in surface.Steps)
				{
					var c = NumberCount(step);
					if (c == 0) continue;
					var a = step.GetScore();
					totalMatch += a  / c;
				}
			}

			Match = Mathf.Clamp(totalMatch / typeCount, 0, 1);
			return true;
		}

		private int NumberCount(DecorationStep step)
		{
			return step switch
			{
				StrokeManual _ => _strokeCount,
				SprinkleManual _ => _sprinkleCount,
				FillManual _ => _fillCount,
				StencilManual _ => _stencilCount,
				MirrorGlazeManual _ => _mirrorCount,
				PhotoManual _ => 0,
				_ => 0
			};
		}

		private void UpdateColors(CakeType cakeType)
		{
#if UNITY_EDITOR
			if (this == null || gameObject == null || EditorUtility.IsPersistent(gameObject))
			{
				return;
			}

			var level = GetComponentInParent<Level>();
			var glitter = level != null && level.GlitterCake;
			var glitterMaterialName = glitter ? "cakeGlitter" : "cake";
			var prefab = AssetDatabase.LoadAssetAtPath<CakeRenderer>(
				"Assets/Game/Gameplay/CakeRenderer.prefab");
			var cakeMaterial = AssetDatabase.LoadAssetAtPath<Material>(
				$"Assets/Game/Models/{glitterMaterialName}.mat");
			var mirrorGlazeMaterial = AssetDatabase.LoadAssetAtPath<Material>(
				"Assets/Game/Gameplay/MirrorGlaze/MirrorGlaze.mat");
			if (CakeRenderer == null)
			{
				CakeRenderer = Instantiate(prefab, layerTransform);
				CakeRenderer.MeshCollider.sharedMesh = colliderMesh;
				if (!EditorApplication.isPlayingOrWillChangePlaymode)
				{
					CakeRenderer.gameObject.hideFlags = HideFlags.HideAndDontSave;
				}
			}
#else
			var level = GetComponentInParent<Level>();
			var glitter = level != null && level.GlitterCake;
			var prefab = AssetManager.Instance.CakeRenderer;
			var cakeMaterial = glitter ? AssetManager.Instance.CakeGlitterMaterial : AssetManager.Instance.CakeMaterial;
			var mirrorGlazeMaterial = AssetManager.Instance.MirrorGlazeMaterial;
			CakeRenderer = Instantiate(prefab, layerTransform);
			CakeRenderer.MeshCollider.sharedMesh = colliderMesh;
#endif
			CakeRenderer.CakeMesh.sharedMesh = cakeMesh;
			CakeRenderer.EdgeMesh.sharedMesh = edgeMesh;
			CakeRenderer.EdgeMesh.gameObject.SetActive(false);
			var stencil = surfaces[0].GetComponentInChildren<StencilManual>();
			var mirror = surfaces[0].GetComponentInChildren<MirrorGlazeManual>();
			if (stencil != null)
			{
				var material = CakeRenderer.Cake.sharedMaterial;
				material = new Material(material);
				CakeRenderer.Cake.sharedMaterial = material;
				material.SetFloat(Diameter, diameter);

				material = CakeRenderer.Mask.sharedMaterial;
				material = new Material(material);
				material.SetFloat(Diameter, diameter);
				CakeRenderer.Mask.sharedMaterial = material;
				CakeRenderer.MaskMesh.sharedMesh = cakeMesh;
			}
			else if (mirror != null)
			{
				CakeRenderer.CakeMesh.sharedMesh = mirrorGlazeMesh;
				var material = new Material(mirrorGlazeMaterial)
				{
					mainTexture = cakeTexture
				};
				CakeRenderer.Cake.sharedMaterial = material;
			}
			else
			{
				var material = new Material(cakeMaterial);
				CakeRenderer.Cake.sharedMaterial = material;
				var cakeTexture1 = cakeTexture;
				var cakeColor1 = cakeColor;
				var toppingTexture1 = toppingTexture;
				var toppingColor1 = toppingColor;

				switch (cakeType)
				{
					case CakeType.Origin:
						break;
					case CakeType.Normal:
						cakeTexture1 = normalCakeTexture;
						cakeColor1 = normalCakeColor;
						toppingTexture1 = normalToppingTexture;
						toppingColor1 = normalToppingColor;
						break;
					case CakeType.Vip:
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(cakeType), cakeType, null);
				}

				material.SetTexture(CakeTex, cakeTexture1);
				material.SetColor(CakeColor, cakeColor1);
				material.SetTextureOffset(CakeTex, cakeTextureOffset);
				material.SetTextureScale(CakeTex, cakeTextureScale);

				material.SetTexture(ToppingTex, toppingTexture1);
				material.SetColor(ToppingColor, toppingColor1);
				material.SetTextureOffset(ToppingTex, toppingTextureOffset);
				material.SetTextureScale(ToppingTex, toppingTextureScale);
			}
		}

		public int SurfaceIndex { get; private set; }
		public bool Finished => surfaces.Length <= 0 && SurfaceIndex < 0 || SurfaceIndex >= surfaces.Length;

		public CakeSurface CurrentSurface => Finished ? null : surfaces[SurfaceIndex];
		public CakeSurface PrevSurface => surfaces[Mathf.Clamp(SurfaceIndex - 1, 0, surfaces.Length - 1)];
		public bool HasKey { get; private set; }
		private bool _started;

		public void Init(bool preview, CakeType type)
		{
			UpdateColors(type);
			SurfaceIndex = 0;
			for (var i = 0; i < surfaces.Length; i++)
			{
				var addKey = !preview && i == 0 && type != CakeType.Vip;
				surfaces[i].Init(preview, addKey);
				surfaces[i].gameObject.SetActive(preview);
				if (addKey && surfaces[0].HasKey)
				{
					HasKey = true;
				}
			}
		}

		public void Begin()
		{
			_started = true;
			if (SurfaceIndex < surfaces.Length)
			{
				BeginSurface(SurfaceIndex);
			}
			SprinkleObject.KinematicEnabled = !sprinklePhysics;
			Plugin.setLiquidVelocityMap(velocityMap ? velocityMap.bytes : null);
		}

		public void DoFrame()
		{
			if (!_started || SurfaceIndex >= surfaces.Length)
			{
				return;
			}

			var surface = surfaces[SurfaceIndex];
			surface.DoFrame();

			if (!surface.Finished)
			{
				return;
			}

			while (SurfaceIndex < surfaces.Length && surfaces[SurfaceIndex].Finished)
			{
				SurfaceIndex++;
			}

			if (SurfaceIndex < surfaces.Length)
			{
				BeginSurface(SurfaceIndex);
			}
		}

		private static void Switch(Transform cameraTransform, Vector3 lightRotation, float duration = 1.0f)
		{
			if (Gameplay.Instance.isPhoto)
				return;
			Gameplay.Instance.CameraController.Move(cameraTransform, duration);
			Gameplay.Instance.Light.transform.DORotate(lightRotation, 0.5f).SetEase(Ease.InOutCubic);
		}

		private void BeginSurface(int index)
		{
			var surface = surfaces[index];
			surface.gameObject.SetActive(true);
			surface.Begin(Gameplay.Instance.PastryBag, Gameplay.Instance.Carafe);
			Switch(surface.CameraTransform, surface.LightTransform.localEulerAngles);
		}

#if UNITY_EDITOR
		private void OnValidate()
		{
			if (EditorApplication.isPlayingOrWillChangePlaymode) return;
			EditorApplication.delayCall += () =>
			{
				Fix();
				UpdateColors(CakeType.Origin);
			};
		}

		private void Fix()
		{
			if (layerTransform != null || this == null || gameObject == null || EditorUtility.IsPersistent(gameObject))
			{
				return;
			}

			var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
			if (prefabStage == null)
			{
				return;
			}

			layerTransform = new GameObject().transform;
			layerTransform.gameObject.name = "LayerTransform";
			layerTransform.SetParent(transform, false);
			layerTransform.localPosition = Vector3.zero;
			layerTransform.localRotation = Quaternion.identity;
			layerTransform.localScale = Vector3.one;
			EditorSceneManager.MarkSceneDirty(prefabStage.scene);
		}
#endif
		public void UpdateColorsOnSurfaceSelect(CakeType cakeType,Cake cakeSelect)
		{
#if UNITY_EDITOR
			if (this == null || gameObject == null || EditorUtility.IsPersistent(gameObject))
			{
				return;
			}

			var level = GetComponentInParent<Level>();
			var glitter = level != null && level.GlitterCake;
			var glitterMaterialName = glitter ? "cakeGlitter" : "cake";
			var cakeMaterial = AssetDatabase.LoadAssetAtPath<Material>(
				$"Assets/Game/Models/{glitterMaterialName}.mat");
			var mirrorGlazeMaterial = AssetDatabase.LoadAssetAtPath<Material>(
				"Assets/Game/Gameplay/MirrorGlaze/MirrorGlaze.mat");
			CakeRenderer.MeshCollider.sharedMesh = cakeSelect.colliderMesh;
			if (!EditorApplication.isPlayingOrWillChangePlaymode)
			{
				CakeRenderer.gameObject.hideFlags = HideFlags.HideAndDontSave;
			}
#else
			var level = GetComponentInParent<Level>();
			var glitter = level != null && level.GlitterCake;
			var cakeMaterial = glitter ? AssetManager.Instance.CakeGlitterMaterial : AssetManager.Instance.CakeMaterial;
			var mirrorGlazeMaterial = AssetManager.Instance.MirrorGlazeMaterial;
			CakeRenderer.MeshCollider.sharedMesh = cakeSelect.colliderMesh;
#endif
			CakeRenderer.CakeMesh.sharedMesh = cakeSelect.cakeMesh;
			CakeRenderer.EdgeMesh.sharedMesh = cakeSelect.edgeMesh;
			CakeRenderer.EdgeMesh.gameObject.SetActive(false);
			var stencil = surfaces[0].GetComponentInChildren<StencilManual>();
			var mirror = surfaces[0].GetComponentInChildren<MirrorGlazeManual>();
			if (stencil != null)
			{
				var material = CakeRenderer.Cake.sharedMaterial;
				material = new Material(material);
				CakeRenderer.Cake.sharedMaterial = material;
				material.SetFloat(Diameter, cakeSelect.diameter);

				material = CakeRenderer.Mask.sharedMaterial;
				material = new Material(material);
				material.SetFloat(Diameter, cakeSelect.diameter);
				CakeRenderer.Mask.sharedMaterial = material;
				CakeRenderer.MaskMesh.sharedMesh = cakeSelect.cakeMesh;
			}
			else if (mirror != null)
			{
				CakeRenderer.CakeMesh.sharedMesh = cakeSelect.mirrorGlazeMesh;
				CakeRenderer.MirrorCollider.sharedMesh = cakeSelect.MirrorGlazeMesh;
				 Plugin.setLiquidVelocityMap(cakeSelect.velocityMap ? cakeSelect.velocityMap.bytes : null);
			}
			else
			{
				var material = new Material(cakeMaterial);
				CakeRenderer.Cake.sharedMaterial = material;
				var cakeTexture1 = cakeSelect.cakeTexture;
				var cakeColor1 = cakeSelect.cakeColor;
				var toppingTexture1 = cakeSelect.toppingTexture;
				var toppingColor1 = cakeSelect.toppingColor;

				switch (cakeType)
				{
					case CakeType.Origin:
						break;
					case CakeType.Normal:
						cakeTexture1 = cakeSelect.normalCakeTexture;
						cakeColor1 = cakeSelect.normalCakeColor;
						toppingTexture1 = cakeSelect.normalToppingTexture;
						toppingColor1 = cakeSelect.normalToppingColor;
						break;
					case CakeType.Vip:
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(cakeType), cakeType, null);
				}

				material.SetTexture(CakeTex, cakeTexture1);
				material.SetColor(CakeColor, cakeColor1);
				material.SetTextureOffset(CakeTex, cakeSelect.cakeTextureOffset);
				material.SetTextureScale(CakeTex, cakeSelect.cakeTextureScale);

				material.SetTexture(ToppingTex, toppingTexture1);
				material.SetColor(ToppingColor, toppingColor1);
				material.SetTextureOffset(ToppingTex, cakeSelect.toppingTextureOffset);
				material.SetTextureScale(ToppingTex, cakeSelect.toppingTextureScale);
			}
		}
		public void MakecakeForPreview(CakeType type,bool mirror,bool stencil,bool glitter)
		{
			#if UNITY_EDITOR
			if (this == null || gameObject == null || EditorUtility.IsPersistent(gameObject))
			{
				return;
			}
			var glitterMaterialName = glitter ? "cakeGlitter" : "cake";
			var prefab = AssetDatabase.LoadAssetAtPath<CakeRenderer>(
				"Assets/Game/Gameplay/CakeRenderer.prefab");
			var cakeMaterial = AssetDatabase.LoadAssetAtPath<Material>(
				$"Assets/Game/Models/{glitterMaterialName}.mat");
			var mirrorGlazeMaterial = AssetDatabase.LoadAssetAtPath<Material>(
				"Assets/Game/Gameplay/MirrorGlaze/MirrorGlaze.mat");
			if (CakeRenderer == null)
			{
				CakeRenderer = Instantiate(prefab, layerTransform);
				if (!EditorApplication.isPlayingOrWillChangePlaymode)
				{
					CakeRenderer.gameObject.hideFlags = HideFlags.HideAndDontSave;
				}
			}
#else
			var cakeMaterial = glitter ? AssetManager.Instance.CakeGlitterMaterial : AssetManager.Instance.CakeMaterial;
			var mirrorGlazeMaterial = AssetManager.Instance.MirrorGlazeMaterial;
			var prefab = AssetManager.Instance.CakeRenderer;
			CakeRenderer = Instantiate(prefab, layerTransform);
			CakeRenderer.MeshCollider.sharedMesh = colliderMesh;
#endif
			CakeRenderer.CakeMesh.sharedMesh = cakeMesh;
			CakeRenderer.EdgeMesh.sharedMesh = edgeMesh;
			CakeRenderer.EdgeMesh.gameObject.SetActive(false);
			if (stencil)
			{
				var material = CakeRenderer.Cake.sharedMaterial;
				material = new Material(material);
				CakeRenderer.Cake.sharedMaterial = material;
				material.SetFloat(Diameter, diameter);

				material = CakeRenderer.Mask.sharedMaterial;
				material = new Material(material);
				material.SetFloat(Diameter, diameter);
				CakeRenderer.Mask.sharedMaterial = material;
				CakeRenderer.MaskMesh.sharedMesh = cakeMesh;
			}
			else if (mirror)
			{
				CakeRenderer.CakeMesh.sharedMesh = mirrorGlazeMesh;
				var material = new Material(mirrorGlazeMaterial)
				{
					mainTexture = cakeTexture
				};
				CakeRenderer.Cake.sharedMaterial = material;
				Texture2D _liquidTexture;
				int Liquid = Shader.PropertyToID("_Liquid");
				_liquidTexture = new Texture2D(
					Funzilla.Plugin.LiquidResolution,
					Funzilla.Plugin.LiquidResolution,
					TextureFormat.RGBA32, false, true)
				{
					anisoLevel = 0,
					wrapMode = TextureWrapMode.Clamp,
				};
				CakeRenderer.Cake.sharedMaterial.SetTexture(Liquid, _liquidTexture);
				_liquidTexture.SetPixels32(Funzilla.Plugin.LiquidPixels, 0);
				_liquidTexture.Apply(false);
			}
			else
			{
				var material = new Material(cakeMaterial);
				CakeRenderer.Cake.sharedMaterial = material;
				var cakeTexture1 = cakeTexture;
				var cakeColor1 = cakeColor;
				var toppingTexture1 = toppingTexture;
				var toppingColor1 = toppingColor;

				switch (type)
				{
					case CakeType.Origin:
						break;
					case CakeType.Normal:
						cakeTexture1 = normalCakeTexture;
						cakeColor1 = normalCakeColor;
						toppingTexture1 = normalToppingTexture;
						toppingColor1 = normalToppingColor;
						break;
					case CakeType.Vip:
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(type), type, null);
				}

				material.SetTexture(CakeTex, cakeTexture1);
				material.SetColor(CakeColor, cakeColor1);
				material.SetTextureOffset(CakeTex, cakeTextureOffset);
				material.SetTextureScale(CakeTex, cakeTextureScale);

				material.SetTexture(ToppingTex, toppingTexture1);
				material.SetColor(ToppingColor, toppingColor1);
				material.SetTextureOffset(ToppingTex, toppingTextureOffset);
				material.SetTextureScale(ToppingTex, toppingTextureScale);
			}

		}

		public void SetTextureForPreviewSurface(Texture texture )
		{
			cakeTexture = texture;
		}

		public Texture CakeTexture => cakeTexture;

		public bool Stencil()
		{
			return surfaces[0].GetComponentInChildren<StencilManual>();
		}
		public bool Mirror()
		{
			return surfaces[0].GetComponentInChildren<MirrorGlazeManual>();
		}

		public bool Glitter()
		{
			var level = GetComponentInParent<Level>();
			var glitter = level != null && level.GlitterCake;
			return glitter;
		}
	}

	[Serializable]
	public enum CakeType
	{
		Origin,
		Normal,
		Vip
	}
}