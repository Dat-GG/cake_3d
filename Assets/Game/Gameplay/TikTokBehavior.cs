
using DG.Tweening;
using UnityEngine;

namespace Nama
{
	public class TikTokBehavior : MonoBehaviour
	{
		[SerializeField] int segmentCount = 4;
		[Range(0.0f, 10.0f)] [SerializeField] float duration = 0.3f;
		[Range(0.0f, 10.0f)] [SerializeField] float delay = 0.5f;
		[SerializeField] Ease ease = Ease.OutBack;

		Sequence tween;
		private void Awake()
		{
			var step = 360f / segmentCount;
			tween = DOTween.Sequence();
			for (int i = 0; i < segmentCount; i++)
			{
				tween.Append(
					transform.DOLocalRotate(
						new Vector3(0, 0, step * (i+1)),
						duration,
						RotateMode.FastBeyond360)
					.SetDelay(delay)
					.SetEase(ease)); ;
			}
			tween.SetLoops(-1);
			tween.Play();
		}

		private void OnDestroy()
		{
			tween.Kill();
		}
	}
}