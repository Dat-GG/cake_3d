
namespace Nama
{
	class GameStateManager
	{
		public GameState CurrentState { get; private set; } = null;

		public void ChangeState(GameState nextState)
		{
			if (CurrentState == nextState)
			{
				return;
			}
			if (CurrentState != null)
			{
				CurrentState.Exit();
			}
			CurrentState = nextState;
			if (CurrentState != null)
			{
				CurrentState.Enter();
			}
		}

		public void Update()
		{
			if (CurrentState != null)
			{
				CurrentState.Execute();
			}
		}
	}
}
