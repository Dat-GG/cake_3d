
using UnityEngine;
using DG.Tweening;

namespace Nama
{
	class LoseState : GameState
	{
		public override void Enter()
		{
			Analytics.Instance.LogEvent(string.Format("level_{0}_failed", Profile.Instance.Level));
			Profile.Instance.PlayCount++;
			if (Preference.Instance.VibrationOn)
			{
				Utils.Instance.VibrateMedium();
			}

			DOVirtual.DelayedCall(1.5f, delegate
			{
				SceneManager.Instance.CloseScene(SceneID.Home);
				SceneManager.Instance.OpenScene(SceneID.Result, false);
			});
			SoundManager.Instance.PlaySFX("game_over");
		}
	}
}
