namespace Nama
{
	internal class PlayState : GameState
	{
		public override void Enter()
		{
			Gameplay.Level?.DoFrame();
			Analytics.Instance.LogEvent($"level_{Profile.Instance.Level}_play");
		}

		public override void Execute()
		{
			Gameplay.Level?.DoFrame();
		}
	}
}

