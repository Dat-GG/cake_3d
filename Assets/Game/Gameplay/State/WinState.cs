namespace Nama
{
	internal class WinState : GameState
	{
		public override void Enter()
		{
			SoundManager.Instance.PlaySFX("success");
			Funzilla.DailyQuestManager.UpdateDay();
			Profile.Instance.PlayCount++;
			if (Gameplay.Level != null && Gameplay.Level.CakeType == CakeType.Vip)
			{
				Profile.Instance.VipLevel++;
				Profile.Instance.PlayVipLevelCurrentTime++;
			}
			else
			{
				Statistics.CollectKey(Profile.Instance.Level);
				Profile.Instance.Level++;
				Profile.Instance.CountShowUpgradeBtn++;
			}
			
			var texture = Gameplay.ToTexture2D(Gameplay.Instance.ResultTexture);
			Profile.Instance.SaveGallery(texture, Gameplay.Level.name);
			Profile.Instance.KeyAmount += Gameplay.Instance.KeyAmount;
			SceneManager.Instance.OpenScene(SceneID.Result, true);
			if (Preference.Instance.VibrationOn)
			{
				Utils.Instance.VibrateMedium();
			}
		}
	}
}
