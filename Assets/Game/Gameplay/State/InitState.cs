using Nama;
using UnityEngine;
using DG.Tweening;

namespace Nama
{
	internal class InitState : GameState
	{
		public override void Enter()
		{
			SceneManager.Instance.HideSplash();
			SceneManager.Instance.HideLoading();
			UIGlobal.HideCoinInfo();
			SceneManager.Instance.OpenScene(SceneID.Home);
			Gameplay.Instance.InitPreview();
			System.GC.Collect();
			Resources.UnloadUnusedAssets();
		}

		public override void Execute()
		{
		}

		public override void Exit()
		{

		}
	}
}
