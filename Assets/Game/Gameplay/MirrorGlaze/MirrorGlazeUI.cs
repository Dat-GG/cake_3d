using DG.Tweening;
using UnityEngine;

namespace Nama
{
	public class MirrorGlazeUI : MonoBehaviour
	{
		[SerializeField] private MirrorGlazeButton[] buttons;
		[SerializeField] private CanvasGroup canvasGroup;

		private static MirrorGlazeUI _instance;
		public static MirrorGlazeUI Instance {
			get
			{
				if (_instance) return _instance;
				_instance = FindObjectOfType<MirrorGlazeUI>();
				return _instance;
			}
		}

		private int optionAds;
		private void Awake()
		{
			_instance = this;
			enabled = false;
		}

		private int SelectIndex { get; set; }

		public void Show(MirrorGlazeOption[] options, int selectIndex,int optionAd)
		{
			if (enabled) return;
			if (options == null || options.Length < 2)
			{
				Hide();
				return;
			}
			Gameplay.Instance.ShowMirrorGlazeUI();
			Gameplay.Instance.ShowOptionsBackground();
			enabled = true;
			gameObject.SetActive(true);
			canvasGroup.DOFade(1.0f, 0.3f);
			for (var i = 0; i < options.Length; i++)
			{
				buttons[i].Init(i, options[i]);
				buttons[i].gameObject.SetActive(true);
				buttons[i].SetActive(false);
				
			}

			SelectIndex = selectIndex;
			buttons[selectIndex].SetActive(true);

			for (var i = options.Length; i < buttons.Length; i++)
			{
				buttons[i].gameObject.SetActive(false);
			}
			if (Gameplay.Level.LevelStype != LevelStyle.FreeStyle)
				return;
			if(wathchedReward)
				return;
			optionAds = optionAd;
			buttons[optionAd].transform.GetChild(2).gameObject.SetActive(true);
		}

		public void Hide()
		{
			if (!enabled) return;
			enabled = false;
			Gameplay.Instance.HideOptionsBackground();
			foreach (var button in buttons) button.Button.interactable = false;
			canvasGroup.DOFade(0, 0.3f).OnComplete(() => gameObject.SetActive(false));
		}

		private bool wathchedReward;
		public void OnOptionSelected(int index)
		{
			buttons[SelectIndex].SetActive(false);
			SelectIndex = index;
			if (Gameplay.Level.LevelStype == LevelStyle.FreeStyle)
			{
				if (index == optionAds&&!wathchedReward)
				{
					Ads.ShowRewardedVideo("Mirrorglaze_optionAds", (result) =>
					{
						if (result != RewardedVideoState.Watched)
						{
							return;
						}
						buttons[SelectIndex].transform.GetChild(2).gameObject.SetActive(false);
						buttons[SelectIndex].SetActive(true);
						Gameplay.Level.CurrentLayer.CurrentSurface.CurrentStep.OnOptionSelected(index);
						wathchedReward = true;
					});
				}
				else
				{
					Gameplay.Level.CurrentLayer.CurrentSurface.CurrentStep.OnOptionSelected(index);
					buttons[SelectIndex].SetActive(true);
				}
			}
			else
			{
				Gameplay.Level.CurrentLayer.CurrentSurface.CurrentStep.OnOptionSelected(index);
				buttons[SelectIndex].SetActive(true);
			}
		}
	}
}

