Shader "Funzilla/MirrorGlaze" {
		
		Properties{
			_MainTex("Main Texture", 2D) = "white" {}
			_Liquid("Liquid Texture", 2D) = "white" {}
			_Pattern("Pattern Texture", 2D) = "white" {}
		}

		SubShader{
			Tags { "RenderType" = "Opaque" }
			LOD 150

			CGPROGRAM
			#pragma surface surf Lambert noforwardadd

			sampler2D _MainTex;
			sampler2D _Liquid;
			sampler2D _Pattern;
			float2 _LightDirection;

			struct Input {
				float2 uv_MainTex;
				float2 uv_Liquid;
			};

			void surf(Input IN, inout SurfaceOutput o) 
			{
				const fixed3 cake = tex2D(_MainTex, IN.uv_MainTex).rgb;
				const fixed4 c = tex2D(_Liquid, IN.uv_Liquid);
				fixed b = tex2D(_Liquid, IN.uv_Liquid - fixed2(0, 0.001)).a;
				b = smoothstep(0.49, 0.6, b) - smoothstep(0.49, 0.6, c.a);
				const fixed3 color = tex2D(_Pattern, IN.uv_Liquid + c.xx).xyz + fixed3(b,b,b);

				const fixed d = smoothstep(0.49, 0.51, c.a);
				o.Albedo = color * d + cake * (1.0 - d);
				o.Alpha = 1.0;
			}
			ENDCG
		}

			Fallback "Diffuse"
}