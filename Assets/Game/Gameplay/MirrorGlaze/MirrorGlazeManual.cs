using System;
using UnityEngine;
using UnityEngine.UI;

namespace Nama
{
	[Serializable]
	public class MirrorGlazeOption
	{
		public Sprite pattern;
	}

	public class MirrorGlazeManual : DecorationStep
	{
		[SerializeField] private MirrorGlazeOption[] options;
		[SerializeField] private int correctOption;

		private MirrorGlazeOption CorrectOption => options[correctOption];
		private int _iSelected;
		private MirrorGlazeOption CurrentOption => options[_iSelected];
		private float _range;

		private CakeRenderer _cakeRenderer;
		private Texture2D _liquidTexture;
		public int optionAds;
		public override void Init(bool preview)
		{
			var cake = GetComponentInParent<Cake>();
			_cakeRenderer = cake.CakeRenderer;
			if (preview)
			{
				_cakeRenderer.Cake.sharedMaterial.SetTexture(Pattern, CorrectOption.pattern.texture);
			}
			else
			{
				Funzilla.Plugin.Validate();
				_cakeRenderer.MirrorCollider.sharedMesh = cake.MirrorGlazeMesh;
				_liquidTexture = new Texture2D(
					Funzilla.Plugin.LiquidResolution,
					Funzilla.Plugin.LiquidResolution,
					TextureFormat.RGBA32, false, true)
				{
					anisoLevel = 0,
					wrapMode = TextureWrapMode.Clamp,
				};
				_cakeRenderer.Cake.sharedMaterial.SetTexture(Liquid, _liquidTexture);
				
				Funzilla.Plugin.resetLiquid();
				_liquidTexture.SetPixels32(Funzilla.Plugin.LiquidPixels, 0);
				_liquidTexture.Apply(false);
			}
		}

		public override float GetScore()
		{
			if (Gameplay.Level.LevelStype != LevelStyle.FreeStyle)
			{
				if (_iSelected != correctOption) return 0.0f;
				var score = 0;
				for (var i = 0; i < Funzilla.Plugin.LiquidPixels.Length; i++)
				{
					if (Funzilla.Plugin.LiquidPixels[i].a > 80) score++;
				}
				return Mathf.Clamp01(2.2f * score / Funzilla.Plugin.LiquidPixels.Length);
			}

			if (_iSelected != correctOption && _iSelected != optionAds) return 0.0f;
			{
				var score = 0;
				for (var i = 0; i < Funzilla.Plugin.LiquidPixels.Length; i++)
				{
					if (Funzilla.Plugin.LiquidPixels[i].a > 80) score++;
				}
				return Mathf.Clamp01(2.2f * score / Funzilla.Plugin.LiquidPixels.Length);
			}
		}

		public override void Begin()
		{
			CreateDisplay("MirrorTmp");
			TutorialSign.Instance.StartTutorial();
			Gameplay.Instance.FinishStep = Finish;
			_cakeRenderer.MirrorCollider.gameObject.SetActive(true);
			OnOptionSelected(_iSelected);
			Setup();
			if(Gameplay.Level.LevelStype==LevelStyle.FreeStyle)
				Gameplay.Instance.ShowLevelFreeProgress();
		}

		private void Setup()
		{
			Gameplay.Instance.HideCompleteButton();
			Gameplay.Instance.HideUndoButton();
			MirrorGlazeUI.Instance.Show(options, _iSelected,optionAds);
			Surface.Carafe.Show(Display.transform, Vector2.zero);
			Surface.Carafe.Stream.SetMeshCollider(_cakeRenderer.MirrorCollider);
			Funzilla.Plugin.resetLiquid();
		}

		private Vector3 _touchPos;
		private void OnTouchDown(Vector3 screenPos)
		{
			_touchPos = screenPos;
			TutorialSign.Instance.EndTutorial();
			Surface.Carafe.Press(Surface.Carafe.Stream.Begin);
			MirrorGlazeUI.Instance.Hide();
		}

		private void OnTouchUp(Vector3 screenPos)
		{
			Surface.Carafe.Release();
			Gameplay.Instance.ShowUndoButton();
			Gameplay.Instance.ShowCompleteButton();
		}

		private void OnTouchHold(Vector3 screenPos)
		{
			var v = (screenPos - _touchPos) * 4.0f / Screen.width;
			_touchPos = screenPos;
			Surface.Carafe.Translate(new Vector3(v.x, 0, v.y));
			if (Gameplay.Level.LevelStype == LevelStyle.FreeStyle)
			{
				if(_iSelected==correctOption||_iSelected==optionAds)
					Surface.Carafe.EffectFreeStyle();
			}
		}

		private void Finish()
		{
			Finished = true;
			Funzilla.DailyQuestManager.UpdateDay();
			Profile.Instance.UseMirrorGlazeCurrentTime++;
			_cakeRenderer.MirrorCollider.gameObject.SetActive(false);
			Surface.Carafe.Stream.SetMeshCollider(null);
			Utils.Instance.VibrateMedium();
			Surface.Carafe.Hide(Gameplay.Instance.transform);
			Gameplay.Instance.HideCompleteButton();
			Gameplay.Instance.HideUndoButton();
			Debug.Log("????"+GetScore());
			if(Gameplay.Level.LevelStype==LevelStyle.FreeStyle)
				Gameplay.Instance.HideLevelFreeProgress();
		}

		private bool _touched;
		private static readonly int Pattern = Shader.PropertyToID("_Pattern");
		private static readonly int Liquid = Shader.PropertyToID("_Liquid");

		public override void DoFrame()
		{
			var eventSystem = UnityEngine.EventSystems.EventSystem.current;
#if UNITY_EDITOR
			if (Input.GetMouseButtonDown(0) && !eventSystem.IsPointerOverGameObject())
			{
				_touched = true;
				OnTouchDown(Input.mousePosition);
			}

			if (Input.GetMouseButtonUp(0) && _touched)
			{
				_touched = false;
				OnTouchUp(Input.mousePosition);
			}

			if (Input.GetMouseButton(0) && _touched)
			{
				OnTouchHold(Input.mousePosition);
			}
#else
			if (Input.touchCount > 0)
			{
				switch (Input.touches[0].phase)
				{
					case TouchPhase.Began:
						if (!eventSystem.IsPointerOverGameObject(Input.touches[0].fingerId))
						{
							_touched = true;
							OnTouchDown(Input.touches[0].position);
						}
						break;

					case TouchPhase.Stationary:
					case TouchPhase.Moved:
						if (_touched)
						{
							OnTouchHold(Input.touches[0].position);
						}
						break;
					case TouchPhase.Ended:
					case TouchPhase.Canceled:
						if (_touched)
						{
							_touched = false;
							OnTouchUp(Input.touches[0].position);
						}
						break;
				}
			}
#endif
			Funzilla.Plugin.updateLiquid(1.0f / 90);
			_liquidTexture.SetPixels32(Funzilla.Plugin.LiquidPixels, 0);
			_liquidTexture.Apply(false);
			if (!Surface.Carafe.Stream.Pouring) return;
			var p = Surface.Carafe.Stream.PourUv * Funzilla.Plugin.LiquidResolution;
			Funzilla.Plugin.pourLiquid((int)p.x, (int)p.y);
			if (Gameplay.Level.LevelStype == LevelStyle.FreeStyle)
				Gameplay.Instance.SetFillAmountFreeProgress(Percent());
		}

		internal override void Undo()
		{
			Setup();
			if (Gameplay.Level.LevelStype == LevelStyle.FreeStyle)
			{
				Surface.Carafe.ClearEffectFreeStyle();
				Gameplay.Instance.ShowLevelFreeProgress();
			}
		}

		public override void OnOptionSelected(int index)
		{
			_iSelected = index;
			Surface.Carafe.SetColor(Color.white, CurrentOption.pattern.texture);
			_cakeRenderer.Cake.sharedMaterial.SetTexture(Pattern, CurrentOption.pattern.texture);
		}

		public override bool IsOptionActive(int index)
		{
			return _iSelected == index;
		}

		public override bool IsOptionCorrect(int index)
		{
			return index == correctOption;
		}

		public override int OptionCount => options.Length;

		public override void RefreshTool()
		{
			Surface.Carafe.SetColor(Color.white, CurrentOption.pattern.texture);
		}

		public override void CompleteFX(Action action)
		{
			Gameplay.Instance.Carafe.CompleteFX(action);
		}

		private float Percent()
		{
			var score = 0;
			for (var i = 0; i < Funzilla.Plugin.LiquidPixels.Length; i++)
			{
				if (Funzilla.Plugin.LiquidPixels[i].a > 80) score++;
			}
			return Mathf.Clamp01(2.2f * score / Funzilla.Plugin.LiquidPixels.Length);
		}
	}
}

