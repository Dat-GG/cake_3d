using System.Collections;
using System.Collections.Generic;
using System.IO;
using Nama;
using UnityEditor;
using UnityEngine;

namespace Funzilla
{
	public class BakeGlazeMap : MonoBehaviour
	{
		[SerializeField] private MeshFilter meshFilter;

		private void Bake(string outputFile)
		{
			var texture = new RenderTexture(
				Plugin.LiquidResolution,
				Plugin.LiquidResolution,
				24,
				RenderTextureFormat.ARGBFloat,
				RenderTextureReadWrite.Default)
			{
				wrapMode = TextureWrapMode.Repeat,
				filterMode = FilterMode.Bilinear,
				autoGenerateMips = false,
				useMipMap = false
			};
			Camera.main.targetTexture = texture;
			Camera.main.backgroundColor = new Color(0.5f, 0.5f, 0.5f, 1.0f);
			Camera.main.Render();
			Camera.main.targetTexture = null;

			var texture2d =
				new Texture2D(texture.width, texture.height, TextureFormat.RGBAFloat, false);
			RenderTexture.active = texture;
			texture2d.ReadPixels(new Rect(0, 0, texture.width, texture.height), 0, 0);
			texture2d.Apply();
			RenderTexture.active = null;

			var pixels = texture2d.GetPixels();
			var uv = new float[pixels.Length * 2];
			for (var i = 0; i < pixels.Length; i++)
			{
				uv[i] = (pixels[i].r - 0.5f) * 0.2f;
				uv[i + pixels.Length] = (pixels[i].g - 0.5f) * 0.2f;
			}

			var writer = new BinaryWriter(File.Open($"Assets/Game/Prefabs/Cakes/{outputFile}.bytes", FileMode.Create));
			foreach (var e in uv)
			{
				writer.Write(e);
			}
			writer.Close();
		}

#if UNITY_EDITOR
		[ContextMenu("Bake")]
		private void FixFinishLine()
		{
			var guids = AssetDatabase.FindAssets("t:prefab", new string[] {"Assets/Game/Prefabs/Cakes"});
			foreach (var guid in guids)
			{
				var path = AssetDatabase.GUIDToAssetPath(guid);
				var prefab = AssetDatabase.LoadMainAssetAtPath(path) as GameObject;
				if (!prefab) continue;
				var cake = prefab.GetComponent<Cake>();
				if (!cake || !cake.MirrorGlazeMesh) continue;
				meshFilter.sharedMesh = cake.MirrorGlazeMesh;
				Bake(prefab.name);
			}
		}
#endif
	}
}

