Shader "Funzilla/BakeGlazeMap"
{
	Properties
	{
	}
	SubShader
	{
		ZTest Always
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vertex_func
			#pragma fragment fragment_func

			struct Vertex
			{
				float4 position : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct Interpolate
			{
				float4 ndc : SV_POSITION;
				float3 normal : NORMAL;
			};

			Interpolate vertex_func(Vertex input)
			{
				Interpolate output;
#if SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_GLCORE
				output.ndc = float4 (input.uv.x * 2 - 1, input.uv.y * 2 - 1, 0.0, 1.0);
#else
				output.ndc = float4 (input.uv.x * 2 - 1, 1 - input.uv.y * 2, 0.0, 1.0);
#endif
				output.normal = input.normal;
				return output;
			}

			fixed4 fragment_func(Interpolate input) : SV_Target
			{
				return float4(normalize(input.normal).xz * 0.5f + 0.5f, 0.0, 1.0);
			}
			ENDCG
		}
	}
}