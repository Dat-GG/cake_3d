using UnityEngine;
using UnityEngine.UI;

namespace Nama
{
	public class MirrorGlazeButton : MonoBehaviour
	{
		[SerializeField] private Button button;
		[SerializeField] private Image sprite;
		[SerializeField] private Image checkIcon;

		public Button Button => button;

		public void Init(int index, MirrorGlazeOption option)
		{
			button.onClick.AddListener(() => MirrorGlazeUI.Instance.OnOptionSelected(index));
			sprite.sprite = option.pattern;
		}

		public void SetActive(bool active)
		{
			button.interactable = !active;
			checkIcon.gameObject.SetActive(active);
		}
	}

}
