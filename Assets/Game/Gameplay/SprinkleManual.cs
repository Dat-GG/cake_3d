﻿using System.Collections.Generic;
using DG.Tweening;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Nama
{
	public class SprinkleManual : DecorationStep
	{
		[SerializeField] private SprinklePath[] paths;
		[SerializeField] private Sprinkles[] options;
		[SerializeField] private bool physicsEnabled;
		[SerializeField] private bool bonus;
		public bool Bonus => bonus;
		
#if UNITY_EDITOR
		public void Init(SprinklePath[] p)
		{
			paths = p;
		}
#endif

		private int _iSelected; // Currently selected option
		private Vector2 _currentPosition;
		private Vector2 _touchPos;
		private bool _pressed;
		private bool _touched;
		private readonly Polyline _poly = new Polyline();
		private Polyline _correctPoly;
		private int _correctPathIndex;
		private int[] _solutions;
		private float _score;
		private Sprinkles.AnimationType _animationType = Sprinkles.AnimationType.Simulate;
		private  float _minPathDistance=>Gameplay.Instance.bigDecorConfig.DataConfig.minDistance;
		private float totalDistance;
		private float maxDistance=>Gameplay.Instance.bigDecorConfig.DataConfig.maxTotalDistance;
		private List<Vector3> _pointsToCountProgress = new List<Vector3>();
		public int optionAds;
		public override void Init(bool preview)
		{
			CreateDisplay("Display of manual sprinkles");
			foreach (var path in paths)
			{
				path.Init(transform);
			}

			RefreshCorrectPoly();

			if (preview && !Bonus)
			{
				foreach (var path in paths)
				{
					foreach (var option in options)
					{
						option.OnStepBegan(Surface, Display.transform);
					}
					path.ShowPreview(options);
				}
			}
			else
			{
				_animationType = physicsEnabled ?
					Sprinkles.AnimationType.Physics : Sprinkles.AnimationType.Simulate;
				_solutions = new int[options.Length];
				for (var i = 0; i < options.Length; i++)
				{
					_solutions[i] = 0;
				}

				foreach (var path in paths)
				{
					for (var i = 0; i < path.SectionCount; i++)
					{
						var section = (SprinklesSection)path.GetSection(i);
						foreach (var option in section.options)
						{
							var s = options[option];
							_solutions[option] += (int)(section.Length / s.Spacing) * s.DropCount;
						}
					}
				}

			}
#if UNITY_EDITOR
			if (EditorApplication.isPlayingOrWillChangePlaymode)
			{
#endif
				if (Gameplay.Instance != null)
				{
					Gameplay.Instance.SetSprinklePreview(options);
				}
#if UNITY_EDITOR
			}
#endif

			var nodeLength = _correctPoly.Length / _correctPoly.Points.Count;
			_clearRadius = Mathf.FloorToInt(0.2f / nodeLength);
			_clearRadius = Mathf.Max(_clearRadius, 1);
			
		}

		internal override void Undo()
		{
			foreach (var option in options)
			{
				option.Clear();
			}
			Begin();
		}

		public override void Begin()
		{
			Gameplay.Instance.HideUndoButton();
			if (Bonus)
			{
				DOVirtual.DelayedCall(1.5f, delegate
				{
					Gameplay.Instance.ShowCompleteButton();
				});
				Gameplay.Instance.ShowBonusUI();
				Analytics.Instance.LogEvent($"level_{Profile.Instance.Level}_start_deco");
			}
			else
			{
				Gameplay.Instance.HideCompleteButton();
			}
			foreach (var option in options)
			{
				option.OnStepBegan(Surface, Display.transform);
			}
			_iSelected = 0;
			var poly = paths[0].Poly;
			_currentPosition = Vector2.zero;
			options[0].Begin(poly, poly.StartPosition, _animationType);
			options[0].ShowCharacter(_currentPosition);
			if(!Bonus)
			{
				options[0].ShowPointer(Gameplay.Instance.Pointer);
			}
			options[0].ShowBottle(Gameplay.Instance.Bottle);
			Gameplay.Instance.OnOptionSelected = OnOptionSelected;
			Gameplay.Instance.FinishStep = Finish;
			Gameplay.Instance.UndoStep = Undo;
			Gameplay.Instance.ShowManualOptions(options,optionAds);
			Gameplay.Instance.ONOptionsSetup = () =>
			{
				Gameplay.Instance.SetCurrentTutorial(2);
				Gameplay.Instance.ActiveTutorial();
			};
			TutorialSign.Instance.StartTutorial();
			if (Gameplay.Level.LevelStype == LevelStyle.FreeStyle)
			{
				Gameplay.Instance.ShowLevelFreeProgress();
				totalDistance = 0;
				_pointsToCountProgress.Clear();
			}
		}

		private float _magnetRadius, _magnetLerp;

		
		public override void DoFrame()
		{
			

			if (Finished) return;
			if (_finished)
			{
				Finished = true;
				return;
			}
			if(options[_iSelected].Special && !Gameplay.Instance.OwnedSprinkle(_iSelected))
			{
				return;
			}

			if (Gameplay.Level.LevelStype == LevelStyle.FreeStyle)
			{
				SetPercentProgress();
			}
			//set type tool tip
#if UNITY_EDITOR
			if (Input.GetMouseButtonDown(0))
			{
				if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
				{
					return;
				}
				options[_iSelected].Press();
				Gameplay.Instance.Pointer.Press(Config.SprinkleDelay);
				Gameplay.Instance.Bottle.Press(Config.SprinkleDelay);
				options[_iSelected].StopIdle();

				_touchPos = Input.mousePosition;
				_touched = true;
				return;
			}
			if (Input.GetMouseButtonUp(0))
			{
				_touched = false;
				options[_iSelected].Release();
				Gameplay.Instance.Pointer.Release();
				Gameplay.Instance.Bottle.Release();
				if (!_pressed) return;
				_pressed = false;
				Gameplay.Instance.ShowCompleteButton();
				return;
			}
			Vector2 p = Input.mousePosition;
#else
			if (Input.touches.Length <= 0)
			{
				return;
			}
			switch (Input.touches[0].phase)
			{
				case TouchPhase.Began:
					if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(
						Input.touches[0].fingerId)) return;
					options[_iSelected].Press();
					Gameplay.Instance.Pointer.Press(Config.SprinkleDelay);
					Gameplay.Instance.Bottle.Press(Config.SprinkleDelay);
					_touchPos = Input.touches[0].position;
					_touched = true;
					return;

				case TouchPhase.Stationary:
				case TouchPhase.Moved:
					break;

				case TouchPhase.Ended:
				case TouchPhase.Canceled:
					_touched = false;
					if (_pressed)
					{
						_pressed = false;
						Gameplay.Instance.ShowCompleteButton();
					}
					options[_iSelected].Release();
					Gameplay.Instance.Pointer.Release();
					Gameplay.Instance.Bottle.Release();
					return;
			}
			var p = Input.touches[0].position;
#endif
			if (!_touched)
			{
				return;
			}

			var swipeDirection = p - _touchPos;
			var swipeLength = 0f;
			if (swipeDirection.magnitude > 0)
			{
				_touchPos = p;
				swipeDirection.y *= 1.2f;
				swipeLength = swipeDirection.magnitude;
				swipeDirection /= swipeLength;
				var scale = 4.0f / Screen.width;
				swipeLength *= scale;
				_currentPosition += swipeDirection * (swipeLength * Config.SWIPE_SPEED);

				options[_iSelected].UpdateCharacterPosition(_currentPosition);
				var pointer = Gameplay.Instance.Pointer.transform.position;
				const int layerMask = Layers.BackgroundMask | Layers.CakeSurfaceMask;

				if (Physics.SphereCast(pointer, 0.2f, pointer + Vector3.down * 5, out var hit, Mathf.Infinity, layerMask))
				{
					Gameplay.Instance.Pointer.gameObject.SetActive(hit.collider.gameObject.layer == Layers.CakeSurface);
				}
			}
			Gameplay.Instance.ShowUndoButton();
			TutorialSign.Instance.EndTutorial();

			if (!_pressed)
			{
				if (!Gameplay.Instance.Bottle.Ready)
				{
					return;
				}
				_pressed = true;
				_poly.Clear();
				_poly.AddPoint(_currentPosition);
				options[_iSelected].Begin(_poly, _poly.StartPosition, _animationType, true);
				return;
			}

			if (_poly.Points.Count > 0)
			{
				var delta = Vector2.Distance(_currentPosition, _poly.LastPoint);
				if (delta < 0.05f)
				{ // It's too short
					return;
				}
			}

			if (!_pressed)
			{
				return;
			}

			if (paths.Length > _correctPathIndex)
			{
				if (paths[_correctPathIndex].Magnet && Config.Instance.MagnetEnable)
				{
					_closest = ClosestPoint(_currentPosition);
					_magnetRadius = Vector2.Distance(_currentPosition, _closest);
					_magnetLerp = _magnetRadius < Config.MAGNET_RADIUS ? (_magnetRadius / Config.MAGNET_RADIUS + swipeLength * Config.LOSE_CONTROL_FORCE) : 1;

					if (_magnetRadius < Config.CORRECT_RADIUS)
					{
						ClearArea(_closest, _clearRadius);
					}
					_currentPosition = Vector2.Lerp(_currentPosition, _closest, Mathf.Lerp(Config.MAGNET_FORCE, 0f, _magnetLerp));
				}
			}
			
			// Add a new point
			_poly.AddPoint(_currentPosition);
			VibrateLight();
			if (_poly.Points.Count > 1)
			{
				options[_iSelected].MoveTo(_poly.LastPosition);
			}

			if (options[_iSelected].PositiveVfxActivated || !IsOptionCorrect(_iSelected)) return;
			options[_iSelected].PositiveVfxActivated = true;
			Gameplay.Instance.PlayPositiveFX();
			
			
		}

		private float _minDistance, _currentDistance;
		private int _clearRadius = 1;
		private Vector2 _closestPoint;

		private Vector2 ClosestPoint(Vector2 p)
		{
			if(_correctPoly.Points.Count <= 0)
			{
				return new Vector2(9, 9);
			}
			_closestPoint = _correctPoly.Points[0];
			_minDistance = 10;
			foreach (var t in _correctPoly.Points)
			{
				_currentDistance = Vector2.Distance(t, p);
				if (_currentDistance >= _minDistance) continue;
				_minDistance = _currentDistance;
				_closestPoint = t;
			}
			return _closestPoint;
		}

		private void ClearArea(Vector3 position, int radius)
		{
			if(_correctPoly.Points.Count <= radius * 2)
			{
				_correctPoly.Clear();
				return;
			}
			var index = _correctPoly.Points.IndexOf(position);
			if(index<0)
			{
				return;
			}
			var min = Mathf.Max(0, index - radius);
			_correctPoly.Points.RemoveRange(min, Mathf.Min(radius * 2, _correctPoly.Points.Count - min));
		}

		void RefreshCorrectPoly()
		{
			_correctPoly = new Polyline();
			for(int i = 0; i < paths.Length; i++)
			{
				if(paths[i].Sections[0].options[0] == _iSelected)
				{
					_correctPoly = paths[i].Poly;
					_correctPathIndex = i;
					return;
				}
			}
		}


		private Vector3 _closest;
#if UNITY_EDITOR && DEBUG_ENABLED
		private void OnDrawGizmos()
		{
			Gizmos.DrawCube(new Vector3(closest.x, .1f, closest.y), Vector3.one * 0.2f);
			Debug.DrawLine(
				new Vector3(currentPosition.x, 0.5f, currentPosition.y),
				new Vector3(closest.x, 0.5f, closest.y), Color.red);
			UnityEditor.Handles.Label(new Vector3(closest.x, .1f, closest.y), magnetRadius.ToString());
			if(correctPoly != null)
			foreach(var p in correctPoly.Points)
			{
				Gizmos.DrawSphere(new Vector3(p.x, .1f, p.y), 0.1f);
			}
		}
#endif

		public override float GetScore()
		{
			if(bonus)
			{
				return 1;
			}

			if (Gameplay.Level.LevelStype == LevelStyle.FreeStyle)
			{
				return 1;
			}
			return _score;
		}

		public override bool IsOptionActive(int index)
		{
			return index == _iSelected;
		}

		public override int OptionCount => options.Length;

		public SprinklePath[] Paths { get => paths; set => paths = value; }
		public Sprinkles[] Options { get => options; set => options = value; }
		public bool PhysicsEnabled { get => physicsEnabled; set => physicsEnabled = value; }

		public override bool IsOptionCorrect(int index)
		{
			return (from path in paths from section in path.Sections from option in section.options select option).Any(option => option == index);
		}

		private void OnOptionSelected(int index)
		{
			if (_iSelected >= 0 && _iSelected < options.Length)
			{
				options[_iSelected].HideCharacter();
			}
			Gameplay.Instance.Tutorial.Hide();
			
			_iSelected = index;
			RefreshTool();
			RefreshCorrectPoly();
		}

		public override void RefreshTool()
		{
			options[_iSelected].ShowCharacter(_currentPosition);
			options[_iSelected].ShowBottle(Gameplay.Instance.Bottle);
			options[_iSelected].ShowPointer(Gameplay.Instance.Pointer);
		}

		private bool _finished;
		private void Finish()
		{
			if (options.Any(option => option.Falling))
			{
				return;
			}
			Gameplay.Instance.HideCompleteButton();
			Utils.Instance.VibrateMedium();
			_finished = true;

			foreach (var option in options)
			{
				option.OnStepEnded();
			}

			// Calculate the score
			float total = 0;
			float fail = 0;
			for (var i = 0; i < options.Length; i++)
			{
				if (options[i].Special) continue;
				total += _solutions[i];
				fail += Mathf.Abs(_solutions[i] - options[i].Amount);
			}
			_score = Mathf.Max(0, total - fail) / total;
			_score = Mathf.Clamp01(_score / 0.9f);
			if (Gameplay.Level.LevelStype == LevelStyle.FreeStyle)
			{
				Gameplay.Instance.HideLevelFreeProgress();
				totalDistance = 0;
				_pointsToCountProgress.Clear();
			}
		}

		public override void CompleteFX(System.Action action)
		{
			Gameplay.Instance.Bottle.CompleteFX(action);
		}
		
		private float DistanceToLastPoint(Vector3 point,List<Vector3> p)
		{
			return !p.Any() ? Mathf.Infinity : Vector3.Distance(p.Last(), point);
		}
		private void SetPercentProgress()
		{
			if (totalDistance >= maxDistance)
				return;
			if (DistanceToLastPoint(Gameplay.Instance.Bottle.transform.position, _pointsToCountProgress) > _minPathDistance)
			{
				if (DistanceToLastPoint(Gameplay.Instance.Bottle.transform.position, _pointsToCountProgress) != Mathf.Infinity)
					totalDistance += DistanceToLastPoint(Gameplay.Instance.Bottle.transform.position, _pointsToCountProgress);
				_pointsToCountProgress.Add(Gameplay.Instance.Bottle.transform.position);
			}
			Gameplay.Instance.SetFillAmountFreeProgress(totalDistance / maxDistance);
		}
	}
}