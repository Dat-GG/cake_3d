
using DG.Tweening;
using UnityEngine;

namespace Nama
{
	public class PatrolBehavior : MonoBehaviour
	{
		[SerializeField] public Vector3 Destination;
		[Range(0.0f, 10.0f)] [SerializeField] float duration = 2;
		[SerializeField] Ease ease = Ease.InOutQuad;
		[SerializeField] LoopType loopType = LoopType.Yoyo;

		Tweener tween;
		private void Awake()
		{
			Destination.z = transform.localPosition.z;
			tween = transform.DOLocalMove(Destination, duration)
				.SetLoops(-1, loopType)
				.SetEase(ease);
		}

		private void OnDestroy()
		{
			tween.Kill();
		}
	}
}