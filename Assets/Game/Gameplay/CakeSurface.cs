using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Nama
{
	[ExecuteInEditMode]
	public class CakeSurface : MonoBehaviour
	{
		[SerializeField] private bool round;
		[SerializeField] public Transform setting;
		[SerializeField] public Transform cameraTransform;
		[SerializeField] public Transform lightTransform;

		public PastryBag PastryBag { get; private set; }
		public Carafe Carafe { get; private set; }

		public Transform CameraTransform => cameraTransform;
		public Transform LightTransform => lightTransform;
		public DecorationStep CurrentStep => Steps.Length <= 0 || StepIndex < 0 || StepIndex >= Steps.Length ? null : Steps[StepIndex];
		public DecorationStep[] Steps { get; private set; }
		public int StepIndex { get; private set; }
		public static int StepId { get; set; }

		public float Radius => round ? -transform.localPosition.z : 0;
		public bool Finished { get; private set; }
		public DecorationStep Previous { get; private set; }
		public bool HasKey { get; private set; }
		private Key _keyGO;

		public void Init(bool preview, bool addKey)
		{
			Steps = GetComponentsInChildren<DecorationStep>();
			if (!Config.Instance.BonusAndSpecialOption || preview)
			{
				foreach (var step in Steps)
				{
					if (step is SprinkleManual {Bonus: true} sprinkleManual)
					{
						sprinkleManual.gameObject.SetActive(false);
					}
				}

				Steps = GetComponentsInChildren<DecorationStep>(false);
			}

			var brushPaths = new List<Polyline>();
			var temp = new List<StrokeManual>();

			foreach (var step in Steps)
			{
#if !UNITY_EDITOR
				step.Surface = this;
#endif
				step.Init(preview);
				if (!(step is StrokeManual strokeManual)) continue;
				brushPaths.Add(strokeManual.Poly);
				temp.Add(strokeManual);
			}

			var level = Profile.Instance.Level;
			var keys = GetComponentsInChildren<Key>();
			var add = false;
			if (brushPaths.Count > 0 && addKey)
			{
				if (level >= Config.Instance.InComeData.Key.StartLevel &&
					level % Config.Instance.InComeData.Key.SpawnRate == 0)
				{
					add = true;
				}
			}

			if (Statistics.IsKeyCollected(level))
				add = false;

			if (add)
			{
				HasKey = true;
				if (keys.Length <= 0)
				{
					var pathIndex = Mathf.FloorToInt(brushPaths.Count * 2f / 3f);
					var pointsIndex = Mathf.FloorToInt(brushPaths[pathIndex].Points.Count * 2f / 3f);
					_keyGO = Instantiate(Resources.Load<Key>("Key"), temp[pathIndex].KeyParent, true);
					var t = _keyGO.transform;
					t.localPosition = new Vector3(
						brushPaths[pathIndex].Points[pointsIndex].x,
						brushPaths[pathIndex].Points[pointsIndex].y,
						-.4f);
					t.localEulerAngles = new Vector3(-80, 0, 0);
				}
			}
			else
			{
				foreach (var key in keys)
				{
					Destroy(key.gameObject);
				}
			}
		}

		public void Begin(PastryBag pastryBag, Carafe carafe)
		{
			if (Steps.Length > 0)
			{
				StartOver(pastryBag, carafe);
				gameObject.SetActive(true);
				Steps[0].Begin();
				SendStepEvent("miniLevelStarted", Steps[StepIndex], StepId);
				UpdateActiveKey(Steps[0]);
				UpdateFollowTool();
			}
			else
			{
				Finished = true;
			}
		}

		private void UpdateActiveKey(DecorationStep step)
		{
			var isStroke = step.GetType() == typeof(StrokeManual);
			if (_keyGO != null) _keyGO.gameObject.SetActive(isStroke);
		}

		private void StartOver(PastryBag pastryBag, Carafe carafe)
		{
			StepIndex = 0;
			PastryBag = pastryBag;
			Carafe = carafe;
		}

		internal void Undo()
		{
			if (StepIndex >= Steps.Length) return;
			var step = Steps[StepIndex];
			if (step is StrokeManual)
			{
				while (true)
				{
					var strokeStep = Steps[StepIndex] as StrokeManual;
					if (strokeStep == null)
					{
						StepIndex++;
						break;
					}

					strokeStep.Clear();
					if (StepIndex == 0) break;
					StepIndex--;
				}

				step = Steps[StepIndex] as StrokeManual;
				if (step != null) step.Begin();
			}
			else
			{
				step.Undo();
			}

			Ads.ShowInterstitial(delegate { SceneManager.Instance.OpenScene(SceneID.Home); }, "Undo");
		}

		public static void SendStepEvent(string eventName, string stepName, int stepId)
		{
			var parameters = new Dictionary<string, object>
			{
				{"miniMissionID", stepId},
				{"miniMissionName", stepName},
			};
			if (Gameplay.Level.CakeType == CakeType.Vip)
			{
				parameters.Add("vipMissionID", Profile.Instance.VipLevel);
			}
			else
			{
				parameters.Add("missionID", Profile.Instance.Level);
			}
			Ads.SendCustomFirebaseEvent(eventName, parameters);
		}

		private static void SendStepEvent(string eventName, DecorationStep step, int stepId)
		{
			SendStepEvent(eventName, StepName(step), stepId);
		}

		private static string StepName(DecorationStep step)
		{
			var stepName = step switch
			{
				SprinkleManual _ => "sprinkle",
				FillManual _ => "glaze",
				StencilManual _ => "stencil",
				BigDecorManual _ => "candle",
				PhotoManual _ => "photo",
				MirrorGlazeManual _ => "mirror",
				_ => "stroke"
			};
			return stepName;
		}

		private void NextStep()
		{
			StepIndex++;
			if (StepIndex >= Steps.Length)
			{
				Finished = true;
				return;
			}

			_stepFinished = false;
			Steps[StepIndex].Begin();
			UpdateFollowTool();
			UpdateActiveKey(Steps[StepIndex]);
			Gameplay.Instance.HideWinFx();
			SendStepEvent("miniLevelStarted", Steps[StepIndex], StepId);
			if(!Gameplay.Instance.isPhoto)
				SceneManager.Instance.OpenScene(SceneID.Home);
		}

		private void FinishStep()
		{
			Previous = Steps[StepIndex];
			var finished = StepIndex + 1 >= Steps.Length;
			if (finished) _stepFinished = true;
			var stepFinished = finished || Steps[StepIndex + 1].GetType() != Previous.GetType();
			if (stepFinished)
			{
				SendStepEvent("miniLevelCompleted", Previous, StepId);
				StepId++;
				SceneManager.Instance.CloseScene(SceneID.Home);
				Gameplay.Instance.CameraController.ResetCameraPosition(1, delegate
				{
					Gameplay.Instance.ShowWinFx();
					Gameplay.Instance.HideUndoButton();
					if (Previous is StrokeManual)
					{
						FlickStroke();
					}

					Previous.CompleteFX(delegate
					{
						if (Config.Instance.StepAdsEnable && !(Previous is PhotoManual))
						{
							Ads.ShowInterstitial(NextStep, StepName(Previous));
						}
						else
						{
							NextStep();
							
						}
						if (Gameplay.Level.LevelStype == LevelStyle.FreeStyle)
						{
							Gameplay.Instance.SetDotColorLevelFree();
						}
					});
				});
			}
			else
			{
				StepIndex++;
				_stepFinished = false;
				Steps[StepIndex].Begin();
				UpdateActiveKey(Steps[StepIndex]);
				UpdateFollowTool();
			}

			Utils.Instance.VibrateLight();
		}

		private bool _stepFinished;

		public void DoFrame()
		{
			if (Finished || StepIndex >= Steps.Length)
			{
				return;
			}

			var step = Steps[StepIndex];
			if (step.Finished)
			{
				if (_stepFinished) return;
				_stepFinished = true;
				FinishStep();
				return;
			}

			step.DoFrame();
		}

		internal void RefreshTool()
		{
			CurrentStep.RefreshTool();
			UpdateFollowTool();
		}

		private void UpdateFollowTool()
		{
			Gameplay.Instance.StrokeManualDecorating = false;
			if (StepIndex >= Steps.Length)
			{
				Gameplay.Instance.FlowTool = null;
				return;
			}

			if (Steps[StepIndex].GetType() == typeof(FillManual) ||
				Steps[StepIndex].GetType() == typeof(MirrorGlazeManual))
			{
				Gameplay.Instance.FlowTool = Gameplay.Instance.Carafe.CarafeMove.gameObject;
			}
			else if (Steps[StepIndex].GetType() == typeof(SprinkleManual))
			{
				Gameplay.Instance.FlowTool = Gameplay.Instance.Bottle.gameObject;
			}
			else if (Steps[StepIndex].GetType() == typeof(StrokeManual))
			{
				Gameplay.Instance.FlowTool = Gameplay.Instance.PastryBag.gameObject;
				var strokeManual = Steps[StepIndex] as StrokeManual;
				if (strokeManual != null && strokeManual.Poly != null)
				{
					var minX = strokeManual.Poly.Points.Min(e => e.x);
					var maxX = strokeManual.Poly.Points.Max(e => e.x);
					Gameplay.Instance.StepWidth = Mathf.Abs(maxX - minX);
					Gameplay.Instance.StrokeManualDecorating = true;
				}
				else
				{
					Gameplay.Instance.StepWidth = 1;
					Gameplay.Instance.StrokeManualDecorating = false;
				}

				var prevStepIndex = StepIndex - 1;
				if (prevStepIndex < 0) return;
				var prevStrokeManual = Steps[prevStepIndex] as StrokeManual;
				if (strokeManual != null && prevStrokeManual != null && strokeManual.Poly != null)
				{
					Gameplay.Instance.DistanceLastStroke = Vector3.Distance(
						strokeManual.Poly.FirstPoint, prevStrokeManual.Poly.LastPoint);
				}
				else
				{
					Gameplay.Instance.DistanceLastStroke = 0;
				}
			}
			else
			{
				Gameplay.Instance.FlowTool = null;
			}
		}

		internal void FlickStroke()
		{
			foreach (var step in Steps)
			{
				if (step is StrokeAuto stroke) stroke.FlickColor();
			}
		}

#if UNITY_EDITOR
		private BrushTexture BrushTexture { get; set; }
		private void Awake()
		{
			if (EditorApplication.isPlayingOrWillChangePlaymode)
			{
				return;
			}

			var prefab = Resources.Load<BrushTexture>("BrushTexture");
			BrushTexture = Instantiate(prefab);
			BrushTexture.gameObject.SetActive(false);
			BrushTexture.transform.localPosition = new Vector3(0, 1000, 0);
			BrushTexture.gameObject.hideFlags = HideFlags.HideAndDontSave;
		}
#endif

#if UNITY_EDITOR

		#region SprinkleAutoToManual

		private readonly List<SprinkleAuto> _sprinkleAutoList = new List<SprinkleAuto>(3);

		public void SprinkleAutoToManual()
		{
			SprinkleManual sprinkleManual = null;
			var steps = GetComponentsInChildren<DecorationStep>();
			foreach (var step in steps)
			{
				if (step.GetType() == typeof(SprinkleManual))
				{
					sprinkleManual = step as SprinkleManual;
				}
				else if (step.GetType() == typeof(SprinkleAuto))
				{
					_sprinkleAutoList.Add(step as SprinkleAuto);
				}
			}

			var options = new List<Sprinkles>(3);
			if (sprinkleManual == null)
			{
				var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(
					"Assets/Game/Gameplay/SprinkleManual.prefab");
				var obj = Instantiate(prefab, Selection.activeGameObject.transform);
				obj.name = "Sprinkles Manual";
				sprinkleManual = obj.GetComponent<SprinkleManual>();
				sprinkleManual.Paths = Array.Empty<SprinklePath>();
			}
			else
			{
				options = sprinkleManual.Options.ToList();
			}

			for (var i = 0; i < sprinkleManual.Paths.Length; i++)
			{
				var trueOption = sprinkleManual.Options[sprinkleManual.Paths[i].Sections[0].options[0]];
				trueOption.transform.SetSiblingIndex(i);
				sprinkleManual.Paths[i].Sections = new[] {new SprinklesSection(new[] {i})};
			}

			var pathLength = sprinkleManual.Paths.Length;
			var sprinklePaths = sprinkleManual.Paths.ToList();

			for (var i = 0; i < _sprinkleAutoList.Count; i++)
			{
				var sprinkleAuto = _sprinkleAutoList[i];
				sprinklePaths.Add(sprinkleAuto.Path);
				sprinkleAuto.Path.Sections = new[] {new SprinklesSection(new[] {i + pathLength})};
				sprinkleAuto.transform.SetParent(sprinkleManual.transform);
				sprinkleAuto.gameObject.name = "Path";
				var optionId = sprinkleAuto.Path.Sections[0].options[0];
				options.Add(sprinkleAuto.Options[optionId]);
				sprinkleAuto.Options[optionId].transform.SetParent(sprinkleManual.transform.Find("Options"));
				sprinkleAuto.Options[optionId].transform.SetSiblingIndex(i + pathLength);
				sprinkleManual.PhysicsEnabled = sprinkleAuto.PhysicsEnabled;
				sprinkleManual.transform.localScale = sprinkleAuto.transform.localScale;
				foreach (var trans in sprinkleAuto.gameObject.GetComponentsInChildren<Transform>(true))
				{
					trans.gameObject.transform.SetParent(sprinkleManual.transform.Find("Options"));
				}

				sprinkleAuto.transform.SetParent(sprinkleManual.transform);
				DestroyImmediate(sprinkleAuto);
			}


			sprinkleManual.Paths = sprinklePaths.ToArray();
			sprinkleManual.Options = options.ToArray();
		}

		[ContextMenu("Stroke Auto to Stroke Manual")]
		public void StrokeAutoToStrokeManual()
		{
			var strokeAutos = GetComponentsInChildren<StrokeAuto>();

			var strokeManualCount = 1;
			foreach (var strokeAuto in strokeAutos)
			{
				var strokeManual = strokeAuto.GetComponent<StrokeManual>();
				if (strokeManual != null) continue;
				strokeManual = strokeAuto.gameObject.AddComponent<StrokeManual>();
				strokeManual.SetStrokeStepData(strokeAuto.GetStrokeStepData());
				var colorOptions = new List<ColorOption>(strokeAuto.Options.Length);
				colorOptions.AddRange(strokeAuto.Options.Select(t => new ColorOption() {colors = new[] {t}}));

				foreach (var section in strokeAuto.Sections)
				{
					if (section.colors.Length <= 1) continue;
					var colors = new Color[section.colors.Length];
					for (var k = 0; k < section.colors.Length; k++)
					{
						colors[k] = strokeAuto.Options[section.colors[k]];
					}

					colorOptions.Add(new ColorOption() {colors = colors});
					section.colors = new[] {colorOptions.Count - 1};
				}

				strokeManual.gameObject.name = $"StrokeManual {strokeManualCount}";
				strokeManualCount++;
				strokeManual.Options = colorOptions.ToArray();
				DestroyImmediate(strokeAuto);
			}
		}

		#endregion

		[ContextMenu("Fix")]
		public void Fix()
		{
			if (cameraTransform == null)
			{
				cameraTransform = transform.Find("cameraTransform");
				if (cameraTransform == null)
				{
					var obj = new GameObject("cameraTransform");
					cameraTransform = obj.transform;
					cameraTransform.SetParent(transform);
					var position = setting.position;
					cameraTransform.position = position;
					cameraTransform.forward = Vector3.zero - position;
					cameraTransform.localScale = Vector3.one;
				}
			}

			if (lightTransform == null)
			{
				lightTransform = transform.Find("lightTransform");
				if (lightTransform == null)
				{
					var obj = new GameObject("lightTransform");
					lightTransform = obj.transform;
					lightTransform.SetParent(transform);
					lightTransform.position = Vector3.up;
					lightTransform.rotation = setting.rotation;
					lightTransform.localScale = Vector3.one;
				}
			}
		}
#endif
		public int TrueCountStep()
		{
			int i = 0;
			foreach (var s in Steps)
			{
				if (s is StrokeManual)
				{
					i++;
				}
			}

			if (i > 1)
			{
				return Steps.Length - i + 1;
			}

			return Steps.Length;
		}
	}

#if UNITY_EDITOR
	public readonly struct StrokeStepData
	{
		public readonly Path Path;

		public StrokeStepData(Path path)
		{
			Path = path;
		}
	}
#endif
}