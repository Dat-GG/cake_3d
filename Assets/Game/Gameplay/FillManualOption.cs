﻿using UnityEngine;

namespace Nama
{
	[System.Serializable]
	public class FillManualOption
	{
		[Tooltip("Paint Text Null For Simple Color")]
		public Texture2D PaintText;
		public Sprite Icon;
		public Color BlendColor = Color.white;
	}
}