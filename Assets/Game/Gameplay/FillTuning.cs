﻿namespace Nama
{
	[System.Serializable]
	public class FillTuning
	{
		public float LoangSpeed = 0.1f;
		public float LoangTime = 1.5f;
		public float GravitySpeed = 0.04f;
	}
}