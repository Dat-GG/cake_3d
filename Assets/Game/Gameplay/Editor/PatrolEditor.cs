﻿
using UnityEditor;
using UnityEngine;

namespace Nama
{
	[CustomEditor(typeof(PatrolBehavior)), CanEditMultipleObjects]
	public class PatrolEditor : Editor
	{
		protected virtual void OnSceneGUI()
		{
			var patrol = (PatrolBehavior)target;

			EditorGUI.BeginChangeCheck();
			var p = patrol.transform.parent.TransformPoint(patrol.Destination);
			Vector3 newTargetPosition = Handles.PositionHandle(p, Quaternion.identity);

			if (EditorGUI.EndChangeCheck())
			{
				Undo.RecordObject(patrol, "Change Target Position");
				patrol.Destination = patrol.transform.parent.InverseTransformPoint(newTargetPosition);
			}
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			if (GUILayout.Button("Set destination to current position"))
			{
				foreach (var obj in Selection.gameObjects)
				{
					var patrol = obj.GetComponent<PatrolBehavior>();
					patrol.Destination = patrol.transform.localPosition;
				}
			}
		}
	}
}