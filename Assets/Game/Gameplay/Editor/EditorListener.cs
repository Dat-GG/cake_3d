using System.Collections.Generic;
using UnityEditor;

using UnityEngine;

[InitializeOnLoad]
public class EditorListener
{
	static EditorListener()
	{
		Selection.selectionChanged += OnSelectionChange;
	}
	static void OnSelectionChange()
	{
		if (EditorApplication.isPlayingOrWillChangePlaymode)
		{
			return;
		}
		var stage = UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage();
		GameObject root = null;
		if (stage != null)
		{
			root = stage.prefabContentsRoot;
		}
		else
		{
			var objs = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
			if (objs.Length > 0)
			{
				root = objs[0];
			}
		}
		if (root == null)
		{
			return;
		}

		ShowDecorationOutline(root);
	}

	static void ShowDecorationOutline(GameObject root)
	{
		var steps = root.GetComponentsInChildren<Nama.DecorationStep>();
		List<Nama.DecorationStep> selectedSteps = new List<Nama.DecorationStep>();
		foreach (var obj in Selection.gameObjects)
		{
			var array = obj.GetComponentsInChildren<Nama.DecorationStep>();
			selectedSteps.AddRange(array);
		}
		foreach (var step in steps)
		{
			step.ShowHighlight(selectedSteps.IndexOf(step) >= 0);
		}
	}
}