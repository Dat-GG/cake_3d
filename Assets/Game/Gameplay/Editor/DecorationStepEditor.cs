using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Nama
{
	[CustomEditor(typeof(DecorationStep), true), CanEditMultipleObjects]
	public class DecorationStepEditor : Editor
	{
		private readonly string[] _choices = {
			"Round Brush",
			"Square Brush",
			"Star Brush",
			"Twist Brush",
			"Sprinkles",
		};

		private int _choiceIndex;

		private static GameObject Change(GameObject obj, int type)
		{
			if (obj.GetComponent<SprinkleManual>() != null)
			{
				return obj;
			}

			string path = null;

			switch (type)
			{
				case 0:
				case 1:
				case 2:
				case 3:
					path = "Assets/Game/Gameplay/StrokeAuto.prefab";
					break;
				case 4:
					path = "Assets/Game/Gameplay/SprinkleAuto.prefab";
					break;
			}

			if (string.IsNullOrEmpty(path))
			{
				return obj;
			}

			// Get the bezier path data
			var bezier = obj.GetComponent<BezierPath>();
			var commands = bezier.Commands;
			var points = bezier.Points;

			obj.GetComponent<DecorationStep>().DestroyDisplay();
			var parent = obj.transform.parent;
			var siblingIndex = obj.transform.GetSiblingIndex();

			var position = obj.transform.localPosition;
			var rotation = obj.transform.localRotation;
			var scale = obj.transform.localScale;

			DestroyImmediate(obj);
			
			var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(path);
			obj = Instantiate(prefab, parent);
			obj.transform.localPosition = position;
			obj.transform.localRotation = rotation;
			obj.transform.localScale = scale;
			obj.transform.SetSiblingIndex(siblingIndex);
			obj.name = prefab.name;

			bezier = obj.GetComponent<BezierPath>();
			bezier.SetData(points, commands);

			if (type < 0 || type > 3) return obj; // stroke types
			var brush = obj.GetComponent<Brush>();
			path = null;
			switch (type)
			{
				case 0:
					path = "Assets/Game/Brushes/Tips/RoundTip.prefab";
					break;
				case 1:
					path = "Assets/Game/Brushes/Tips/SquareTip.prefab";
					break;
				case 2:
					path = "Assets/Game/Brushes/Tips/StarTip.prefab";
					break;
				case 3:
					path = "Assets/Game/Brushes/Tips/FlowerTip.prefab";
					brush.Twist = 1.0f;
					break;

			}
			if (!string.IsNullOrEmpty(path))
			{
				brush.SetTip(AssetDatabase.LoadAssetAtPath<BrushTip>(path));
			}
			return obj;
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			if (Selection.activeGameObject == null)
			{
				return;
			}

			GUILayout.BeginHorizontal();
			GUILayout.Label("Type", GUILayout.Width(40));
			_choiceIndex = EditorGUILayout.Popup(_choiceIndex, _choices);
			if (GUILayout.Button("Change"))
			{
				EditorApplication.delayCall += () =>
				{
					Selection.activeGameObject = Change(Selection.activeGameObject, _choiceIndex);
					var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
					if (prefabStage != null)
					{
						EditorSceneManager.MarkSceneDirty(prefabStage.scene);
					}
				};
			}
			GUILayout.EndHorizontal();

			if (!GUILayout.Button("Refresh")) return;
			foreach (var obj in Selection.gameObjects)
			{
				obj.GetComponent<DecorationStep>().RefreshPreview();
			}
		}

		public void OnSceneGUI()
		{
			if (EditorApplication.isPlayingOrWillChangePlaymode)
			{
				return;
			}

			var e = Event.current;

			//Check the event type and make sure it's left click.
			if (e.button != 0)
			{
				return;
			}

			switch (e.type)
			{
				case EventType.MouseDown:
					DecorationStep.LeftMouseButtonDown = true;
					break;
				case EventType.MouseUp:
				{
					if (DecorationStep.LeftMouseButtonDown)
					{
						foreach (var obj in Selection.gameObjects)
						{
							var step = obj.GetComponent<DecorationStep>();
							if (step != null && step.CheckTransformChange())
							{
								step.RefreshPreview();
							}
						}
					}
					DecorationStep.LeftMouseButtonDown = false;
					break;
				}
			}
		}
	} // class PathEditor
} // namespace Nama