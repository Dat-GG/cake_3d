
using UnityEditor;
using UnityEngine;

namespace Nama
{
	[CustomEditor(typeof(Cake)), CanEditMultipleObjects]
	public class CakeEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
		}
	}
}