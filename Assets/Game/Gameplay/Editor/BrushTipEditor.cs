
using UnityEditor;
using UnityEngine;
using System.Runtime.InteropServices;
using System.Collections.Generic;

namespace Nama
{
	[CustomEditor(typeof(BrushTip)), CanEditMultipleObjects]
	public class BrushTipEditor : Editor
	{
		[DllImport(Funzilla.Plugin.Name)] private static extern int svgOpen(string fileName, double fit);
		[DllImport(Funzilla.Plugin.Name)] private static extern int svgPathVertexCount(int pathIndex);
		[DllImport(Funzilla.Plugin.Name)] private static extern void svgReceivePath(
			int pathIndex, Vector2d[] points, uint[] commands);
		[DllImport(Funzilla.Plugin.Name)] private static extern void setSpacing(double spacing);
		[DllImport(Funzilla.Plugin.Name)] private static extern void getPolyline(Vector2[] output);
		[DllImport(Funzilla.Plugin.Name)] private static extern int makePolyline(
			Vector2d[] points, uint[] commands, int count,
			float x, float y, float angle, float sx, float sy);

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			if (!GUILayout.Button("Import SVG")) return;
			if (Selection.gameObjects.Length <= 0)
			{
				return;
			}

			var selectedObj = Selection.gameObjects[0];
			if (selectedObj == null)
			{
				return;
			}

			var tip = selectedObj.GetComponent<BrushTip>();
			if (tip == null)
			{
				return;
			}

			var path = EditorUtility.OpenFilePanel("Open SVG file", "", "svg");
			if (string.IsNullOrEmpty(path))
			{
				return;
			}
			var nPaths = svgOpen(path, 1.0);
			if (nPaths <= 0)
			{
				return;
			}

			var n = svgPathVertexCount(0);
			var points = new Vector2d[n];
			var commands = new uint[n];
			svgReceivePath(0, points, commands);
			setSpacing(0.05f);
			n = makePolyline(points, commands, n, 0, 0, 0, 1, 1);
			if (n < 3)
			{
				return;
			}

			var poly = new Vector2[n];
			getPolyline(poly);
			var tipSegments = MakeTip(poly);
			tip.Import(tipSegments);
		}

		private static TipShapeSegment[] MakeTip(IList<Vector2> poly)
		{
			var segments = new List<TipShapeSegment>();
			var vertices = new List<Vector2>();
			var normals = new List<Vector2>();

			var direction = (poly[1] - poly[0]).normalized;
			vertices.Add(poly[0]);
			normals.Add(new Vector2(-direction.y, direction.x));

			var minY = poly[0].y;
			var minX = poly[0].x;
			var maxX = poly[0].x;

			for (var i = 1; i < poly.Count; i++)
			{
				if (minY > poly[i].y)
				{
					minY = poly[i].y;
				}
				if (minX > poly[i].x)
				{
					minX = poly[i].x;
				}
				if (maxX < poly[i].x)
				{
					maxX = poly[i].x;
				}
				var v = (poly[i] - poly[i - 1]).normalized;

				if (Vector2.Dot(v, direction) < 0.70710678f)
				{
					segments.Add(new TipShapeSegment()
					{
						points = vertices.ToArray(),
						normals = normals.ToArray(),
						uvs = new float[vertices.Count]
					});
					vertices.Clear();
					normals.Clear();

					vertices.Add(poly[i - 1]);
					normals.Add(new Vector2(-v.y, v.x).normalized);
					direction = Vector2.zero;
				}

				vertices.Add(poly[i]);
				normals.Add(new Vector2(-v.y - direction.y, v.x + direction.x).normalized);

				direction = v;
			}
			segments.Add(new TipShapeSegment()
			{
				points = vertices.ToArray(),
				normals = normals.ToArray(),
				uvs = new float[vertices.Count]
			});
			var scale = 1 / (maxX - minX);
			minY *= scale;

			foreach (var segment in segments)
			{
				for (var i = 0; i < segment.points.Length; i++)
				{
					segment.points[i].x *= scale;
					segment.points[i].y = segment.points[i].y * scale - minY;
				}
			}

			float uv = 0;
			foreach (var segment in segments)
			{
				var p = segment.points[0];
				for (var i = 0; i < segment.points.Length; i++)
				{
					uv += Vector2.Distance(segment.points[i], p);
					p = segment.points[i];
					segment.uvs[i] = uv;
				}
			}
			foreach (var segment in segments)
			{
				for (var i = 0; i < segment.points.Length; i++)
				{
					segment.uvs[i] /= uv;
				}
			}

			//var lastSeg = segments[segments.Count - 1];
			//lastSeg.points[lastSeg.points.Length - 1] = segments[0].points[0];
			//lastSeg.normals[lastSeg.points.Length - 1] = segments[0].normals[0];
			return segments.ToArray();
		}
	}
}