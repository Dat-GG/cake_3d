﻿using UnityEditor;
using UnityEngine;
using System.Runtime.InteropServices;
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;

namespace Nama
{
	[CustomEditor(typeof(CakeSurface)), CanEditMultipleObjects]
	public class CakeSurfaceEditor : Editor
	{
		[DllImport(Funzilla.Plugin.Name)] private static extern int svgOpen(string fileName, double fit);

		[DllImport(Funzilla.Plugin.Name)] private static extern int svgPathVertexCount(int pathIndex);

		[DllImport(Funzilla.Plugin.Name)] private static extern void svgReceivePath(
			int pathIndex, Vector2d[] points, uint[] commands);

		private static void ImportStrokeManual()
		{
			var path = EditorUtility.OpenFilePanel("Open SVG file", "", "svg");
			if (string.IsNullOrEmpty(path))
			{
				return;
			}

			var cake = Selection.activeGameObject.GetComponentInParent<Cake>();
			var cakeSize = cake != null ? cake.Size : 2.0f;
			int nPaths = svgOpen(path, cakeSize);
			if (nPaths <= 0)
			{
				return;
			}

			var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(
				"Assets/Game/Gameplay/StrokeManual.prefab");
			for (int i = 0; i < nPaths; i++)
			{
				// Create a new step
				var obj = Instantiate(prefab, Selection.activeGameObject.transform);
				obj.name = $"Stroke Manual {i}";

				// Get path data
				var n = svgPathVertexCount(i);
				var points = new Vector2d[n];
				var commands = new uint[n];
				svgReceivePath(i, points, commands);
				obj.GetComponent<BezierPath>().SetData(points, commands);
				obj.GetComponent<DecorationStep>().RefreshPreview();
			}

			var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
			if (prefabStage != null)
			{
				EditorSceneManager.MarkSceneDirty(prefabStage.scene);
			}
		}

		private static void ImportManualSprinkles()
		{
			var path = EditorUtility.OpenFilePanel("Open SVG file", "", "svg");
			if (string.IsNullOrEmpty(path))
			{
				return;
			}

			var cake = Selection.activeGameObject.GetComponentInParent<Cake>();
			var cakeSize = cake != null ? cake.Size : 2.0f;
			var nPaths = svgOpen(path, cakeSize);

			if (nPaths <= 0)
			{
				return;
			}

			var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(
				"Assets/Game/Gameplay/SprinkleManual.prefab");
			var obj = Instantiate(prefab, Selection.activeGameObject.transform);
			obj.name = "Sprinkles Manual";

			var paths = new SprinklePath[nPaths];
			for (var i = 0; i < nPaths; i++)
			{
				var sections = new SprinklesSection[1];
				sections[0] = new SprinklesSection(new[] {0});

				var bzObj = new GameObject("Path");
				var bz = bzObj.AddComponent<BezierPath>();
				var t = bz.transform;
				t.SetParent(obj.transform, false);
				t.localPosition = Vector3.zero;
				t.localScale = Vector3.one;
				t.localRotation = Quaternion.identity;

				// Get path data
				int n = svgPathVertexCount(i);
				Vector2d[] points = new Vector2d[n];
				uint[] commands = new uint[n];
				svgReceivePath(i, points, commands);
				bz.SetData(points, commands);
				paths[i] = new SprinklePath();
				paths[i].Init(bz, sections);
			}

			var step = obj.GetComponent<SprinkleManual>();
			step.Init(paths);
			step.RefreshPreview();

			var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
			if (prefabStage != null)
			{
				EditorSceneManager.MarkSceneDirty(prefabStage.scene);
			}
		}

		private static void ImportPNGFillColor()
		{
			var fillManualPrefab = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Game/Gameplay/FillManual.prefab");

			var manualSurface = Instantiate(fillManualPrefab, Selection.activeGameObject.transform);
			var fillManual = manualSurface.GetComponent<FillManual>();

			var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
			Selection.activeGameObject = fillManual.gameObject;
			if (prefabStage != null)
			{
				EditorSceneManager.MarkSceneDirty(prefabStage.scene);
			}
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			if (GUILayout.Button("Import Stroke Manual") && Selection.activeGameObject != null)
			{
				ImportStrokeManual();
			}

			if (GUILayout.Button("Import Manual Sprinkles") && Selection.activeGameObject != null)
			{
				ImportManualSprinkles();
			}

			if (Selection.activeGameObject.name != "SurfaceTop" || Selection.activeGameObject == null) return;
			if (GUILayout.Button("Create Fill Color"))
			{
				ImportPNGFillColor();
			}
		}
	}
}