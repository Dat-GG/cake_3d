
using UnityEditor;
using UnityEngine;

namespace Nama
{
	[CustomEditor(typeof(Level)), CanEditMultipleObjects]
	public class LevelEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			if (!GUILayout.Button("Play in Normal Mode")) return;
			if (Selection.gameObjects.Length <= 0)
			{
				return;
			}

			var parentObject = PrefabUtility.GetPrefabInstanceHandle(Selection.activeGameObject);
			var path = parentObject != null
				? AssetDatabase.GetAssetPath(parentObject)
				: UnityEditor.SceneManagement.PrefabStageUtility
					.GetPrefabStage(Selection.gameObjects[0]).assetPath;
			PlayerPrefs.SetString("TestLevel", path);

			var scene = UnityEngine.SceneManagement.SceneManager.GetActiveScene();
			var objs = scene.GetRootGameObjects();
			if (objs.Length <= 0 || !objs[0].GetComponentInChildren<Gameplay>()) return;
			EditorApplication.ExecuteMenuItem("Edit/Play");
		}
	}
}