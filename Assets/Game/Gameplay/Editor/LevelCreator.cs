
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

namespace Nama
{
	public static class LevelCreator
	{
		static void CreateCake(string cakeName)
		{
			string path = EditorUtility.SaveFilePanel(
				"Create New Level",
				"Assets/Resources/Levels",
				"level_001",
				"prefab");
			if (string.IsNullOrEmpty(path))
			{
				return;
			}
			var name = System.IO.Path.GetFileNameWithoutExtension(path);
			var levelObj = new GameObject(name);
			var level = levelObj.AddComponent<Level>();

			var cakePrefab = AssetDatabase.LoadAssetAtPath<Cake>(
				"Assets/Game/Prefabs/Cakes/" + cakeName + ".prefab");
			var cakeObj = PrefabUtility.InstantiatePrefab(cakePrefab.gameObject, level.transform) as GameObject;
			cakeObj.name = cakeName;
			var asset = PrefabUtility.SaveAsPrefabAsset(levelObj, path);
			Object.DestroyImmediate(levelObj);

			AssetDatabase.OpenAsset(asset);
		}

		// new custom cake -----------------------------------------------------------------
		[MenuItem("Tạo Bánh/Custom/Bánh Mì")]
		public static void CreateCakeCustomBakery()
		{
			CreateCake("new_bakery");
		}
		[MenuItem("Tạo Bánh/Custom/Miếng Bánh Mì")]
		public static void CreateCakeCustomBakerySlice()
		{
			CreateCake("new_slice_bakery");
		}
		[MenuItem("Tạo Bánh/Custom/Chocolate")]
		public static void CreateCakeCustomChocolate()
		{
			CreateCake("new_chocolate");
		}
		[MenuItem("Tạo Bánh/Custom/Cup Cake")]
		public static void CreateCakeCustomCupcake()
		{
			CreateCake("new_cupcake");
		}
		[MenuItem("Tạo Bánh/Custom/Dâu Tây")]
		public static void CreateCakeCustomStrawberry()
		{
			CreateCake("new_strawberry");
		}
		
		// -----------------------------------------------------------------------------------
		[MenuItem("Tạo Bánh/Tròn/Lớn/Góc vuông")]
		public static void CreateCakeCircleLSharp()
		{
			CreateCake("CircleLSharp");
		}

		[MenuItem("Tạo Bánh/Bánh Mì/Tròn")]
		public static void CreateCakeCircleLRounded()
		{
			CreateCake("Bakery");
		}

		[MenuItem("Tạo Bánh/Tròn/Nhỏ/Góc vuông")]
		public static void CreateCakeCircleMSharp()
		{
			CreateCake("CircleMSharp");
		}

		[MenuItem("Tạo Bánh/Tròn/Nhỏ/Góc tròn")]
		public static void CreateCakeCircleMRounded()
		{
			CreateCake("CircleMRounded");
		}

		[MenuItem("Tạo Bánh/Vuông/Lớn/Góc vuông")]
		public static void CreateCakeSquareLSharp()
		{
			CreateCake("SquareLSharp");
		}

		[MenuItem("Tạo Bánh/Vuông/Lớn/Góc tròn")]
		public static void CreateCakeSquareLRounded()
		{
			CreateCake("SquareLRounded");
		}

		[MenuItem("Tạo Bánh/Vuông/Nhỏ/Góc vuông")]
		public static void CreateCakeSquareMSharp()
		{
			CreateCake("SquareMSharp");
		}

		[MenuItem("Tạo Bánh/Vuông/Nhỏ/Góc tròn")]
		public static void CreateCakeSquareMRounded()
		{
			CreateCake("SquareMRounded");
		}

		[MenuItem("Tạo Bánh/5 góc/Lớn/Góc vuông")]
		public static void CreateCakePentagonLSharp()
		{
			CreateCake("PentagonLSharp");
		}

		[MenuItem("Tạo Bánh/5 góc/Lớn/Góc tròn")]
		public static void CreateCakePentagonLRounded()
		{
			CreateCake("PentagonLRounded");
		}

		[MenuItem("Tạo Bánh/5 góc/Nhỏ/Góc vuông")]
		public static void CreateCakePentagonMSharp()
		{
			CreateCake("PentagonMSharp");
		}

		[MenuItem("Tạo Bánh/5 góc/Nhỏ/Góc tròn")]
		public static void CreateCakePentagonMRounded()
		{
			CreateCake("PentagonMRounded");
		}

		[MenuItem("Tạo Bánh/6 góc/Lớn/Góc vuông")]
		public static void CreateCakeHexagonLSharp()
		{
			CreateCake("HexagonLSharp");
		}

		[MenuItem("Tạo Bánh/6 góc/Lớn/Góc tròn")]
		public static void CreateCakeHexagonLRounded()
		{
			CreateCake("HexagonLRounded");
		}

		[MenuItem("Tạo Bánh/6 góc/Nhỏ/Góc vuông")]
		public static void CreateCakeHexagonMSharp()
		{
			CreateCake("HexagonMSharp");
		}

		[MenuItem("Tạo Bánh/6 góc/Nhỏ/Góc tròn")]
		public static void CreateCakeHexagonMRounded()
		{
			CreateCake("HexagonMRounded");
		}

		[MenuItem("Tạo Bánh/8 góc/Lớn/Góc vuông")]
		public static void CreateCakeOctagonLSharp()
		{
			CreateCake("OctagonLSharp");
		}

		[MenuItem("Tạo Bánh/8 góc/Lớn/Góc tròn")]
		public static void CreateCakeOctagonLRounded()
		{
			CreateCake("OctagonLRounded");
		}

		[MenuItem("Tạo Bánh/8 góc/Nhỏ/Góc vuông")]
		public static void CreateCakeOctagonMSharp()
		{
			CreateCake("OctagonMSharp");
		}

		[MenuItem("Tạo Bánh/8 góc/Nhỏ/Góc tròn")]
		public static void CreateCakeOctagonMRounded()
		{
			CreateCake("OctagonMRounded");
		}

		[MenuItem("Tạo Bánh/Chữ nhật/Góc vuông")]
		public static void CreateCakeRectangleLSharp()
		{
			CreateCake("RectangleLSharp");
		}

		[MenuItem("Tạo Bánh/Chữ nhật/Góc tròn")]
		public static void CreateCakeRectangleLRounded()
		{
			CreateCake("RectangleLRounded");
		}

		[MenuItem("Tạo Bánh/Trái tim/Góc vuông")]
		public static void CreateCakeHeartLSharp()
		{
			CreateCake("HeartLSharp");
		}

		[MenuItem("Tạo Bánh/Trái tim/Góc tròn")]
		public static void CreateCakeHeartLRounded()
		{
			CreateCake("HeartLRounded");
		}

		[MenuItem("Tạo Bánh/Donut")]
		public static void CreateCakeDonut()
		{
			CreateCake("DonutSharp");
		}

		[MenuItem("Tạo Bánh/Miếng bị cắt")]
		public static void CreateCakePie()
		{
			CreateCake("SlicedPie");
		}
	}
}
