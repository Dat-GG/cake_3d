
using UnityEditor;
using UnityEngine;

namespace Nama
{
	[CustomEditor(typeof(BrushTexture)), CanEditMultipleObjects]
	public class BrushTextureEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			if (GUILayout.Button("Render"))
			{
				foreach (var obj in Selection.gameObjects)
				{
					var brushTexture = obj.GetComponent<BrushTexture>();
					brushTexture.Bake(new Color[] { Color.green, Color.yellow });
				}
			}
		}
	}
}