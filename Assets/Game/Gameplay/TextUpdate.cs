using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Funzilla
{
    public class TextUpdate : MonoBehaviour
    {
        [SerializeField] private TextMeshPro Text;

        public void SetText(float Progress)
        {
            Text.SetText($"{(Progress * 100).ToString("N2")}%");
        }
    }
}

