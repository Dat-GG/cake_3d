﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


	public class TutorialSign : MonoBehaviour
	{
		public static TutorialSign Instance;

		[SerializeField]
		private GameObject fillTutorial;

		public void Awake()
		{
			Instance = this;
		}

		public void StartTutorial()
		{
			fillTutorial.SetActive(true);
		}

		public void EndTutorial()
		{

			fillTutorial.SetActive(false);
		}

		private void OnDestroy()
		{
			Instance = null;
		}

	}
