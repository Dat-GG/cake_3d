using UnityEngine;

namespace Nama
{
	public class SprinkleCharacterPart : MonoBehaviour
	{
		public virtual void SetPosition(Vector3 position)
		{
			
		}
		
		public virtual void Appear()
		{
			
		}

		public virtual void Disappear()
		{

		}

		public virtual void Press(float animDuration)
		{
			
		}

		public virtual void Release()
		{
			
		}
	}
}