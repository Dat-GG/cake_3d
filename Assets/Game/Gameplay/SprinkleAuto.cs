﻿
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEditor;

namespace Nama
{
	[System.Serializable]
	public class SprinklesSection : PolylineSectionByWeight
	{
		public SprinklesSection(int[] options)
		{
			this.options = options;
		}
		public int[] options;
	}

	/// <summary>
	/// Description about sprinkles along a path
	/// </summary>
	[System.Serializable]
	public class SprinklePath : IPolylineSectionDistribution
	{
		[SerializeField] private Path path;
		[SerializeField] private SprinklesSection[] sections;
		[SerializeField] private bool magnet;
		public bool Magnet => magnet;

		public SprinklesSection[] Sections { get => sections; set => sections = value; }

#if UNITY_EDITOR
		public void Init(Path p, SprinklesSection[] s)
		{
			path = p;
			sections = s;
		}
#endif

		public Polyline Poly { get; private set; }

		public int SectionCount => sections.Length;

		public PolylineSectionByWeight GetSection(int index) => sections[index];

		public void Init(Transform transformation)
		{
			Poly = new Polyline(path, transformation);
			Poly.Distribute(this);
		}

		public void ShowPreview(Sprinkles[] options)
		{
			foreach (var section in sections)
			{
				foreach (var option in section.options)
				{
					if (Application.isPlaying && options[option].Special)
					{
						continue;
					}
					options[option].Begin(Poly, section.Start, Sprinkles.AnimationType.None);
					options[option].MoveTo(section.Finish);
#if UNITY_EDITOR
					if (!EditorApplication.isPlayingOrWillChangePlaymode) continue;
#endif
					if (Gameplay.Instance == null) continue;
					if (Gameplay.Instance.SprinkleObjectsFirst.Count == 0)
					{
						Gameplay.Instance.SprinkleObjectsFirst =
							options[option].SprinkleObjects.ToList();
					}
					else
					{
						Gameplay.Instance.SprinkleObjectsSecond =
							options[option].SprinkleObjects.ToList();
					}
				}
			}
		}
	}

	public class SprinkleAuto : DecorationStep
	{
		[SerializeField] private SprinklePath path;
		[SerializeField] private Sprinkles[] options;
		[SerializeField] private bool physicsEnabled;

		public bool PhysicsEnabled => physicsEnabled;

		private Sprinkles[] _active;
		private Sprinkles[] _inactive;
		private int _activeCount;

		private Guideline _guideline;

		// Current position on path
		private PolylinePosition _currentPosition;
		private Sprinkles.AnimationType _animationType = Sprinkles.AnimationType.Simulate;

		public override void Init(bool preview)
		{
			CreateDisplay("Display of auto sprinkles");
			_active = new Sprinkles[options.Length];
			_inactive = new Sprinkles[options.Length];
			for (var i = 0; i < _active.Length; i++)
			{
				_active[i] = null;
				_inactive[i] = null;
			}
			foreach (var section in path.Sections)
			{
				if (section.options.Length <= 1) continue;
				break;
			}
			path.Init(transform);
			_currentPosition = path.Poly.StartPosition;
			_animationType = physicsEnabled ?
				Sprinkles.AnimationType.Physics : Sprinkles.AnimationType.Simulate;
			if (preview)
			{
				foreach (var option in options)
				{
					option.OnStepBegan(Surface, Display.transform);
				}
				path.ShowPreview(options);
			}
			else
			{
				_guideline = AssetManager.Instance.NewGuideline(
					Display.transform, path.Poly, Surface.Radius);
			}

		}

		#region Scoring

		private readonly List<int[]> _userOptions = new List<int[]>();
		private readonly List<float> _userSections = new List<float>();

		public SprinkleAuto()
		{
			physicsEnabled = false;
		}

		public override float GetScore()
		{
			SaveSectionData();
			float match = 0;
			const float splitLength = .3f;
			var score = 1.0f / (_currentPosition.Offset / splitLength);
			for (var i = 1; i < _userSections.Count; i++)
			{
				var count = Mathf.RoundToInt((_userSections[i] - _userSections[i - 1]) / splitLength);
				for (var j = 0; j < count; j++)
				{
					var p = Mathf.Lerp(_userSections[i - 1], _userSections[i], (float)j / count);
					var matchPercent = CalculateMatch(_userOptions[i - 1], GetOptions(p));
					match += score * matchPercent;
				}
			}
			match = Mathf.Min(1.0f, match);
			return match;
		}

		private int[] GetOptions(float position)
		{
			for (var i = path.Sections.Length - 1; i >= 0; i--)
			{
				if (position <= 0.8f) continue;
				if (path.Sections[i].Start.Offset <= position)
				{
					return path.Sections[i].options;
				}
			}
			return path.Sections[0].options;
		}

		private int[] GetActiveOptions()
		{
			var activeOptions = new List<int>(1);
			for (var i = 0; i < _active.Length; i++)
			{
				if (_active[i])
					activeOptions.Add(i);
			}
			return activeOptions.ToArray();
		}

		private void SaveSectionData()
		{
			_userSections.Add(_currentPosition.Offset);
			_userOptions.Add(GetActiveOptions());
		}
		#endregion

		private void CheckKeepGoing()
		{
			if (Surface.Previous == null)
			{
				return;
			}

			if (Surface.Previous.GetType() != typeof(SprinkleAuto))
			{
				return;
			}

			if (Surface.Previous.OptionCount != OptionCount)
			{
				return;
			}

			var keepGoing = false;
			for (var i = 0; i < options.Length; i++)
			{
				if (!Surface.Previous.IsOptionActive(i)) continue;
				OptionOn(i);
				keepGoing = true;
			}

			if (!keepGoing) return;
			_currentPosition = path.Poly.LastPosition;
			DOVirtual.DelayedCall(0.5f, () =>
			{
				_currentPosition = path.Poly.StartPosition;
			});
		}

		public override void Begin()
		{
			_guideline.gameObject.SetActive(true);
			foreach (var option in options)
			{
				option.OnStepBegan(Surface, Display.transform);
			}
			_currentPosition = path.Poly.StartPosition;
			options[0].Begin(path.Poly, _currentPosition, _animationType);
			options[0].ShowCharacter(_currentPosition.Pos);
			_inactive[0] = options[0];
			CheckKeepGoing();
			Gameplay.Instance.OptionOn = OptionOn;
			Gameplay.Instance.OptionOff = OptionOff;
			Gameplay.Instance.ShowAutoOptions(options);
		}

		public override void DoFrame()
		{
			if (_activeCount <= 0)
			{
				return;
			}

			if (_currentPosition.Offset >= path.Poly.Length)
			{
				return;
			}

			// Move forward and update current position
			var delta = Config.DrawSpeed * Time.smoothDeltaTime;
			_currentPosition = path.Poly.Advance(delta, _currentPosition);

			// Update display
			foreach (var sprinkles in _active)
			{
				if (sprinkles != null)
				{
					VibrateLight();
					sprinkles.MoveTo(_currentPosition);
					sprinkles.UpdateCharacterPosition(_currentPosition.Pos);
				}
			}

			// Check whether if it's finished or not
			if (!(_currentPosition.Offset >= path.Poly.Length)) return;
			Utils.Instance.VibrateMedium();
			DOVirtual.DelayedCall(1, Finish);
			Finish();
		}

		private void Finish()
		{
			_guideline.gameObject.SetActive(false);
			foreach (var option in options)
			{
				option.HideCharacter();
				option.OnStepEnded();
			}
			Finished = true;
		}

		public override bool IsOptionActive(int index)
		{
			return _active[index];
		}

		public override int OptionCount => options.Length;

		public SprinklePath Path { get => path; set => path = value; }
		public Sprinkles[] Options { get => options; set => options = value; }

		public override bool IsOptionCorrect(int index)
		{
			return path.GetSection(0) is SprinklesSection section && section.options[0] == index;
		}

		private void OptionOn(int index)
		{
			if (index < 0 || index >= _active.Length || _active[index] != null)
			{
				return;
			}

			for (var i = 0; i < _inactive.Length; i++)
			{
				if (_inactive[i] == null) continue;
				_inactive[i].HideCharacter();
				_inactive[i] = null;
			}

			_activeCount++;
			_active[index] = options[index];
			_active[index].Begin(path.Poly, _currentPosition, _animationType);
			_active[index].ShowCharacter(_currentPosition.Pos);
			if (_activeCount == 1)
			{
				_active[index].Press();
			}
			else
			{
				_active[index].Press(false);
			}
			SaveSectionData();
		}

		private void OptionOff(int index)
		{
			if (index < 0 || index >= _active.Length || _active[index] == null)
			{
				return;
			}

			_activeCount--;
			if (_activeCount > 0)
			{
				_active[index].HideCharacter();
			}
			else
			{
				_active[index].Release();
				_inactive[index] = _active[index];
			}
			_active[index] = null;
		}
	}
}