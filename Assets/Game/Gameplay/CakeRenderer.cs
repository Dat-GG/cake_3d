

using UnityEngine;

namespace Nama
{
	public class CakeRenderer : MonoBehaviour
	{
		[SerializeField] private MeshRenderer cake;
		[SerializeField] private MeshRenderer mask;
		[SerializeField] private MeshRenderer edge;
		[SerializeField] private MeshFilter cakeMesh;
		[SerializeField] private MeshFilter maskMesh;
		[SerializeField] private MeshFilter edgeMesh;
		[SerializeField] private MeshCollider meshCollider;
		[SerializeField] private MeshCollider mirrorCollider;

		public MeshRenderer Cake => cake;
		public MeshRenderer Mask => mask;
		public MeshRenderer Edge => edge;
		public MeshFilter CakeMesh => cakeMesh;
		public MeshFilter MaskMesh => maskMesh;
		public MeshFilter EdgeMesh => edgeMesh;
		public MeshCollider MeshCollider => meshCollider;
		public MeshCollider MirrorCollider => mirrorCollider;
	}
}