
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Nama
{
	public class GuideLineSection
	{
		public float Length;
		public readonly List<Color> Colors = new List<Color>(3);
	}
	
	public class Guideline : MonoBehaviour
	{
		[DllImport(Funzilla.Plugin.Name)] private static extern int makeGuidelineMesh(
			Vector2[] points, int nPoints, float length, float width, float surfaceRadius);

		[SerializeField] private MeshFilter back;
		[SerializeField] private MeshFilter front;
		[SerializeField] private new MeshRenderer renderer;

		//private const float GuidelineWidth = 0.1f;

		public readonly List<GuideLineSection> Sections = new List<GuideLineSection>();

		private float _length;

		public float T { get; set; }

		private void Update()
		{
			T -= Time.smoothDeltaTime;
			if (T < -_length - 1 - 0.1f)
			{
				T = 0;
			}

			float l = 0;
			
			foreach (var t in Sections)
			{
				l += t.Length;
				if (-l >= T) continue;
				SetColor(t.Colors[0]);
				break;
			}
			
			renderer.material.mainTextureOffset = new Vector2(T, 0);
		}

		public void SetColor(Color color)
		{
			renderer.material.color = color;
		}
		
		public void Init(Polyline poly, float surfaceRadius)
		{
			Funzilla.Plugin.Validate();
			var parent = transform.parent;
			var zz = parent.localPosition.z / parent.localScale.z;
			if (zz > 0)
			{
				transform.localPosition = new Vector3(0, 0, -zz);
			}

			var points = new Vector2[poly.Points.Count];
			for (var i = 0; i < poly.Points.Count; i++)
			{
				points[i] = poly.Points[i];
			}
			var width = BrushConfig.Instance.GuideLineWidth;
			var nVertices = makeGuidelineMesh(
				points,
				points.Length,
				poly.Length,
				width,
				//GuidelineWidth,
				surfaceRadius);
			front.sharedMesh = back.sharedMesh = StrokeDisplay.GetMesh(nVertices);
			
			_length = poly.Length / 2 + 1;
		}
	}
}
