using UnityEngine;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;

namespace Nama
{
	[System.Serializable]
	public class BezierPath : Path
	{
#if UNITY_IOS && !UNITY_EDITOR
		private const string PluginName = "__Internal";
#else
		private const string PluginName = "Funzilla";
#endif
		[DllImport(PluginName)] private static extern void setSpacing(double spacing);
		[DllImport(PluginName)] private static extern void getPolyline(Vector2[] output);
		[DllImport(PluginName)] private static extern int makePolyline(
			Vector2d[] points, uint[] commands, int count,
			float x, float y, float angle, float sx, float sy);

		[SerializeField] private Vector2d[] svgPoints;
		[SerializeField] private uint[] svgCommands;
		[SerializeField] private bool reverse;
		[Range(0, 1)]
		[SerializeField] private float offset;

		public BezierPath()
		{
			offset = 0;
		}

#if UNITY_EDITOR
		public void SetData(Vector2d[] points, uint[] commands)
		{
			svgPoints = points;
			svgCommands = commands;
		}

		public Vector2d[] Points => svgPoints;
		public uint[] Commands => svgCommands;

#endif

		public override List<Vector2> GeneratePolyline(Transform transformation)
		{
#if UNITY_EDITOR
			if (svgPoints == null || svgPoints.Length <= 0 ||
				svgCommands == null || svgCommands.Length <= 0)
			{ // Avoid crash in editor
				Debug.LogError("No path data!");
				return null;
			}
#endif

			setSpacing(0.01);
			var localScale = transformation.localScale;
			var n = makePolyline(
				svgPoints, svgCommands, svgPoints.Length,
				0, 0, transformation.localEulerAngles.z * Mathf.Deg2Rad,
				localScale.x, localScale.y);
			var array = new Vector2[n];
			getPolyline(array);
			
			if (reverse)
			{
				array = array.Reverse().ToArray();
			}

			var points = new List<Vector2>(array);
			if (points.Count <= 2) return points;
			var spinCount = Mathf.FloorToInt(array.Length * offset);
			var closePath = Vector2.Distance(points[0], points[points.Count - 1]) < 0.01f;
			if (spinCount <= 0) return points;
			points.RemoveAt(0);
			for (var i = 0; i < spinCount; i++)
			{
				points.Add(points[0]);
				points.Remove(points.Last());
			}
			if (closePath)
			{
				points.Add(points[0]);
			}
			else
			{
				points.Add(points[points.Count - 1] + (points[points.Count - 1] - points[points.Count - 2]));
			}
			return points;
		}

	}
}