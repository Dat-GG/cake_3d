using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace Nama
{
	public class PastryBag : MonoBehaviour
	{
		[SerializeField] private Transform bagTransform;
		[SerializeField] private ParticleSystem fx;
		[SerializeField] private GameObject completeFX;
		[SerializeField] private GameObject tip;
		[SerializeField] private Transform rotateTransform;
		private bool _dirty;
		public PastrySkin Skin { get; private set; }
		public bool Drawing => tip.activeSelf;
		public Color Color { get; private set; }

		private Tweener _verticalAnim;
		private Tweener _moveAnim;
		private Tweener _rotateAnim;

		public bool Finished { get; private set; }

		private void Init(string skinName)
		{
			var prefab = Resources.Load<PastrySkin>("PastrySkins/" + skinName);
			Skin = Instantiate(prefab, rotateTransform);
			if (!Finished)
			{
				Release(false);
			}
			completeFX.gameObject.SetActive(false);
			Color = Color.black;
		}

		public void ReloadSkin()
		{
			var penName = Profile.Instance.PastryBag.name;
			if (string.IsNullOrEmpty(penName))
			{
				penName = "Default";
			}
			LoadSkin(penName);
		}

		public void LoadSkin(string skinName)
		{
			if (Skin != null)
			{
				Destroy(Skin.gameObject);
			}
			Init(skinName);
		}

		private void StopVerticalAnimation()
		{
			if (_verticalAnim == null) return;
			_verticalAnim.Kill();
			_verticalAnim = null;
		}

		private void StopMoveAnimation()
		{
			if (_moveAnim == null) return;
			_moveAnim.Kill();
			_moveAnim = null;
		}

		public void Finish(Transform parent)
		{
			if (Finished)
			{
				return;
			}
			Finished = true;
			Release(false);
			transform.SetParent(parent, true);
			StopMoveAnimation();
		}

		public void Press()
		{
			_dirty = true;
			_rotateAnim?.Kill();
			_rotateAnim = rotateTransform
				.DOLocalRotate(new Vector3(0, 0, 0), 0.3f)
				.OnComplete(() => _rotateAnim = null);

			Skin.StopIdle();
			StopVerticalAnimation();
			_verticalAnim = bagTransform
				.DOLocalMoveZ(0, 0.05f)
				.OnComplete(() =>
				{
					_verticalAnim = null;
					tip.SetActive(true);
				});
		}

		public void Release(bool effect)
		{
			tip.SetActive(false);
			StopVerticalAnimation();
			Skin.StartIdle();
			_verticalAnim = bagTransform
				.DOLocalMoveZ(-0.5f, 0.1f)
				.OnComplete(() => _verticalAnim = null);

			SetEmissionRate(0);
			if (!effect) return;
			var sequence = DOTween.Sequence();
			foreach (var r in Skin.Renderers)
			{
				var c = Color * 1.5f;
				c.a = 0.5f;
				sequence.Join(r.material.DOColor(c, 0.1f));
			}
			sequence.OnComplete(() =>
			{
				foreach (var r in Skin.Renderers)
				{
					var c = Color * 1.1f;
					c.a = 0.5f;
					r.material.DOColor(c, 0.2f);
				}
			});
		}

		private void SetEmissionRate(float rate)
		{
			var emission = fx.emission;
			emission.rateOverTime = rate;
		}

		public bool Ready => _verticalAnim == null && _rotateAnim == null;
		public bool Moving => _moveAnim != null;

		private static void Move(
			Vector2 position,
			float surfaceRadius,
			System.Action<Vector3, Quaternion> move)
		{
			Quaternion rot;
			Vector3 pos;
			if (surfaceRadius > 0)
			{
				var x = position.x;
				var y = position.y;
				var a = x / surfaceRadius * 0.5f;
				rot = Quaternion.Euler(0, Mathf.Rad2Deg * a, 0);
				var s = Mathf.Sin(a);
				var c = Mathf.Cos(a);
				pos = new Vector3(x * c, y, x * s);
			}
			else
			{
				rot = Quaternion.identity;
				pos = position;
			}
			move(pos, rot);
		}

		public void Move(Vector2 position, float surfaceRadius)
		{
			Move(position, surfaceRadius, (pos, rot) =>
			{
				var t = transform;
				t.localPosition = pos;
				t.localRotation = rot;
			});
		}

		public void Rotate()
		{
			_rotateAnim?.Kill();
			_rotateAnim = rotateTransform
				.DOLocalRotate(new Vector3(0, 0, -360), 0.3f, RotateMode.FastBeyond360)
				.OnComplete(() => _rotateAnim = null);
		}

		public void SetColor(Color color, bool animate = false)
		{
			if (Color == color)
			{
				return;
			}
			if (animate)
			{
				Rotate();
			}
			Color = color;
			foreach (var r in Skin.Renderers)
			{
				var c = color * 1.1f;
				c.a = 0.5f;
				r.material.DOColor(c, 0.25f);
			}
		}

		public int IndexOption { get; set; } = -1;

		public void Begin(
			Transform parent,
			Vector2 position,
			Color color,
			float radius,
			bool effect = false)
		{
			StopCompleteFx();
			Skin.Begin();
			Finished = false;
			_dirty = false;
			SetColor(color);
			SetEmissionRate(0);
			transform.SetParent(parent, false);
			Rotate();

			StopMoveAnimation();
			Move(position, radius, (pos, rot) =>
			{
				var mainFx = fx.main;
				mainFx.startColor = color;
				fx.GetComponent<ParticleSystemRenderer>().material.color = color;
				if (effect)
				{
					StopMoveAnimation();
					_moveAnim = transform
						.DOLocalMove(pos, 0.5f)
						.OnComplete(() => _moveAnim = null);
				}
				else
				{
					transform.localPosition = pos;
				}

				transform.localRotation = rot;
			});
		}

		private Coroutine _completeFx;
		private Sequence _completeTween;
		private System.Action _onFxComplete;
		public void CompleteFX(System.Action action)
		{
			StopCompleteFx();
			if (!_dirty)
			{
				action.Invoke();
				return;
			}
			_onFxComplete = action;
			Finished = true;
			_completeFx = StartCoroutine(IECompleteFX(action));
		}

		private void StopCompleteFx()
		{
			if (_completeFx != null)
			{
				StopCoroutine(_completeFx);
				_completeFx = null;
			}

			if (_completeTween != null)
			{
				_completeTween.Kill();
				_completeTween = null;
			}

			_onFxComplete?.Invoke();
			_onFxComplete = null;
		}

		private IEnumerator IECompleteFX(System.Action action)
		{
			var newParent = Gameplay.Instance.PastrybagParent;
			Skin.StopIdle(); 
			completeFX.gameObject.SetActive(false);
			yield return new WaitForEndOfFrame();
			completeFX.gameObject.SetActive(true);
			transform.SetParent(newParent);
			yield return new WaitForSeconds(0.5f);
			_completeTween = DOTween.Sequence();
			_completeTween.Join(transform.DOLocalJump(Vector3.zero, 1, 1, 1));
			_completeTween.Join(transform.DOLocalRotate(Vector3.zero, 1));
			_completeTween.Join(Skin.transform.DOLocalRotate(Skin.RestRotation,1));
			_completeTween.Play().OnComplete(()=>_completeTween = null);
			yield return new WaitForSeconds(1.5f);
			Skin.StopIdle();
			_completeFx = null;
			_onFxComplete = null;
			action?.Invoke();
		}
	}
}
