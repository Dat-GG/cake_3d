using DG.Tweening;
using Nama;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Funzilla
{
    public class UpgradeAreaController : MonoBehaviour
    {

		[SerializeField] private List<UpgradeItems> items;
		[SerializeField] private List<UpgradeWalls> walls;
		[SerializeField] private List<UpgradeFloors> floors;
		public bool UpgradeItemMax => Profile.Instance.UpgradeDecoCount >= items.Count;
		public bool CanUpgradeItem => !UpgradeItemMax && Profile.Instance.CoinAmount >= GetUpgradeItemPrice();

		public bool UpgradeWallMax => Profile.Instance.UpgradeWallCount >= walls.Count;
		public bool CanUpgradeWall => !UpgradeWallMax && Profile.Instance.CoinAmount >= GetUpgradeWallPrice();

		public bool UpgradeFloorMax => Profile.Instance.UpgradeFloorCount >= floors.Count;
		public bool CanUpgradeFloor => !UpgradeFloorMax && Profile.Instance.CoinAmount >= GetUpgradeFloorPrice();

		private void Start()
		{
			Init();
		}

		private void Init()
		{
			foreach (var item in items)
			{
				item.Hide();
			}
			DisplayItem(Profile.Instance.UpgradeDecoCount);

			foreach (var wall in walls)
			{
				wall.Hide();
			}
			DisplayWall(Profile.Instance.UpgradeWallCount);

			foreach (var floor in floors)
			{
				floor.Hide();
			}
			DisplayFloor(Profile.Instance.UpgradeFloorCount);
			//Splash();
		}

		public int GetUpgradeItemPrice()
		{
			if (UpgradeItemMax)
				return 0;

			List<int> costs = Config.Instance.CostData.Background.UpgradeCosts;
			int index = Profile.Instance.UpgradeDecoCount;
			index = Mathf.Clamp(index, 0, items.Count - 1);
			return costs[index];
		}

		public int NextItemPrice()
        {
			List<int> costs = Config.Instance.CostData.Background.UpgradeCosts;
			int index = Profile.Instance.UpgradeDecoCount;
			index = Mathf.Clamp(index, 0, items.Count);
			return costs[index];
		}

		public int NextWallPrice()
		{
			List<int> costs = Config.Instance.CostData.Wall.UpgradeWallCosts;
			int index = Profile.Instance.UpgradeWallCount;
			index = Mathf.Clamp(index, 0, walls.Count);
			return costs[index];
		}

		public int NextFloorPrice()
		{
			List<int> costs = Config.Instance.CostData.Floor.UpgradeFloorCosts;
			int index = Profile.Instance.UpgradeFloorCount;
			index = Mathf.Clamp(index, 0, floors.Count);
			return costs[index];
		}

		public int GetUpgradeWallPrice()
		{
			if (UpgradeWallMax)
				return 0;

			List<int> costs = Config.Instance.CostData.Wall.UpgradeWallCosts;
			int index = Profile.Instance.UpgradeWallCount;
			index = Mathf.Clamp(index, 0, walls.Count - 1);
			return costs[index];
		}

		public int GetUpgradeFloorPrice()
		{
			if (UpgradeFloorMax)
				return 0;

			List<int> costs = Config.Instance.CostData.Floor.UpgradeFloorCosts;
			int index = Profile.Instance.UpgradeFloorCount;
			index = Mathf.Clamp(index, 0, floors.Count - 1);
			return costs[index];
		}
		public void UpgradeItemSuccess()
		{
			items[Profile.Instance.UpgradeDecoCount].Show(true);
			Profile.Instance.UpgradeDecoCount++;
			Profile.Instance.RequestSave();
		}

		public void UpgradeWallSuccess()
		{
			walls[Profile.Instance.UpgradeWallCount].Show(true);
            if (Profile.Instance.UpgradeWallCount > 0)
            {
				walls[Profile.Instance.UpgradeWallCount - 1].Hide();
			}
			Profile.Instance.UpgradeWallCount++;
			Profile.Instance.RequestSave();
		}

		public void UpgradeFloorSuccess()
		{
			floors[Profile.Instance.UpgradeFloorCount].Show(true);
			if (Profile.Instance.UpgradeFloorCount > 0)
			{
				floors[Profile.Instance.UpgradeFloorCount - 1].Hide();
			}
			Profile.Instance.UpgradeFloorCount++;
			Profile.Instance.RequestSave();
		}
		private void DisplayItem(int itemIndex)
		{
			for (int i = 0; i < items.Count; i++)
			{
				if (i < itemIndex)
				{
					items[i].Show(false);
				}
				else
				{
					items[i].Hide();
				}
			}
		}

		private void DisplayWall(int wallIndex)
		{
			for (int i = 0; i < walls.Count; i++)
			{
				if (i < wallIndex)
				{
					walls[i].Show(false);
				}
				else
				{
					walls[i].Hide();
				}
			}
		}

		private void DisplayFloor(int floorIndex)
		{
			for (int i = 0; i < floors.Count; i++)
			{
				if (i < floorIndex)
				{
					floors[i].Show(false);
				}
				else
				{
					floors[i].Hide();
				}
			}
		}

		int index = 0;
			[ContextMenu("Test")]
			private void Test()
			{
				index++;
				for (int i = 0; i < index; i++)
				{
					items[i].Show(true);
				}
			}

	}
}

