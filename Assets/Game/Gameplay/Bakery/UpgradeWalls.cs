using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Funzilla
{
    public class UpgradeWalls : MonoBehaviour
    {
		[SerializeField] private GameObject showWall;
		[SerializeField] private GameObject[] hideWall;
		private GameObject sparkFx, smokeFx;

		private void Awake()
		{
			smokeFx = this.transform.GetChild(0).Find("Smoke").gameObject;
			sparkFx = this.transform.GetChild(0).Find("Sparks").gameObject;
			smokeFx.SetActive(false);
			sparkFx.SetActive(false);
		}

		public void Show(bool withFx)
		{
			showWall?.SetActive(true);

			foreach (var hideWall in hideWall)
			{
				hideWall?.SetActive(false);
			}

			if (withFx)
			{
				StartCoroutine(IE_ShowFX());
			}
		}

		private IEnumerator IE_ShowFX()
		{
			showWall.SetActive(true);
			smokeFx.SetActive(true);
			yield return new WaitForSeconds(0.3f);
			sparkFx.SetActive(true);
		}

		public void Hide()
		{
			showWall?.SetActive(false);
		}
	}
}

