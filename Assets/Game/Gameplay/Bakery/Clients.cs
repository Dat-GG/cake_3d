using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Nama;

namespace Funzilla
{
    public class Clients : MonoBehaviour
    {
        [SerializeField] Animator animator;
        [SerializeField] Vector3 start;
        [SerializeField] Vector3 end;
		[SerializeField] Vector3 endToGiveCake;
		[SerializeField] SpriteRenderer preview;
        public Transform cakeTransform;
		public Transform cakeGiveTransform;
		[SerializeField] private ParticleEmitter[] PositiveEmitters;
		[SerializeField] private ParticleEmitter[] NagetiveEmitters;
		private Tween moveTween = null, turnTween = null;
		public Transform CakeTransform => cakeTransform;

		public void Init()
		{
			transform.localPosition = start;
			preview.transform.parent.gameObject.SetActive(false);
		}

        public void PlayPositiveFX()
		{
			foreach (var e in PositiveEmitters)
			{
				e.Emit();
			}
		}

		public void PlayNegativeFX()
		{
			foreach (var e in NagetiveEmitters)
			{
				e.Emit();
			}
		}

		public void SetPreview(Texture2D texture)
		{
			preview.sprite = Sprite.Create(texture, new Rect(0, 0, 256, 256), new Vector2(.5f, .5f));
		}

		public void TakeCake(GameObject cake)
		{
            if (Gameplay.Level.Match > .3f)
            {
				PlayPositiveFX();
			}
            else
            {
				PlayNegativeFX();

			}
			cake.transform.SetParent(cakeTransform);
			cake.transform.DOLocalRotate(new Vector3(0, cake.transform.localEulerAngles.y, 0), .45f);
			cake.transform.DOLocalMove(Vector3.zero, .45f);
		}

		public void WalkIn()
		{
			animator.SetTrigger("walk_in");
			Turn(end, 1);
			MoveTo(end, 5f, delegate
			{
				Turn(end - Vector3.forward, .8f, delegate
				{
					OnClientWalkinComplete();
				});
				Idle();
			});
		}

		public void WalkOut()
		{
			animator.SetTrigger("walk_out");
			DOVirtual.DelayedCall(.7f, delegate
			{
				Turn(start, .5f);
				MoveTo(start, 5f, delegate
				{
					Idle();
				});
			});
		}

		public void WalkToGive()
        {
			animator.SetTrigger("walk_out");
			MoveTo(endToGiveCake, 5f, delegate
			{
				Giving();
			});
		}

		public void Giving()
        {
			animator.SetTrigger("giving");
			Gameplay.Level.transform.SetParent(cakeGiveTransform);
			Gameplay.Level.transform.DOLocalMove(new Vector3(0, 0, 0), 0);
			Gameplay.Level.transform.DOLocalRotate(new Vector3(0, 0, 0), 0);
			DOVirtual.DelayedCall(2f, delegate
			{
				Idle();
				DOVirtual.DelayedCall(2.7f, delegate
				{
					Excited();
				});
			});

		}

		public void Excited()
        {
			animator.SetTrigger("excited");
        }
		public void WalkOutWithOutCake()
		{
			animator.SetTrigger("walk_in");
			preview.transform.parent.gameObject.SetActive(false);
			DOVirtual.DelayedCall(.7f, delegate
			{
				Turn(start, .5f);
				MoveTo(start, 5f, delegate
				{
					Idle();
				});
			});
		}

		public void Idle()
		{
			animator.SetTrigger("idle_" + Random.Range(0, 2));
		}

		public void Waiting()
		{
			transform.position = end;
			Idle();
		}

		public void Pay(bool happy)
		{
			animator.SetTrigger("pay_" + (happy ? 0 : 1));
		}

		void OnClientWalkinComplete()
		{
			// TODO: Khach hang den
			// Pet.Dance
			preview.transform.parent.gameObject.SetActive(true);
			preview.transform.parent.localScale = Vector3.zero;
			preview.transform.parent.DOScale(1, .3f);
		}

		void MoveTo(Vector3 position, float speed, System.Action onComplete = null)
		{
			var t = Vector3.Distance(position, transform.position) / speed;
			if (moveTween != null)
			{
				moveTween.Kill();
				moveTween = null;
			}
			transform.DOMove(position, t).SetEase(Ease.Linear).OnComplete(delegate
			{
				onComplete?.Invoke();
			});
		}

		void Turn(Vector3 towards, float time, System.Action onComplete = null)
		{
			if (turnTween != null)
			{
				turnTween.Kill();
				turnTween = null;
			}
			turnTween = transform.DOLookAt(towards, time).OnComplete(delegate
			{
				onComplete?.Invoke();
			});
		}
	}
}
