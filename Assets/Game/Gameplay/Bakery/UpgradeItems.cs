using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Funzilla
{
    public class UpgradeItems : MonoBehaviour
    {
		[SerializeField] private GameObject showItem;
		[SerializeField] private GameObject[] hideItems;
		private GameObject sparkFx, smokeFx;

		private void Awake()
		{
			smokeFx = this.transform.GetChild(0).Find("Smoke").gameObject;
			sparkFx = this.transform.GetChild(0).Find("Sparks").gameObject;
			smokeFx.SetActive(false);
			sparkFx.SetActive(false);
		}

		public void Show(bool withFx)
		{
			showItem?.SetActive(true);
			showItem.transform.localScale = Vector3.zero;
			showItem.transform.DOScale(1, 1f).SetEase(Ease.OutBack);

			foreach (var hideItem in hideItems)
			{
				hideItem?.SetActive(false);
			}

			if (withFx)
			{
				StartCoroutine(IE_ShowFX());
			}
		}

		private IEnumerator IE_ShowFX()
		{
			showItem.SetActive(true);
			smokeFx.SetActive(true);
			yield return new WaitForSeconds(0.3f);
			sparkFx.SetActive(true);
		}

		public void Hide()
		{
			showItem?.SetActive(false);
		}
	}
}

