using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class VipUI : MonoBehaviour
{
    [SerializeField] private GameObject noTksButton;
    // Start is called before the first frame update
    private void OnEnable()
    {
        DOVirtual.DelayedCall(4f, () => noTksButton.SetActive(true));
    }
}
