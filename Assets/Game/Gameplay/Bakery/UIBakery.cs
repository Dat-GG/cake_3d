using System;
using Nama;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Linq;
using Tabtale.TTPlugins;
using Random = UnityEngine.Random;
using NiobiumStudios;

namespace Funzilla
{
	internal class UIBakery : Scene
	{
		[SerializeField] private Text tapToPlay;
		[SerializeField] private Transform cakeTransform;
		[SerializeField] private Animator boxAnimator;
		[SerializeField] private Animator lidAnimator;
		[SerializeField] private Camera previewCamera;
		[SerializeField] private MeshRenderer table;
		public Clients _client;
		private const float AnimateTime = 1.0f;
		private const int Multiplier = 3;
		private bool _order = true;

		private readonly string[] _characterList =
			{"Camille", "Lenny", "Greg", "Willi", "Emilie", "Martha"};
		private readonly string[] _characterListFreeStyle =
			{"Lenny", "Greg", "Willi"};
		private readonly int[] _unlockMilestones = {3, 5, 7, 9};

		private string _lastClientName = "";
		private static readonly int PUT = Animator.StringToHash("put");
		private static readonly int Take = Animator.StringToHash("take");
		[SerializeField] private Image completeUnlockBar;
		[SerializeField] private Text percentageText;
		public GameObject unlockItem;
		[SerializeField] private Image[] item;
		[SerializeField] private Image[] popupItem;
		[SerializeField] private GameObject unlockBarFx;
		[SerializeField] private GameObject unlockFx;
		[SerializeField] private GameObject oderTable;
		[SerializeField] private GameObject tipJar;
		[SerializeField] private GameObject cakeBox;
		[SerializeField] private GameObject popupUnlockItem;
		private bool _popupOff = true;
		private bool _popupTipOff = true;
		private bool _upgradeOff = true;
		public bool _checkStart = true;
		[SerializeField] private GameObject vipUI;
		[SerializeField] private GameObject startUI;
		public GameObject tipUI;
		[SerializeField] private Button playVipButton;
		[SerializeField] private Button notksButton;
		[SerializeField] private GameObject popupTipJar;
		[SerializeField] private Text lostIt;
		[SerializeField] private Button rvTipButton;
		[SerializeField] private GameObject effTipButton;
		[SerializeField] private Button rvTipButtonInPopup;
		[SerializeField] private Image dark;
		[SerializeField] private Transform gemReward;
		[SerializeField] private GameObject diamondPrefab;
		[SerializeField] private Transform coinTarget;
		[SerializeField] private Text tipText;
		[SerializeField] private Transform upgradeCamPos;
		[SerializeField] private Transform initCamPos;
		[SerializeField] private Button backButton;
		[SerializeField] private Camera Camera;
		[SerializeField] private GameObject initUi;
		[SerializeField] private GameObject upgradeUi;
		[SerializeField] private UpgradeAreaController upgradeAreaController;
		public Animator buttonClaimRvAnim;
		public GameObject buttonVipEffect;
		public Animator buttonClaimTipAnim;
		public GameObject buttonTipEffect;
		public Text notAd;

		[SerializeField] private Button galleryButton;

		[Space] [Header("Shop")]
		[SerializeField] private Button shopButton;

		[SerializeField] private GameObject shopNotification;
		[SerializeField] private Animator shopButtonAnim;
		[SerializeField] private Image shopSignal;

		[Space] [Header("Upgrade")] [SerializeField]
		private Button upgradeButton;

		[SerializeField] private GameObject upgradeNotification;
		[SerializeField] private Animator upgradeButtonAnim;
		[SerializeField] private Button upgradeItemButton;
		[SerializeField] private Button upgradeWallButton;
		[SerializeField] private Button upgradeFloorButton;
		[SerializeField] private GameObject upgradeItem;
		[SerializeField] private GameObject upgradeWall;
		[SerializeField] private GameObject upgradeFloor;
		[SerializeField] private GameObject upgradeItemNoti;
		[SerializeField] private GameObject upgradeWallNoti;
		[SerializeField] private GameObject upgradeFloorNoti;
		[SerializeField] private Animator upgradeItemAnim;
		[SerializeField] private Animator upgradeWallAnim;
		[SerializeField] private Animator upgradeFloorAnim;
		[SerializeField] private GameObject floor0;
		[SerializeField] private GameObject wall0;

		[Space] [Header("Daily")]

		[SerializeField] private GameObject dailyRewardUi;
		[SerializeField] private Button dailyQuestButton;
		[SerializeField] private Animator dailyQuestButtonAnim;
		[SerializeField] private GameObject dailyQuestNotification;
		[SerializeField] private GameObject daily;
		[SerializeField] private Transform boxProgressDots;
		[SerializeField] private Text _day;
		[SerializeField] private Image progressIcon1;
		[SerializeField] private Image progressIcon2;
		[SerializeField] private Image progressIcon3;
		[SerializeField] private Image progressIcon4;
		[SerializeField] private Image bgProgress1;
		[SerializeField] private Image bgProgress2;
		public GameObject dailyQuestPopup;
		public DailyRewards DailyRewards;
		public DailyQuestManager DailyQuestManager;
		public static UIBakery Instance;
		private enum State
		{
			None,
			Begin,
			Idle,
			Start,
			Finish
		}

		private State _state = State.None;

		public override void Init(object data)
		{
			QualitySettings.shadowDistance = 50f;
			base.Init(data);
			_order = data == null || (bool) data;
			GameManager.IsLoadedGameplay = true;
			playVipButton.onClick.AddListener(OnClickAds);
			notksButton.onClick.AddListener(OnClickNotks);
			rvTipButton.onClick.AddListener(OnClickTipAdsOnTipJar);
			rvTipButtonInPopup.onClick.AddListener(OnClickTipAdsOnPopup);
			upgradeButton.onClick.AddListener(OnClickUpgrade);
			backButton.onClick.AddListener(OnClickBackButton);
			dailyQuestButton.onClick.AddListener(OnClickDailyQuestButton);
		}

		public void HidePopUp()
		{
			popupUnlockItem.transform.DOScale(0, AnimateTime / 2.5f).SetEase(Ease.InBack);
			if (_popupOff == false && Profile.Instance.TipCount >= 4)
			{
				_popupOff = true;
				UpdateTip();
			}
			else
			{
				_popupOff = true;
				NextClient();
			}
		}

		public void ShowPopUpTip()
		{
			_popupTipOff = false;
			popupTipJar.SetActive(true);
			dailyQuestPopup.SetActive(false);
			dailyRewardUi.SetActive(false);
			rvTipButton.gameObject.SetActive(false);
			effTipButton.SetActive(false);
			HideDaily();
			var rvReady = TTPRewardedAds.IsReady() ? 1 : 0;
			SendRvEvent("x3tip", "rvImpression", new Dictionary<string, object> {{"adReady", rvReady}});
			DOVirtual.DelayedCall(2f, delegate { lostIt.gameObject.SetActive(true); });
		}

		public void HidePopUpTip()
		{
			_popupTipOff = true;
			popupTipJar.transform.DOScale(0, AnimateTime / 2).SetEase(Ease.InBack);
			lostIt.gameObject.SetActive(false);
			rvTipButton.gameObject.SetActive(true);
			var rvReady = TTPRewardedAds.IsReady() ? 1 : 0;
			SendRvEvent("tip", "rvImpression", new Dictionary<string, object> {{"adReady", rvReady}});
			effTipButton.SetActive(true);
			Profile.Instance.ShowTipButton = true;
			tipText.text = "" + Profile.Instance.TipMoney;
			tipText.gameObject.SetActive(true);
			NextClient();
			ShowButton();
		}

		private const float Range = 580;
		private const float FillTime = 2;

		private void PlayUnlockBarFx()
		{
			unlockBarFx.SetActive(true);
			unlockBarFx.transform.DOLocalMoveX(Range, FillTime);
			DOVirtual.DelayedCall(1f, delegate { unlockBarFx.SetActive(false); });
		}

		private int GetCurrentUnlockItem()
		{
			int itemIndex = -1;
			for (var i = 0; i < _unlockMilestones.Length; i++)
			{
				if (_unlockMilestones[i] >= Profile.Instance.Level)
				{
					itemIndex = i;
					break;
				}
			}

			return itemIndex;
		}

		private float _currentProgress;
		private int _itemIndex = -1;

		private void UpdateUnlockItem()
		{
			if (Gameplay.PreviewLevel.CakeType == CakeType.Vip || _itemIndex < 0) return;
			unlockItem.gameObject.SetActive(true);
			popupItem[0].gameObject.SetActive(false);
			completeUnlockBar.DOFillAmount(_currentProgress, FillTime / 2)
				.OnUpdate(UpdateUnlockPercentText);
			PlayUnlockBarFx();
			if (_currentProgress < 1) return;
			_popupOff = false;
			item[_itemIndex].transform.DOScale(1.5f, AnimateTime).SetEase(Ease.OutBack);
			popupItem[_itemIndex].gameObject.SetActive(true);
			DOVirtual.DelayedCall(1.5f, delegate
			{
				startUI.SetActive(false);
				HideDaily();
				popupUnlockItem.gameObject.SetActive(true);
				unlockFx.SetActive(true);
				DOVirtual.DelayedCall(2f, delegate
				{
					if (_popupOff == false)
					{
						HidePopUp();
					}
				});
			});
		}

		private void UpdateTip()
		{
			if (Gameplay.PreviewLevel.CakeType == CakeType.Vip || _itemIndex < 0) return;
			Profile.Instance.TipCount++;
			if (Profile.Instance.TipCount < 4) return;
			if (_popupOff == true)
			{
				startUI.SetActive(false);
				ShowPopUpTip();
				HideButton();
				Profile.Instance.TipCount = 0;
			}
		}

		private void HideStartButton()
		{
			tapToPlay.gameObject.SetActive(false);
		}

		private void ShowStartButton()
		{
			tapToPlay.gameObject.SetActive(true);
			tapToPlay.transform.localScale = Vector3.zero;
			tapToPlay.transform.DOScale(1, AnimateTime).SetEase(Ease.OutBack);
		}

		private void ShowVipPlayButton()
		{
			var rvReady = TTPRewardedAds.IsReady() ? 1 : 0;
			SendVipRvEvent("rvImpression", new Dictionary<string, object> {{"adReady", rvReady}});
			playVipButton.transform.localScale = Vector3.zero;
			playVipButton.transform.DOScale(1, AnimateTime).SetEase(Ease.OutBack);
		}

		private static void PlayGame()
		{
			SceneManager.Instance.OpenScene(SceneID.Gameplay);
			SceneManager.Instance.CloseScene(SceneID.UIBakery);
		}

		private void Awake()
		{
			Instance = this;
			cakeBox.SetActive(false);
			_state = State.Start;
		}

		public Vector3 _touchPos;
		public Vector3 screenPos;

		public void OnTouchDown()
		{
			switch (_state)
			{
				case State.Begin:
					_state = State.Start;
					_touchPos = screenPos;
					break;
				case State.Start:
                    if (_checkStart)
                    {
						PlayGame();
					}
					break;
				case State.Finish:
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private bool _touched;

		private void DoFrame()
		{
			var eventSystem = UnityEngine.EventSystems.EventSystem.current;
			if (_state == State.None) return;
#if UNITY_EDITOR
			if (Input.GetMouseButtonDown(0) && !eventSystem.IsPointerOverGameObject())
			{
				_touched = true;
				screenPos = Input.mousePosition;
				OnTouchDown();
			}
#else
			if (Input.touchCount <= 0)
			{
				return;
			}

			switch (Input.touches[0].phase)
			{
				case TouchPhase.Began:
					if (!eventSystem.IsPointerOverGameObject(Input.touches[0].fingerId))
					{
						_touched = true;
						OnTouchDown();
					}
					break;

				case TouchPhase.Stationary:
					break;
			}
#endif
		}

		private void LoadClient()
		{
			if (AssetManager.Instance.ClientName == "")
			{
				var list = _characterList.ToList();
				if (list.Contains(_lastClientName))
					list.Remove(_lastClientName);
				_lastClientName = list.GetRandomElement();
				AssetManager.Instance.ClientName = _lastClientName;
			}
			else
			{
				_lastClientName = AssetManager.Instance.ClientName;
			}

			var go = Resources.Load<Clients>("Clients/" + _lastClientName);
			if (go == null) return;
			if (_client != null)
			{
				Destroy(_client.gameObject);
			}
			_client = Instantiate(go, transform);
			_client.Init();
            if (_upgradeOff == false)
            {
				_client.gameObject.SetLayerRecursively(5);
			}
		}

		private void PreviewNextLevel(Action onComplete)
		{
			if (Gameplay.PreviewLevel) Destroy(Gameplay.PreviewLevel.gameObject);
			var prefab = AssetManager.GetLevel();
			StartCoroutine(PreviewCake(prefab, texture =>
			{
				Gameplay.CorrectTexture = texture;
				onComplete?.Invoke();
			}));
		}

		private IEnumerator PreviewCake(Level prefab, Action<RenderTexture> onComplete)
		{
			Gameplay.PreviewLevel = Instantiate(prefab, AssetManager.Instance.transform);
			Gameplay.PreviewLevel.Init(true);
			yield return new WaitForEndOfFrame();
			table.gameObject.SetActive(true);
			Gameplay.SetupForPreview(Gameplay.PreviewLevel, previewCamera, table);
			var texture = Gameplay.RenderTexture2D(previewCamera, 256,
				Gameplay.PreviewLevel.PreviewCameraTransform);
			Gameplay.ResetAfterPreview(Gameplay.PreviewLevel, table);
			table.gameObject.SetActive(false);
			Gameplay.PreviewLevel.gameObject.SetActive(false);
			onComplete?.Invoke(texture);
		}

		private void ShowButton()
		{
			if (Profile.Instance.ShowShopButton == true)
			{
				shopButton.gameObject.SetActive(true);
			}

			if (Profile.Instance.ShowTipButton == true)
			{
				rvTipButton.gameObject.SetActive(true);
				tipText.gameObject.SetActive(true);
				effTipButton.SetActive(true);
			}

			if (Profile.Instance.ShowUpgradeButton == true)
			{
				upgradeButton.gameObject.SetActive(true);
			}

			if (Profile.Instance.ShowGalleryButton == true)
			{
				galleryButton.gameObject.SetActive(true);
			}

            if (Profile.Instance.ShowDailyQuestButton == true)
            {
				dailyQuestButton.gameObject.SetActive(true);
			}
			
			tipJar.SetActive(true);
		}

		private void HideButton()
		{
			shopButton.gameObject.SetActive(false);
			upgradeButton.gameObject.SetActive(false);
			tipJar.SetActive(false);
			rvTipButton.gameObject.SetActive(false);
			tipText.gameObject.SetActive(false);
			effTipButton.SetActive(false);
			galleryButton.gameObject.SetActive(false);
			dailyQuestButton.gameObject.SetActive(false);
		}

		private void InitClientWithRequest()
		{
			PreviewNextLevel(() =>
			{
				if (Gameplay.PreviewLevel != null && Gameplay.PreviewLevel.CakeType == CakeType.Vip)
				{
					var clients = AssetManager.VipClients;
					AssetManager.Instance.ClientName =
						clients[(Profile.Instance.VipLevel - 1) % clients.Length];
					HideStartButton();
					startUI.SetActive(false);
					vipUI.SetActive(true);
					HideButton();
					HideDaily();
					ShowVipPlayButton();
					unlockItem.SetActive(false);
				}
				else
				{
					if (Gameplay.PreviewLevel != null && Gameplay.PreviewLevel.LevelStype == LevelStyle.FreeStyle)
					{
						var list =  _characterListFreeStyle.ToList();
						if (list.Contains(_lastClientName))
							list.Remove(_lastClientName);
						_lastClientName = list.GetRandomElement();
						AssetManager.Instance.ClientName = _lastClientName;

					}
					ShowButton();
					ShowDaily();
				}

				LoadClient();
				_client.WalkIn();
				if(Gameplay.PreviewLevel.LevelStype==LevelStyle.Normal)
					_client.SetPreview(Gameplay.ToTexture2D(Gameplay.CorrectTexture));
				else
				{
					_client.SetPreview(Gameplay.PreviewLevel.FreeStyleTexture);
				}
			});
			DOVirtual.DelayedCall(2.0f, delegate
			{
				if (Gameplay.PreviewLevel.CakeType != CakeType.Vip)
				{
					startUI.SetActive(true);
					ShowStartButton();
				}
			});
		}

		private void OpenHome()
		{
			UpdateDaily();
			InitClientWithRequest();
			ShowUnlockUI();
			completeUnlockBar.fillAmount = _currentProgress;
			UpdateUnlockPercentText();
			ShowNextUnlockItem();
			ShowShopButton();
			ShowDaily();
			CheckDailyQuestButtonNotification();
		}

		private void UpdateUnlockPercentText()
		{
			percentageText.text = Mathf.FloorToInt(completeUnlockBar.fillAmount * 100) + "%";
		}

		private void ShowUnlockUI()
		{
			_itemIndex = GetCurrentUnlockItem();
			vipUI.SetActive(Gameplay.PreviewLevel != null &&
			                Gameplay.PreviewLevel.CakeType == CakeType.Vip);
			if (!vipUI.activeSelf && _itemIndex >= 0 && _itemIndex < _unlockMilestones.Length)
			{
				var previous = _itemIndex > 0 ? _unlockMilestones[_itemIndex - 1] : 1;
				var next = _unlockMilestones[_itemIndex];
				var count = next - previous;
				var progress = Profile.Instance.Level - previous;
				_currentProgress = (float) progress / count;
				var previousProgress = (float) (progress - 1) / count;
				completeUnlockBar.fillAmount = previousProgress;
				UpdateUnlockPercentText();
				unlockItem.SetActive(true);
				item[_itemIndex].gameObject.SetActive(true);
			}
			else
			{
				unlockItem.SetActive(false);
			}
		}

		private void ReceiveCake()
		{
			_itemIndex = -1;
			if (Gameplay.PreviewLevel.CakeType == CakeType.Vip)
			{
				unlockItem.SetActive(false);
				HideButton();
				HideDaily();
			}
			else
			{
				ShowDaily();
				ShowUnlockUI();
				ShowButton();
			}

			StartCoroutine(IEReceiveCake());
		}

		private IEnumerator IEReceiveCake()
		{
			LoadClient();
			UpdateDaily();
			cakeBox.SetActive(true);
			_client.transform.DOLocalRotate(new Vector3(0, 180, 0), 0.1f);
			_client.Waiting();
			yield return new WaitForSeconds(.4f);
			_client.Pay(Gameplay.Level.Match > .3f);
			yield return new WaitForSeconds(.3f);
			yield return new WaitForSeconds(.7f);
			boxAnimator.SetTrigger(Take);
			yield return new WaitForSeconds(1.0f);
			lidAnimator.SetTrigger(PUT);
			yield return new WaitForSeconds(0.4f);
			_client.WalkOut();
			_client.TakeCake(cakeTransform.gameObject);
           
            UpdateUnlockItem();
            yield return new WaitForSeconds(0.2f);
			UpdateTip();
			yield return new WaitForSeconds(0.2f);

			if (Gameplay.Level.LevelStype == LevelStyle.FreeStyle)
			{
				OpenGiveCakeScene();
			}
			else
			{
				var obj = Gameplay.Level.gameObject;
				obj.SetActive(false);
				Destroy(obj);
				Gameplay.Level = null;
				yield return new WaitForSeconds(2f);
				AssetManager.Instance.ClientName = "";
				yield return new WaitForSeconds(0.2f);
			
				if (_popupOff == true && _popupTipOff == true)
				{
					NextClient();
				}
			}
		}

		private void OpenGiveCakeScene()
        {
			SceneManager.Instance.ShowLoading(false, 1.0f, () =>
			{
				SceneManager.Instance.HideLoading();
				Gameplay.Level.transform.SetParent(AssetManager.Instance.transform);
				SceneManager.Instance.CloseScene(SceneID.UIBakery);
				SceneManager.Instance.OpenScene(SceneID.GiveCake);
			});
		}

        private void ShowNextUnlockItem()
		{
			if (_currentProgress >= 1)
			{
				_currentProgress = 0;
				completeUnlockBar.DOFillAmount(0, FillTime / 2).OnUpdate(()
					=>
				{
					percentageText.text =
						Mathf.FloorToInt(completeUnlockBar.fillAmount * 100) + "%";
				});
				item[_itemIndex].gameObject.SetActive(false);
				popupItem[_itemIndex].gameObject.SetActive(false);
				_itemIndex++;
			}

			if (_itemIndex < 0 || _itemIndex >= item.Length)
			{
				unlockItem.gameObject.SetActive(false);
				popupUnlockItem.gameObject.SetActive(false);
			}
			else
			{
				item[_itemIndex].gameObject.SetActive(true);
			}
		}

		public void NextClient()
		{
			ShowNextUnlockItem();
			DOVirtual.DelayedCall(0.5f, delegate
			{
				LoadClient();
				OpenHome();
				_checkStart = true;
			});
			unlockFx.gameObject.SetActive(false);
		}

		private void Start()
		{
			DailyQuestManager.CheckQuestStatus();
			galleryButton.onClick.AddListener(() => { SceneManager.Instance.OpenPopup(SceneID.Gallery); });
			galleryButton.gameObject.SetActive(Profile.Instance.Level >= 3);
            if (Profile.Instance.Level >= 3)
            {
				Profile.Instance.ShowGalleryButton = true;
			}

			if (Profile.Instance.CountShowUpgradeBtn >= 2)
			{
				Profile.Instance.ShowUpgradeButton = true;
				upgradeButton.gameObject.SetActive(true);
			}

            if (Profile.Instance.Level >= 3)
            {
				Profile.Instance.ShowDailyQuestButton = true;
			}

			if (Profile.Instance.Level >= 3 && Profile.Instance.CheckClaimDailyReward == false)
			{
				unlockItem.transform.DOScale(0, 0);
				dailyRewardUi.SetActive(true);
			}
			SceneManager.Instance.HideSplash();
			SceneManager.Instance.HideLoading();
			HideStartButton();

			if (_order)
			{
				OpenHome();
			}
			else
			{
				if (Gameplay.Level)
				{
					if (string.IsNullOrEmpty(AssetManager.Instance.ClientName) &&
						Gameplay.PreviewLevel.CakeType == CakeType.Vip)
					{ // This should never happens, just to be safe
						var clients = AssetManager.VipClients;
						AssetManager.Instance.ClientName =
							clients[(Profile.Instance.VipLevel - 2) % clients.Length];
					}

					Gameplay.Level.gameObject.SetLayerRecursively(Layers.Bakery);
					Gameplay.Level.transform.localScale = Vector3.one * 0.3f;
					Gameplay.Level.transform.localEulerAngles = Vector3.zero;
					var objects = Gameplay.Level.GetComponentsInChildren<SprinkleObject>();
					foreach (var obj in objects)
					{
						if (!obj.OnSurface)
						{
							Destroy(obj.gameObject);
						}
					}
					var boxTexture = Resources.Load<Texture>("Boxes/" + Random.Range(1, 7));
					if (boxTexture != null)
					{
						boxAnimator.GetComponent<Renderer>().sharedMaterial.mainTexture =
							boxTexture;
						lidAnimator.GetComponent<Renderer>().sharedMaterial.mainTexture =
							boxTexture;
					}

					Gameplay.Level.transform.SetParent(cakeTransform, false);
				}
				_checkStart = false;
				boxAnimator.gameObject.SetActive(true);
				ReceiveCake();
				if (Profile.Instance.Level % 4 == 1)
				{
					Profile.Instance.DayCount++;
				}
			}
		}

		private static void SendVipRvEvent(string eventName,
			Dictionary<string, object> parameters = null)
		{
			parameters ??= new Dictionary<string, object>();
			parameters.Add("lastVipMissionID", Profile.Instance.VipLevel);
			parameters.Add("location", "vip");
			Ads.SendCustomFirebaseEvent(eventName, parameters);
		}
		
		private static void SendRvEvent(string placement, string eventName, Dictionary<string, object> parameters = null)
		{
			parameters ??= new Dictionary<string, object>();
			if (Gameplay.PreviewLevel.CakeType == CakeType.Vip)
			{
				parameters.Add("lastVipMissionID",  Profile.Instance.VipLevel);
			}
			else
			{
				parameters.Add("lastMissionID",  Profile.Instance.Level);
			}
			
			parameters.Add("location", "vip");
			Ads.SendCustomFirebaseEvent(eventName, parameters);
		}

		public void OnClickAds()
		{
			notksButton.gameObject.GetComponent<Button>().interactable = false;
			var rvReady = TTPRewardedAds.IsReady() ? 1 : 0;
			SendVipRvEvent("rvClicked", new Dictionary<string, object> {{"adReady", rvReady}});
			Ads.ShowRewardedVideo("vip", (result) =>
			{
				if (result != RewardedVideoState.Watched)
				{
					notksButton.gameObject.GetComponent<Button>().interactable = true;
					return;
				}

				SendVipRvEvent("rvWatched");
				PlayGame();
				DailyQuestManager.CheckQuestStatus();
			});
		}

		public void OnClickNotks()
		{
			playVipButton.gameObject.GetComponent<Button>().interactable = false;
			notksButton.gameObject.SetActive(false);
			playVipButton.transform.DOScale(0, AnimateTime).SetEase(Ease.InBack);
			_client.WalkOutWithOutCake();
			Profile.Instance.VipLevel++;
			vipUI.SetActive(false);
			DOVirtual.DelayedCall(2f, () =>
			{
				AssetManager.Instance.ClientName = "";
				NextClient();
			});
		}

		public void OnClickTipAdsOnPopup()
		{
			SendRvEvent("x3tip", "rvClicked");
			Ads.ShowRewardedVideo("x3tip", (result) =>
			{
				if (result != RewardedVideoState.Watched)
				{
					return;
				}

				SendRvEvent("x3tip", "rvWatched");
				StartCoroutine(RewardGems(Profile.Instance.TipMoney * Multiplier));
				DOVirtual.DelayedCall(0.3f, delegate
				{
					_popupTipOff = true;
					popupTipJar.transform.DOScale(0, AnimateTime / 2).SetEase(Ease.InBack);
					lostIt.gameObject.SetActive(false);
					dark.gameObject.SetActive(false);
					NextClient();
				});
			});
		}

		public void OnClickTipAdsOnTipJar()
		{
			var rvReady = TTPRewardedAds.IsReady() ? 1 : 0;
			SendRvEvent("tip", "rvClicked", new Dictionary<string, object> {{"adReady", rvReady}});
			Ads.ShowRewardedVideo("tip", (result) =>
			{
				if (result != RewardedVideoState.Watched)
				{
					return;
				}

				SendRvEvent("tip", "rvWatched");
				Analytics.Instance.LogEvent("x1money_watched");
				StartCoroutine(RewardGems(Profile.Instance.TipMoney));
				DOVirtual.DelayedCall(0.5f, delegate
				{
					dark.gameObject.SetActive(false);
					rvTipButton.gameObject.SetActive(false);
					tipText.gameObject.SetActive(false);
					effTipButton.SetActive(false);
				});
			});
			Profile.Instance.ShowTipButton = false;
		}

		private IEnumerator RewardGems(int amount, float timeScale = 1f)
		{
			dark.gameObject.SetActive(true);
			gemReward.gameObject.SetActive(true);
			for (var i = 0; i < 20; i++)
			{
				var diamond = Instantiate(diamondPrefab, gemReward).transform;
				diamond.localRotation = Random.rotation;
				diamond.DOLocalMove(Random.insideUnitSphere, Random.Range(0.3f, 0.7f) / timeScale)
					.SetEase(Ease.OutCubic);
				diamond.DOLocalRotate(Random.rotation.eulerAngles, 10).SetEase(Ease.Linear);
			}

			yield return new WaitForSeconds(0.7f / timeScale);
			UIGlobal.ShowCoinInfo();

			var counter = 0;
			var gemCount = gemReward.childCount;
			var n = amount / gemCount;
			var a = amount % gemCount;
			for (var i = 0; i < gemReward.childCount; i++)
			{
				var diamond = gemReward.GetChild(i);
				diamond.transform.localScale *= 150;
				diamond.DOMove(coinTarget.transform.position, 0.5f / timeScale)
					.SetEase(Ease.InCubic)
					.SetDelay(i * 0.05f)
					.OnComplete(() =>
					{
						diamond.gameObject.SetActive(false);
						counter += n + (a > 0 ? 1 : 0);
						a--;
						if (counter <= amount)
						{
							UIGlobal.ShowCoinAmount(Profile.Instance.CoinAmount + counter);
						}
					});
			}

			yield return new WaitForSeconds(gemReward.childCount * 0.05f + 0.6f / timeScale);
			Profile.Instance.CoinAmount += amount;
			Profile.Instance.MoneyCurrentEarn += amount;
			UIGlobal.ShowCoinAmount(Profile.Instance.CoinAmount);
			Profile.Instance.TipMoney += 25;
			yield return new WaitForSeconds(0.8f);
			UIGlobal.HideCoinInfo();
		}

		private void OnEnable()
		{
			ShowShopButton();
			CheckUpgradeButtonNotification();
			CheckUpgradeButtonState();
			CheckDailyQuestButtonNotification();
			DailyRewards.onClaimPrize += OnClaimPrizeDailyRewards;
		}

		public void HideShopButton()
        {
			shopButton.gameObject.SetActive(false);
		}

		public void ShowShopButton()
		{
			// Skin button
			shopButton.interactable = true;
			var isNew =
				(ShopManager.Instance.PastryBagsUnlockable &&
				 !ShopManager.Instance.AllPastryBagsUnlocked) ||
				(ShopManager.Instance.SprinkleBottleUnlockable &&
				 !ShopManager.Instance.AllSprinklBottleUnlocked) ||
				(PetManager.Instance.Unlockable && !PetManager.Instance.AllUnlocked) ||
				(GlazeManager.Instance.Unlockable && !GlazeManager.Instance.AllUnlocked);

			shopButton.gameObject.SetActive(
				ShopManager.Instance.PastryBagsUnlockable ||
				Profile.Instance.PastryBagAmount > 1 ||
				ShopManager.Instance.SprinkleBottleUnlockable ||
				Profile.Instance.SprinkleBottleAmount > 1 ||
				PetManager.Instance.Unlockable ||
				Profile.Instance.PetAmount > 0 ||
				GlazeManager.Instance.Unlockable ||
				Profile.Instance.GlazeAmount > 1);

			if (Profile.Instance.ShowShopButton == true)
			{
				shopButton.gameObject.SetActive(true);
			}

			shopButtonAnim.enabled = false;
			if (isNew)
			{
				Profile.Instance.ShowShopButton = true;
				shopButtonAnim.enabled = true;
				Profile.Instance.ShopNotified = true;
				shopSignal.gameObject.SetActive(true);
			}

			shopNotification.SetActive(shopButtonAnim.enabled);
			if (!shopButtonAnim.enabled)
			{
				shopButtonAnim.transform.localEulerAngles = Vector3.zero;
				shopSignal.gameObject.SetActive(false);
			}
		}

		public void OnClickShop()
		{
			_checkStart = false;
			SceneManager.Instance.ShowLoading(false, 1.0f, () =>
			{
				SceneManager.Instance.HidePopupShield();
				SceneManager.Instance.OpenScene(SceneID.Shop);
			});
		}

		public void OnClickUpgrade()
		{
			_upgradeOff = false;
			Camera.transform.DOLocalMove(upgradeCamPos.position, 1f);
			Camera.transform.DOLocalRotateQuaternion(upgradeCamPos.localRotation, 1f);
			_client.gameObject.SetLayerRecursively(5);
			oderTable.SetActive(false);
			initUi.SetActive(false);
			rvTipButton.gameObject.SetActive(false);
			tipText.gameObject.SetActive(false);
			effTipButton.SetActive(false);
			upgradeUi.SetActive(true);
			UIGlobal.ShowCoinInfo();
		}

		public void OnClickBackButton()
		{
			_upgradeOff = true;
			Camera.transform.DOLocalMove(initCamPos.position, 1f);
			Camera.transform.DOLocalRotateQuaternion(initCamPos.localRotation, 1f);
			DOVirtual.DelayedCall(0.2f, delegate
			{
				_client.gameObject.SetLayerRecursively(3);
				oderTable.SetActive(true);
			});
			initUi.SetActive(true);
			DOVirtual.DelayedCall(1f, delegate
			{
				if (Profile.Instance.ShowTipButton == true)
				{
					rvTipButton.gameObject.SetActive(true);
					tipText.gameObject.SetActive(true);
					effTipButton.SetActive(true);
				}
			});
			upgradeUi.SetActive(false);
			UIGlobal.HideCoinInfo();
			CheckDailyQuestButtonNotification();
		}

		public void OnClickUpgradeItemsButton()
		{
			int upgradePrice = upgradeAreaController.GetUpgradeItemPrice();
			if (Profile.Instance.CoinAmount >= upgradePrice)
			{
				Profile.Instance.CoinAmount -= upgradePrice;
				Profile.Instance.UpgradeBakeryCurrentTime++;
				UIGlobal.ShowCoinAmount(Profile.Instance.CoinAmount);
				upgradeAreaController.UpgradeItemSuccess();
				CheckUpgradeButtonState();
			}
		}

		public void OnClickUpgradeWallButton()
		{
			int upgradePrice = upgradeAreaController.GetUpgradeWallPrice();
			if (Profile.Instance.CoinAmount >= upgradePrice)
			{
				Profile.Instance.CoinAmount -= upgradePrice;
				Profile.Instance.UpgradeBakeryCurrentTime++;
				UIGlobal.ShowCoinAmount(Profile.Instance.CoinAmount);
				upgradeAreaController.UpgradeWallSuccess();
				CheckUpgradeButtonState();
			}
			wall0.SetActive(false);
		}

		public void OnClickUpgradeFloorButton()
		{
			int upgradePrice = upgradeAreaController.GetUpgradeFloorPrice();
			if (Profile.Instance.CoinAmount >= upgradePrice)
			{
				Profile.Instance.CoinAmount -= upgradePrice;
				Profile.Instance.UpgradeBakeryCurrentTime++;
				UIGlobal.ShowCoinAmount(Profile.Instance.CoinAmount);
				upgradeAreaController.UpgradeFloorSuccess();
				CheckUpgradeButtonState();
			}
			floor0.SetActive(false);
		}

		private void CheckUpgradeItemButtonState()
		{
			Text gemText = upgradeItemButton.transform.Find("TextGem").GetComponent<Text>();
			Text lvText = upgradeItem.transform.Find("Level").GetComponent<Text>();
			var lvNumber = Profile.Instance.UpgradeDecoCount + 1;
			if (upgradeAreaController.UpgradeItemMax)
			{
				upgradeItemNoti.SetActive(false);
				upgradeItemAnim.enabled = false;
				upgradeItemButton.gameObject.SetActive(false);
				lvText.text = "Level " + (lvNumber - 1);
			}
			else if (!upgradeAreaController.CanUpgradeItem && !upgradeAreaController.UpgradeItemMax)
			{
				upgradeItemNoti.SetActive(false);
				upgradeItemAnim.enabled = false;
				upgradeItemButton.gameObject.GetComponent<Button>().interactable = false;
				gemText.text = upgradeAreaController.NextItemPrice().ToString();
				lvText.text = "Level " + lvNumber;
			}
			else
			{
				upgradeItemNoti.SetActive(true);
				upgradeItemAnim.enabled = true;
				upgradeItemButton.gameObject.GetComponent<Button>().interactable = true;
				gemText.text = upgradeAreaController.GetUpgradeItemPrice().ToString();
				lvText.text = "Level " + lvNumber;
			}
		}

		private void CheckUpgradeWallButtonState()
		{
			Text gemText = upgradeWallButton.transform.Find("TextGem").GetComponent<Text>();
			Text lvText = upgradeWall.transform.Find("Level").GetComponent<Text>();
			var lvNumber = Profile.Instance.UpgradeWallCount + 1;
			if (upgradeAreaController.UpgradeWallMax)
			{
				upgradeWallNoti.SetActive(false);
				upgradeWallAnim.enabled = false;
				upgradeWallButton.gameObject.SetActive(false);
				lvText.text = "Level " + (lvNumber - 1);
			}
			else if (!upgradeAreaController.CanUpgradeWall && !upgradeAreaController.UpgradeWallMax)
			{
				upgradeWallNoti.SetActive(false);
				upgradeWallAnim.enabled = false;
				upgradeWallButton.gameObject.GetComponent<Button>().interactable = false;
				gemText.text = upgradeAreaController.NextWallPrice().ToString();
				lvText.text = "Level " + lvNumber;
			}
			else
			{
				upgradeWallNoti.SetActive(true);
				upgradeWallAnim.enabled = true;
				upgradeWallButton.gameObject.GetComponent<Button>().interactable = true;
				gemText.text = upgradeAreaController.GetUpgradeWallPrice().ToString();
				lvText.text = "Level " + lvNumber;
			}
		}

		private void CheckUpgradeFloorButtonState()
		{
			Text gemText = upgradeFloorButton.transform.Find("TextGem").GetComponent<Text>();
			Text lvText = upgradeFloor.transform.Find("Level").GetComponent<Text>();
			var lvNumber = Profile.Instance.UpgradeFloorCount + 1;
			if (upgradeAreaController.UpgradeFloorMax)
			{
				upgradeFloorNoti.SetActive(false);
				upgradeFloorAnim.enabled = false;
				upgradeFloorButton.gameObject.SetActive(false);
				lvText.text = "Level " + (lvNumber - 1);
			}
			else if (!upgradeAreaController.CanUpgradeFloor &&
			         !upgradeAreaController.UpgradeFloorMax)
			{
				upgradeFloorNoti.SetActive(false);
				upgradeFloorAnim.enabled = false;
				upgradeFloorButton.gameObject.GetComponent<Button>().interactable = false;
				gemText.text = upgradeAreaController.NextFloorPrice().ToString();
				lvText.text = "Level " + lvNumber;
			}
			else
			{
				upgradeFloorNoti.SetActive(true);
				upgradeFloorAnim.enabled = true;
				upgradeFloorButton.gameObject.GetComponent<Button>().interactable = true;
				gemText.text = upgradeAreaController.GetUpgradeFloorPrice().ToString();
				lvText.text = "Level " + lvNumber;
			}
		}

		private void CheckUpgradeButtonNotification()
		{
			var condition = (!upgradeAreaController.UpgradeItemMax &&
			                 upgradeAreaController.CanUpgradeItem) ||
			                (!upgradeAreaController.UpgradeWallMax &&
			                 upgradeAreaController.CanUpgradeWall) ||
			                (!upgradeAreaController.UpgradeFloorMax &&
			                 upgradeAreaController.CanUpgradeFloor);

			upgradeButtonAnim.enabled = false;
			if (condition)
			{
				upgradeButtonAnim.enabled = true;
				Profile.Instance.UpgradeNotified = true;
			}

			upgradeNotification.SetActive(upgradeButtonAnim.enabled);
			if (!upgradeButtonAnim.enabled)
			{
				upgradeButtonAnim.transform.localEulerAngles = Vector3.zero;
			}
		}

		public void CheckUpgradeButtonState()
		{
			CheckUpgradeItemButtonState();
			CheckUpgradeWallButtonState();
			CheckUpgradeFloorButtonState();
			CheckUpgradeButtonNotification();
			ShowShopButton();
			DailyQuestManager.CheckQuestStatus();
		}

		private void UpdateDaily()
		{
			var level = Profile.Instance.Level - 2;
			var p = AssetManager.Instance.CurrentProgress(Mathf.Max(level, 1));
			var n = level % 4;
			if (!Config.Instance.DisableProgress && p != null)
			{
				n = level - p.levelStart + 1;
			}

			for (var i = 0; i < 4; i++)
			{
				var green = i <= n;
				//boxProgressDots.GetChild(i).GetChild(0).gameObject.SetActive(!green);
				boxProgressDots.GetChild(i).GetChild(1).gameObject.SetActive(green);
				boxProgressDots.GetChild(i).GetChild(2).gameObject.SetActive(i == n + 1);
				boxProgressDots.GetChild(i).gameObject.SetActive((p != null && i <= p.levelEnd - p.levelStart) || Config.Instance.DisableProgress);
                if (Profile.Instance.Level % 4 == 1)
                {
					boxProgressDots.GetChild(0).GetChild(2).gameObject.SetActive(true);
					boxProgressDots.GetChild(1).GetChild(1).gameObject.SetActive(false);
					boxProgressDots.GetChild(2).GetChild(1).gameObject.SetActive(false);
					boxProgressDots.GetChild(3).GetChild(1).gameObject.SetActive(false);					
				}
                if (Profile.Instance.Level <5)
                {
					progressIcon1.gameObject.SetActive(true);
					progressIcon2.gameObject.SetActive(true);
				}
				else if (Profile.Instance.Level >= 5 && Profile.Instance.Level < 9)
                {
					progressIcon1.gameObject.SetActive(false);
					progressIcon2.gameObject.SetActive(false);
					progressIcon3.gameObject.SetActive(true);
					progressIcon4.gameObject.SetActive(true);
				}
                else
                {
					bgProgress1.gameObject.SetActive(false);
					bgProgress2.gameObject.SetActive(false);
				}
			}
		}

		private void ShowDaily()
        {
			daily.SetActive(true);			
			_day.text = "DAY " + (Profile.Instance.DayCount + 1);
        }

		private void HideDaily()
		{
			daily.SetActive(false);
		}

		public void OnClickDailyRewardButton()
        {
			dailyRewardUi.SetActive(true);
			dailyQuestPopup.SetActive(false);
        }

		public void OnClickDailyQuestButton()
        {
			dailyQuestPopup.SetActive(true);
			//dailyRewardUi.SetActive(true);
			DailyQuestManager.Instance.CheckQuestStatus();
			unlockItem.transform.DOScale(0, 0);
			tipUI.SetLayerRecursively(5);
		}

		public void OnClickCloseDailyRewardButton()
        {
			dailyRewardUi.SetActive(false);
        }

		void OnDisable()
		{
			DailyRewards.onClaimPrize -= OnClaimPrizeDailyRewards;
		}
		public void OnClaimPrizeDailyRewards(int day)
		{
			Reward myReward = DailyRewards.GetReward(day);
			print(myReward.reward);
			Profile.Instance.CoinAmount += myReward.reward;
			Profile.Instance.CheckClaimDailyReward = true;
			UIGlobal.ShowCoinInfo();
		}

		public void CheckDailyQuestButtonNotification()
		{
			var condition = (Profile.Instance.MoneyCurrentSpendOnShop >= DailyQuestManager.moneySpendOnShopGoal
							&& Profile.Instance.CompleteQ1 < 1) ||
							(Profile.Instance.MoneyCurrentEarn >= DailyQuestManager.moneyEarnGoal
							&& Profile.Instance.CompleteQ2 < 1) ||
							(Profile.Instance.UpgradeBakeryCurrentTime >= DailyQuestManager.upgradeBakeryTimeGoal
							&& Profile.Instance.CompleteQ3 < 1) ||
							(Profile.Instance.PlayVipLevelCurrentTime >= DailyQuestManager.playVipLevelTimeGoal
							&& Profile.Instance.CompleteQ4 < 1) ||
							(Profile.Instance.UseMirrorGlazeCurrentTime >= DailyQuestManager.useMirrorGlazeTimeGoal
							&& Profile.Instance.CompleteQ5 < 1) ||
							(Profile.Instance.LitTheCandlesCurrentTime >= DailyQuestManager.litTheCandlesTimeGoal
							&& Profile.Instance.CompleteQ6 < 1);

			dailyQuestButtonAnim.enabled = false;
			if (condition)
			{
				dailyQuestButtonAnim.enabled = true;
				Profile.Instance.DailyQuestNotified = true;
			}

			dailyQuestNotification.SetActive(dailyQuestButtonAnim.enabled);
			if (!dailyQuestButtonAnim.enabled)
			{
				dailyQuestButtonAnim.transform.localEulerAngles = Vector3.zero;
			}
		}
	}
}
