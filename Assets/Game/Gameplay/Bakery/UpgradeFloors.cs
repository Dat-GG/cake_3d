using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Funzilla
{
    public class UpgradeFloors : MonoBehaviour
    {
		[SerializeField] private GameObject showFloor;
		[SerializeField] private GameObject[] hideFloor;
		private GameObject sparkFx, smokeFx;

		private void Awake()
		{
			smokeFx = this.transform.GetChild(0).Find("Smoke").gameObject;
			sparkFx = this.transform.GetChild(0).Find("Sparks").gameObject;
			smokeFx.SetActive(false);
			sparkFx.SetActive(false);
		}

		public void Show(bool withFx)
		{
			showFloor?.SetActive(true);

			foreach (var hideFloor in hideFloor)
			{
				hideFloor?.SetActive(false);
			}

			if (withFx)
			{
				StartCoroutine(IE_ShowFX());
			}
		}

		private IEnumerator IE_ShowFX()
		{
			showFloor.SetActive(true);
			smokeFx.SetActive(true);
			yield return new WaitForSeconds(0.3f);
			sparkFx.SetActive(true);
		}

		public void Hide()
		{
			showFloor?.SetActive(false);
		}
	}
}

