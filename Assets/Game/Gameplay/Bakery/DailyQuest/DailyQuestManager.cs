using System;
using UnityEngine;
using UnityEngine.UI;
using Nama;
using DG.Tweening;
using Random = UnityEngine.Random;
using System.Collections;

namespace Funzilla
{
    public class DailyQuestManager : MonoBehaviour
    {
        [SerializeField] private Button backButton;
        [SerializeField] private Text textTimeDue;
        [SerializeField] private Transform gemReward;
        [SerializeField] private GameObject diamondPrefab;
        [SerializeField] private Transform coinTarget;

        [Header("Quest1")]
        [SerializeField] private Button quest1;
        public int moneySpendOnShopGoal;
        [SerializeField] Text moneyCurrentSpend;
        [SerializeField] private Image progressbarQ1;
        [SerializeField] private GameObject barQ1;
        [SerializeField] private Transform gemRewardQ1;
        [SerializeField] private Text descriptionQ1;
        [SerializeField] private Text tomorrowQ1;

        [Header("Quest2")]
        [SerializeField] private Button quest2;
        public int moneyEarnGoal;
        [SerializeField] Text moneyCurrentEarn;
        [SerializeField] private Image progressbarQ2;
        [SerializeField] private GameObject barQ2;
        [SerializeField] private Transform gemRewardQ2;
        [SerializeField] private Text descriptionQ2;
        [SerializeField] private Text tomorrowQ2;

        [Header("Quest3")]
        [SerializeField] private Button quest3;
        public int upgradeBakeryTimeGoal;
        [SerializeField] Text upgradeBakeryCurrentTime;
        [SerializeField] private Image progressbarQ3;
        [SerializeField] private GameObject barQ3;
        [SerializeField] private Transform gemRewardQ3;
        [SerializeField] private Text descriptionQ3;
        [SerializeField] private Text tomorrowQ3;

        [Header("Quest4")]
        [SerializeField] private Button quest4;
        public int playVipLevelTimeGoal;
        [SerializeField] Text playVipLevelCurrentTime;
        [SerializeField] private Image progressbarQ4;
        [SerializeField] private GameObject barQ4;
        [SerializeField] private Transform gemRewardQ4;
        [SerializeField] private Text descriptionQ4;
        [SerializeField] private Text tomorrowQ4;

        [Header("Quest5")]
        [SerializeField] private Button quest5;
        public int useMirrorGlazeTimeGoal;
        [SerializeField] Text useMirrorGlazeCurrentTime;
        [SerializeField] private Image progressbarQ5;
        [SerializeField] private GameObject barQ5;
        [SerializeField] private Transform gemRewardQ5;
        [SerializeField] private Text descriptionQ5;
        [SerializeField] private Text tomorrowQ5;

        [Header("Quest6")]
        [SerializeField] private Button quest6;
        public int litTheCandlesTimeGoal;
        [SerializeField] Text litTheCandlesCurrentTime;
        [SerializeField] private Image progressbarQ6;
        [SerializeField] private GameObject barQ6;
        [SerializeField] private Transform gemRewardQ6;
        [SerializeField] private Text descriptionQ6;
        [SerializeField] private Text tomorrowQ6;


        public static DailyQuestManager Instance;

        private void Awake()
        {
            Instance = this;
            quest1.gameObject.SetActive(false);
            quest2.gameObject.SetActive(false);
            quest3.gameObject.SetActive(false);
            quest4.gameObject.SetActive(false);
            quest5.gameObject.SetActive(false);
            quest6.gameObject.SetActive(false);
        }

        public static bool UpdateDay()
        {
            var date = Profile.Instance.DailyTaskDate;
            if ((DateTime.Now - date).Days == 0) return false;
         
            Profile.Instance.DailyTaskDate = DateTime.Now;
            Debug.LogError("ResetQuest");
            
            Profile.Instance.MoneyCurrentSpendOnShop = 0;
            Profile.Instance.MoneyCurrentEarn = 0;
            Profile.Instance.UpgradeBakeryCurrentTime = 0;
            Profile.Instance.PlayVipLevelCurrentTime = 0;
            Profile.Instance.UseMirrorGlazeCurrentTime = 0;
            Profile.Instance.LitTheCandlesCurrentTime = 0;
            Profile.Instance.CompleteQ1 = 0;
            Profile.Instance.CompleteQ2 = 0;
            Profile.Instance.CompleteQ3 = 0;
            Profile.Instance.CompleteQ5 = 0;
            Profile.Instance.CompleteQ4 = 0;
            Profile.Instance.CompleteQ6 = 0;
            return true;
        }    

        public void CheckQuestStatus()
        {
            if (UpdateDay())
            {
                ResetQuest();
                Debug.LogError("ResetQuest");
            }
            moneyCurrentSpend.text = Profile.Instance.MoneyCurrentSpendOnShop + "/" + moneySpendOnShopGoal;
            progressbarQ1.DOFillAmount((float)Profile.Instance.MoneyCurrentSpendOnShop / moneySpendOnShopGoal, 0f);
            if (Profile.Instance.MoneyCurrentSpendOnShop >= moneySpendOnShopGoal && Profile.Instance.CompleteQ1 < 1)
            {
                quest1.gameObject.SetActive(true);
                barQ1.SetActive(false);
            }
            if (Profile.Instance.CompleteQ1 >= 1)
            {
                barQ1.SetActive(false);
                descriptionQ1.gameObject.SetActive(false);
                tomorrowQ1.gameObject.SetActive(true);
            }

            moneyCurrentEarn.text = Profile.Instance.MoneyCurrentEarn + "/" + moneyEarnGoal;
            progressbarQ2.DOFillAmount((float)Profile.Instance.MoneyCurrentEarn / moneyEarnGoal, 0f);
            if (Profile.Instance.MoneyCurrentEarn >= moneyEarnGoal && Profile.Instance.CompleteQ2 < 1)
            {
                quest2.gameObject.SetActive(true);
                barQ2.SetActive(false);
            }
            if (Profile.Instance.CompleteQ2 >= 1)
            {
                barQ2.SetActive(false);
                descriptionQ2.gameObject.SetActive(false);
                tomorrowQ2.gameObject.SetActive(true);
            }

            upgradeBakeryCurrentTime.text = Profile.Instance.UpgradeBakeryCurrentTime + "/" + upgradeBakeryTimeGoal;
            progressbarQ3.DOFillAmount((float)Profile.Instance.UpgradeBakeryCurrentTime / upgradeBakeryTimeGoal, 0f);
            if (Profile.Instance.UpgradeBakeryCurrentTime >= upgradeBakeryTimeGoal && Profile.Instance.CompleteQ3 < 1)
            {
                quest3.gameObject.SetActive(true);
                barQ3.SetActive(false);
            }
            if (Profile.Instance.CompleteQ3 >= 1)
            {
                barQ3.SetActive(false);
                descriptionQ3.gameObject.SetActive(false);
                tomorrowQ3.gameObject.SetActive(true);
            }

            playVipLevelCurrentTime.text = Profile.Instance.PlayVipLevelCurrentTime + "/" + playVipLevelTimeGoal;
            progressbarQ4.DOFillAmount((float)Profile.Instance.PlayVipLevelCurrentTime / playVipLevelTimeGoal, 0f);
            if (Profile.Instance.PlayVipLevelCurrentTime >= playVipLevelTimeGoal && Profile.Instance.CompleteQ4 < 1)
            {
                quest4.gameObject.SetActive(true);
                barQ4.SetActive(false);
            }
            if (Profile.Instance.CompleteQ4 >= 1)
            {
                barQ4.SetActive(false);
                descriptionQ4.gameObject.SetActive(false);
                tomorrowQ4.gameObject.SetActive(true);
            }

            useMirrorGlazeCurrentTime.text = Profile.Instance.UseMirrorGlazeCurrentTime + "/" + useMirrorGlazeTimeGoal;
            progressbarQ5.DOFillAmount((float)Profile.Instance.UseMirrorGlazeCurrentTime / useMirrorGlazeTimeGoal, 0f);
            if (Profile.Instance.UseMirrorGlazeCurrentTime >= useMirrorGlazeTimeGoal && Profile.Instance.CompleteQ5 < 1)
            {
                quest5.gameObject.SetActive(true);
                barQ5.SetActive(false);
            }
            if (Profile.Instance.CompleteQ5 >= 1)
            {
                barQ5.SetActive(false);
                descriptionQ5.gameObject.SetActive(false);
                tomorrowQ5.gameObject.SetActive(true);
            }

            litTheCandlesCurrentTime.text = Profile.Instance.LitTheCandlesCurrentTime + "/" + litTheCandlesTimeGoal;
            progressbarQ6.DOFillAmount((float)Profile.Instance.LitTheCandlesCurrentTime / litTheCandlesTimeGoal, 0f);
            if (Profile.Instance.LitTheCandlesCurrentTime >= litTheCandlesTimeGoal && Profile.Instance.CompleteQ6 < 1)
            {
                quest6.gameObject.SetActive(true);
                barQ6.SetActive(false);
            }
            if (Profile.Instance.CompleteQ6 >= 1)
            {
                barQ6.SetActive(false);
                descriptionQ6.gameObject.SetActive(false);
                tomorrowQ6.gameObject.SetActive(true);
            }
        }
        public void Quest1()
        {
            quest1.gameObject.SetActive(false);
            descriptionQ1.gameObject.SetActive(false);
            tomorrowQ1.gameObject.SetActive(true);
            Profile.Instance.CompleteQ1++;
            gemReward = gemRewardQ1;
            CompleteQuest();
        }

        public void Quest2()
        {
            quest2.gameObject.SetActive(false);
            descriptionQ2.gameObject.SetActive(false);
            tomorrowQ2.gameObject.SetActive(true);
            Profile.Instance.CompleteQ2++;
            gemReward = gemRewardQ2;
            CompleteQuest();
        }

        public void Quest3()
        {
            quest3.gameObject.SetActive(false);
            descriptionQ3.gameObject.SetActive(false);
            tomorrowQ3.gameObject.SetActive(true);
            Profile.Instance.CompleteQ3++;
            gemReward = gemRewardQ3;
            CompleteQuest();
        }

        public void Quest4()
        {
            quest4.gameObject.SetActive(false);
            descriptionQ4.gameObject.SetActive(false);
            tomorrowQ4.gameObject.SetActive(true);
            Profile.Instance.CompleteQ4++;
            gemReward = gemRewardQ4;
            CompleteQuest();
        }

        public void Quest5()
        {
            quest5.gameObject.SetActive(false);
            descriptionQ5.gameObject.SetActive(false);
            tomorrowQ5.gameObject.SetActive(true);
            Profile.Instance.CompleteQ5++;
            gemReward = gemRewardQ5;
            CompleteQuest();
        }

        public void Quest6()
        {
            quest6.gameObject.SetActive(false);
            descriptionQ6.gameObject.SetActive(false);
            tomorrowQ6.gameObject.SetActive(true);
            Profile.Instance.CompleteQ6++;
            gemReward = gemRewardQ6;
            CompleteQuest();
        }

        private void CompleteQuest()
        {
            StartCoroutine(RewardGems(Profile.Instance.DailyQuestReward));
            DailyQuestReward.Instance.CollectReward();
            UIBakery.Instance.CheckUpgradeButtonState();
        }

        public void onClickBackBtn()
        {
            UIBakery.Instance.dailyQuestPopup.SetActive(false);
            UIBakery.Instance.CheckDailyQuestButtonNotification();
            UIBakery.Instance.tipUI.SetLayerRecursively(3);
            UIBakery.Instance.unlockItem.transform.DOScale(1, 0);
        }

        private void ResetQuest()
        {
            

            quest1.gameObject.SetActive(false);
            barQ1.SetActive(true);
            descriptionQ1.gameObject.SetActive(true);
            tomorrowQ1.gameObject.SetActive(false);
            
            
            quest2.gameObject.SetActive(false);
            barQ2.SetActive(true);
            descriptionQ2.gameObject.SetActive(true);
            tomorrowQ2.gameObject.SetActive(false);
            

            quest3.gameObject.SetActive(false);
            barQ3.SetActive(true);
            descriptionQ3.gameObject.SetActive(true);
            tomorrowQ3.gameObject.SetActive(false);
            

            quest4.gameObject.SetActive(false);
            barQ4.SetActive(true);
            descriptionQ4.gameObject.SetActive(true);
            tomorrowQ4.gameObject.SetActive(false);
            

            quest5.gameObject.SetActive(false);
            barQ5.SetActive(true);
            descriptionQ5.gameObject.SetActive(true);
            tomorrowQ5.gameObject.SetActive(false);
            

            quest6.gameObject.SetActive(false);
            barQ6.SetActive(true);
            descriptionQ6.gameObject.SetActive(true);
            tomorrowQ6.gameObject.SetActive(false);
            
        }

        private IEnumerator RewardGems(int amount, float timeScale = 1f)
        {
            //dark.gameObject.SetActive(true);
            gemReward.gameObject.SetActive(true);
            for (var i = 0; i < 20; i++)
            {
                var diamond = Instantiate(diamondPrefab, gemReward).transform;
                diamond.localRotation = Random.rotation;
                diamond.DOLocalMove(Random.insideUnitSphere, Random.Range(0.3f, 0.7f) / timeScale)
                    .SetEase(Ease.OutCubic);
                diamond.DOLocalRotate(Random.rotation.eulerAngles, 10).SetEase(Ease.Linear);
            }

            yield return new WaitForSeconds(0.7f / timeScale);
            UIGlobal.ShowCoinInfo();

            var counter = 0;
            var gemCount = gemReward.childCount;
            var n = amount / gemCount;
            var a = amount % gemCount;
            for (var i = 0; i < gemReward.childCount; i++)
            {
                var diamond = gemReward.GetChild(i);
                diamond.transform.localScale *= 275;
                diamond.DOMove(coinTarget.transform.position, 0.5f / timeScale)
                    .SetEase(Ease.InCubic)
                    .SetDelay(i * 0.05f)
                    .OnComplete(() =>
                    {
                        diamond.gameObject.SetActive(false);
                        counter += n + (a > 0 ? 1 : 0);
                        a--;
                        if (counter <= amount)
                        {
                            //UIGlobal.ShowCoinAmount(Profile.Instance.CoinAmount + counter);
                        }
                    });
            }

            yield return new WaitForSeconds(gemReward.childCount * 0.05f + 0.6f / timeScale);
            UIGlobal.HideCoinInfo();
        }
    }
}

