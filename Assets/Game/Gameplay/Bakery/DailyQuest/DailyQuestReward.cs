using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Nama;

namespace Funzilla
{
    public class DailyQuestReward : MonoBehaviour
    {
        public static DailyQuestReward Instance;
        internal enum RewardType
        {
            Cash,
            Skin
        }

        [SerializeField] private RewardType rewardType;

        private void Awake()
        {
            Instance = this;
        }

        public void CollectReward()
        {
            if (rewardType == RewardType.Cash)
            {
                Profile.Instance.CoinAmount += Profile.Instance.DailyQuestReward;
                UIGlobal.ShowCoinInfo();
                Profile.Instance.DailyQuestReward += 50;
            }
            else
            {

            }
        }
    }
}


