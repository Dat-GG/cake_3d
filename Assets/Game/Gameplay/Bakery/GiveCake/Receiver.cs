using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Funzilla
{
    public class Receiver : MonoBehaviour
    {
        [SerializeField] Animator animator;
        public Transform cakeTransform;

        public void Idle()
        {
            animator.SetTrigger("idle_0");
        }

        public void ReceiveAndEatCake()
        {
            animator.SetTrigger("eat");
        }
    }
}

