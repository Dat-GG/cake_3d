using Nama;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Funzilla
{
    public class GiveCakeController : Scene
    {
        private Clients _client;
        private string _lastClientName = "";
        [SerializeField] Transform cakeTransform;
        [SerializeField] Receiver _receiver;
        [SerializeField] ParticleSystem _dirty;

        public override void Init(object data)
        {
            base.Init(data);
            _dirty.gameObject.SetActive(false);
        }
        private void Start()
        {
            LoadReceiver();
            LoadClient();
            Gameplay.Level.gameObject.SetLayerRecursively(0);
            Gameplay.Level.transform.localScale = Vector3.one * 0.5f;
            Gameplay.Level.transform.localEulerAngles = Vector3.zero;
            var objects = Gameplay.Level.GetComponentsInChildren<SprinkleObject>();
            foreach (var obj in objects)
            {
                if (!obj.OnSurface)
                {
                    Destroy(obj.gameObject);
                }
            }
            Gameplay.Level.transform.SetParent(_client.cakeTransform);
            Gameplay.Level.transform.DOLocalMove(new Vector3(0, 0, 0), 0);
            
        }

        private void LoadClient()
        {           
            _lastClientName = AssetManager.Instance.ClientName;            
            var go = Resources.Load<Clients>("Clients/" + _lastClientName);
            if (go == null) return;
            if (_client != null)
            {
                Destroy(_client.gameObject);
            }
            _client = Instantiate(go, transform);
            _client.transform.DOLocalMove(new Vector3(-15, -8, -3), 0);
            _client.transform.DOLocalRotate(new Vector3(0, 90, 0), 0);
            _client.gameObject.SetLayerRecursively(0);
            _client.Init();
            DOVirtual.DelayedCall(0.2f, delegate
            {
                _client.WalkToGive();
            });
            
        }

        private void LoadReceiver()
        {
            _receiver.Idle();
            DOVirtual.DelayedCall(3f, delegate
            {
                _receiver.ReceiveAndEatCake();
                DOVirtual.DelayedCall(0.3f, delegate
                {
                    Gameplay.Level.transform.SetParent(_receiver.cakeTransform);
                    Gameplay.Level.transform.DOLocalMove(new Vector3(0, 0, 0), 0);
                    Gameplay.Level.transform.DOLocalRotate(new Vector3(0, 0, 0), 0);
                    DOVirtual.DelayedCall(2.7f, delegate
                    {
                        Gameplay.Level.transform.SetParent(this.transform);
                        Gameplay.Level.transform.DOLocalRotate(new Vector3(-40, -5, 35), 1.2f);
                        Gameplay.Level.transform.DOLocalMoveY(-7.3f, 1.2f).SetEase(Ease.InOutCubic);
                        DOVirtual.DelayedCall(2.1f, delegate
                        {
                            _dirty.gameObject.SetActive(true);                            
                            DOVirtual.DelayedCall(2.5f, delegate
                            {
                                var obj = Gameplay.Level.gameObject;
                                obj.SetActive(false);
                                Destroy(obj);
                                Gameplay.Level = null;
                                CloseScene();
                            });
                        });                       
                    });
                });              
            });            
        }

        public void CloseScene()
        {
            SceneManager.Instance.ShowLoading(false, 1.0f, () =>
            {
                Destroy(_client.gameObject);
                SceneManager.Instance.HideLoading();
                SceneManager.Instance.CloseScene(SceneID.GiveCake);
                SceneManager.Instance.OpenScene(SceneID.UIBakery);              
                AssetManager.Instance.ClientName = "";
                UIBakery.Instance.NextClient();
            });
        }
    }
}

