using UnityEditor;
using UnityEngine;

namespace Nama
{
	public class BrushTexture : MonoBehaviour
	{
		[SerializeField] new Camera camera;
		[SerializeField] SpriteRenderer[] sprites;

		private void Awake()
		{
			gameObject.SetActive(false);
		}

		public Texture Bake(Color[] colors)
		{
			var texture = new RenderTexture(
				64,
				32,
				0,
				RenderTextureFormat.ARGB32,
				RenderTextureReadWrite.Default);
			texture.wrapMode = TextureWrapMode.Repeat;
			texture.filterMode = FilterMode.Bilinear;
			texture.autoGenerateMips = false;
			texture.useMipMap = false;
			texture.anisoLevel = 0;
			camera.targetTexture = texture;

			gameObject.SetActive(true);
			var step = 1.0f / colors.Length;
			for (int i = 0; i < colors.Length; i++)
			{
				sprites[i].gameObject.SetActive(true);
				sprites[i].color = colors[i];
				sprites[i].transform.localScale = new Vector3(8, step);
				sprites[i].transform.localPosition = new Vector3(0, -0.5f + step * (i + 0.5f), 1);
			}
			for (int i = colors.Length; i < sprites.Length; i++)
			{
				sprites[i].gameObject.SetActive(false);
			}
			camera.Render();
			gameObject.SetActive(false);
			camera.targetTexture = null;
			return texture;
		}

		static void LoadBrushTexture()
		{
			var prefab = Resources.Load<BrushTexture>("BrushTexture");
			brushTexture = Instantiate(prefab);
			brushTexture.gameObject.SetActive(false);
			brushTexture.transform.localPosition = new Vector3(0, 1000, 0);
			brushTexture.gameObject.hideFlags = HideFlags.HideAndDontSave;
		}

		static BrushTexture brushTexture;
		public static BrushTexture Instance
		{
			get
			{
				if (brushTexture != null)
				{
#if UNITY_EDITOR
					if (UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage() == null)
					{
						return brushTexture;
					}
#else
					return brushTexture;
#endif
				}
				LoadBrushTexture();
				return brushTexture;
			}
		}
	}
}