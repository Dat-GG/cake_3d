﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextAnimation : MonoBehaviour
{
	private void OnEnable()
	{
		var anim = DOTween.Sequence();
		anim.Append(this.transform.DOScale(Vector3.one * 1.02f, 0.5f));
		anim.Append(this.transform.DOScale(Vector3.one, 0.5f));
		anim.SetLoops(-1).Play();
	}
}
