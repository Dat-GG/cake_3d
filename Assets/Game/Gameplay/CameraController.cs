
using System;
using DG.Tweening;
using UnityEngine;

namespace Nama
{
	public class CameraController : MonoBehaviour
	{
		private Camera _cam;

		private void Awake()
		{
			_cam = GetComponent<Camera>();
		}

		private Sequence _cameraAnimation;
		public void Move(Transform t, float duration)
		{
			_cameraAnimation?.Kill();
			_cameraAnimation = DOTween.Sequence();
			_cameraAnimation.Append(
				transform.DOLocalMove(t.position, duration).SetEase(Ease.InOutCubic));
			_cameraAnimation.Join(
				transform.DOLocalRotateQuaternion(t.rotation, duration).SetEase(Ease.InOutCubic));
			_cameraAnimation.OnComplete(() =>
			{
				_cameraAnimation = null;
			});
		}


		private bool _positionReseting;

		public void ResetCameraPosition(float speed, Action onComplete = null)
		{
			if (!Gameplay.Instance)
				return;
			var camPos = _cam.transform.localPosition;
			var distance = Mathf.Abs(camPos.x);
			var duration = distance / speed;
			_positionReseting = true;
			if (Gameplay.Instance.FlowTool == null)
			{
				onComplete?.Invoke();
			}
			else
			{
				Gameplay.Instance.FlowTool.transform.DOMoveX(
					Gameplay.Instance.FlowTool.transform.position.x - camPos.x,
					duration
				).SetDelay(0.25f);
				_cam.transform.DOLocalMoveX(0, duration)
					.SetEase(Ease.InOutSine)
					.Play()
					.OnComplete(() =>
						{
							_positionReseting = false;
							onComplete?.Invoke();
						}
					);
			}

		}

		private void Update()
		{
			if (Gameplay.Instance && Gameplay.Instance.Playing && !_positionReseting)
			{
				CameraFlow();
			}
		}

		public float toolX, toolSpeed;
		public bool drawing;

		private void CameraFlow()
		{
			toolSpeed = 1;
			if (Gameplay.Instance.FlowTool != null)
			{
				toolX = Gameplay.Instance.FlowTool.transform.position.x;
			}

			if (Gameplay.Instance.StrokeManualDecorating
				&& Gameplay.Instance.PastryBag != null)
			{
				drawing = !Gameplay.Instance.PastryBag.Finished;

				if(drawing)
				{
					if (Gameplay.Instance.StepWidth > Config.MIN_STROKE_WIDTH)
					{
						toolSpeed = Config.LONG_STROKE_SPEED;
					}
					else
					{
						toolSpeed = Gameplay.Instance.StepWidth * Config.SHORT_STROKE_SPEED;
					}
				} else
				{
					toolSpeed = Config.NEXT_STROKE_SPEED * Gameplay.Instance.DistanceLastStroke * Config.NEXT_STROKE_SPEED_FACTOR;
				}
				if(!Gameplay.Instance.CameraFlowEnable)
				{
					toolSpeed = 1;
					toolX = 0;
				}
			} else
			{
				toolSpeed = 1;
			}

			var t1 = toolX / CameraConfig.Instance.areaRange + 0.5f;
			t1 = Mathf.Clamp01(t1);
			var cameraX = Mathf.Lerp(-CameraConfig.Instance.cameraRange, CameraConfig.Instance.cameraRange, t1);

			var camPos = _cam.transform.localPosition;

			if (Mathf.Abs(toolX) >= CameraConfig.Instance.minAreaRange)
			{
				camPos.x = Mathf.MoveTowards(camPos.x, cameraX, CameraConfig.Instance.cameraMoveVelocity * toolSpeed * Time.smoothDeltaTime);
			}

			_cam.transform.localPosition = camPos;
		}

		private Tween _zoomTween;

		public void ZoomIn()
		{
			_zoomTween?.Kill();
			_zoomTween = _cam.DOFieldOfView(50, 1f).SetDelay(0.5f);
		}

		public void ZoomOut()
		{
			_zoomTween?.Kill();
			_zoomTween = _cam.DOFieldOfView(60, 1f).SetDelay(0.5f);
		}
	}

}