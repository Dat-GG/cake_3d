
using DG.Tweening;
using UnityEngine;

namespace Nama
{
	public class RotateBehavior : MonoBehaviour
	{
		[Range(0.0f, 10.0f)] [SerializeField] float duration = 2;
		[SerializeField] float from = 0;
		[SerializeField] float to = 360;
		[SerializeField] LoopType loopType = LoopType.Incremental;
		[SerializeField] Ease ease = Ease.Linear;

		Tweener tween;
		private void Awake()
		{
			var a = transform.localEulerAngles;
			transform.localEulerAngles = new Vector3(a.x, a.y, from);
			tween = transform.DOLocalRotate(new Vector3(a.x, a.y, to), duration, RotateMode.FastBeyond360)
				.SetLoops(-1, loopType)
				.SetEase(ease);
		}

		private void OnDestroy()
		{
			tween.Kill();
		}
	}
}