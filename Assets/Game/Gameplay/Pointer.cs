using UnityEngine;
using DG.Tweening;

namespace Nama
{
	public class Pointer : SprinkleCharacterPart
	{
		[SerializeField] private SpriteRenderer border;
		[SerializeField] private SpriteRenderer pointer;
		[SerializeField] private SpriteRenderer x;
		[SerializeField] private float targetScale = 0.6f;

		private readonly Color _borderColorDefault = new Color(230 / 255f, 71 / 255f, 132 / 255f, 1);
		private readonly Color _borderColorHighlight = new Color(47 / 255f, 141 / 255f, 250 / 255f, 1);
		public override void SetPosition(Vector3 position)
		{
			transform.localPosition = new Vector3(position.x, position.y, -0.1f);
		}

		private Tweener _rotateTween;

		private void Awake()
		{
			_rotateTween = border.transform
				.DOLocalRotate(new Vector3(0, 0, 360), 2, RotateMode.FastBeyond360)
				.SetLoops(-1)
				.SetEase(Ease.Linear);
			x.gameObject.SetActive(false);
		}

		public override void Disappear()
		{
			gameObject.SetActive(false);
		}

		public override void Appear()
		{
			if (gameObject.activeSelf)
			{
				return;
			}
			gameObject.SetActive(true);
			pointer.DOKill();
			border.DOKill();

			var pointerColor = pointer.color;
			pointerColor.a = 0;
			pointer.color = pointerColor;

			pointer.DOFade(1, 0.5f);
			border.DOFade(1, 0.5f);
			RotateNormal();
		}

		private void RotateNormal()
		{
			border.material.color = _borderColorDefault;
			if (_rotateTween == null) return;
			_rotateTween.timeScale = 1;
			_rotateTween.timeScale = 1;
		}

		private void RotateFast()
		{
			if (_rotateTween != null)
			{
				_rotateTween.timeScale = 2;
			}
		}

		private Sequence _anim;
		public bool Ready => _anim == null;

		private void Animate(float scale, Color color, float duration)
		{
			_anim?.Kill();
			_anim = DOTween.Sequence();
			_anim.Join(border.transform
				.DOScale(scale, duration)
				.SetEase(Ease.Linear));

			_anim.Join(border.material.DOColor(color, 1));
			_anim.Play().OnComplete(()=>
			{
				_anim = null;
			});
		}

		private float _animDuration = 1;

		public override void Press(float duration)
		{
			_animDuration = duration;
			RotateFast();
			Animate(targetScale, _borderColorHighlight, duration);
		}

		public override void Release()
		{
			Animate(1, _borderColorDefault, _animDuration / 2);
			RotateNormal();
		}

		internal void ShowX()
		{
			x.gameObject.SetActive(true);
			border.gameObject.SetActive(false);
			pointer.gameObject.SetActive(false);
		}

		internal void HideX()
		{
			x.gameObject.SetActive(false);
			border.gameObject.SetActive(true);
			pointer.gameObject.SetActive(true);
		}
	}
}