using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class FocusTutorial : MonoBehaviour
{
    [SerializeField] RectTransform circle;
    [SerializeField] RectTransform[] rects;
    [SerializeField] CanvasGroup canvasGroup;
    Image[] images;

	private void Awake()
	{
        images = new Image[rects.Length];
        for(int i = 0; i < rects.Length; i++)
		{
            images[i] = rects[i].GetComponent<Image>();
		}
    }

	// Update is called once per frame
	void Update()
    {
        Refresh();
    }

    [ContextMenu("Refresh")]
    void Refresh()
	{
        rects[0].anchoredPosition = new Vector2(circle.sizeDelta.x, rects[0].anchoredPosition.y);
        rects[2].anchoredPosition = new Vector2(circle.sizeDelta.x / 2, circle.sizeDelta.y / 2);
        rects[2].sizeDelta = new Vector2(circle.sizeDelta.x, rects[2].sizeDelta.y);
        rects[3].anchoredPosition = new Vector2(circle.sizeDelta.x / 2, -circle.sizeDelta.y / 2);
        rects[3].sizeDelta = new Vector2(circle.sizeDelta.x, rects[2].sizeDelta.y);
    }

    public void Focus(Vector3 target)
	{
        canvasGroup.alpha = 0;
        canvasGroup.DOFade(1, 0.5f);
        transform.position = target;
        circle.sizeDelta = new Vector2(3000, 3000);
        foreach (var image in images)
        {
            image.raycastTarget = true;
        }
        circle.DOSizeDelta(new Vector2(400, 400), 1f).OnComplete(delegate
        {
            circle.GetComponent<Image>().raycastTarget = false;
        });

    }

    public void Hide()
	{
        canvasGroup.DOFade(0, 0.4f).OnComplete(delegate
        {
            gameObject.SetActive(false);
        });
    }
}
