using Nama;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEditor;

namespace Funzilla
{
	[ExecuteInEditMode]
	public class CakeBox : MonoBehaviour
	{
		[SerializeField] Color strapColor = Utils.ColorFromUint(0xFFC7B6FF);
		[SerializeField] Color capColor = Utils.ColorFromUint(0xCFB6FFFF);
		[SerializeField] Color color = Utils.ColorFromUint(0xCFB6FFFF);
		[SerializeField] Material upgrade;
		[SerializeField] Transform cap;
		[SerializeField] CakeSurface surface;

		public CakeSurface Surface => surface;
		public PastryBag Pencil { get; private set; }

		public void Animate(Cake cake)
		{
			cake.transform.SetParent(Gameplay.Level.transform);
			cake.transform.localPosition = Vector3.zero;
			cake.transform.localScale = Vector3.one;
			cake.transform.localRotation = Quaternion.identity;
			NamaUtils.SetLayerRecursively(cake.gameObject, Layers.Gameplay);
			cap.localPosition = new Vector3(0, 7, 0);
			//Gameplay.Instance.CameraController.Move(
			//	new Vector3(0, 9.5f, -7.5f), Vector3.up, 0.5f);

			var sequence = DOTween.Sequence();
			sequence.Append(
				cake.transform.DOLocalMove(new Vector3(0, 10, 0), 0.5f).OnComplete(() => {
					//cake.Disk.gameObject.SetActive(false);
					cake.transform.parent.localRotation = Quaternion.identity;
				}));
			sequence.Append(transform.DOLocalMove(Vector3.zero, 0.5f));
			sequence.Append(cake.transform.DOLocalMove(new Vector3(0, 0.5f, 0), 0.5f));
			sequence.Append(cap.DOLocalMove(Vector3.zero, 0.5f).OnComplete(() => {
				//Gameplay.Instance.CameraController.Move(
				//	new Vector3(0, 4.7f, -10.5f), Vector3.up, 0.5f);
			}));

			Pencil = Instantiate(Resources.Load<PastryBag>("PastryBag"), transform);
			//Pencil.Init("Pencil");

			sequence.OnComplete(() => {
				cake.gameObject.SetActive(false);
				//Gameplay.Instance.DecorateBox();
				surface.Init(false, false);
				surface.Begin(Pencil, null);
			});
		}

		public void Upgrade()
		{
			var renderers = GetComponentsInChildren<MeshRenderer>();
			renderers[0].sharedMaterial =
			renderers[1].sharedMaterial =
			renderers[2].sharedMaterial =
				new Material(upgrade);
		}

		public void Downgrade(Material template)
		{
			var renderers = GetComponentsInChildren<MeshRenderer>();
			var material = new Material(template);
			material.color = strapColor;
			renderers[1].sharedMaterial = material;

			material = new Material(template);
			material.color = capColor;
			renderers[0].sharedMaterial = material;

			material = new Material(template);
			material.color = color;
			renderers[2].sharedMaterial = material;
		}

		public void Init(bool upgraded)
		{
			if (upgraded)
			{
				Upgrade();
			}
			else
			{
				//Downgrade(AssetManager.Instance.BoxMaterial);
			}
		}

#if UNITY_EDITOR
		private void Awake()
		{
			if (EditorApplication.isPlayingOrWillChangePlaymode)
			{
				return;
			}

			Upgrade();
			EditorApplication.delayCall += () =>
			{
				surface.Init(true, false);
			};
		}

		bool upgraded = true;
		private void OnValidate()
		{
			if (EditorApplication.isPlayingOrWillChangePlaymode)
			{
				return;
			}
			Refresh();
		}

		public void Refresh()
		{
			if (upgraded)
			{
				if (upgrade == null)
				{
					return;
				}
				Upgrade();
			}
			else
			{
				var material = AssetDatabase.LoadAssetAtPath<Material>(
					"Assets/Game/BoxCollection/white.mat");
				Downgrade(material);
			}
		}

		public void SetUpgrade(Material material)
		{
			upgraded = true;
			upgrade = material;
			Refresh();
		}

		public void Switch()
		{
			upgraded = !upgraded;
			Refresh();
		}
#endif
	}
}
