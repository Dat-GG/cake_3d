
using Funzilla;
using System;
using System.Reflection;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Nama
{
	[CustomEditor(typeof(CakeBox), true), CanEditMultipleObjects]
	public class CakeBoxEditor : Editor
	{
		string[] _choices = new[] {
			"Blue Stripe",
			"Purple Stripe",
			"Yellow Stripe",
			"Pattern 1",
			"Pattern 2",
			"Pattern 3",
			"Pattern 4",
			"Pattern 5",
			"Pattern 6",
			"Pattern 7",
			"Pattern 8",
		};
		int _choiceIndex = 0;

		Material GetMaterial()
		{
			string materialName;
			switch (_choiceIndex)
			{
				default:
				case 0:
					materialName = "box_stripe_blue.mat";
					break;

				case 1:
					materialName = "box_stripe_purple.mat";
					break;

				case 2:
					materialName = "box_stripe_yellow.mat";
					break;

				case 3:
					materialName = "box_pattern_1.mat";
					break;

				case 4:
					materialName = "box_pattern_2.mat";
					break;

				case 5:
					materialName = "box_pattern_3.mat";
					break;

				case 6:
					materialName = "box_pattern_4.mat";
					break;

				case 7:
					materialName = "box_pattern_5.mat";
					break;

				case 8:
					materialName = "box_pattern_6.mat";
					break;

				case 9:
					materialName = "box_pattern_7.mat";
					break;

				case 10:
					materialName = "box_pattern_8.mat";
					break;
			}

			return AssetDatabase.LoadAssetAtPath<Material>(
				"Assets/Game/BoxCollection/boxes/" + materialName);
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			if (Selection.gameObjects == null)
			{
				return;
			}

			GUILayout.BeginHorizontal();
			GUILayout.Label("Type", GUILayout.Width(40));
			_choiceIndex = EditorGUILayout.Popup(_choiceIndex, _choices);
			if (GUILayout.Button("Change"))
			{
				foreach (var obj in Selection.gameObjects)
				{
					var box = obj.GetComponent<CakeBox>();
					if (box == null)
					{
						continue;
					}
					box.SetUpgrade(GetMaterial());
				}

				var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
				if (prefabStage != null)
				{
					EditorSceneManager.MarkSceneDirty(prefabStage.scene);
				}
			}
			GUILayout.EndHorizontal();

			if (GUILayout.Button("Switch"))
			{
				foreach (var obj in Selection.gameObjects)
				{
					obj.GetComponent<CakeBox>().Switch();
				}
			}
		}
	} // class PathEditor
} // namespace Nama