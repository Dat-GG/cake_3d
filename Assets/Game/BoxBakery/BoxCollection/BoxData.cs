
using UnityEditor;
using UnityEngine;

namespace Nama
{

	[CreateAssetMenu(fileName = "Level", menuName = "Level Design/Box Data", order = 1)]
	public class BoxData : ScriptableObject
	{
		public string[] boxes;
	}
}
