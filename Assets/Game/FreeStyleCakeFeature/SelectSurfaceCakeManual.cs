using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Nama;
using UnityEngine;

public class SelectSurfaceCakeManual : DecorationStep
{
    public List<Cake> options;
    int iSelected; // Currently selected option
    private Cake _currentCake;
    public int optionAds;
    public override void Init(bool preview)
    {
        if(preview)
            return;
        Debug.Log("??????");
    }

    public override void Begin()
    {
        _currentCake = GetComponentInParent<Cake>();
        Gameplay.Instance.ShowCakeSurfaceManualOptions(options,TrueTexture(),Mirror(),Stencil(),Glitter(),optionAds);
        Gameplay.Instance.OnOptionSelected = OnOptionSelected;
        Gameplay.Instance.FinishStep = Finish;
        Gameplay.Instance.HideCompleteButton();
        TutorialSign.Instance.EndTutorial();
        Gameplay.Instance.PreviewImage.gameObject.SetActive(false);
    }
    private void OnOptionSelected(int index) 
    {
        iSelected = index;
        _currentCake.CakeRenderer.transform.localScale=Vector3.one;
        _currentCake.UpdateColorsOnSurfaceSelect(_currentCake.GetComponentInParent<Level>().CakeType,options[iSelected]);
        _currentCake.CakeRenderer.transform.DOScaleY(1.5f, 0.2f).OnComplete(() => _currentCake.CakeRenderer.transform.DOScaleY(1, 0.2f));
        _currentCake.CakeRenderer.transform.DOScaleX(0.8f, 0.2f).OnComplete(() => _currentCake.CakeRenderer.transform.DOScaleX(1, 0.2f));
        Gameplay.Instance.ShowCompleteButton();
    }
    private void Finish()
    {
        Finished = true;
    }
    public override void CompleteFX(Action onFinished)
    {
        onFinished?.Invoke();
    }

    private Texture TrueTexture()
    {
        return GetComponentInParent<Cake>().CakeTexture;
    }

    private bool Mirror()
    {
        return _currentCake.Mirror();
    }

    private bool Stencil()
    {
        return _currentCake.Stencil();
    }

    private bool Glitter()
    {
        return _currentCake.Glitter();
    }
}
