using System.Collections;
using System.Collections.Generic;
using Nama;
using UnityEngine;

public class MakePreviewSurface : MonoBehaviour
{
    public void MakePreview(Camera orthographicCamera,CakeType type,GameObject go,Texture texture,bool mirror,bool stencil,bool glitter)
    {
        var c = GetComponent<Cake>();
        c.SetTextureForPreviewSurface(texture);
        c.MakecakeForPreview(type,mirror,stencil,glitter);
        go.layer = Layers.SpinklesPreview;
        go.transform.SetParent(orthographicCamera.transform);
        go.transform.position = Vector3.zero;
        go.SetLayerRecursively(Layers.SpinklesPreview);
        // PreviewUI.transform.localPosition += _offset;
        // PreviewUI.transform.localScale = _localScaleOnPreview;
    }

}

