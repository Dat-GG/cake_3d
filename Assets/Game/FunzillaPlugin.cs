using System.Runtime.InteropServices;
using UnityEngine;

namespace Funzilla
{
	public static class Plugin
	{
#if UNITY_IOS && !UNITY_EDITOR
		public const string Name = "__Internal";
#else
		public const string Name = "Funzilla";
#endif
		private const int CurveResolution = 255;
		
		[DllImport(Name)] private static extern bool init(string key);
		[DllImport(Name)] private static extern void configureStroke(
			float brushHeight,
			float maxFallTime,
			float maxFallRange,
			float startWidthPercent,
			float maxScaleTime,
			float scaleRange);
		[DllImport(Name)] private static extern void setHeightCurve(float[] values, int count);
		[DllImport(Name)] private static extern void setScaleCurve(float[] values, int count);
		[DllImport(Name)] private static extern void configureGalze(
			float glazeHeight,
			float glazeArvgHeight,
			float[] curveValues,
			float curveValueCount);

		private static bool _initialized;

		private static float Evaluate01(this AnimationCurve curve, float t)
		{
			var keys = curve.keys;
			if (keys.Length <= 0) return 0.0f;
			if (keys.Length == 1) return keys[0].value;
			t = (keys[keys.Length - 1].time - keys[0].time) * t + keys[0].time;
			return curve.Evaluate(t);
		}

		internal static float[] Quantitize(this AnimationCurve curve)
		{
			var values = new float[CurveResolution + 1];
			for (var i = 0; i <= CurveResolution; i++)
			{
				values[i] = curve.Evaluate01((float)i / CurveResolution);
			}

			return values;
		}
		
		// Liquid simulation specific
		public const int LiquidResolution = 128;
		[DllImport(Name)] private static extern void initLiquid(
			int resolution,
			float diff,
			int pourRadius,
			float pourRate,
			float maxPour,
			float dim,
			float fill);
		[DllImport(Name)] private static extern void setLiquidPixels(Color32[] pixles);
		[DllImport(Name)] internal static extern void pourLiquid(int x, int y);
		[DllImport(Name)] internal static extern void updateLiquid(float dt);
		[DllImport(Name)] internal static extern void resetLiquid();
		[DllImport(Name)] internal static extern void saveImage();
		public static readonly Color32[] LiquidPixels = new Color32[LiquidResolution * LiquidResolution];
		[DllImport(Name)] internal static extern void setLiquidVelocityMap(byte[] data);

		
		// Make sure to initialize Funzilla plugin before the game start
		internal static void Validate()
		{
			if (_initialized) return;
			_initialized = init("Apiudgg\\");
			if (!_initialized) return;

			var brushConfig = BrushConfig.Instance;
			configureStroke(
				brushConfig.BrushHeight,
				brushConfig.FallTime,
				brushConfig.FallRange,
				brushConfig.StartWidthPercent,
				brushConfig.ScaleTime,
				brushConfig.ScaleRange);
			var heightCurve = brushConfig.Cuver.Quantitize();
			setHeightCurve(heightCurve, heightCurve.Length);
			var scaleCurve = brushConfig.ScaleCuver.Quantitize();
			setScaleCurve(scaleCurve, scaleCurve.Length);

			var glazeCurve = brushConfig.fillColorCurver.Quantitize();
			configureGalze(
				brushConfig.FillHeight,
				Nama.FillColorSurface.FillArvgHeight,
				glazeCurve,
				glazeCurve.Length
				);

			// Don't change these settings or we have troubles
			initLiquid(LiquidResolution, 0.0002f, 10, 0.6f, 1000.0f, 0.99f, 0.5f);
			setLiquidPixels(LiquidPixels);
		}
	}
}

