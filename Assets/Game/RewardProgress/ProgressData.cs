using UnityEngine;

[System.Serializable]
public class ProgressData : MonoBehaviour
{
	public int levelStart;
	public int levelEnd;
	public string mold;
	public string[] vipMolds;
	public Color background = Nama.Utils.ColorFromUint(0xF9E783);
}