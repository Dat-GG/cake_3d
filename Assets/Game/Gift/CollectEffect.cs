using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Nama
{
	public class CollectEffect : MonoBehaviour
	{
		[SerializeField] Canvas canvas;
		[SerializeField] GameObject group;
		[SerializeField] int objectCount = 10;
		[SerializeField] float radius = 500;
		[SerializeField] bool moveToOtherCanvas = true;
		GameObject[] items;

		IEnumerator IEPlay(
			Vector3 target,
			int initial,
			int amount,
			Text text,
			float timeScale = 1f,
			System.Action onComplete = null)
		{
			if (canvas.renderMode != RenderMode.ScreenSpaceOverlay)
			{
				if (canvas.worldCamera.orthographic)
				{
					if (moveToOtherCanvas)
					{
						var p = new Vector3(target.x / Screen.width, target.y / Screen.height, target.z);
						target = canvas.worldCamera.ViewportToWorldPoint(p);
					}
				}
				else
				{
					var x = group.transform.position.z - canvas.worldCamera.transform.position.z;
					var h = Mathf.Tan(canvas.worldCamera.fieldOfView * Mathf.Deg2Rad * 0.5f) * x;
					float W = 540f;
					float H = W * Screen.height / Screen.width;
					var w = h * Screen.width / Screen.height;
					target.x = (target.x - W) / W * w;
					target.y = (target.y - H) / H * h;
					target.z = 1;
				}
			}

			group.SetActive(true);
			items = new GameObject[objectCount];
			items[0] = group.transform.GetChild(0).gameObject;
			for (int i = 1; i < objectCount; i++)
			{
				items[i] = Instantiate(group.transform.GetChild(0).gameObject, group.transform);
				items[i].transform.localRotation = Random.rotation;
				var p = Random.insideUnitSphere * radius;
				p.z = items[i].transform.localPosition.z;
				items[i].transform
					.DOLocalMove(p, Random.Range(.3f, .7f) / timeScale)
					.SetEase(Ease.OutCubic);
				items[i].transform
					.DOLocalRotate(Random.rotation.eulerAngles, 10)
					.SetEase(Ease.Linear);
			}

			yield return new WaitForSeconds(0.7f / timeScale);
			int counter = 0;
			int step = amount / items.Length;
			int num = amount - (step * items.Length);
			for (int i = 0; i < items.Length; i++)
			{
				var item = items[i].transform;
				item.DOMove(target, 0.9f / timeScale)
					.SetEase(Ease.InCubic)
					.SetDelay(i * 0.05f)
					.OnComplete(() =>
					{
						item.gameObject.SetActive(false);
						if (num > 0)
						{
							num--;
						}
						counter += step + (num > 0 ? 1 : 0);
						text.text = (initial + counter).ToString();
					});
			}

			yield return new WaitForSeconds(items.Length * 0.05f + 1.0f / timeScale);
			text.text = (initial + amount).ToString();
			onComplete?.Invoke();
		}

		public void Play(
			Vector3 target,
			int initial,
			int amount,
			Text text,
			float timeScale = 1f,
			System.Action onComplete = null)
		{
			StartCoroutine(IEPlay(target, initial, amount, text, timeScale, onComplete));
		}

		private void Awake()
		{
			group.gameObject.SetActive(false);
		}
	}
}