﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Nama
{
	public class GiftScene : Scene
	{
		[SerializeField] private Transform rotateTransform;
		[SerializeField] private Camera cam;
		[SerializeField] private Transform openButton;
		[SerializeField] private Transform nextButton;
		[SerializeField] private Text gemText;
		[SerializeField] private GameObject gemObject;
		[SerializeField] private CollectEffect collectEffect;
		[SerializeField] private GameObject giftBox;
		[SerializeField] private GameObject openFX;
		[SerializeField] private GameObject congratsAnim;
		[SerializeField] private GameObject confettiBlastRainbow;

		GiftItem rewardGift;

		float startTime;
		[Header("Preview")]
		[SerializeField] private PastryBagPreview pastryBagPreview;
		[SerializeField] private SprinkleBottlePreview sprinkleBottlePreview;
		[SerializeField] private PetPreview petPreview;
		[SerializeField] private GlazeToolPreview glazePreview;
		[SerializeField] private GameObject preview;
		private void OnEnable()
		{
			startTime = Time.realtimeSinceStartup;
			Analytics.Instance.LogEvent("Gift_open");
			_finished = false;
			SceneManager.Instance.HideLoading();
			RandomGift();
			Profile.Instance.GiftOpenCount++;
			Ads.SendRvImpression("RandomGift");
			openButton.gameObject.SetActive(true);
			openButton.localScale = Vector3.zero;
			openButton.DOScale(1, .5f).SetEase(Ease.OutBack);
			rotateTransform.localScale = Vector3.zero;
			gemText.transform.localScale = Vector3.zero;
			gemText.transform.DOScale(1, .5f).SetEase(Ease.OutBack);
			nextButton.gameObject.SetActive(false);
			congratsAnim.gameObject.SetActive(false);
			confettiBlastRainbow.gameObject.SetActive(false);

			DOVirtual.DelayedCall(Config.Instance.DelayDisplaySkipButtonTime, () =>
			{
				nextButton.localScale = Vector3.zero;
				nextButton.DOScale(1, .5f).SetEase(Ease.OutBack);
				nextButton.gameObject.SetActive(true);
			});

			SceneManager.Instance.OpenScene(SceneID.GlobalUI);
			UIGlobal.HideCoinInfo();
		}

		const string SETTINGS_NAME = "GiftPreviewTransform";
		Transform settingsPreview;

		void RandomGift()
		{
			// Random logic

			gemObject.SetActive(false);
			openFX.SetActive(false);
			giftBox.transform.localScale = Vector3.zero;
			rotateTransform.localScale = Vector3.zero;

			var giftGroupIndex = Mathf.Clamp(Profile.Instance.GiftOpenCount, 0, Config.Instance.InComeData.GiftProgess.TopPrizes.Count - 1);
			var giftGroup = Config.Instance.InComeData.GiftProgess.TopPrizes[giftGroupIndex];

			var giftData = giftGroup.GetRandomReward();


			if (giftData == null)
			{
				rewardGift = new GiftItem(GiftItemType.Gem, Config.Instance.InComeData.TopPrizeGem);
			}
			else
			{
				rewardGift = giftData.Gift;
			}

			var disablePet = true;

			if (disablePet)
			{
				if (rewardGift.type == GiftItemType.Pet)
				{
					rewardGift.type = GiftItemType.Random;
				}
			}

			switch (rewardGift.type)
			{
				case GiftItemType.Pet:

					List<PetData> avaiablePets = Profile.Instance.GetAvailablePets();
					PetData petData = avaiablePets.Find(p => p.id == rewardGift.id);

					if (petData != null)
					{
						ShowPet(petData);
						break;
					}
					else
					{
						if (avaiablePets.Count > 0)
						{
							var randomPet = avaiablePets.GetRandomElement();
							ShowPet(randomPet);
							break;
						}
						else
						{
							goto default;
						}
					}


				case GiftItemType.SprinkleCup:

					List<StorageSkin> avaiableSprinkleBottles = Profile.Instance.GetAvailableSprinkleBottles();
					StorageSkin sprinkleBottle = avaiableSprinkleBottles.Find(t => t.id == rewardGift.id);
					if (sprinkleBottle != null)
					{
						ShowSprinkCup(sprinkleBottle);

						break;
					}
					else
					{
						if (avaiableSprinkleBottles.Count > 0)
						{
							var randomSprinkleBottle = avaiableSprinkleBottles.GetRandomElement();
							ShowSprinkCup(randomSprinkleBottle);
							break;
						}
						else
						{
							goto default;
						}
					}
				case GiftItemType.IcingBag:

					List<CharacterSkin> avaiablePastrybags = Profile.Instance.GetAvailablePastryBags();
					CharacterSkin pastrybagSkin = avaiablePastrybags.Find(t => t.id == rewardGift.id);
					if (pastrybagSkin != null)
					{
						ShowIceBag(pastrybagSkin);
						break;
					}
					else
					{
						if (avaiablePastrybags.Count > 0)
						{
							var randomPastry = avaiablePastrybags.GetRandomElement();
							ShowIceBag(randomPastry);
							break;
						}
						else
						{
							goto default;
						}
					}

				case GiftItemType.GlazeCup:

					List<GlazeData> avaibleGlazes = Profile.Instance.GetAvailableGlazes();
					GlazeData glazeData = avaibleGlazes.Find(t => t.id == rewardGift.id);
					if (glazeData != null)
					{
						ShowGlaze(glazeData);
						break;
					}
					else
					{
						if (avaibleGlazes.Count > 0)
						{
							var randomGlaze = avaibleGlazes.GetRandomElement();
							ShowGlaze(randomGlaze);
							break;
						}
						else
						{
							goto default;
						}
					}

				case GiftItemType.Gem:
					gemText.text = string.Format("${0}", rewardGift.gem);
					break;

				case GiftItemType.Random:
					List<GiftItem> gifts = Profile.Instance.GetAvaibleGifts();

					if (gifts.Count > 0)
					{
						rewardGift = gifts.GetRandomElement();

						if (rewardGift.type == GiftItemType.Pet)
						{
							goto case GiftItemType.Pet;
						}
						else if (rewardGift.type == GiftItemType.SprinkleCup)
						{
							goto case GiftItemType.SprinkleCup;
						}
						else if (rewardGift.type == GiftItemType.IcingBag)
						{
							goto case GiftItemType.IcingBag;
						}
						else if (rewardGift.type == GiftItemType.GlazeCup)
						{
							goto case GiftItemType.GlazeCup;
						}
						else
						{
							goto default;
						}
					}
					else
					{
						goto default;
					}

				default:


					rewardGift = new GiftItem(GiftItemType.Gem, Config.Instance.InComeData.TopPrizeGem);
					gemText.text = string.Format("${0}", rewardGift.gem);

					break;
			}

			Analytics.Instance.LogEvent(string.Format("Gift_{0}_{1}_Show", rewardGift.type, rewardGift.id));

			if (rewardGift.type != GiftItemType.Pet && rewardGift.type != GiftItemType.Gem)
			{
				rotateTransform.eulerAngles = new Vector3(0, -45, 0);
				rotateTransform.DORotate(new Vector3(0, 45, 0), 2.5f, RotateMode.Fast).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutQuad);
			}

			StartCoroutine(OpenGiftBox());
		}

		private void ShowGlaze(GlazeData galzeData)
		{
			rewardGift.id = galzeData.id;
			DOVirtual.DelayedCall(0.6f,()=>
			{
				preview.SetActive(true);
				glazePreview.ShowWithId(galzeData.id);
			});
		}

		private void ShowIceBag(CharacterSkin pastrybagSkin)
		{
			rewardGift.id = pastrybagSkin.id;
			Debug.Log(rewardGift.id);
			DOVirtual.DelayedCall(1f,()=>
			{
				preview.SetActive(true);
				pastryBagPreview.ShowWithId(pastrybagSkin.name);
			});
		}

		private void ShowSprinkCup(StorageSkin sprinkleBottle)
		{
			rewardGift.id = sprinkleBottle.id;
			Debug.Log(rewardGift.id);
			DOVirtual.DelayedCall(1f,()=>
			{
				preview.SetActive(true);
				sprinkleBottlePreview.ShowWithId(sprinkleBottle.id);
			});
		}

		private void ShowPet(PetData petData)
		{
			var prefab = Resources.Load<GameObject>("Pets/" + petData.Name);
			var pet = Instantiate(prefab, rotateTransform);
			pet.gameObject.SetLayerRecursively(Layers.UI);

			// Setup preview
			settingsPreview = pet.transform.Find(SETTINGS_NAME);
			if (settingsPreview != null)
			{
				pet.transform.localPosition = settingsPreview.transform.localPosition;
				pet.transform.localEulerAngles = settingsPreview.transform.localEulerAngles;
				pet.transform.localScale = settingsPreview.transform.localScale;
			}

			rewardGift.id = petData.id;
			Debug.Log(rewardGift.id);
			DOVirtual.DelayedCall(1f,()=>
			{
				preview.SetActive(true);
				petPreview.ShowWithId(petData.Name);
			});
		}

		IEnumerator OpenGiftBox()
		{
			giftBox.transform.DOScale(.5f, .25f).SetEase(Ease.OutBack);
			yield return new WaitForSeconds(1f);
			openFX.SetActive(true);
			giftBox.transform.DOScale(0f, .25f).SetEase(Ease.OutBack);
			rotateTransform.DOScale(2f, .6f).SetEase(Ease.OutBack);
			if (rewardGift.type == GiftItemType.Gem)
			{
				gemObject.SetActive(true);
			}
		}

		public void Collect()
		{
			if (_finished)
			{
				return;
			}

			_finished = true;
			Analytics.Instance.LogEvent("Gift_ad_clicked");
			Analytics.Instance.LogEvent(string.Format("Gift_{0}_{1}_WatchAd", rewardGift.type, rewardGift.id));
			Ads.SendRvClicked("RandomGift");
			Ads.ShowRewardedVideo("RandomGift", (state) =>
			{
				if (state != RewardedVideoState.Watched)
				{
					_finished = false;
					return;
				}

				Ads.SendRvWatched("RandomGift");
				Analytics.Instance.LogEvent("Gift_ad_watched");
				if (rewardGift.type == GiftItemType.Gem)
				{
					UIGlobal.ShowCoinInfo();
					confettiBlastRainbow.gameObject.SetActive(true);
					openButton.gameObject.SetActive(false);
					nextButton.gameObject.SetActive(false);
					collectEffect.Play(
						UIGlobal.CoinAnimationTarget,
						Profile.Instance.CoinAmount,
						rewardGift.gem,
						UIGlobal.SoftCurrencyText,
						1,
						delegate
						{
							Profile.Instance.CoinAmount += rewardGift.gem;
							UIGlobal.HideCoinInfo();
							DOVirtual.DelayedCall(1.5f, delegate
							{
								openButton.gameObject.SetActive(false);
								Close();
							});
						});
				}
				else
				{
					Profile.Instance.CollectGift(rewardGift);
					congratsAnim.gameObject.SetActive(true);
					confettiBlastRainbow.gameObject.SetActive(true);
					openButton.gameObject.SetActive(false);
					nextButton.gameObject.SetActive(false);
					DOVirtual.DelayedCall(2f, GoStore);
				}
			});
		}

		private bool _finished;

		public void Skip()
		{
			if (_finished)
			{
				return;
			}

			_finished = true;
			Analytics.Instance.LogEvent("Gift_skip");
			Analytics.Instance.LogEvent($"Gift_{rewardGift.type}_{rewardGift.id}_Skip");
			Close();
		}

		public void Close()
		{
			Analytics.Instance.LogEvent("Gift_close", "duration", Time.realtimeSinceStartup - startTime);

			if (Profile.Instance.KeyAmount >= 3)
			{
				SceneManager.Instance.ShowLoading(false, 1.0f, () =>
				{
					SceneManager.Instance.OpenScene(SceneID.ChestRoom);
					SceneManager.Instance.CloseScene(SceneID.Gift);
					SceneManager.Instance.CloseScene(SceneID.Gameplay);
				});
				return;
			}

			SceneManager.Instance.ShowPopupShield();
			Ads.ShowInterstitial(() =>
			{
				SceneManager.Instance.HidePopupShield();
				SceneManager.Instance.ShowLoading(false, 1.0f, () =>
				{
					//SceneManager.Instance.OpenScene(SceneID.Gameplay);
					SceneManager.Instance.CloseScene(SceneID.Gift);
					SceneManager.Instance.OpenScene(SceneID.UIBakery, false);
				});
			}, "gift");
		}

		private void GoStore()
		{
			SceneManager.Instance.ShowLoading(false, 1.0f, () =>
			{
				if (rewardGift.type != GiftItemType.Gem || rewardGift.type != GiftItemType.Random)
				{
					Profile.Instance.rewardGift = rewardGift;
				}
				SceneManager.Instance.HidePopupShield();
				SceneManager.Instance.OpenScene(SceneID.Shop);
				SceneManager.Instance.CloseScene(SceneID.Gift);
				SceneManager.Instance.CloseScene(SceneID.Gameplay);
				SceneManager.Instance.CloseScene(SceneID.Home);
			});
		}
	}
}