Shader "Unlit/UnlitFloating"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _FloatingTex ("Floating Texture", 2D) = "white" {}
        _Speed("display name", Vector) = (10, 0, 0)
    }
    SubShader
    {
        Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"}
        Blend SrcAlpha OneMinusSrcAlpha
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _FloatingTex;
            float4 _MainTex_ST;
            float4 _FloatingTex_ST;
            fixed3 _Speed;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.uv1 = TRANSFORM_TEX(v.uv1 + _Time.x * _Speed, _FloatingTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                fixed4 col1 = tex2D(_FloatingTex, i.uv1);
                col1.a = col.a;
                return col1;
            }
            ENDCG
        }
    }
}
