using UnityEngine;

namespace Nama
{
	public class LoadingCheck : MonoBehaviour
	{
		public void LoadingDone()
		{
			GameManager.IsLoadedGameplay = true;
		}
	}
}

