using Tabtale.TTPlugins;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Nama
{
	public class Loading : Scene
	{
		[SerializeField] private Text txtPercent;
		[SerializeField] private Image fillImage;

		[SerializeField] private float timeMin = 2f;
		[SerializeField] private float timeMax = 4f;
		private float _timeLoad;
		private float _t;

		private void Start()
		{
			_timeLoad = Random.Range(timeMin, timeMax);
		}

		private bool _isDone;

		private void Update()
		{
			if (_isDone) return;

			if (_t >= (_timeLoad * 2 / 3) && !GameManager.IsLoadedGameplay)
			{
				return;
			}

			_t += Time.deltaTime;
			var percent = _t / _timeLoad;

			txtPercent.text = Mathf.RoundToInt(percent * 100) + "%";
			fillImage.fillAmount = percent;

			if (!(_t >= _timeLoad * .98f)) return;
			_isDone = true;
			SceneManager.Instance.ShowLoading(false, 1f, () =>
			{
				SceneManager.Instance.CloseScene(SceneID.Loading);
				SceneManager.Instance.HideLoading();
				TTPBanners.Show();
			});
		}
	}
}