
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Nama
{
	internal class ShopManager : Singleton<ShopManager>
	{
		[SerializeField] private ShopData data = null;
		public ShopData Data => data;
		private void Awake()
		{
#if UNITY_EDITOR
			if (data == null)
			{
				data = AssetDatabase.LoadAssetAtPath<ShopData>("Assets/Game/Shop/ShopData.prefab");
			}
#endif
		}

		#region Icing pipe
		public List<CharacterSkin> PastryBags => data.characterSkins;

		public CharacterSkin GetPastryBag(string id)
		{
			foreach (CharacterSkin item in data.characterSkins)
			{
				if (item.id.Equals(id))
				{
					return item;
				}
			}
			return data.characterSkins.Count > 0 ? data.characterSkins[0] : null;
		}

		public CharacterSkin RandomPastryBag(string notId = null, string notId1 = null)
		{
			var avaibleList = data.characterSkins.FindAll(
				e => !Profile.Instance.PastryBagOwned(e.id) &&
					 e.id != notId && e.id != notId1
			);
			return avaibleList.GetRandomElement();
		}

		public bool HasPastryBag(string itemId)
		{
			CharacterSkin characterSkin = GetPastryBag(itemId);
			return characterSkin != null && characterSkin.id == itemId;
		}

		public bool AllPastryBagsUnlocked
		{
			get
			{
				for (int i = 0; i < PastryBags.Count; i++)
				{
					if (!Profile.Instance.PastryBagOwned(PastryBags[i].id))
					{
						return false;
					}
				}
				return true;
			}
		}


		public bool PastryBagsUnlockable
		{
			get
			{
				return Profile.Instance.CoinAmount >= Profile.Instance.PastryBagPrice;
			}
		}

		#endregion

		#region Storage
		public List<StorageSkin> SprinkleBottles => data.storageSkins;

		public StorageSkin GetSprinkleBottle(string id)
		{
			foreach (StorageSkin item in data.storageSkins)
			{
				if (item.id.Equals(id))
				{
					return item;
				}
			}
			return data.storageSkins.Count > 0 ? data.storageSkins[0] : null;
		}

		public StorageSkin RandomSprinkleBottom(string notId = null, string notId1 = null)
		{
			var avaibleList = data.storageSkins.FindAll(
				e => !Profile.Instance.SprinkleBottleOwned(e.id) &&
					 e.id != notId && e.id != notId1
			);
			if (avaibleList.Count <= 0)
			{
				return null;
			}
			return data.storageSkins.Count > 0 ? avaibleList[Random.Range(0, avaibleList.Count)] : null;
		}

		public bool HasSprinkleBottle(string itemId)
		{
			var bottle =  GetSprinkleBottle(itemId);
			return bottle != null && bottle.id == itemId;
		}

		public bool AllSprinklBottleUnlocked
		{
			get
			{
				for (int i = 0; i < SprinkleBottles.Count; i++)
				{
					if (!Profile.Instance.SprinkleBottleOwned(SprinkleBottles[i].id))
					{
						return false;
					}
				}
				return true;
			}
		}

		public bool SprinkleBottleUnlockable
		{
			get
			{
				return Profile.Instance.CoinAmount >= Profile.Instance.SprinkleBottlePrice;
			}
		}

		#endregion

	}
}
