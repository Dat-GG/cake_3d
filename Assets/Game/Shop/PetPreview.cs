
using UnityEngine;

namespace Nama
{
	public class PetPreview : MonoBehaviour
	{
		[SerializeField] GameObject[] tables;
		[SerializeField] GameObject[] tableCloth;

		GameObject petObj;

		public void Show()
		{
			if (petObj != null)
			{
				Destroy(petObj);
				petObj = null;
			}
			if (Profile.Instance.Pet == null)
			{
				return;
			}

			gameObject.SetActive(true);
			var prefab = Resources.Load<GameObject>("Pets/" + Profile.Instance.Pet.Name);
			petObj = Instantiate(prefab, transform);
			petObj.transform.localPosition = Vector3.zero;
			petObj.transform.localRotation = Quaternion.Euler(0, 34.5f, 0);
			NamaUtils.SetLayerRecursively(petObj, Layers.Shop);
			var petAni = petObj.GetComponent<PetShopAnimation>();
			petAni.StartNewAnimamtion();

		}
		public void ShowWithId(string name)
		{
			if (petObj != null)
			{
				Destroy(petObj);
				petObj = null;
			}
			if (Profile.Instance.Pet == null)
			{
				return;
			}

			gameObject.SetActive(true);
			var prefab = Resources.Load<GameObject>("Pets/" + name);
			petObj = Instantiate(prefab, transform);
			petObj.transform.localPosition = Vector3.zero;
			petObj.transform.localRotation = Quaternion.Euler(0, 34.5f, 0);
			NamaUtils.SetLayerRecursively(petObj, Layers.Shop);
			var petAni = petObj.GetComponent<PetShopAnimation>();
			petAni.StartNewAnimamtion();

		}
		public void Hide()
		{
			foreach (var obj in tables) obj.SetActive(false);
			foreach (var obj in tableCloth) obj.SetActive(false);
			gameObject.SetActive(false);
		}
	}
}