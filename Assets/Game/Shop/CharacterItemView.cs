using Funzilla;
using UnityEngine;
using UnityEngine.UI;

namespace Nama
{
	class CharacterItemView : ShopItemView
	{
        protected override bool Owned(string itemId)
		{
			return Profile.Instance.PastryBagOwned(itemId);
		}
		protected override bool Active(string itemId)
		{
			return Profile.Instance.PastryBagName.Equals(itemId);
		}
		protected override void SetActive(string itemId)
		{
			Analytics.Instance.LogEvent(string.Format("item_select_{0}", itemId));
			Profile.Instance.PastryBagName = itemId;
			UIShop.Instance.ChangePastryBag(this);
		}
		protected override void Buy(string itemId)
		{
			Profile.Instance.AddPastryBag(itemId);
			Select();
			Profile.Instance.BuyCharCount++;
			Analytics.Instance.LogEvent(string.Format("Shop_{0}_Unlock_{1}", GiftItemType.IcingBag.ToString(), Profile.Instance.BuyCharCount));
			UIShop.Instance.RefreshPastryBagUnlockButton();
		}
		public override void Buy()
		{
			if (Profile.Instance.CoinAmount < Profile.Instance.PastryBagPrice)
			{
				return;
			}

			selectButton.SetActive(false);
			selectedIcon.gameObject.SetActive(true);
			PlayFocusEff();
			Profile.Instance.CoinAmount -= Profile.Instance.PastryBagPrice;
			DailyQuestManager.UpdateDay();
			Profile.Instance.MoneyCurrentSpendOnShop += Profile.Instance.PastryBagPrice;
			Buy(data.id);
			Refresh();
		}

		protected override Sprite GetSprite(ShopItemData data)
		{
			return Resources.Load<Sprite>("Sprites/Characters/" + data.image);
		}

		protected override void PostSelectEvent(ShopItemData data)
		{
			Analytics.Instance.LogEvent(string.Format("character_select_shop_{0}", data.id));
		}
	}
}
