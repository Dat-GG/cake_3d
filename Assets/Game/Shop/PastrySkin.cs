using DG.Tweening;
using UnityEngine;

namespace Nama
{
	public class PastrySkin : MonoBehaviour
	{
		[SerializeField] Renderer[] renderers;

		[SerializeField] private Vector3 restRotation;

		public Vector3 RestRotation => restRotation;
		
		public Renderer[] Renderers => renderers;
		Sequence idleAnim;

		public virtual void Fall()
		{
		}

		public virtual void Begin()
		{
		}

		public virtual void Release()
		{
		}

		Tweener Shake()
		{
			return transform.DOShakeRotation(
				0.2f, new Vector3(30, 30, 30), 10, 45, false);
		}

		public virtual void StartIdle()
		{
			StopIdle();
			idleAnim = DOTween.Sequence();
			idleAnim.Append(Shake().SetDelay(2));
			idleAnim.Append(Shake().SetDelay(5).SetLoops(-1));
			idleAnim.Play();
		}

		public virtual void StopIdle()
		{
			if (idleAnim != null)
			{
				idleAnim.Kill();
				idleAnim = null;
				transform.DOLocalRotate(Vector3.zero, 0.1f);
			}
		}
	}
}