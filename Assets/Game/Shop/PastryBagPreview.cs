
using UnityEngine;

namespace Nama
{
	public class PastryBagPreview : MonoBehaviour
	{
		[SerializeField] private Path path;
		[SerializeField] private PastryBag pastryBag;
		[SerializeField] private Brush brush;
		[SerializeField] private Material strokeMaterial;
		[SerializeField] private GameObject cake;

		private Polyline _poly;
		private PolylinePosition _currentPosition;
		private StrokeDisplay _stroke;
		private GameObject _display;

		private void Awake()
		{
			var t = path.transform;
			_poly = new Polyline(path, t);
			_display = new GameObject("display");
			_display.transform.SetParent(t.parent);
			_display.transform.localPosition = path.transform.localPosition;
			_display.transform.localScale = Vector3.one;
			_display.transform.localRotation = Quaternion.identity;

			_stroke = StrokeDisplay.Create(_display.transform);
			_stroke.gameObject.layer = Layers.Shop;
		}

		public void Show()
		{
			cake.SetActive(true);
			gameObject.SetActive(true);
			_display.SetActive(true);
			_currentPosition = _poly.StartPosition;
			pastryBag.LoadSkin(Profile.Instance.PastryBag.name);
			foreach (var r in pastryBag.Skin.GetComponentsInChildren<MeshRenderer>())
			{
				r.gameObject.layer = Layers.Shop;
			}
			pastryBag.Press();
			pastryBag.Begin(
				_display.transform,
				_currentPosition.Pos,
				Profile.Instance.PastryBag.color,
				0);

			_stroke.Init(
				brush,
				_poly,
				_currentPosition,
				0,
				new[] { Profile.Instance.PastryBag.color },
				null,
				strokeMaterial);
		}
		public void ShowWithId(string skinName)
		{
			cake.SetActive(true);
			gameObject.SetActive(true);
			_display.SetActive(true);
			_currentPosition = _poly.StartPosition;
			pastryBag.LoadSkin(skinName);
			foreach (var r in pastryBag.Skin.GetComponentsInChildren<MeshRenderer>())
			{
				r.gameObject.layer = Layers.Shop;
			}
			pastryBag.Press();
			pastryBag.Begin(
				_display.transform,
				_currentPosition.Pos,
				Profile.Instance.PastryBag.color,
				0);

			_stroke.Init(
				brush,
				_poly,
				_currentPosition,
				0,
				new[] { Profile.Instance.PastryBag.color },
				null,
				strokeMaterial);
		}
		public void Hide()
		{
			gameObject.SetActive(false);
			_display.SetActive(false);
			cake.SetActive(false);
		}

		private void Update()
		{
			if (_stroke == null || !pastryBag.Ready || _currentPosition.Offset >= _poly.Length)
			{
				return;
			}

			var delta = Config.DrawSpeed * Time.smoothDeltaTime;
			_currentPosition = _poly.Advance(delta, _currentPosition);
			_stroke.Advance(delta);
			pastryBag.Move(_currentPosition.Pos, 0);
			if (_currentPosition.Offset < _poly.Length) return;
			pastryBag.Release(false);
			pastryBag.Rotate();
		}
	}
}