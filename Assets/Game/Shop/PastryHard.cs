
using DG.Tweening;
using UnityEngine;

namespace Nama
{
	public class PastryHard : PastrySkin
	{
		[SerializeField] new Rigidbody rigidbody;

		Vector3 position;

		public override void Fall()
		{
			foreach (var collider in GetComponentsInChildren<Collider>())
			{
				collider.enabled = true;
			}
			rigidbody.isKinematic = false;
		}

		public override void Begin()
		{
			foreach (var collider in GetComponentsInChildren<SphereCollider>())
			{
				collider.enabled = false;
			}
			rigidbody.isKinematic = true;
			rigidbody.transform.DOLocalMove(new Vector3(-0.12f, 0.12f, 0.02f), 0.3f);
			rigidbody.transform.DOLocalRotate(Vector3.zero, 0.3f);
		}

		public override void Release()
		{

		}
	}
}