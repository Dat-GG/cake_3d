using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using Funzilla;

namespace Nama
{
	internal class UIShop : Scene
	{
		[SerializeField] private Button[] tabButtons;
		[SerializeField] private ShopItemView[] characterItems;
		[SerializeField] private ShopItemView[] storageItems;
		[SerializeField] private ShopItemView[] petItems;
		[SerializeField] private ShopItemView[] glazeItems;

		[SerializeField] private Text priceText;
		[SerializeField] private Button unlockRandomBtnMoney;
		[SerializeField] private ScrollRect scrollIcingPipe;
		[SerializeField] private ScrollRect scrollStorage;
		[SerializeField] private ScrollRect scrollPet;
		[SerializeField] private ScrollRect scrollGalzeTool;

		[Header("Preview")]
		[SerializeField] private PastryBagPreview pastryBagPreview;
		[SerializeField] private SprinkleBottlePreview sprinkleBottlePreview;
		[SerializeField] private PetPreview petPreview;
		[SerializeField] private GlazeToolPreview glazePreview;
		[SerializeField] private GameObject buttonEffect;
		private ShopItemView _selectedSkin;

		public Text pastryBagItemCountText;
		public Text sprinkleItemCountText;
		public Text glazeToolItemCountText;
		public static UIShop Instance { get; private set; }

		private void Awake()
		{
			pastryBagItemCountText.text = Profile.Instance.PastryBagAmount + "/12";
			sprinkleItemCountText.text = Profile.Instance.SprinkleBottleAmount + "/12";
			glazeToolItemCountText.text = Profile.Instance.GlazeAmount + "/12";
			Instance = this;
			UIGlobal.ShowCoinInfo();
			SceneManager.Instance.HideLoading();

			tabButtons[0].onClick.AddListener(ShowPastryBagTab);
			tabButtons[1].onClick.AddListener(ShowSprinkleBottleTab);
			tabButtons[2].onClick.AddListener(ShowGlazeToolTab);
			tabButtons[3].onClick.AddListener(ShowPetTab);
		}

		private bool _active;
		private float _startTime;
		private void OnEnable()
		{
			_active = true;
			_startTime = Time.realtimeSinceStartup;
			if (Profile.Instance.FirstTwoTimeGoStore == 2 && !ShopManager.Instance.PastryBagsUnlockable)
            {
				ShowSprinkleBottleTab();
			}
            else
            {
				ShowPastryBagTab();
			}
			
			SceneManager.Instance.HideLoading();
			Analytics.Instance.LogEvent("Shop_open");

			var gameplay = Gameplay.Instance;
			if (gameplay)
			{
				gameplay.gameObject.SetActive(false);
			}

			if (Profile.Instance.rewardGift == null) return;
			switch (Profile.Instance.rewardGift.type)
			{
				case GiftItemType.Pet:
					ShowPetTab();
					break;
				case GiftItemType.SprinkleCup:
					ShowSprinkleBottleTab();
					break;
				case GiftItemType.IcingBag:
					ShowPastryBagTab();
					break;
				case GiftItemType.GlazeCup:
					ShowGlazeToolTab();
					break;
			}

			Profile.Instance.rewardGift = null;
		}

		private void OnDestroy()
		{
			Instance = null;
		}

		public override void Init(object data)
		{
		}

		#region Icing pipe
		public void ChangePastryBag(ShopItemView newSelectedItem)
		{
			if (_selectedSkin != null)
			{
				_selectedSkin.Refresh();
			}
			_selectedSkin = newSelectedItem;
			pastryBagPreview.Show();
		}

		private void UnlockRandomPastryBag()
		{
			if (Profile.Instance.CoinAmount < Profile.Instance.PastryBagPrice /*&& NotWatchingVideo*/)
			{
				return;
			}

			var checkedNewRewardList =  false;
			var availableList = new List<int>(10);
			for (var i = 0; i < 6; i++)
			{
				if (!Profile.Instance.PastryBagOwned(ShopManager.Instance.PastryBags[i].id))
				{
					availableList.Add(i);
				}
			}

			if (availableList.Count <= 0)
			{
				for (var i = 6; i < ShopManager.Instance.PastryBags.Count; i++)
				{
					if (!Profile.Instance.PastryBagOwned(ShopManager.Instance.PastryBags[i].id))
					{
						availableList.Add(i);
					}
				}

				checkedNewRewardList = true;
			}

			if (availableList.Count <= 0 && checkedNewRewardList)
			{
				return;
			}
			
			if (availableList.Count == 1  &&  checkedNewRewardList)
			{
				characterItems[availableList[0]].Buy();
				priceText.text = Profile.Instance.PastryBagPrice.ToString();
				pastryBagItemCountText.text = Profile.Instance.PastryBagAmount + "/8";
				unlockRandomBtnMoney.interactable = false;
				return;
			}

			var unlockedItem = characterItems[availableList[Random.Range(0, availableList.Count)]];
			var pageNumber = Mathf.FloorToInt(unlockedItem.GetComponent<RectTransform>().anchoredPosition.x / 1080.0f);
			scrollIcingPipe.DONormalizedPos(Vector2.right * pageNumber, .5f).OnComplete(delegate
			{
				DOVirtual.DelayedCall(.3f, delegate
				{
					unlockedItem.Buy();
					priceText.text = Profile.Instance.PastryBagPrice.ToString();
					pastryBagItemCountText.text = Profile.Instance.PastryBagAmount + "/8";
					unlockRandomBtnMoney.interactable = ShopManager.Instance.PastryBagsUnlockable;
                    if (!ShopManager.Instance.PastryBagsUnlockable)
                    {
						buttonEffect.SetActive(false);
                    }
				});
			});
			unlockRandomBtnMoney.interactable = false;
		}

		#endregion

		#region Sprinkle characters

		public void ChangeSprinkleBottle(ShopItemView newSelectedItem)
		{
			if (_selectedSkin != null)
			{
				_selectedSkin.Refresh();
			}
			_selectedSkin = newSelectedItem;
			sprinkleBottlePreview.Show();
		}

		public void ChangePet(ShopItemView newSelectedItem)
		{
			if (_selectedSkin != null)
			{
				_selectedSkin.Refresh();
			}
			_selectedSkin = newSelectedItem;
			petPreview.Show();
		}

		public void ChangeGlaze(ShopItemView newSelectedItem)
		{
			if (_selectedSkin != null)
			{
				_selectedSkin.Refresh();
			}
			_selectedSkin = newSelectedItem;
			glazePreview.Show();
		}

		private void UnlockStorageRandom()
		{
			if (Profile.Instance.CoinAmount < Profile.Instance.SprinkleBottlePrice)
			{
				return;
			}
			
			var checkedNewRewardList = false;

			var availableList = new List<int>(10);
			for (var i = 0; i < 6; i++)
			{
				if (!Profile.Instance.SprinkleBottleOwned(ShopManager.Instance.SprinkleBottles[i].id))
				{
					availableList.Add(i);
				}
			}
			

			if (availableList.Count <= 0)
			{
				for (var i = 6; i < ShopManager.Instance.SprinkleBottles.Count; i++)
				{
					if (!Profile.Instance.SprinkleBottleOwned(ShopManager.Instance.SprinkleBottles[i].id))
					{
						availableList.Add(i);
					}
				}

				checkedNewRewardList = true;
			}
			
			if (availableList.Count <= 0  &&  checkedNewRewardList)
			{
				return;
			}

			if (availableList.Count == 1 &&  checkedNewRewardList)
			{
				storageItems[availableList[0]].Buy();
				priceText.text = Profile.Instance.SprinkleBottlePrice.ToString();
				sprinkleItemCountText.text = Profile.Instance.SprinkleBottleAmount + "/8";
				unlockRandomBtnMoney.interactable = false;
				return;
			}

			var unlockedItem = storageItems[availableList[Random.Range(0, availableList.Count)]];
			var pageNumber = Mathf.FloorToInt(unlockedItem.GetComponent<RectTransform>().anchoredPosition.x / 1080.0f);
			scrollStorage.DONormalizedPos(Vector2.right * pageNumber, .5f).OnComplete(delegate
			{
				DOVirtual.DelayedCall(.3f, delegate
				{
					unlockedItem.Buy();
					priceText.text = Profile.Instance.SprinkleBottlePrice.ToString();
					sprinkleItemCountText.text = Profile.Instance.SprinkleBottleAmount + "/8";
					unlockRandomBtnMoney.interactable = ShopManager.Instance.SprinkleBottleUnlockable;
					if (!ShopManager.Instance.SprinkleBottleUnlockable)
					{
						buttonEffect.SetActive(false);
					}
				});
			});
			unlockRandomBtnMoney.interactable = false;
		}

		#endregion

		private void UnlockRandomPet()
		{
			if (Profile.Instance.CoinAmount < Profile.Instance.PetPrice)
			{
				return;
			}
		
			var availableList = new List<int>(10);
			for (var i = 0; i < PetManager.Instance.Pets.Count; i++)
			{
				priceText.text = Profile.Instance.PetPrice.ToString();
				if (!Profile.Instance.PetOwned(PetManager.Instance.Pets[i].id))
				{
					availableList.Add(i);
				}
			}

			if (availableList.Count <= 0)
			{
				return;
			}

			if (availableList.Count == 1)
			{
				petItems[availableList[0]].Buy();
				unlockRandomBtnMoney.interactable = false;
				return;
			}

			var unlockedItem = petItems[availableList[Random.Range(0, availableList.Count)]];
			var pageNumber = Mathf.FloorToInt(
				unlockedItem.GetComponent<RectTransform>().anchoredPosition.x / 1080.0f);
			scrollPet.DONormalizedPos(Vector2.right * pageNumber, .5f).OnComplete(delegate
			{
				DOVirtual.DelayedCall(.3f, delegate
				{
					unlockedItem.Buy();
					unlockRandomBtnMoney.interactable = PetManager.Instance.Unlockable;
				});
			});
			unlockRandomBtnMoney.interactable = false;
		}

		public void RefreshPastryBagUnlockButton()
		{
			unlockRandomBtnMoney.gameObject.SetActive(!ShopManager.Instance.AllPastryBagsUnlocked);
			unlockRandomBtnMoney.interactable = ShopManager.Instance.PastryBagsUnlockable;
		}

		public void RefreshSprinkleBottleUnlockButton()
		{
			unlockRandomBtnMoney.gameObject.SetActive(!ShopManager.Instance.AllSprinklBottleUnlocked);
			unlockRandomBtnMoney.interactable = ShopManager.Instance.SprinkleBottleUnlockable;
		}

		public void RefreshPetUnlockButton()
		{
			unlockRandomBtnMoney.gameObject.SetActive(!PetManager.Instance.AllUnlocked);
			unlockRandomBtnMoney.interactable = PetManager.Instance.Unlockable;
		}


		public void RefreshGlazeUnlockButton()
		{
			unlockRandomBtnMoney.gameObject.SetActive(!GlazeManager.Instance.AllUnlocked);
			unlockRandomBtnMoney.interactable = GlazeManager.Instance.Unlockable;
		}

		private void ShowTabButton(int index)
		{
			for (var i = 0; i < tabButtons.Length; i++)
			{
				tabButtons[i].transform.GetChild(0).gameObject.SetActive(i == index);
				tabButtons[i].transform.GetChild(1).gameObject.SetActive(i != index);
			}
		}
		private void ShowGlazeToolTab()
		{
			scrollGalzeTool.gameObject.SetActive(true);
			scrollIcingPipe.gameObject.SetActive(false);
			scrollStorage.gameObject.SetActive(false);
			scrollPet.gameObject.SetActive(false);
			ShowTabButton(2);
			RefreshGlazeUnlockButton();
			priceText.text = Profile.Instance.GlazePrice.ToString();
			unlockRandomBtnMoney.onClick.RemoveAllListeners();
			unlockRandomBtnMoney.onClick.AddListener(UnlockRandomGlazeTool);


			if (_selectedSkin)
			{
				_selectedSkin = null;
			}
			var n = Mathf.Min(
				glazeItems.Length,
				GlazeManager.Instance.Glazes.Count);

			for (var i = 0; i < n; i++)
			{
				glazeItems[i].Set(GlazeManager.Instance.Glazes[i]);
				if (glazeItems[i].Selected)
				{
					_selectedSkin = glazeItems[i];
				}
			}

			sprinkleBottlePreview.Hide();
			petPreview.Hide();
			pastryBagPreview.Hide();
			glazePreview.Show();
			if (GlazeManager.Instance.Unlockable)
			{
				buttonEffect.SetActive(true);
			}
			else
			{
				buttonEffect.SetActive(false);
			}
		}

		private void UnlockRandomGlazeTool()
		{

			if (Profile.Instance.CoinAmount < Profile.Instance.GlazePrice /*&& NotWatchingVideo*/)
			{
				return;
			}
			
			var checkedNewRewardList = false;

			var availableList = new List<int>(10);
			for (var i = 0; i < 6; i++)
			{
				if (!Profile.Instance.GlazeSkinOwned(GlazeManager.Instance.Glazes[i].id))
				{
					availableList.Add(i);
				}
			}

			if (availableList.Count <= 0)
			{
				for (var i = 6; i < GlazeManager.Instance.Glazes.Count; i++)
				{
					if (!Profile.Instance.GlazeSkinOwned(GlazeManager.Instance.Glazes[i].id))
					{
						availableList.Add(i);
					}
				}
				checkedNewRewardList = true;
			}
			
			if (availableList.Count <= 0  &&   checkedNewRewardList)
			{
				return;
			}

			if (availableList.Count == 1 && checkedNewRewardList)
			{
				glazeItems[availableList[0]].Buy();
				priceText.text = Profile.Instance.GlazePrice.ToString();
				glazeToolItemCountText.text = Profile.Instance.GlazeAmount + "/8";
				unlockRandomBtnMoney.interactable = false;
				return;
			}

			var unlockedItem = glazeItems[availableList[Random.Range(0, availableList.Count)]];
			var pageNumber = Mathf.FloorToInt(unlockedItem.GetComponent<RectTransform>().anchoredPosition.x / 1080.0f);
			scrollStorage.DONormalizedPos(Vector2.right * pageNumber, .5f).OnComplete(delegate
			{
				DOVirtual.DelayedCall(.3f, delegate
				{
					unlockedItem.Buy();
					priceText.text = Profile.Instance.GlazePrice.ToString();
					glazeToolItemCountText.text = Profile.Instance.GlazeAmount + "/8";
					unlockRandomBtnMoney.interactable = GlazeManager.Instance.Unlockable;
                    if (!GlazeManager.Instance.Unlockable)
                    {
						buttonEffect.SetActive(false);
                    }
				});
			});
			unlockRandomBtnMoney.interactable = false;
		}

		private void ShowPastryBagTab()
		{
			scrollGalzeTool.gameObject.SetActive(false);
			scrollIcingPipe.gameObject.SetActive(true);
			scrollStorage.gameObject.SetActive(false);
			scrollPet.gameObject.SetActive(false);
			RefreshPastryBagUnlockButton();

			ShowTabButton(0);

			priceText.text = Profile.Instance.PastryBagPrice.ToString();
			unlockRandomBtnMoney.onClick.RemoveAllListeners();
			unlockRandomBtnMoney.onClick.AddListener(UnlockRandomPastryBag);

			if (_selectedSkin)
			{
				_selectedSkin = null;
			}
			var n = Mathf.Min(
				characterItems.Length,
				ShopManager.Instance.PastryBags.Count);

			for (var i = 0; i < n; i++)
			{
				characterItems[i].Set(ShopManager.Instance.PastryBags[i]);
				if (characterItems[i].Selected)
				{
					_selectedSkin = characterItems[i];
				}
			}

			glazePreview.Hide();
			sprinkleBottlePreview.Hide();
			petPreview.Hide();
			pastryBagPreview.Show();
            if (ShopManager.Instance.PastryBagsUnlockable)
            {
				buttonEffect.SetActive(true);
            }
            else
            {
				buttonEffect.SetActive(false);
            }
		}

		private void ShowSprinkleBottleTab()
		{
			scrollGalzeTool.gameObject.SetActive(false);
			scrollIcingPipe.gameObject.SetActive(false);
			scrollStorage.gameObject.SetActive(true);
			scrollPet.gameObject.SetActive(false);
			RefreshSprinkleBottleUnlockButton();

			ShowTabButton(1);

			priceText.text = Profile.Instance.SprinkleBottlePrice.ToString();
			unlockRandomBtnMoney.onClick.RemoveAllListeners();
			unlockRandomBtnMoney.onClick.AddListener(UnlockStorageRandom);

			_selectedSkin = null;

			var n = Mathf.Min(
				storageItems.Length,
				ShopManager.Instance.SprinkleBottles.Count);


			for (var i = 0; i < n; i++)
			{
				storageItems[i].Set(ShopManager.Instance.SprinkleBottles[i]);
				if (storageItems[i].Selected)
				{
					_selectedSkin = storageItems[i];
				}
			}
			glazePreview.Hide();
			pastryBagPreview.Hide();
			petPreview.Hide();
			sprinkleBottlePreview.Show();
			if (ShopManager.Instance.SprinkleBottleUnlockable)
			{
				buttonEffect.SetActive(true);
			}
			else
			{
				buttonEffect.SetActive(false);
			}
		}

		private void ShowPetTab()
		{
			scrollGalzeTool.gameObject.SetActive(false);
			scrollIcingPipe.gameObject.SetActive(false);
			scrollStorage.gameObject.SetActive(false);
			scrollPet.gameObject.SetActive(true);
			unlockRandomBtnMoney.onClick.RemoveAllListeners();
			unlockRandomBtnMoney.onClick.AddListener(UnlockRandomPet);
			RefreshPetUnlockButton();
			ShowTabButton(3);

			priceText.text = Profile.Instance.PetPrice.ToString();

			var n = Mathf.Min(
				petItems.Length,
				PetManager.Instance.Pets.Count);

			_selectedSkin = null;
			for (var i = 0; i < n; i++)
			{
				petItems[i].Set(PetManager.Instance.Pets[i]);
				if (petItems[i].Selected)
				{
					_selectedSkin = petItems[i];
				}
			}

			glazePreview.Hide();
			sprinkleBottlePreview.Hide();
			pastryBagPreview.Hide();
			petPreview.Show();
		}

		public void Close()
		{
			if (!_active) return;
			_active = false;
			Analytics.Instance.LogEvent("Shop_close", "duration", Time.realtimeSinceStartup - _startTime);
			//UIGlobal.HideCoinInfo();
			SceneManager.Instance.ShowLoading(false, 1.0f, () =>
			{
				gameObject.SetActive(false);
				SceneManager.Instance.CloseScene(SceneID.Shop);
				//var gameplay = Gameplay.Instance;
    //            if (gameplay)
    //            {
                    SceneManager.Instance.HideLoading();
				//                gameplay.ReloadToolSkins();
				//                gameplay.gameObject.SetActive(true);
				//                Gameplay.Level.CurrentLayer.CurrentSurface.RefreshTool();
				//                SceneManager.Instance.OpenScene(SceneID.Home);
				//            }
				//            else
				//            {
				//                //SceneManager.Instance.OpenScene(SceneID.Gameplay);
				SceneManager.Instance.OpenScene(SceneID.UIBakery, false);
				UIBakery.Instance.ShowShopButton();
				UIBakery.Instance._checkStart = true;
				UIBakery.Instance.CheckDailyQuestButtonNotification();
				if (Gameplay.PreviewLevel.CakeType == CakeType.Vip)
                {
					UIBakery.Instance.HideShopButton();
                }
				//            }
			});
		}

		public override void OnBackButtonPressed()
		{
			Close();
		}
	}
}
