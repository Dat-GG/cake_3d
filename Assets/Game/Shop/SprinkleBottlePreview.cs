
using UnityEngine;

namespace Nama
{
	public class SprinkleBottlePreview : MonoBehaviour
	{
		[SerializeField] private Path path;
		[SerializeField] private CakeSurface surface;
		[SerializeField] private Sprinkles[] options;
		[SerializeField] private GameObject cake;

		private Polyline _poly;
		private PolylinePosition _currentPosition;
		private GameObject _display;
		private Bottle _bottle;
		private Sprinkles _option;
		int fillStep;
		PolylinePosition lastStrewPos;

		private void Awake()
		{
			_poly = new Polyline(path, path.transform);
			_display = new GameObject("display");
			_display.transform.SetParent(path.transform.parent);
			_display.transform.localPosition = path.transform.localPosition;
			_display.transform.localScale = Vector3.one;
			_display.transform.localRotation = Quaternion.identity;

			foreach (var option in options)
			{
				option.Init();
			}
		}

		public void Show()
		{
			// Load bottle skin
			if (_bottle != null)
			{
				Destroy(_bottle.gameObject);
			}

			_bottle = AssetManager.GetBottle(Profile.Instance.SprinkleBottleSkinName);
			_bottle.transform.SetParent(transform);

			if (_option != null)
			{
				_option.Clear();
			}

			// Init sprinkles
			_option = options[Random.Range(0, options.Length)];
			_option.OnStepBegan(surface, _display.transform);
			_option.ShowBottle(_bottle);
			NamaUtils.SetLayerRecursively(_bottle.gameObject, Layers.Shop);
			_option.Character.gameObject.SetActive(true);
			_option.Character.transform.GetChild(0).localScale = Vector3.one;

			// Start
			fillStep = 0;
			lastStrewPos = _currentPosition = _poly.StartPosition;
			_option.ShowCharacter(_currentPosition.Pos, 0.6f);
			_option.Press(true, false);
			_bottle.Press(Config.SprinkleDelay);
			_option.StopIdle();
			gameObject.SetActive(true);
			cake.SetActive(true);
		}
		public void ShowWithId(string bottleName)
		{
			// Load bottle skin
			if (_bottle != null)
			{
				Destroy(_bottle.gameObject);
			}

			_bottle = AssetManager.GetBottle(bottleName);
			_bottle.transform.SetParent(transform);

			if (_option != null)
			{
				_option.Clear();
			}

			// Init sprinkles
			_option = options[Random.Range(0, options.Length)];
			_option.OnStepBegan(surface, _display.transform);
			_option.ShowBottle(_bottle);
			NamaUtils.SetLayerRecursively(_bottle.gameObject, Layers.Shop);
			_option.Character.gameObject.SetActive(true);
			_option.Character.transform.GetChild(0).localScale = Vector3.one;

			// Start
			fillStep = 0;
			lastStrewPos = _currentPosition = _poly.StartPosition;
			_option.ShowCharacter(_currentPosition.Pos, 0.6f);
			_option.Press(true, false);
			_bottle.Press(Config.SprinkleDelay);
			_option.StopIdle();
			gameObject.SetActive(true);
			cake.SetActive(true);
		}
		public void Hide()
		{
			cake.SetActive(false);
			gameObject.SetActive(false);
			if (_option != null)
			{
				_option.Clear();
			}
		}

		private void Update()
		{
			if (_bottle == null || !_bottle.Ready ||
				_currentPosition.Offset >= _poly.Length)
			{
				return;
			}

			var delta = Config.DrawSpeed * Time.smoothDeltaTime;
			_currentPosition = _poly.Advance(delta, _currentPosition);
			_option.UpdateCharacterPosition(_currentPosition.Pos);
			int n = (int)(_currentPosition.Offset / _option.Spacing);
			for (int i = fillStep; i < n; i++)
			{
				Advance(_option.Spacing);
				Strew();
			}

			if (_currentPosition.Offset >= _poly.Length)
			{
				_option.Release();
				_bottle.Release();
			}
			fillStep = n;
		}

		public void Advance(float delta)
		{
			lastStrewPos.Offset += delta;
			var remain = delta;
			var points = _poly.Points;
			var nSegs = _poly.Points.Count - 1;

			for (; lastStrewPos.Seg < nSegs && remain > 0;)
			{
				var segDirection = points[lastStrewPos.Seg + 1] - points[lastStrewPos.Seg];
				var segLength = segDirection.magnitude;
				segDirection /= segLength;

				var segRemainLength = segLength - lastStrewPos.SegOffset;
				if (remain > segRemainLength)
				{
					remain -= segRemainLength;
					lastStrewPos.SegOffset = 0;
					lastStrewPos.Seg++;
					if (lastStrewPos.Seg >= nSegs)
					{
						break;
					}
				}
				else
				{
					lastStrewPos.SegOffset += remain;
					lastStrewPos.Pos = points[lastStrewPos.Seg] + segDirection * lastStrewPos.SegOffset;
					return;
				}
			} // for
			lastStrewPos.Pos = points[lastStrewPos.Seg];
			return;
		}

		void Strew()
		{
			for (int i = 0; i < _option.DropCount; i++)
			{
				var obj = _option.NewObject();
				Vector3 p = Random.insideUnitCircle * _option.DropRadius * 0.5f + lastStrewPos.Pos;
				p.z = -0.6f;
				obj.transform.localPosition = p;
				Destroy(obj);
			} // for
		}
	}
}