using Funzilla;
using UnityEngine;

namespace Nama
{
	class StorageItemView : ShopItemView
	{
		protected override bool Owned(string itemId)
		{
			return Profile.Instance.SprinkleBottleOwned(itemId);
		}
		protected override bool Active(string itemId)
		{
			return Profile.Instance.SprinkleBottleSkinName.Equals(itemId);
		}
		protected override void SetActive(string itemId)
		{
			Analytics.Instance.LogEvent(string.Format("item_select_{0}", itemId));
			Profile.Instance.SprinkleBottleSkinName = itemId;
			UIShop.Instance.ChangeSprinkleBottle(this);
		}
		protected override void Buy(string itemId)
		{
			Profile.Instance.AddSprinkleBottle(itemId);
			Select();
			Profile.Instance.SprinkleBottleBuyCount++;
			Analytics.Instance.LogEvent(string.Format("Shop_{0}_Unlock_{1}", GiftItemType.SprinkleCup.ToString(), Profile.Instance.SprinkleBottleBuyCount));
			UIShop.Instance.RefreshSprinkleBottleUnlockButton();
		}
		public override void Buy()
		{
			if (Profile.Instance.CoinAmount < Profile.Instance.SprinkleBottlePrice)
			{
				return;
			}
			selectButton.SetActive(false);
			selectedIcon.gameObject.SetActive(true);
			PlayFocusEff();
			Profile.Instance.CoinAmount -= Profile.Instance.SprinkleBottlePrice;
			DailyQuestManager.UpdateDay();
			Profile.Instance.MoneyCurrentSpendOnShop += Profile.Instance.SprinkleBottlePrice;
			Buy(data.id);
			Refresh();
		}
		protected override Sprite GetSprite(ShopItemData data)
		{
			return Resources.Load<Sprite>("Sprites/Storages/" + data.image);
		}
		protected override void PostSelectEvent(ShopItemData data)
		{
//			Analytics.Instance.LogEvent(string.Format("character_select_shop_{0}", data.id));
		}
	}
}
