
using UnityEngine;

namespace Nama
{
	public class PastrySoft : PastrySkin
	{
		[SerializeField] Transform rootBone;

		bool finished = false;

		public override void Fall()
		{
			if (finished)
			{
				return;
			}
			finished = true;
			foreach (var body in GetComponentsInChildren<Rigidbody>())
			{
				body.velocity = Vector3.zero;
				body.useGravity = true;
			}
		}

		public override void Begin()
		{
			if (!finished)
			{
				return;
			}
			var bodies = GetComponentsInChildren<Rigidbody>();
			foreach (var body in bodies)
			{
				body.velocity = Vector3.zero;
				body.useGravity = false;
			}
			bodies[0].transform.localPosition = new Vector3(0, 0, -0.005f);
			bodies[0].transform.localEulerAngles = new Vector3(90, 0, 0);
			for (int i = 1; i < bodies.Length; i++)
			{
				bodies[0].transform.localPosition = new Vector3(0, 0.0025f, 0);
				bodies[0].transform.localEulerAngles = Vector3.zero;
			}
		}

		public override void Release()
		{

		}

		private void Update()
		{
			if (!finished)
			{
				rootBone.localPosition = new Vector3(0, 0, -0.005f);
				rootBone.localEulerAngles = new Vector3(90, 0, 0);
			}
		}
	}
}