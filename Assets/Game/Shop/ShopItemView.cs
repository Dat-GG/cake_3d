using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
namespace Nama
{
	internal abstract class ShopItemView : MonoBehaviour
	{
		[SerializeField] private Image image = null;
		[SerializeField] protected GameObject selectButton;
		[SerializeField] protected Image selectedIcon;
		[SerializeField] private GameObject unknown;
		[SerializeField] private Transform panel;
		[SerializeField] private ParticleSystem particle;
		[SerializeField] private GameObject FocusEff;
		protected ShopItemData data = null;

		public bool Selected { get; private set; }
		public void Set(ShopItemData data)
		{
			this.data = data;
			Refresh();
		}
		protected virtual bool Owned(string itemId)
		{
			return true;
		}
		protected virtual bool Active(string itemId)
		{
			return true;
		}
		protected virtual void SetActive(string itemId)
		{
		}
		protected abstract void Buy(string itemId);
		public void Refresh()
		{
			selectButton.SetActive(true);
			selectedIcon.gameObject.SetActive(false);
			image.gameObject.SetActive(false);
			unknown.SetActive(false);
			gameObject.SetActive(data != null);
			if (!gameObject.activeSelf)
			{ // For safety: avoid exception
				return;
			}
			image.sprite = GetSprite(data);
			if (Owned(data.id))
			{
				image.gameObject.SetActive(true);
				Selected = Active(data.id);
				selectButton.SetActive(!Selected);
				selectedIcon.gameObject.SetActive(Selected);
				unknown.SetActive(false);
			}
			else
			{
				image.gameObject.SetActive(false);
				selectButton.SetActive(false);
				unknown.SetActive(true);
			}
		}
		protected abstract Sprite GetSprite(ShopItemData data);
		public virtual void Select()
		{
			bool owned = Owned(data.id);
			if (data == null || !owned)
			{
				return;
			}
			PostSelectEvent(data);
			if (!Active(data.id))
			{
				SetActive(data.id);
			}
			Selected = true;
			Refresh();
			PlayEffect();
			//PlayFocusEff();
			SoundManager.Instance.PlaySFX("equip");
		}
		protected virtual void PostSelectEvent(ShopItemData data)
		{
		}
		public abstract void Buy();
		void PlayEffect()
		{
			particle.Play();
			panel.transform.localEulerAngles = new Vector3(0, 90, 0);
			panel.transform.DOLocalRotate(Vector3.zero, .7f);
		}

		protected virtual void PlayFocusEff()
		{
			FocusEff.gameObject.SetActive(true);
		}
	}
}
