
using System.Collections.Generic;
using UnityEngine;

namespace Nama
{
	[System.Serializable]
	class ShopItemData
	{
		public string id; // Also used to refer to asset name
		public string image;
	}

	[System.Serializable]
	class CharacterSkin : ShopItemData
	{
		public string name;
		public Color color = Utils.ColorFromUint(0xFFFF8900);
	}
	
	[System.Serializable]
	class StorageSkin : ShopItemData
	{
		public string name;
	}
	
	class ShopData : MonoBehaviour
	{
		public List<CharacterSkin> characterSkins;
		public List<StorageSkin> storageSkins;
	}
}
