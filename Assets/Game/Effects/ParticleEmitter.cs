using UnityEngine;

namespace Funzilla
{
	public class ParticleEmitter : MonoBehaviour
	{
		[SerializeField] private int emisson;
		[SerializeField] private float lifeTime = 2f;
		private ParticleSystem.EmissionModule _emission;
		private float _duration;

		public static ParticleEmitter Instance;

		private void Awake()
		{
			Instance = this;
			_emission = GetComponent<ParticleSystem>().emission;
		}

		public void Emit()
		{
			_emission.rateOverTime = emisson;
			_duration = lifeTime;
		}

		private void Update()
		{
			if (_duration < 0) return;
			_duration -= Time.deltaTime;
			if (_duration <= 0)
			{
				_emission.rateOverTime = 0;
			}
		}
	}
}