using Nama;
using UnityEngine;
using UnityEngine.UI;

namespace Funzilla
{
	public class Gallery : Popup
	{
		public class CakeItem
		{
			public string Name;
			public Texture2D Texture;
		}

		[SerializeField] private OptimizedScrollViewY scollView;
		[SerializeField] private Button backButton;

		public CakeItem[] Items { get; private set; }
		public static Gallery Instance { get; private set; }

		private void Awake()
		{
			Instance = this;
		}

		private void Start()
		{
			var cakeNames = Profile.Instance.GalleryCakes;
			Items = new CakeItem[cakeNames.Count];
			for (var i = 0; i < Items.Length; i++)
			{
				Items[i] = new CakeItem {Name = cakeNames[i]};
			}
			scollView.Init((Items.Length + 1) / 2);
			backButton.onClick.AddListener(Close);
		}

		public override void OnBackButtonPressed()
		{
			Close();
		}

		private void Close()
		{
			SceneManager.Instance.ClosePopup();
		}
	}
}

