using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace Funzilla
{
	public class GalleryItem : OptimizedScrollItem
	{
		[SerializeField] private RawImage leftImage;
		[SerializeField] private RawImage rightImage;
		[SerializeField] private Image leftFrame;
		[SerializeField] private Image rightFrame;

		private void ShowItem(int index, RawImage image, Behaviour frame)
		{
			var valid = index < Gallery.Instance.Items.Length;
			frame.enabled = valid;
			frame.transform.GetChild(0).gameObject.SetActive(valid);
			if (!valid) return;

			var item = Gallery.Instance.Items[index];
			if (item.Texture != null)
			{
				image.texture = item.Texture;
				return;
			}

			StartCoroutine(GetTexture(item, image));
		}

		private static IEnumerator GetTexture(Gallery.CakeItem item, RawImage image)
		{
			using var uwr = UnityWebRequestTexture.GetTexture($"file://{Application.persistentDataPath}/{item.Name}.png");
			yield return uwr.SendWebRequest();

			if (uwr.result != UnityWebRequest.Result.Success)
			{
				Debug.Log(uwr.error);
			}
			else
			{
				// Get downloaded asset bundle
				item.Texture = DownloadHandlerTexture.GetContent(uwr);
				image.texture = item.Texture;
			}
		}

		internal override void OnVisible(int index)
		{
			ShowItem(index * 2 + 0, leftImage, leftFrame);
			ShowItem(index * 2 + 1, rightImage, rightFrame);
		}
	}
}

