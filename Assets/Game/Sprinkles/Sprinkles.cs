
using System.Collections.Generic;
using DG.Tweening;
using UnityEditor;
using UnityEngine;

namespace Nama
{
	public class Sprinkles : MonoBehaviour
	{
		private const float NormalHeight = 1.5f;
		private const float PressedHeight = 1.2f;
		private const float PressDuration = 0.2f;
		private const float ScaleDuration = 0.2f;

		[SerializeField] private bool special;
		public bool Special => special && Config.Instance.BonusAndSpecialOption;

		[SerializeField] private SprinkleObject[] objects;
		public SprinkleObject[] Objects => objects;
		[SerializeField] private AnimationCurve objectSizeX = new AnimationCurve(new Keyframe[] {
				new Keyframe(0, 0.05f, 0, 0, 0, 0),
				new Keyframe(1, 0.20f, 0, 0, 0, 0),
			});
		[SerializeField] private AnimationCurve objectSizeY = new AnimationCurve(new Keyframe[] {
				new Keyframe(0, 0.05f, 0, 0, 0, 0),
				new Keyframe(1, 0.20f, 0, 0, 0, 0),
			});
		[SerializeField] private AnimationCurve objectSizeZ = new AnimationCurve(new Keyframe[] {
				new Keyframe(0, 0.05f, 0, 0, 0, 0),
				new Keyframe(1, 0.20f, 0, 0, 0, 0),
			});
		[SerializeField] private AnimationCurve rotationX = new AnimationCurve(new Keyframe[] {
				new Keyframe(0, 0.0f, 0, 0, 0, 0),
				new Keyframe(1, 360f, 0, 0, 0, 0),
			});
		[SerializeField] private AnimationCurve rotationY = new AnimationCurve(new Keyframe[] {
				new Keyframe(0, 0.0f, 0, 0, 0, 0),
				new Keyframe(1, 360f, 0, 0, 0, 0),
			});
		[SerializeField] private AnimationCurve rotationZ = new AnimationCurve(new Keyframe[] {
				new Keyframe(0, 0.0f, 0, 0, 0, 0),
				new Keyframe(1, 360f, 0, 0, 0, 0),
			});
		[SerializeField] private Gradient color;
		[Range(0.01f, 1.0f)]
		[SerializeField] private float spacing = 0.4f;
		[Range(0.0f, 2.0f)]
		[SerializeField] private float radius = 0.4f;
		[SerializeField] private int count = 10;

		private enum SprinklePreviewType { ForeverAlone, Star, Sand, Zigzag, AloneFruit, CoupleFruit, Cherry }
		[SerializeField] private SprinklePreviewType sprinklePreviewType = 0;

		public GameObject Character { get; private set; }

		private GameObject _group;
		private GameObject _vertical;
		private Tweener _scaleTween;
		private Tweener _verticalTween;
		private Sequence _idleAnim;
		private float _height = 1.4f;

		private CakeSurface _surface;
		private Polyline _poly;
		private PolylinePosition _lastStrewPos;
		private int _fillStep;
		public int Amount { get; private set; }
		public float Spacing => spacing;
		public int DropCount => count;

		public float DropRadius => radius;
		
		public bool Falling { get; private set; }

		public enum AnimationType { None, Simulate, Physics }

		private AnimationType _animationType = AnimationType.Simulate;

		public IEnumerable<SprinkleObject> SprinkleObjects => _group.GetComponentsInChildren<SprinkleObject>();
		internal bool PositiveVfxActivated;

		private void Awake()
		{
			Init();
		}

		public void Init()
		{
			if (Character != null)
			{
				return;
			}
			foreach (var obj in objects)
			{
				obj.Init();
			}
			Character = new GameObject("Sprinkles Character");
			Character.SetActive(false);
			Character.transform.localPosition = Vector3.zero;
			Character.transform.SetParent(transform.parent, false);

			_vertical = new GameObject("vertical controller");
			_vertical.transform.SetParent(Character.transform);
			_vertical.transform.localRotation = Quaternion.identity;
			_vertical.transform.localScale = Vector3.zero;

			Release(false);
		}
		
		public void Clear()
		{
			Amount = 0;
			if (_group == null) return;
			Character.transform.SetParent(transform, false);
			Destroy(_group);
			_group = null;
		}

		private void CreateSprinkleInBottle(float scale, Bottle bottle)
		{
			var prefab = objects[Random.Range(0, objects.Length)];
			if (prefab == null)
			{
				return;
			}

			var obj = Instantiate(prefab.ObjInBottle, bottle.SpawnTrans);

			obj.Rigidbody.isKinematic = true;
			foreach (var c in obj.GetComponents<Collider>())
			{
				c.enabled = false;
			}
			obj.gameObject.SetActive(true);

			var x = Utils.RandomCurveValue(objectSizeX);
			var y = Utils.RandomCurveValue(objectSizeY);
			var z = Utils.RandomCurveValue(objectSizeZ);

			var max = Mathf.Max(x, y, z);

			var dimensions = obj.MeshFilter.sharedMesh.bounds.size;
			var maxSize = Mathf.Max(dimensions.x, dimensions.y, dimensions.z);

			var space = maxSize * max / 2;

			var t = obj.transform;
			t.localScale = new Vector3(x * scale, y * scale, z * scale);
			var getX = Random.Range(-bottle.RangeX + space, bottle.RangeX - space);
			var getY = Random.Range(-bottle.RangeY + space, bottle.RangeY - space);
			var getZ = Random.Range(-bottle.RangeZ + space, bottle.RangeZ - space);
			t.localPosition = new Vector3(getX, getY, getZ);
			t.localEulerAngles = new Vector3(
				Random.Range(-180, 180),
				Random.Range(-180, 180),
				Random.Range(-180, 180));
			obj.MeshRenderer.material.color = this.color.Evaluate(Random.Range(0.0f, 1.0f));
		}

		/// <summary>
		/// Called when the step has started
		/// </summary>
		public void OnStepBegan(CakeSurface surface, Transform displayTransform)
		{
			_surface = surface;
			_group = new GameObject("sprinkles");
			_group.transform.SetParent(displayTransform, false);
		}

		/// <summary>
		/// Called when the step has finished
		/// </summary>
		public void OnStepEnded()
		{
			_group = null;
		}

		/// <summary>
		/// Called when 
		/// </summary>
		/// <param name="surfaceRadius"></param>
		/// <param name="poly"></param>
		/// <param name="start"></param>
		public void Begin(
			Polyline poly,
			PolylinePosition start,
			AnimationType animationType,
			bool strew = false)
		{
			_poly = poly;
			_animationType = (_surface == null || _surface.Radius == 0) ?
				animationType : AnimationType.Simulate;
			_lastStrewPos = start;
			_fillStep = (int)(start.Offset / spacing);
			if (strew)
			{
				Strew();
			}
		}

		public void MoveTo(PolylinePosition target)
		{
#if UNITY_EDITOR
			_refreshCount++;
#endif
			var n = (int)(target.Offset / spacing);
			for (var i = _fillStep; i < n; i++)
			{
				Advance(spacing);
				Strew();
			}
			_fillStep = n;
		}

		private void MoveVertical(float height, bool animate)
		{
			if (_verticalTween != null)
			{
				_verticalTween.Kill();
			}
			if (animate)
			{
				_verticalTween = _vertical.transform
					.DOLocalMoveZ(-height, PressDuration)
					.OnComplete(() => _verticalTween = null);
			}
			else
			{
				_vertical.transform.localPosition = new Vector3(0, 0, -height);
			}
		}

		private Tweener Shake()
		{
			return Character.transform.GetChild(0).DOShakeRotation(
				0.2f, new Vector3(20, 20, 20), 10, 45, false);
		}

		private void StartIdle()
		{
			StopIdle();
			_idleAnim = DOTween.Sequence();
			_idleAnim.Append(Shake().SetDelay(2));
			_idleAnim.Append(Shake().SetDelay(5).SetLoops(-1));
			_idleAnim.Play();
		}

		public void StopIdle()
		{
			if (_idleAnim == null) return;
			_idleAnim.Kill();
			_idleAnim = null;
			Character.transform.DOLocalRotate(Vector3.zero, 0.1f);
		}

		public void Press(bool animate = true, bool checkTutorial = true)
		{
			if (Gameplay.Instance != null && checkTutorial && Gameplay.Instance.Tutorial != null)
			{
				Gameplay.Instance.Tutorial.ControlAnim.enabled = false;
				Gameplay.Instance.Tutorial.Hide();
			}
			if(Gameplay.Instance != null && Gameplay.Instance.Pointer != null)
				ShowPointer(Gameplay.Instance.Pointer);
			MoveVertical(_height, animate);
		}

		public void Release(bool animate = true)
		{
			MoveVertical(NormalHeight, animate);
			StartIdle();
		}

		public Vector3 CharacterWorldPosition => Character.transform.position;

		public GameObject Preview { get; private set; }

		public void ShowCharacter(Vector2 position, float height = 1.2f)
		{
			if (Character == null)
			{
				Analytics.Instance.LogEvent($"null_character_{Profile.Instance.Level}_{Profile.Instance.CharacterSkinName}");
				return;
			}
			_height = height;
			var x = Mathf.Clamp(position.x, -Statistics.PaintRangeX, Statistics.PaintRangeX);
			var y = Mathf.Clamp(position.y, -Statistics.PaintRangeZ, Statistics.PaintRangeZ);
			position.x = x;
			position.y = y;
			UpdateCharacterPosition(position);

			Character.SetActive(true);
			Character.transform.SetParent(_group.transform, false);
			Character.transform.localRotation = Quaternion.identity;

			_scaleTween?.Kill();

			var duration = ScaleDuration * (1 - Character.transform.localScale.x);
			_scaleTween = _vertical.transform.DOScale(1, duration)
				.SetEase(Ease.InOutCubic)
				.OnComplete(() => _scaleTween = null);
		}

		public void ShowPointer(Pointer pointer)
		{
			if(Character == null)
			{
				return;
			}

			var t = pointer.transform;
			t.SetParent(Character.transform);
			t.localScale = Vector3.one;
			t.localPosition = new Vector3(0, 0, -0.15f);
			pointer.Appear();
		}

		public void ShowBottle(Bottle bottle, int n = 0)
		{
			for (var i = 0; i < _vertical.transform.childCount; i++)
			{
				_vertical.transform.GetChild(i).SetParent(bottle.SpawnTrans);
			}
			var t = bottle.transform;
			t.SetParent(_vertical.transform);
			t.localPosition = bottle.StartPosition;
			t.localScale = bottle.Scale;
			t.localEulerAngles = bottle.StartRotation;

			for (var i = 0; i < bottle.SpawnTrans.childCount; i++)
			{
				Destroy(bottle.SpawnTrans.GetChild(i).gameObject);
			}

			var unit = Instantiate(objects[0].ObjInBottle, transform);

			var sizeX = unit.GetComponent<MeshFilter>().mesh.bounds.size.x;
			var sizeY = unit.GetComponent<MeshFilter>().mesh.bounds.size.y;
			var sizeZ = unit.GetComponent<MeshFilter>().mesh.bounds.size.z;

			var scaleX = Utils.RandomCurveValue(objectSizeX);
			var scaleY = Utils.RandomCurveValue(objectSizeY);
			var scaleZ = Utils.RandomCurveValue(objectSizeZ);

			unit.gameObject.SetActive(false);

			var max = Mathf.Max(sizeX * scaleX, sizeY * scaleY, sizeZ * scaleZ);
			var b = bottle.MeshFilter.mesh.bounds;
			var x = b.size.x;
			var y = b.size.y;
			var z = b.size.z;

			var minSize = Mathf.Min(x, y, z);
			var maxSize = Mathf.Max(x, y, z);

			float scale;
			int number;

			if (minSize / max < 2)
			{
				number = 6;
				scale = minSize / (max * 2);
			}
			else if (maxSize / max > 4)
			{
				number = 64;
				scale = 1;
			}
			else
			{
				var numberX = (int)(x / max);
				var numberY = (int)(x / max);
				var numberZ = (int)(x / max);
				number = numberX * numberY * numberZ;
				scale = 1;
			}
			
			for (var i = 0; i < number; i++)
			{
				CreateSprinkleInBottle(scale, bottle);
			}
			bottle.Appear();
		}

		public void HideCharacter()
		{
			Release(false);
			_scaleTween?.Kill();

			var duration = ScaleDuration * Character.transform.localScale.x;
			_scaleTween = _vertical.transform.DOScale(0, duration)
				.SetEase(Ease.InOutCubic)
				.OnComplete(() =>
				{
					Character.SetActive(false);
					_scaleTween = null;
				});
		}

		public void UpdateCharacterPosition(Vector2 position)
		{
			float radius = 1;

			if(Character == null)
			{
				return;
			}
			if(_surface != null)
			{
				radius = _surface.Radius;
			}

			var x = Mathf.Clamp(position.x, -Statistics.PaintRangeX, Statistics.PaintRangeX);
			var y = Mathf.Clamp(position.y, -Statistics.PaintRangeZ, Statistics.PaintRangeZ);
			Vector3 pos;
			if (radius > 0)
			{
				var a = x / radius * 0.5f;
				var s = Mathf.Sin(a);
				var c = Mathf.Cos(a);
				Character.transform.localEulerAngles = new Vector3(0, Mathf.Rad2Deg * a, 0);
				pos = new Vector3(x * c, y, x * s);
			}
			else
			{
				pos = new Vector3(x, y, 0);
			}

			Character.transform.localPosition = pos;
		}

		private void Advance(float delta)
		{
			if(_poly == null) return;
			if(_poly.Points.Count <  2)  return;
			_lastStrewPos.Offset += delta;
			var remain = delta;
			var points = _poly.Points;
			var nSegs = _poly.Points.Count - 1;

			for (; _lastStrewPos.Seg < nSegs && remain > 0;)
			{
				var segDirection = points[_lastStrewPos.Seg + 1] - points[_lastStrewPos.Seg];
				var segLength = segDirection.magnitude;
				segDirection /= segLength;

				var segRemainLength = segLength - _lastStrewPos.SegOffset;
				if (remain > segRemainLength)
				{
					remain -= segRemainLength;
					_lastStrewPos.SegOffset = 0;
					_lastStrewPos.Seg++;
					if (_lastStrewPos.Seg >= nSegs)
					{
						break;
					}
				}
				else
				{
					_lastStrewPos.SegOffset += remain;
					_lastStrewPos.Pos = points[_lastStrewPos.Seg] + segDirection * _lastStrewPos.SegOffset;
					return;
				}
			} // for
			_lastStrewPos.Pos = points[Mathf.Clamp(_lastStrewPos.Seg,0,nSegs -1)];
		}

		private void GenerateObject()
		{
			var prefab = objects[Random.Range(0, objects.Length)];
			if (prefab == null || _group == null)
			{
				return;
			}
			var obj = Instantiate(prefab, _group.transform);

			obj.gameObject.SetActive(true);
			obj.transform.localScale = new Vector3(
				Utils.RandomCurveValue(objectSizeX),
				Utils.RandomCurveValue(objectSizeY),
				Utils.RandomCurveValue(objectSizeZ));

			obj.RefreshRadius(prefab);
			
			Vector3 p = Random.insideUnitCircle * radius * 0.5f + _lastStrewPos.Pos;

			if (_surface.Radius > 0)
			{
				obj.AnimationType = AnimationType.None;
				var a = p.x / _surface.Radius * 0.5f;
				var s = Mathf.Sin(a);
				var c = Mathf.Cos(a);
				obj.transform.localPosition = new Vector3(p.x * c, p.y, p.x * s);
				obj.Rigidbody.isKinematic = true;
			}
			else
			{
				obj.AnimationType = _animationType;
				switch (_animationType)
				{
					case AnimationType.None:
						obj.Rigidbody.isKinematic = true;
						obj.transform.localPosition = p;
						if (gameObject.scene.GetPhysicsScene().Raycast(
							obj.transform.position + Vector3.up * 5, 
							Vector3.down, out var hit, 
							20, 
							Layers.CakeSurfaceMask | Layers.BackgroundMask))
						{
							p = hit.point;
							obj.transform.position = p;
							p = obj.transform.localPosition;
						}
						p.z -= obj.Radius;
						break;
					case AnimationType.Simulate:
						p.z = -PressedHeight;
						obj.gameObject.layer = Layers.NoCollideSprinkle;
						break;
					case AnimationType.Physics:
						obj.gameObject.layer = Layers.Gameplay;
						p.z = -PressedHeight;
						break;
				}
				obj.transform.localPosition = p;
			}

			obj.transform.localEulerAngles = new Vector3(
				Utils.RandomCurveValue(rotationX),
				Utils.RandomCurveValue(rotationY),
				Utils.RandomCurveValue(rotationZ));
			obj.Init(prefab);
			var color = this.color.Evaluate(Random.Range(0.0f, 1.0f));

#if UNITY_EDITOR
			if (!EditorApplication.isPlayingOrWillChangePlaymode)
			{
				var material = new Material(prefab.MeshRenderer.sharedMaterial)
				{
					color = color
				};
				obj.MeshRenderer.sharedMaterial = material;
				return;
			}
#endif
			obj.MeshRenderer.material.color = color;
		}

		public SprinkleObject NewObject()
		{
			if (objects == null) return null;
			if (objects.Length < 1) return null;
			var prefab = objects[Random.Range(0, objects.Length)];
			if (prefab == null)	
			{
				return null;
			}
			if (_group == null) return null;
			
			var obj = Instantiate(prefab, _group.transform);
			obj.gameObject.layer = Layers.Shop;
			obj.gameObject.SetActive(true);
			obj.transform.localScale = new Vector3(
				Utils.RandomCurveValue(objectSizeX),
				Utils.RandomCurveValue(objectSizeY),
				Utils.RandomCurveValue(objectSizeZ));
			obj.transform.localEulerAngles = new Vector3(
				Utils.RandomCurveValue(rotationX),
				Utils.RandomCurveValue(rotationY),
				Utils.RandomCurveValue(rotationZ));

			var color = this.color.Evaluate(Random.Range(0.0f, 1.0f));

#if UNITY_EDITOR
			if (!EditorApplication.isPlayingOrWillChangePlaymode)
			{
				var material = new Material(prefab.MeshRenderer.sharedMaterial)
				{
					color = color
				};
				obj.MeshRenderer.sharedMaterial = material;
			}
			else
			{
				obj.MeshRenderer.material.color = color;
			}
#else
			obj.MeshRenderer.material.color = color;
#endif
			obj.Init(prefab);
			return obj;
		}

		private Tween _fallingTween;

		private void Strew()
		{
			Amount += count;
			for (var i = 0; i < count; i++)
			{
				GenerateObject();
			} // for

			Falling = true;
			if (_fallingTween != null)
			{
				_fallingTween.Kill();
				_fallingTween = null;
			}
			DOVirtual.DelayedCall(1, () =>
			{
				_fallingTween = null;
				Falling = false;
			});
		}

		private Bounds _bounds;
		public void MakePreview(Camera orthographicCamera, float camerasize = 2f)
		{
			Preview = AssetManager.Instance.GetSprinklesPreviewPreset((int)sprinklePreviewType);
			var previewScale = Preview.transform.localScale;
			var previewOffset = Preview.transform.localPosition;
			Preview.transform.localScale = Vector3.one;

			var transforms = Preview.GetComponentsInChildren<Transform>();

			if (transforms.Length > 1)
			{
				for (var i = 1; i < transforms.Length; i++)
				{
					var prefab = objects[Random.Range(0, objects.Length)];
					if (prefab == null)
					{
						continue;
					}
					var obj = Instantiate(prefab, transforms[i], false);
					obj.Rigidbody.isKinematic = true;
					obj.gameObject.SetActive(true);
					obj.transform.localScale = new Vector3(
						Utils.RandomCurveValue(objectSizeX),
						Utils.RandomCurveValue(objectSizeY),
						Utils.RandomCurveValue(objectSizeZ));
					obj.transform.localEulerAngles = new Vector3(
						Utils.RandomCurveValue(rotationX) + transforms[i].localEulerAngles.x,
						Utils.RandomCurveValue(rotationY) + transforms[i].localEulerAngles.y,
						Utils.RandomCurveValue(rotationZ) + transforms[i].localEulerAngles.z);
					obj.MeshRenderer.material.color = color.Evaluate((float)i / transforms.Length);
					obj.transform.localPosition = Vector3.zero;
					obj.Inside1Unit();
				}
			}

			Preview.transform.SetParent(orthographicCamera.transform);
			Preview.transform.localPosition = Vector3.zero + Vector3.forward * 2;
			Preview.SetLayerRecursively(Layers.SpinklesPreview);

			// Fit camera
			_bounds = new Bounds(Preview.transform.position, Vector3.one);
			foreach (var r in Preview.GetComponentsInChildren<Renderer>())
			{
				_bounds.Encapsulate(r.bounds);
			}

			var max = Mathf.Max(_bounds.size.x, _bounds.size.y);

			orthographicCamera.orthographicSize = (max / orthographicCamera.aspect) / camerasize;
			var localPosition = Preview.transform.localPosition;
			localPosition -= _bounds.center;
			localPosition += Vector3.forward * 2;
			Preview.transform.localScale = previewScale;
			localPosition += previewOffset;
			Preview.transform.localPosition = localPosition;
		}


#if UNITY_EDITOR
		private int _refreshCount = 0;

		public Sprinkles()
		{
			_group = null;
		}

		private void OnValidate()
		{
			if (EditorApplication.isPlayingOrWillChangePlaymode ||
				_refreshCount <= 0)
			{
				return;
			}

			EditorApplication.delayCall += () =>
			{
				var step = GetComponentInParent<DecorationStep>();
				if (step != null)
				{
					step.RefreshPreview();
				}
			};
		}
#endif
	}
}
