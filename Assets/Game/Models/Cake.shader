Shader "Custom/Cake" {
	Properties {
		_CakeTex ("Base (RGB)", 2D) = "white" {}
		_CakeColor("Main Color", COLOR) = (1,1,1,1)
		_ToppingTex ("Overlay Texture Color (RGB) Alpha (A)", 2D) = "black" {}
		_SurfaceTopText("Surface Top Texture Color (RGB) Alpha (A)", 2D) = "black" {}
		_ToppingColor("Topping Color", COLOR) = (1,1,1,1)
	}

	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 150

		CGPROGRAM
		#pragma surface surf Lambert noforwardadd

		sampler2D _CakeTex;
		float4 _CakeColor;

		sampler2D _ToppingTex;
		sampler2D _SurfaceTopText;
		float4 _ToppingColor;

		fixed4 c1, c2, c3;
		fixed3 result;
		struct Input {
			float2 uv_CakeTex : TEXCOORD0;
			float2 uv_ToppingTex : TEXCOORD0;
			float2 uv_SurfaceTopText : TEXCOORD0;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			c1 = tex2D(_CakeTex, IN.uv_CakeTex );
			c2 = tex2D(_ToppingTex, IN.uv_ToppingTex);
			c3 = tex2D(_SurfaceTopText, IN.uv_SurfaceTopText);
			result = (c1.rgb * _CakeColor.rgb) * (1 - c2.a) + (c2.rgb * _ToppingColor.rgb) * c2.a;
			o.Albedo = result.rgb * (1 - c3.a) + (c3.rgb * _ToppingColor.rgb) * c3.a;
			o.Alpha = 1.0;
		}
		ENDCG
	}

	Fallback "Mobile/VertexLit"
}