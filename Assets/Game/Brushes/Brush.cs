
using Funzilla;
using UnityEditor;
using UnityEngine;

namespace Nama
{
	public class Brush : MonoBehaviour
	{
		// Some global settings for brush

		[SerializeField] private BrushTip tip;
		[SerializeField] private AnimationCurve size = new AnimationCurve(new[] {
				new Keyframe(0, 0.1f, 0, 0, 0, 0),
				new Keyframe(1, 0.1f, 0, 0, 0, 0),
			});
		[SerializeField] private float twist;

		public BrushTip Tip => tip;
		public float Twist
		{
			get { return twist; }
#if UNITY_EDITOR
			set
			{
				twist = value;
			}
#endif
		}

		private float _tipSize;

		public void SetTip(BrushTip newTip)
		{
			tip = newTip;
		}

		public void Init()
		{
			_tipSize = -100;
			foreach (var segment in tip.segments)
			{
				foreach (var p in segment.points)
				{
					if (_tipSize < p.y) _tipSize = p.y;
				}
			}
			_tipSize *= 0.5f;
		}

		internal float[] Widths => size.Quantitize();

#if UNITY_EDITOR
		private int _refreshCount;
		private void OnValidate()
		{
			if (EditorApplication.isPlayingOrWillChangePlaymode ||
				_refreshCount <= 0)
			{
				return;
			}

			EditorApplication.delayCall += () =>
			{
				var step = GetComponentInParent<DecorationStep>();
				if (step != null)
				{
					step.RefreshPreview();
				}
			};
		}
#endif
	}
}
