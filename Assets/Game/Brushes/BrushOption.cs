﻿using UnityEngine;
using System.Collections;
using System;

namespace Nama
{
	[Serializable]
	public class BrushSectionSet
	{
		public StrokeSection[] sections;
	}

	/// <summary>
	/// Brush section
	/// </summary>
	[Serializable] public class StrokeSection : PolylineSectionByWeight
	{
		public StrokeSection(int[] colors)
		{
			this.colors = colors;
		}
		public int[] colors = new int[] { 0 };
	}

	[Serializable] public class BrushOptionSet
	{
		[SerializeField] Color[] options = new Color[] { Color.red, Color.yellow, Color.green };
		public Color[] Options => options;
	}

}