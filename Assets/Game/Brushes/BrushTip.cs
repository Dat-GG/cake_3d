
using UnityEngine;

namespace Nama
{
	[System.Serializable]
	public class TipShapeSegment
	{
		public Vector2[] points;
		public Vector2[] normals;
		public float[] uvs;
	}

	public class BrushTip : MonoBehaviour
	{
		public TipShapeSegment[] segments;

#if UNITY_EDITOR
		public void Import(TipShapeSegment[] segments)
		{
			this.segments = segments;
		}
#endif
	}
}
