Shader "Custom/Mixed" {
    Properties{
        _MainTex("Base (RGB)", 2D) = "white" {}
        _Pattern("Pattern", 2D) = "white" {}
    }

    SubShader{
        Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
        LOD 150
        Blend SrcAlpha OneMinusSrcAlpha

        CGPROGRAM
        #pragma surface surf Lambert noforwardadd alpha

        sampler2D _MainTex;
        sampler2D _Pattern;

        struct Input {
            float2 uv_MainTex;
        };


        void surf(Input IN, inout SurfaceOutput o) {
            float4 c1 = tex2D(_MainTex, IN.uv_MainTex);
            float4 c2 = tex2D(_Pattern, IN.uv_MainTex);
            o.Albedo = c1.rgb;
            o.Alpha = c2.a;
        }
        ENDCG
    }

    Fallback "Mobile/VertexLit"
}