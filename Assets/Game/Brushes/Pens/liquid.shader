Shader "Custom/Liquid" {
    Properties{
        _MainTex("Base (RGB)", 2D) = "white" {}
        _Color("Main Color", COLOR) = (1,1,1,1)
    }
        SubShader{
            Tags { "RenderType" = "Opaque" }
            LOD 150

        CGPROGRAM
        #pragma surface surf Lambert noforwardadd

        sampler2D _MainTex;
        float4 _Color;

        struct Input {
            float2 uv_MainTex;
        };



        void surf(Input IN, inout SurfaceOutput o) {
            fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
            o.Albedo = c.rgb * c.a + _Color.rgb * (1.0 - c.a);
            o.Alpha = 1.0;
        }
        ENDCG
    }

        Fallback "Mobile/VertexLit"
}