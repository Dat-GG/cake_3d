﻿using DG.Tweening;
using UnityEngine;

namespace Nama
{
	public class SprinkleObject : MonoBehaviour
	{
		[SerializeField] MeshRenderer meshRenderer;
		[SerializeField] new Rigidbody rigidbody;
		[SerializeField] private SprinkleObject inBottleObj;
		[SerializeField] MeshFilter meshFilter;
		public bool OnSurface { get; set; } = false;
		public SprinkleObject ObjInBottle => inBottleObj;

		public MeshRenderer MeshRenderer => meshRenderer;
		public Rigidbody Rigidbody => rigidbody;
		public MeshFilter MeshFilter => meshFilter;

		float maxSpeed = .5f;
		Vector3 velocity;
		Vector3 outSidePosition;
		bool outOfEdge = false;
		private Quaternion rotation;
		public static bool KinematicEnabled;

		public bool OnTable { get; set; }


		int layerMask = Layers.BackgroundMask | Layers.CakeSurfaceMask;
		Vector3 d;
		public Sprinkles.AnimationType AnimationType { get; set; }

		public float Radius { get; private set; }
		public void RefreshRadius(SprinkleObject original)
		{
			Radius = original.bottom * transform.localScale.magnitude * 0.7f;
		}

		float bottom = 0;

		public void Init()
		{
			bottom = GetComponent<BoxCollider>().size.z / 2;
		}

		public void Init(SprinkleObject original)
		{
			Radius = original.bottom * transform.localScale.magnitude * 0.7f;
			rotation = transform.localRotation;
		}

		private void OnCollisionEnter(Collision collision)
		{
			if (collision.gameObject.layer == Layers.Background)
			{
				OnTable = true;
			}


			if (rigidbody.isKinematic)
			{
				return;
			}

			if (AnimationType == Sprinkles.AnimationType.Simulate)
			{
				if (collision.gameObject.layer != Layers.Gameplay)
				{
					rigidbody.isKinematic = true;
					var p = transform.position;
					p.y = collision.contacts[0].point.y;
					foreach (var contact in collision.contacts)
					{
						if (p.y > contact.point.y)
						{
							p.y = contact.point.y;
						}
					}
					p.y += Radius;
					transform.position = p;
					transform.localRotation = rotation;
					AnimationType = Sprinkles.AnimationType.None;
					if (collision.gameObject.layer == Layers.CakeSurface)
					{
						OnSurface = true;
					}
				}
				return;
			}

			if (Gameplay.Instance != null)
			{
				d = transform.position - Gameplay.Level.transform.position;
			}

			outSidePosition = transform.position + d.normalized * .5f;
			if (collision.gameObject.layer == Layers.CakeSurface)
			{
				OnSurface = true;

				RaycastHit hit;
				// Does the ray intersect any objects excluding the player layer
				if (Physics.Raycast(outSidePosition, Vector3.down, out hit, Mathf.Infinity, layerMask))
				{
					outOfEdge = hit.collider.gameObject.layer == Layers.Background;
				}
				else
				{
					outOfEdge = false;
				}

				if (!outOfEdge)
				{
					rigidbody.isKinematic = KinematicEnabled;
				}
			}
			else
			{
				outOfEdge = true;
			}
		}

		public void Inside1Unit()
		{
			var bounds = meshRenderer.bounds;
			var max = Mathf.Max(bounds.extents.x, bounds.extents.y, bounds.extents.z);
			transform.localScale *= .5f / max;
		}

		private void Update()
		{
			if (rigidbody.isKinematic || AnimationType == Sprinkles.AnimationType.Simulate || outOfEdge)
			{
				return;
			}
			if (Mathf.Abs(rigidbody.velocity.x) > maxSpeed || Mathf.Abs(rigidbody.velocity.z) > maxSpeed)
			{
				velocity = Vector3.ClampMagnitude(rigidbody.velocity, maxSpeed);
			}

			var v = rigidbody.velocity;
			velocity.y = v.y > maxSpeed ? maxSpeed : v.y;

			v = velocity;
			rigidbody.velocity = v;
		}
	}
}