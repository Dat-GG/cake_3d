﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Nama;

public class Key : MonoBehaviour
{
	[SerializeField] Animator animator;
	[SerializeField] GameObject effect;
	bool collected = false;

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.layer != Nama.Layers.Pastry || collected || !Gameplay.Instance.PastryBag.Drawing)
		{
			return;
		}
		collected = true;
		Collect();
	}

	// Update is called once per frame
	void Collect()
	{
		Utils.Instance.VibrateMedium();
		Gameplay.Instance.KeyGroup.gameObject.SetActive(true);
		effect.SetActive(true);
		foreach (var fx in effect.GetComponentsInChildren<ParticleSystem>())
		{
			fx.Play();
		}
		animator.SetTrigger("collect");
		var p = Gameplay.Instance.KeyGroup.KeyTarget;
		p.y -= 1;
		Gameplay.Instance.KeyAmount++;
		transform.DOMove(p, .5f).SetDelay(.5f).OnComplete(delegate
		{
			Gameplay.Instance.KeyGroup.UpdateKey(true);
			animator.enabled = false;
			animator.transform.DOScale(0, .3f);
			Statistics.Instance.KeyCount++;
		});
	}
}
