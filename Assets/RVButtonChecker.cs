﻿using System.Collections;
using System.Collections.Generic;
using Tabtale.TTPlugins;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Funzilla;

public class RVButtonChecker : MonoBehaviour
{
    [SerializeField] UIResult UIResult;
    [SerializeField] UIBakery uiBakery;
    
    
    void OnEnable()
    {
        TTPRewardedAds.ReadyEvent += SetRewardedButtonState;
        SetRewardedButtonState(TTPRewardedAds.IsReady());
    }
    
    void OnDisable()
    {
        TTPRewardedAds.ReadyEvent -= SetRewardedButtonState;
    }
    
    void SetRewardedButtonState(bool isRvReady)
    {
        if (this.gameObject.GetComponent<Button>() == null)
            return;
            
        if (isRvReady)
            this.gameObject.GetComponent<Button>().interactable = true;
        else
        {
            this.gameObject.GetComponent<Button>().interactable = false;
            if (UIResult != null)
            {
                UIResult.buttonClaimRvAnim.enabled = false;
                UIResult.buttonEffect.SetActive(false);
            }

            if (uiBakery != null)
            {
                uiBakery.buttonClaimRvAnim.enabled = false;
                uiBakery.buttonVipEffect.SetActive(false);
                uiBakery.buttonClaimTipAnim.enabled = false;
                uiBakery.buttonTipEffect.SetActive(false);
            }
        }
            
        
    }
    public void ShowTextAdNotAvailable()
    {
        if (this.gameObject.GetComponent<Button>().interactable == false)
        {
            if (UIResult != null)
                UIResult.notAd.gameObject.SetActive(true);
            if (uiBakery != null) 
                uiBakery.notAd.gameObject.SetActive(true);
            DOVirtual.DelayedCall(2f, delegate
            {
                if (UIResult != null)
                    UIResult.notAd.gameObject.SetActive(false);
                if (uiBakery != null)
                    uiBakery.notAd.gameObject.SetActive(false);
            });
        }       
    }
}

