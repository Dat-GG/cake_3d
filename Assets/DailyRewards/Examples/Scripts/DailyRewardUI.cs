﻿/***************************************************************************\
Project:      Daily Rewards
Copyright (c) Niobium Studios.
Author:       Guilherme Nunes Barbosa (gnunesb@gmail.com)
\***************************************************************************/
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

/* 
 * Daily Reward Object UI representation
 */
namespace NiobiumStudios
{
    /** 
     * The UI Representation of a Daily Reward.
     * 
     *  There are 3 states:
     *  
     *  1. Unclaimed and available:
     *  - Shows the Color Claimed
     *  
     *  2. Unclaimed and Unavailable
     *  - Shows the Color Default
     *  
     *  3. Claimed
     *  - Shows the Color Claimed
     *  
     **/
    public class DailyRewardUI : MonoBehaviour
    {
        public bool showRewardName;

        [Header("UI Elements")]
        //public Text textDay;                // Text containing the Day text eg. Day 12
        public Text textReward;             // The Text containing the Reward amount
        public Image imageBgUnclaimedUnavailable; // The Reward Image Background
        public Image imageBgUnclaimedAvailable;
        public Image imageBgClaimed;
        public Image imageDone;
        public Image imageReward;           // The Reward Image
        //public Color colorClaim;            // The Color of the background when claimed
        //private Color colorUnclaimed;       // The Color of the background when not claimed

        [Header("Internal")]
        public int day;

        [HideInInspector]
        public Reward reward;

        public DailyRewardState state;

        // The States a reward can have
        public enum DailyRewardState
        {
            UNCLAIMED_AVAILABLE,
            UNCLAIMED_UNAVAILABLE,
            CLAIMED
        }

        void Awake()
        {
            this.transform.DOLocalMoveZ(0, 0);
            //this.transform.DOScale(0.7f, 0);
            //imageBgUnclaimedUnavailable.transform.DOScaleX(1.4f, 0);
            //imageBgUnclaimedAvailable.transform.DOScaleX(1.4f, 0);
            //imageBgClaimed.transform.DOScaleX(1.4f, 0);
            //colorUnclaimed = imageReward.color;
        }

        public void Initialize()
        {
            //textDay.text = string.Format("Day {0}", day.ToString());
            if (reward.reward > 0)
            {
                if (showRewardName)
                {
                    textReward.text = reward.reward + " " + reward.unit;
                }
                else
                {
                    textReward.text = reward.reward.ToString();
                }
            }
            else
            {
                textReward.text = reward.unit.ToString();
            }
            imageReward.sprite = reward.sprite;
        }

        // Refreshes the UI
        public void Refresh()
        {
            switch (state)
            {
                case DailyRewardState.UNCLAIMED_AVAILABLE:
                    imageBgUnclaimedUnavailable.gameObject.SetActive(false);
                    imageBgUnclaimedAvailable.gameObject.SetActive(true);
                    imageBgClaimed.gameObject.SetActive(false);
                    imageDone.gameObject.SetActive(false);
                    //imageRewardBackground.color = colorClaim;
                    break;
                case DailyRewardState.UNCLAIMED_UNAVAILABLE:
                    imageBgUnclaimedUnavailable.gameObject.SetActive(true);
                    imageBgUnclaimedAvailable.gameObject.SetActive(false);
                    imageBgClaimed.gameObject.SetActive(false);
                    imageDone.gameObject.SetActive(false);
                    //imageRewardBackground.color = colorUnclaimed;
                    break;
                case DailyRewardState.CLAIMED:
                    imageBgUnclaimedUnavailable.gameObject.SetActive(false);
                    imageBgUnclaimedAvailable.gameObject.SetActive(false);
                    imageBgClaimed.gameObject.SetActive(true);
                    imageDone.gameObject.SetActive(true);
                    //imageRewardBackground.color = colorClaim;
                    break;
            }
        }
    }
}