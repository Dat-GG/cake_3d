
using DG.Tweening;
using UnityEditor;
using UnityEngine;

namespace Nama
{
	[ExecuteInEditMode]
	public class GradientBackground : MonoBehaviour
	{
		[SerializeField] new Camera camera;
		[SerializeField] Gradient gradient;
		[SerializeField] Shader shader;

		GameObject bgObj;

		Mesh CreateMesh(Color color0, Color color1)
		{
			Mesh _mesh = new Mesh();

			// Define arrays for vertices, normals, uvs, and colors - each vertex need all of this
			Vector3[] _vertices = new Vector3[4];
			Vector3[] _normals = new Vector3[4];
			Vector2[] _uvs = new Vector2[4];
			Color[] _colors = new Color[4];

			_vertices[2] = new Vector3(0.5f, 0.5f, 0);
			_vertices[3] = new Vector3(-0.5f, 00.5f, 0);
			_vertices[0] = new Vector3(-0.5f, -0.5f, 0);
			_vertices[1] = new Vector3(0.5f, -0.5f, 0);
			_normals[0] = _normals[1] = _normals[2] = _normals[3] = -Vector3.forward;
			_uvs[0] = _uvs[1] = _uvs[2] = _uvs[3] = Vector2.zero;
			_colors[0] = _colors[1] = color0;
			_colors[2] = _colors[3] = color1;

			// Send the vertex array to the mesh
			_mesh.vertices = _vertices;

			// Define array for triangles of the mesh, each triangle consists of three vertices and we do two triangles at a time (quad)
			int[] _triangles = new int[6];
			_triangles[0] = 0;
			_triangles[1] = 2;
			_triangles[2] = 1;
			_triangles[3] = 0;
			_triangles[4] = 3;
			_triangles[5] = 2;

			// Set triangles, normals, UVs and colors of the mesh from the arrays
			_mesh.triangles = _triangles;
			_mesh.normals = _normals;
			_mesh.uv = _uvs;
			_mesh.colors = _colors;

			// Return the mesh
			return _mesh;
		}

		/// <summary>
		/// Creates the gradient child object if the component is enabled
		/// </summary>
		void OnEnable()
		{
			color0 = gradient.Evaluate(0);
			color1 = gradient.Evaluate(1);
			CreateOrGetBackgroundObject();
		}

		/// <summary>
		/// Reset is called when a component is added or when reset is pressed for the object in the inspector.    
		/// </summary>
		void Reset()
		{
			// Create the child object
			color0 = gradient.Evaluate(0);
			color1 = gradient.Evaluate(1);
			CreateOrGetBackgroundObject();
		}

		GameObject CreateGradientObject(float distance)
		{
			// Otherwise - create a new gameobject
			var obj = new GameObject();

			// Set the name of the object - this is used when the object is destroyed so the name is important
			obj.name = "GradientBackgroundObject";

			// Set the parent of the transform to the camera transform
			obj.transform.SetParent(transform, false);

			// Reset the local rotation so it's "zero"
			obj.transform.localRotation = Quaternion.identity;

			// Hide the child gameobject in the inspector - we don't need to see it there
			obj.hideFlags = HideFlags.DontSave | HideFlags.HideInHierarchy | HideFlags.HideInInspector;
			obj.layer = Layers.Background;

			// Place the gameobject just behind the near clipping plane or in front of the far clipping plane
			obj.transform.localPosition = new Vector3(0, 0, distance);

			// Add MeshFilter and MeshRenderer components to the child gameobject
			obj.AddComponent<MeshFilter>();
			obj.AddComponent<MeshRenderer>();

			// Create a MeshRenderer for the childObject
			CreateMeshRenderer(obj.transform);

			// Set the Material to the shader with ZWrite off and RenderQueue set to Gemetry-1000 to ensure it's always rendered as a background
			Material _material = new Material(shader);
			obj.GetComponent<MeshRenderer>().sharedMaterial = _material;
			return obj;
		}

		Color color0, color1;

		void CreateOrGetBackgroundObject()
		{
			// If there is a child gameobject named CameraFixedGradientSky...
			if (bgObj == null)
			{
				bgObj = CreateGradientObject(100);
			}

			// Create the gradient mesh and sett it to the sharedMesh for the MeshFilter
			bgObj.GetComponent<MeshFilter>().sharedMesh = CreateMesh(color0, color1);

			// Set the mesh local scale to ensure it fills the camera based on its position and camera field of view and size
			SetMeshLocalScale();
		}

		protected virtual void CreateMeshRenderer(Transform _transform = null)
		{
			if (_transform == null)
				_transform = transform;
			MeshRenderer _meshRenderer = _transform.GetComponent<MeshRenderer>();
#if UNITY_2017_2_OR_NEWER
			_meshRenderer.allowOcclusionWhenDynamic = false;
#endif
			_meshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
			_meshRenderer.receiveShadows = false;
			_meshRenderer.lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off;
			_meshRenderer.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;
		}

		public void UpdateGradient()
		{
			CreateOrGetBackgroundObject();
		}

		void SetMeshLocalScale()
		{
			// ResetAspect needs to be called to ensure that aspect ratio is updated when changing between aspect ratios and between edit/play
			camera.ResetAspect();

			// Calculate the furstrum height based on position and field of view
			float size = camera.orthographicSize * 2;

			// Set the local scale width/height
			bgObj.transform.localScale = new Vector3(size * camera.aspect, size, 0);
		}

		public void SetGradient(Gradient gradient)
		{
			this.gradient = gradient;
			color0 = gradient.Evaluate(0);
			color1 = gradient.Evaluate(1);
			UpdateGradient();
		}

#if UNITY_EDITOR
		private void OnValidate()
		{
			if (!EditorApplication.isPlayingOrWillChangePlaymode)
			{
				Color newColor0 = gradient.Evaluate(0);
				Color newColor1 = gradient.Evaluate(1);
				if (newColor0 != color0 || newColor1 != color1)
				{
					SetGradientColor(newColor0, newColor1);
				}
			}
		}
#endif

		public void FadeTo(Gradient gradient)
		{
			Color newColor0 = gradient.Evaluate(0);
			Color newColor1 = gradient.Evaluate(1);
			Color color0 = this.color0;
			Color color1 = this.color1;
			DOVirtual.Float(0, 1, 1f, (t) =>
			{
				SetGradientColor(
					newColor0 * t + color0 * (1 - t),
					newColor1 * t + color1 * (1 - t));
			});
		}

		public void SetGradientColor(Color color0, Color color1)
		{
			this.color0 = color0;
			this.color1 = color1;
			UpdateGradient();
		}
	}
}
