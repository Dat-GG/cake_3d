using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Nama
{
	public class UITuning : MonoBehaviour
	{
		[SerializeField] Toggle toggleHide;
		[SerializeField] Button btnSave;
		[SerializeField] Button btnLoad;
		[SerializeField] GameObject TunningPanel;
		[SerializeField] GameObject prefab;
		[SerializeField] Transform content;

		private Dictionary<string, UISlider> dictUISlider = new Dictionary<string, UISlider>();
		private List<string> listKey = new List<string>();

		private void Awake()
		{
			toggleHide.onValueChanged.AddListener(OnValueChanged);
			btnSave.onClick.AddListener(Save);
			btnLoad.onClick.AddListener(Load);
			toggleHide.isOn = false;

			AddNewTunningOption("MAGNET_RADIUS", Config.MAGNET_RADIUS, 0, 1,
							delegate (float value) { Config.MAGNET_RADIUS = (float)value; });
			AddNewTunningOption("CORRECT_RADIUS", Config.CORRECT_RADIUS, 0, 1,
							delegate (float value) { Config.CORRECT_RADIUS = (float)value; });
			AddNewTunningOption("LOSE_CONTROL_FORCE", Config.LOSE_CONTROL_FORCE, 0, 10,
							delegate (float value) { Config.LOSE_CONTROL_FORCE = (float)value; });
			AddNewTunningOption("MAGNET_FORCE", Config.MAGNET_FORCE, 0, 1,
							delegate (float value) { Config.MAGNET_FORCE = (float)value; });

			//AddNewTunningOption("MAX_DRAW_SPEED_1", Config.MAX_DRAW_SPEED_1, 0, 15,
			//				delegate (float value) { Config.MAX_DRAW_SPEED_1 = (float)value; });

			//AddNewTunningOption("MAX_DRAW_SPEED_2", Config.MAX_DRAW_SPEED_2, 0, 15,
			//				delegate (float value) { Config.MAX_DRAW_SPEED_2 = (float)value; });

			//AddNewTunningOption("DRAW_SPEED_POINT", Config.DRAW_SPEED_POINT, 0, 15,
			//				delegate (float value) { Config.DRAW_SPEED_POINT = (float)value; });

			//AddNewTunningOption("MIN_LENGTH_FACTOR", Config.MIN_LENGTH_FACTOR, 0, 10,
			//				delegate (float value) { Config.MIN_LENGTH_FACTOR = (float)value; });

			//AddNewTunningOption("MAX_LENGTH_FACTOR", Config.MAX_LENGTH_FACTOR, 0, 10,
			//				delegate (float value) { Config.MAX_LENGTH_FACTOR = (float)value; });

			//AddNewTunningOption("DRAW_SPEED_MULTIPLY", Config.DRAW_SPEED_MULTIPLY, 0, 5,
			//				delegate (float value) { Config.DRAW_SPEED_MULTIPLY = (float)value; });

			//AddNewTunningOption("MIN_STROKE_WIDTH", Config.MIN_STROKE_WIDTH, 0, 5,
			//				delegate (float value) { Config.MIN_STROKE_WIDTH = (float)value; });

			//AddNewTunningOption("NEXT_STROKE_SPEED", Config.NEXT_STROKE_SPEED, 0, 5,
			//				delegate (float value) { Config.NEXT_STROKE_SPEED = (float)value; });

			//AddNewTunningOption("LONG_STROKE_SPEED", Config.LONG_STROKE_SPEED, 0, 5,
			//				delegate (float value) { Config.LONG_STROKE_SPEED = (float)value; });

			//AddNewTunningOption("SHORT_STROKE_SPEED", Config.SHORT_STROKE_SPEED, 0, 5,
			//				delegate (float value) { Config.SHORT_STROKE_SPEED = (float)value; });

			//AddNewTunningOption("NEXT_STROKE_SPEED_FACTOR", Config.NEXT_STROKE_SPEED_FACTOR, 0, 5,
			//				delegate (float value) { Config.NEXT_STROKE_SPEED_FACTOR = (float)value; });

			Load();
		}

		private void AddNewTunningOption(string id, float value, float min, float max, System.Action<float> onValueChanged)
		{
			if (dictUISlider.ContainsKey(id))
			{
				Debug.LogErrorFormat("Already added this key: {0}", id);
				return;
			}
			GameObject _obj = Instantiate(prefab) as GameObject;
			_obj.transform.SetParent(content);
			_obj.name = id;
			_obj.transform.localScale = new Vector3(1, 1, 1);

			UISlider _slider = _obj.GetComponent<UISlider>();
			_slider.Init(id, value, min, max, onValueChanged);
			listKey.Add(id);
			dictUISlider.Add(id, _slider);
		}

		private void OnValueChanged(bool isOn)
		{
			TunningPanel.SetActive(isOn);
		}

		private void Save()
		{
			for (int i = 0; i < listKey.Count; i++)
			{
				UISlider _slider = dictUISlider[listKey[i]];
				PlayerPrefs.SetFloat("tunning_" + listKey[i], _slider.value);
				Debug.LogFormat("Save {0} = {1}", listKey[i], _slider.value);
			}
		}

		private void Load()
		{
			for (int i = 0; i < listKey.Count; i++)
			{
				if (PlayerPrefs.HasKey("tunning_" + listKey[i]))
				{
					UISlider _slider = dictUISlider[listKey[i]];
					_slider.SetValue(PlayerPrefs.GetFloat("tunning_" + listKey[i]));
					Debug.LogFormat("Load {0} = {1}", listKey[i], _slider.value);
				}
				else
				{
					Debug.LogFormat("Doesn't has key {0}", listKey[i]);
				}
			}
		}
	}
}

