
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Nama
{
	public class UISlider : MonoBehaviour
	{
		[SerializeField] Text txtName;
		[SerializeField] Slider slider;
		[SerializeField] Text txtNumb;

		private float minValue = 0;
		private float maxValue = 1;

		private Action<float> onValueChanged = null;

		private float _curvalue;
        public float value
        {
            get { return _curvalue; }
        }

		private void Awake()
		{
			slider.onValueChanged.RemoveAllListeners();
			slider.onValueChanged.AddListener(UpdateValue);
		}

		private void UpdateValue(float val)
		{
            _curvalue = val;

            txtNumb.text = _curvalue.ToString("0.##");
			if (onValueChanged != null)
			{
				onValueChanged.Invoke(_curvalue);
			}
		}

		public void Init(string name, float value, float min, float max, Action<float> onValueChanged)
		{
			txtName.text = name;
            minValue = min;
            maxValue = max;
            this.onValueChanged = onValueChanged;
            slider.minValue = min;
            slider.maxValue = max;
			SetValue(value);
		}

		public void SetValue(float val)
        {
            slider.value = val;
		}
	}

}
