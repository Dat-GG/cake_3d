﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Nama
{
	[CustomEditor(typeof(UILocalization)), CanEditMultipleObjects]
	public class UILocalizationEditor : Editor
	{
		List<string> suggest = new List<string>(5);
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			var component = (UILocalization)target;
			if (GUI.changed)
			{
				suggest = Localization.GetSuggest(component.key);
			}
			for (int i = 0; i < suggest.Count; i++)
			{
				if (i <= 5)
				{
					if (GUILayout.Button(suggest[i]))
					{
						component.key = suggest[i];
					}
				}
			}
		}
	}
}