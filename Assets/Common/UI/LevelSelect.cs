﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Nama
{
	public class LevelSelect : MonoBehaviour
	{
		[SerializeField] private Dropdown dropdown;
		// Start is called before the first frame update
		private void Start()
		{
			var optionDatas = new List<Dropdown.OptionData>();
			for (var i = 0; i < AssetManager.LevelList.Count; i++)
			{
				optionDatas.Add(new Dropdown.OptionData(i.ToString()));
			}
			dropdown.AddOptions(optionDatas);
			dropdown.value = Profile.Instance.Level - 1;
			dropdown.onValueChanged.AddListener(OnValueChange);
		}

		private static void OnValueChange(int index)
		{
			if(Profile.Instance.Level == index + 1)
			{
				return;
			}
			Profile.Instance.Level = index + 1;
			SceneManager.Instance.ReloadScene(SceneID.Gameplay);
		}
	}
}