﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Nama {
	public class UIProgress : MonoBehaviour
	{
		[SerializeField] Text leftText;
		[SerializeField] Text rightText;
		[SerializeField] Image fillImage;
		[SerializeField] Image rightImage;
		[SerializeField] RectTransform fillRT;
		[SerializeField] CanvasGroup canvasGroup;

		private void OnEnable()
		{
			leftText.text = Profile.Instance.Level.ToString();
			rightText.text = (Profile.Instance.Level + 1).ToString();
			fillRT.sizeDelta = new Vector2(0, fillRT.sizeDelta.y);
		}

		public void SetProgress(float percent)
		{
			
			if(percent >= .99f)
			{
				rightImage.color = fillImage.color;
				return;
			}
			fillRT.sizeDelta = new Vector2(percent * 280, fillRT.sizeDelta.y);
		}

		public void Hide()
		{
			canvasGroup.DOFade(0, 0.5f).OnComplete(() => gameObject.SetActive(false));
		}
	}
}