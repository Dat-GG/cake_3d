using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Nama
{
	public class UIListWrapper : MonoBehaviour
	{
		int spacing = 0;
		[SerializeField] ScrollRect scroll = null;
		[SerializeField] RectTransform viewport = null;
		[SerializeField] RectTransform content = null;
		[SerializeField] int capacity = 10;
		float itemSize;

		GameObject itemPrefab = null;

		// Characteristics
		bool initialized = false;
		int nItems = 0;
		float viewportHeight;

		// Visible items
		int iFirstVisible = 0;
		int iLastVisible = 0;
		LinkedList<RectTransform> items;
		LinkedListNode<RectTransform> last;
		int maxVisible;

		public int ItemCount { set { nItems = value; UpdateItemCount(); } }

		// Calback
		public delegate void OnItemVisible(GameObject item, int index);
		public OnItemVisible onItemVisible = null;

		// Use this for initialization
		public void Init(int itemCount)
		{
			viewportHeight = viewport.rect.height;
			scroll.onValueChanged.AddListener((position)=> { OnScrolled(); });

			// Max visible items
			items = new LinkedList<RectTransform>();
			GameObject prefab;
			int capacity;
			if (itemPrefab != null)
			{
				prefab = itemPrefab;
				capacity = this.capacity;
			}
			else
			{
				prefab = content.GetChild(0).gameObject;
				items.AddLast((RectTransform)prefab.transform);
				capacity = this.capacity - 1;
			}
			for (int i = 0; i < capacity; i++)
			{
				var item = Instantiate(prefab, Vector3.zero, Quaternion.identity, content);
				item.SetActive(false);
				items.AddLast((RectTransform)item.transform);
			}
			itemSize = content.GetChild(0).GetComponent<RectTransform>().sizeDelta.y + spacing;
			maxVisible = (int)(viewportHeight / itemSize + 1);
			ItemCount = itemCount;
		}

		void OnScrolled()
		{
			if (nItems <= 0)
			{
				return;
			}

			float y = ContentPosition;
			int iMax = nItems - 1;
			int iNewFirst = (int)(y / itemSize);
			int iNewLast = (int)((y + viewportHeight) / itemSize);
			if (iNewFirst == iFirstVisible && iNewLast == iLastVisible)
			{
				return;
			}

			iNewFirst = Mathf.Clamp(iNewFirst, 0, iMax);
			iNewLast = Mathf.Clamp(iNewLast, 0, iMax);
			int iOldFirst = iFirstVisible;
			int iOldLast = iLastVisible;
			iFirstVisible = iNewFirst;
			iLastVisible = iNewLast;

			bool refreshing =
				iNewFirst - iOldFirst >= maxVisible ||
				iOldLast - iNewLast >= maxVisible;

			int k = iOldLast - iOldFirst;

			if (refreshing)
			{
				var iter = items.First;
				for (int i = iOldFirst; i <= iOldLast; i++, iter = iter.Next)
				{
					iter.Value.gameObject.SetActive(false);
				}
			}
			else
			{
				for (int i = iOldFirst; i < iNewFirst && k >= 1; i++, k--)
				{
					var item = items.First;
					items.RemoveFirst();
					items.AddLast(item);
					item.Value.gameObject.SetActive(false);
				}

				for (int i = iNewLast; i < iOldLast && k >= 1; i++, k--)
				{
					last.Value.gameObject.SetActive(false);
					last = last.Previous;
				}
			}

			if (refreshing)
			{
				y = 0;
				last = items.First;
				for (int i = iNewFirst; i <= iNewLast; i++, last = last.Next)
				{
					ShowItem(last.Value, y, i);
					y -= itemSize;
				}
				last = last.Previous;
			}
			else
			{
				y = last.Value.anchoredPosition.y;
				for (int i = iOldLast + 1, j = k + 1; i <= iNewLast; i++, j++)
				{
					last = last.Next;
					y -= itemSize;
					ShowItem(last.Value, y, i);
				}

				y = items.First.Value.anchoredPosition.y;
				for (int i = iOldFirst - 1; i >= iNewFirst; i--)
				{
					y += itemSize;
					var item = items.Last;
					items.RemoveLast();
					items.AddFirst(item);
					ShowItem(item.Value, y, i);
				}
			}
		}

		void ShowItem(RectTransform item, float pos, int index)
		{
			item.anchoredPosition = new Vector2(0, pos);
			onItemVisible(item.gameObject, index);
			item.gameObject.SetActive(true);
		}

		void UpdateItemCount()
		{
			iFirstVisible = iLastVisible = nItems;
			content.anchoredPosition = new Vector2(0,0);
			content.sizeDelta = new Vector2(0, nItems * itemSize - spacing);
			OnScrolled();
		}

		float ContentPosition { get { return content.anchoredPosition.y; } }
	}
}