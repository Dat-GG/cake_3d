﻿using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class CustomToggle : MonoBehaviour
{
    [SerializeField] GameObject on;
    [SerializeField] GameObject off;

	public void Set(bool enabled)
	{
		on.SetActive(enabled);
		off.SetActive(!enabled);
	}
}
