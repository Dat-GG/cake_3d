
using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Nama
{
	class UIGlobal : Scene
	{
		[SerializeField] Text coinText;
		[SerializeField] Text coinTextShadow;
		[SerializeField] RectTransform coinTransform;
		[SerializeField] RectTransform coinAnimTarget;

		public static Vector3 CoinAnimationTarget =>
			instance.coinAnimTarget.TransformPoint(Vector3.zero) /
			instance.transform.transform.lossyScale.x;

		public static Text SoftCurrencyText => instance.coinText;
		
		static bool initialized = false;
		static bool showCoinInfo = false;
		bool coinInfoVisible = false;
		bool animatingCoinInfo = false;

		static UIGlobal instance = null;

		public static void ShowCoinAmount(int n)
		{
			if (instance != null)
			{
				instance.coinText.text = n.ToString();
				instance.coinTextShadow.text = n.ToString();
				Utils.Instance.VibrateFlash();
			}
		}

		private void Awake()
		{
			initialized = true;
			instance = this;
			if (SceneManager.Initialized)
			{
				coinTransform.anchoredPosition = new Vector2(0, 150);
			}
		}

		private void OnEnable()
		{
			var eventManager = EventManager.Instance;
			if (eventManager != null)
			{
				eventManager.Subscribe(EventType.CoinAmountChanged, OnCoinAmountChanged);
				eventManager.Subscribe(EventType.ProfileReloaded, OnProfileReloaded);
			}
			OnCoinAmountChanged(null);
		}

		private void OnDisable()
		{
			var eventManager = EventManager.Instance;
			if (eventManager != null)
			{
				eventManager.Unsubscribe(EventType.CoinAmountChanged, OnCoinAmountChanged);
				eventManager.Unsubscribe(EventType.ProfileReloaded, OnProfileReloaded);
			}
		}

		void OnProfileReloaded(object data)
		{
			OnCoinAmountChanged(null);
		}

		void OnCoinAmountChanged(object data)
		{
			coinText.text = Profile.Instance.CoinAmount.ToString();
			coinTextShadow.text = coinText.text;
		}

		void AnimateCoinInfoIn()
		{
			coinInfoVisible = true;
			animatingCoinInfo = true;
			coinTransform.DOAnchorPos(new Vector2(5, -40), 0.3f).onComplete = () => {
				animatingCoinInfo = false;
			};
		}

		void AnimateCoinInfoOut()
		{
			coinInfoVisible = false;
			animatingCoinInfo = true;
			coinTransform.DOAnchorPos(new Vector2(0, 150), 0.3f).onComplete = () => {
				animatingCoinInfo = false;
			};
		}

		static void CheckInit()
		{
			if (!initialized)
			{
				initialized = true;
				SceneManager.Instance.OpenScene(SceneID.GlobalUI);
			}
		}

		public static void s()
		{
			CheckInit();
			showCoinInfo = true;
		}

		public static void Hide()
		{
			CheckInit();
			showCoinInfo = false;
		}

		public static void ShowCoinInfo()
		{
			CheckInit();
			showCoinInfo = true;
		}

		public static void HideCoinInfo()
		{
			CheckInit();
			showCoinInfo = false;
		}

		public override void AnimateIn()
		{
			StartCoroutine(WaitInAnimation());
		}

		public override void AnimateOut()
		{
			StartCoroutine(WaitOutAnimation());
		}

		IEnumerator WaitInAnimation()
		{
			yield return new WaitForSeconds(0.3f);
			SceneManager.Instance.OnSceneAnimatedIn(this);
		}

		IEnumerator WaitOutAnimation()
		{
			yield return new WaitForSeconds(0.3f);
			SceneManager.Instance.OnSceneAnimatedOut(this);
		}

		private void Update()
		{
			if (!animatingCoinInfo && coinInfoVisible != showCoinInfo)
			{
				if (showCoinInfo)
				{
					AnimateCoinInfoIn();
				}
				else
				{
					AnimateCoinInfoOut();
				}
			}
		}
	}
}
