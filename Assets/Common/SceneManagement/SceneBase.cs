
using UnityEngine;

namespace Nama
{
	public class SceneBase : MonoBehaviour
	{
		// Should this scene be destroyed after it's closed?
		[SerializeField] bool aliveAfterClose = true;
		public bool AliveAfterClose { get { return aliveAfterClose; } }

		public SceneID id { get; set; }

		public virtual void Init(object data)
		{

		}

		public virtual void OnBackButtonPressed()
		{

		}

		public virtual void AnimateIn()
		{
			SceneManager.Instance.OnSceneAnimatedIn(this);
		}

		public virtual void AnimateOut()
		{
			SceneManager.Instance.OnSceneAnimatedOut(this);
		}
	}
}
