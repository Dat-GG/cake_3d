
using UnityEngine;

namespace Nama
{
	public class Popup : SceneBase
	{
		[SerializeField] new PopupAnimation animation = null;

		public override void AnimateIn()
		{
			if (animation == null)
			{
				SceneManager.Instance.OnSceneAnimatedIn(this);
			}
			else
			{
				animation.AnimateIn();
			}
		}

		public override void AnimateOut()
		{
			if (animation == null)
			{
				SceneManager.Instance.OnSceneAnimatedOut(this);
			}
			else
			{
				animation.AnimateOut();
			}
		}
	}
}
