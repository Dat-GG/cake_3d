
using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Nama
{
	internal class LoadingShield : SceneShield
	{
		[SerializeField] private Image icon;

		private Tween _rotateTween;
		private Tween _iconTween;

		private void StopIconTween()
		{
			if (_iconTween != null)
			{
				_iconTween.Kill();
			}
		}

		public new void Hide()
		{
			base.Hide();
			StopIconTween();
			_iconTween = icon.DOFade(0.0f, 0.5f);
			_iconTween.onComplete = ()=> {
				_rotateTween.Pause();
			};
		}

		public void Show(bool loadingAnimationEnabled = true, float opacity = 0.7f, Action onComplete = null)
		{
			Show(opacity, onComplete);
			icon.gameObject.SetActive(loadingAnimationEnabled);
			if (!loadingAnimationEnabled) return;
			_rotateTween.Restart();
			StopIconTween();
			icon.DOFade(1.0f, 0.5f);
		}

		private void Awake()
		{
			_rotateTween = icon.transform.DORotate(new Vector3(0, 0, 360), 1, RotateMode.LocalAxisAdd).SetLoops(-1).SetEase(Ease.Linear);
			_rotateTween.Pause();
		}
	}
}
