using System.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using DG.Tweening;
using UnityEditor;

namespace Nama
{
	public enum SceneOpenType
	{
		Single,
		Additive
	}

	class SceneManager : Singleton<SceneManager>
	{
		[SerializeField] Image loadingScreen = null;
		[SerializeField] SceneShield popupShield = null;
		[SerializeField] LoadingShield loadingShield = null;
		[SerializeField] GameObject bannerShield = null;

		Transform sceneNode = null;
		Transform popupNode = null;

		enum ActionType
		{
			OpenScene,
			CloseScene,
			CloseScenes,
			PreloadScene,
			ReloadScene,
			ReloadScenes,
			OpenPopup,
			ClosePopup,
			ClosePopups,
		}

		class Action
		{
			public Action(ActionType type) { this.type = type; }
			public ActionType type;
		}

		// Scene action that's taking place
		Action action = null;

		class SceneAction : Action
		{
			public SceneAction(ActionType type, SceneID sceneId) : base(type)
			{
				this.sceneId = sceneId;
			}
			public SceneID sceneId;
		}

		class SceneOpenAction : SceneAction
		{
			public SceneOpenAction(SceneID sceneId, SceneOpenType openType, object data)
				: base(ActionType.OpenScene, sceneId)
			{
				this.openType = openType;
				this.data = data;
			}
			public SceneOpenType openType;
			public object data;
		}

		class SceneReloadAction : SceneAction
		{
			public SceneReloadAction(SceneID sceneId, object data) : base(ActionType.ReloadScene, sceneId)
			{
				this.data = data;
			}
			public object data;
			public int index = 0;
		}

		class PopupOpenAction : Action
		{
			public PopupOpenAction(SceneID sceneId, object data) : base(ActionType.OpenPopup)
			{
				this.sceneId = sceneId;
				this.data = data;
			}
			public SceneID sceneId;
			public object data;
		}

		// Node that stores inactive loaded scenes
		Transform poolNode;

		// Currently active scene
		List<Scene> visibleScenes = new List<Scene>(4);

		// Currently active popups
		List<Popup> visiblePopups = new List<Popup>(4);

		// Queued actions
		Queue<Action> actions = new Queue<Action>(4);

		// Loaded scenes
		Dictionary<string, SceneBase> scenes = new Dictionary<string, SceneBase>(10);

		SceneID GetSceneID(string name)
		{
			for (int i = 0; i < (int)SceneID.END; i++)
			{
				var sceneName = SceneNames.ScenesNameArray[i];
				if (sceneName.Equals(name))
				{
					return (SceneID)i;
				}
			}
			return SceneID.END;
		}

		public static bool Initialized { get; private set; } = false;
		private void Awake()
		{
			Initialized = true;
			UnityEngine.SceneManagement.SceneManager.sceneLoaded += Instance.OnSceneLoaded;
			poolNode = new GameObject("pool").transform;
			poolNode.SetParent(transform, false);
			sceneNode = new GameObject("scenes").transform;
			sceneNode.SetParent(transform, false);
			popupNode = new GameObject("popups").transform;
			popupNode.SetParent(transform, false);

#if UNITY_EDITOR
			if (loadingShield == null)
			{
				var prefab = AssetDatabase.LoadAssetAtPath<LoadingShield>(
					"Assets/Common/SceneManagement/LoadingShield.prefab");
				loadingShield = Instantiate(prefab, transform);
				loadingShield.gameObject.SetActive(false);
			}
			if (popupShield == null)
			{
				var prefab = AssetDatabase.LoadAssetAtPath<SceneShield>(
					"Assets/Common/SceneManagement/SceneShield.prefab");
				popupShield = Instantiate(prefab, transform);
			}

			if (bannerShield == null && Config.Instance.BannerEnabled)
			{
				var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(
					"Assets/Common/SceneManagement/BannerShield.prefab");
				bannerShield = Instantiate(prefab, transform);
			}
#endif
			if(Config.Instance.BannerEnabled)
			{
				var rt = bannerShield.transform.GetChild(0).GetComponent<RectTransform>();
				var heightDp = 50;
				rt.sizeDelta = new Vector2(Screen.width, heightDp * (Screen.dpi / 160));
			}
			
			for (int i = 0; i < UnityEngine.SceneManagement.SceneManager.sceneCount; i++)
			{
				var activeScene = UnityEngine.SceneManagement.SceneManager.GetSceneAt(i);
				var scene = GetScene(activeScene);
				if (scene == null) continue;

				scene.id = GetSceneID(activeScene.name);
				scene.name = activeScene.name;
				scenes.Add(scene.name, scene);
				if (scene as Scene)
				{
					visibleScenes.Add(scene as Scene);
					scene.gameObject.transform.SetParent(sceneNode, false);
				}
				else
				{
					visiblePopups.Add(scene as Popup);
					scene.gameObject.transform.SetParent(popupNode, false);
				}
				UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync(activeScene);
				popupShield.transform.SetSiblingIndex(sceneNode.GetSiblingIndex());
			}
		}

		public void OpenScene(SceneID sceneId, object data = null, SceneOpenType openType = SceneOpenType.Additive)
		{
			actions.Enqueue(new SceneOpenAction(sceneId, openType, data));
		}

		public void PreloadScene(SceneID sceneId)
		{
			actions.Enqueue(new SceneAction(ActionType.PreloadScene, sceneId));
		}

		public void CloseScene()
		{
			actions.Enqueue(new SceneAction(ActionType.CloseScene, SceneID.END));
		}

		public void CloseScene(SceneID sceneId)
		{
			actions.Enqueue(new SceneAction(ActionType.CloseScene, sceneId));
		}

		public void CloseScenes()
		{
			actions.Enqueue(new Action(ActionType.CloseScenes));
		}

		public void OpenPopup(SceneID sceneId, object data = null)
		{
			actions.Enqueue(new PopupOpenAction(sceneId, data));
		}

		public void ClosePopup()
		{
			actions.Enqueue(new Action(ActionType.ClosePopup));
		}

		public void ClosePopups()
		{
			actions.Enqueue(new Action(ActionType.ClosePopups));
		}

		public void ReloadScenes()
		{
			actions.Enqueue(new Action(ActionType.ReloadScenes));
		}

		public void ReloadScene(SceneID sceneId, object data = null)
		{
			actions.Enqueue(new SceneReloadAction(sceneId, data));
		}

		public void ShowLoading(bool loadingAnimationEnabled = true, float opacity = 0.7f, System.Action onComplete = null)
		{
			loadingShield.Show(loadingAnimationEnabled, opacity, onComplete);
		}

		public void HideLoading()
		{
			loadingShield.Hide();
		}

		public void ShowPopupShield()
		{
			popupShield.Show();
		}

		public void HidePopupShield()
		{
			popupShield.Hide();
		}

		public void ShowBannerShield()
		{
			bannerShield.gameObject.SetActive(true);
		}

		public void HideBannerShield()
		{
			bannerShield.gameObject.SetActive(false);
		}

		SceneBase GetLoadedScene(SceneID sceneId)
		{
			var sceneName = SceneNames.GetSceneName(sceneId);
			SceneBase scene;
			if (scenes.TryGetValue(sceneName, out scene))
			{
				return scene;
			}
			return null;
		}

		void ReloadScene(SceneID sceneId)
		{
			var sceneName = SceneNames.GetSceneName(sceneId);

			SceneBase scene;
			if (!scenes.TryGetValue(sceneName, out scene) || !scene.gameObject.activeSelf)
			{ // Nothing happen as the scene is not active
				action = null;
				return;
			}

			var sceneReloadAction = (SceneReloadAction)action;
			if (scene.AliveAfterClose)
			{
				scene.Init(sceneReloadAction.data);
			}
			else
			{
				sceneReloadAction.index = scene.transform.GetSiblingIndex();
				visibleScenes.Remove((Scene)scene);
				scenes.Remove(sceneName);
				Destroy(scene.gameObject);
				UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(
					sceneName, UnityEngine.SceneManagement.LoadSceneMode.Additive);
			}
		}

		void LoadScene(SceneID sceneId)
		{
			var sceneName = SceneNames.GetSceneName(sceneId);

			SceneBase scene;
			if (scenes.TryGetValue(sceneName, out scene))
			{
				if (!scene.gameObject.activeSelf)
				{
					scene.gameObject.SetActive(true);
					OnSceneLoaded(scene);
				}
				else
				{
					action = null;
				}
			}
			else
			{
				UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(
					sceneName, UnityEngine.SceneManagement.LoadSceneMode.Additive);
			}
		}

		void OnSceneLoaded(SceneBase scene)
		{
			if (action == null)
			{
				return;
			}

			switch (action.type)
			{
				case ActionType.OpenScene:
					var sceneOpenAction = (SceneOpenAction)action;
					if (sceneOpenAction.openType == SceneOpenType.Single)
					{
						foreach (var s in visibleScenes)
						{
							if (!s.AliveAfterClose)
							{
								scenes.Remove(s.name);
								Destroy(s.gameObject);
							}
							else
							{
								s.transform.SetParent(poolNode, false);
								s.gameObject.SetActive(false);
							}
						}
						visibleScenes.Clear();
					}
					scene.transform.SetParent(sceneNode, false);
					scene.id = sceneOpenAction.sceneId;
					visibleScenes.Add((Scene)scene);
					scene.Init(sceneOpenAction.data);
					scene.AnimateIn();
					break;

				case ActionType.PreloadScene:
					var sceneAction = (SceneAction)action;
					scene.transform.SetParent(poolNode, false);
					scene.gameObject.SetActive(false);
					scene.id = sceneAction.sceneId;
					action = null;
					break;

				case ActionType.ReloadScene:
					var sceneReloadAction = (SceneReloadAction)action;
					scene.transform.SetParent(sceneNode, false);
					scene.id = sceneReloadAction.sceneId;
					visibleScenes.Insert(sceneReloadAction.index, (Scene)scene);
					scene.Init(sceneReloadAction.data);
					scene.AnimateIn();
					break;

				case ActionType.OpenPopup:
					scene.transform.SetParent(popupNode, false);
					var popupOpenAction = (PopupOpenAction)action;
					scene.id = popupOpenAction.sceneId;
					visiblePopups.Add((Popup)scene);
					scene.Init(popupOpenAction.data);
					scene.AnimateIn();
					break;

				default:
					break;
			}
		}

		void OnSceneLoaded(
			UnityEngine.SceneManagement.Scene loadedScene,
			UnityEngine.SceneManagement.LoadSceneMode mode)
		{
			OnSceneLoaded(loadedScene);
		}

		void ConsumeScene(SceneBase scene)
		{
			if (scene.AliveAfterClose)
			{
				scene.gameObject.SetActive(false);
				scene.transform.SetParent(poolNode, false);
			}
			else
			{
				scenes.Remove(scene.name);
				Destroy(scene.gameObject);
			}
		}

		public void OnSceneAnimatedOut(SceneBase scene)
		{
			switch (action.type)
			{
				case ActionType.OpenScene:
					break;
				case ActionType.CloseScene:
					ConsumeScene(scene);
					action = null;
					break;
				case ActionType.OpenPopup:
					{
						scene.gameObject.SetActive(false);
						var sceneId = ((PopupOpenAction)action).sceneId;
						LoadScene(sceneId);
					}
					break;
				case ActionType.ReloadScenes:
					break;
				case ActionType.ClosePopup:
					ConsumeScene(scene);
					if (visiblePopups.Count > 0)
					{ // Let the last popup appear
						var popup = visiblePopups.Last();
						popup.gameObject.SetActive(true);
						popup.AnimateIn();
					}
					else
					{ // No popup active, hide the shield now
						action = null;
					}
					break;
				case ActionType.ClosePopups:
					ConsumeScene(scene);
					foreach (var popup in visiblePopups)
					{
						ConsumeScene(popup);
					}
					visiblePopups.Clear();
					Instance.popupShield.Hide();
					action = null;
					break;
			}
		}

		public void HideSplash()
		{
			if (loadingScreen != null && loadingScreen.transform.parent.gameObject.activeSelf)
			{
				loadingScreen.DOFade(0, 0.2f).onComplete = () =>
				{
					loadingScreen.transform.parent.gameObject.SetActive(false);
				};
			}
		}

		public void OnSceneAnimatedIn(SceneBase scene)
		{
			action = null;
		}

		static SceneBase GetScene(UnityEngine.SceneManagement.Scene loadedScene)
		{
			SceneBase scene = null;
			foreach (var obj in loadedScene.GetRootGameObjects())
			{
				scene = obj.GetComponent<SceneBase>();
				if (scene == null)
				{
					scene = obj.GetComponentInChildren<SceneBase>();
				}
				if (scene != null)
				{
					break;
				}
			}
			return scene;
		}

		void OnSceneLoaded(UnityEngine.SceneManagement.Scene loadedScene)
		{
			SceneBase scene = GetScene(loadedScene);
			foreach (var obj in loadedScene.GetRootGameObjects())
			{
				scene = obj.GetComponent<SceneBase>();
				if (scene == null)
				{
					scene = obj.GetComponentInChildren<SceneBase>();
				}
				if (scene != null)
				{
					break;
				}
			}

			if (scene != null)
			{
				scene.name = loadedScene.name;
				scenes.Add(loadedScene.name, scene);

				OnSceneLoaded(scene);
				UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync(loadedScene);
			}
		}

		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				if (visiblePopups.Count > 0)
				{
					visiblePopups.Last().OnBackButtonPressed();
				}
				else if (visibleScenes.Count > 0)
				{
					visibleScenes.Last().OnBackButtonPressed();
				}
			}
			if (action != null || actions.Count <= 0)
			{
				return;
			}

			action = actions.Dequeue();
			switch (action.type)
			{
				case ActionType.OpenScene:
					{
						var sceneOpenAction = (SceneOpenAction)action;
						var sceneId = sceneOpenAction.sceneId;
						LoadScene(sceneId);
					}
					break;
				case ActionType.PreloadScene:
					{
						var sceneAction = (SceneAction)action;
						var sceneId = sceneAction.sceneId;
						LoadScene(sceneId);
					}
					break;
				case ActionType.CloseScene:
					{
						var sceneAction = (SceneAction)action;
						if (sceneAction.sceneId == SceneID.END)
						{
							if (visibleScenes.Count <= 0)
							{
								return;
							}
							var scene = visibleScenes.Last();
							visibleScenes.RemoveAt(visibleScenes.Count - 1);
							scene.AnimateOut();
						}
						else
						{
							SceneBase sceneBase = GetLoadedScene(sceneAction.sceneId);
							if (sceneBase != null)
							{
								var scene = sceneBase.gameObject.GetComponent<Scene>();
								if (scene != null)
								{
									visibleScenes.Remove(scene);
									scene.AnimateOut();
									return;
								}
							}
							action = null;
						}
					}
					break;
				case ActionType.CloseScenes:
					foreach (var scene in visibleScenes)
					{
						ConsumeScene(scene);
					}
					visibleScenes.Clear();
					action = null;
					break;
				case ActionType.ReloadScene:
					{
						ReloadScene(((SceneReloadAction)action).sceneId);
					}
					break;
				case ActionType.ReloadScenes:
					{
						foreach (var scene in visibleScenes)
						{
							OpenScene(scene.id);
							ConsumeScene(scene);
						}
						visibleScenes.Clear();
						action = null;
					}
					break;
				case ActionType.OpenPopup:
					{
						var sceneId = ((PopupOpenAction)action).sceneId;
						var scene = GetLoadedScene(sceneId);
						if (scene != null && scene.gameObject.activeSelf)
						{ // Should not open a popup already opened
							action = null;
							return;
						}
						if (visiblePopups.Count > 0)
						{ // Hide the current active popup first
							visiblePopups.Last().AnimateOut();
						}
						else
						{
							popupShield.Show();
							LoadScene(sceneId);
						}
					}
					break;
				case ActionType.ClosePopups:
				case ActionType.ClosePopup:
					{
						if (visiblePopups.Count <= 0)
						{
							action = null;
							return;
						}
						var popup = visiblePopups.Last();
						visiblePopups.RemoveAt(visiblePopups.Count - 1);
						popup.AnimateOut();
						if (visiblePopups.Count <= 0)
						{
							popupShield.Hide();
						}
					}
					break;
			}
		}
	}
}
