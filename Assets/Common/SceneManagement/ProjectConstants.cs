// This file is auto-generated. Modifications are not saved.

namespace Nama
{
    public enum SceneID
    {
        GameManager,
        Loading,
        Gameplay,
        Home,
        Rate,
        Exit,
        GlobalUI,
        Shop,
        Settings,
        Result,
        UIBakery,
        GiveCake,
        ChestRoom,
        Gift,
        UnlockItem,
        Gallery,
        END
    }

    public static class Layers
    {
        /// <summary>
        /// Index of layer 'Default'.
        /// </summary>
        public const int Default = 0;
        /// <summary>
        /// Index of layer 'TransparentFX'.
        /// </summary>
        public const int TransparentFX = 1;
        /// <summary>
        /// Index of layer 'Ignore Raycast'.
        /// </summary>
        public const int Ignore_Raycast = 2;
        /// <summary>
        /// Index of layer 'Bakery'.
        /// </summary>
        public const int Bakery = 3;
        /// <summary>
        /// Index of layer 'Water'.
        /// </summary>
        public const int Water = 4;
        /// <summary>
        /// Index of layer 'UI'.
        /// </summary>
        public const int UI = 5;
        /// <summary>
        /// Index of layer 'Gameplay'.
        /// </summary>
        public const int Gameplay = 8;
        /// <summary>
        /// Index of layer 'Effect'.
        /// </summary>
        public const int Effect = 9;
        /// <summary>
        /// Index of layer 'Background'.
        /// </summary>
        public const int Background = 10;
        /// <summary>
        /// Index of layer 'Pastry'.
        /// </summary>
        public const int Pastry = 11;
        /// <summary>
        /// Index of layer 'Shop'.
        /// </summary>
        public const int Shop = 12;
        /// <summary>
        /// Index of layer 'BrushTexture'.
        /// </summary>
        public const int BrushTexture = 13;
        /// <summary>
        /// Index of layer 'SpinklesPreview'.
        /// </summary>
        public const int SpinklesPreview = 14;
        /// <summary>
        /// Index of layer 'CakeSurface'.
        /// </summary>
        public const int CakeSurface = 15;
        /// <summary>
        /// Index of layer 'NoCollideSprinkle'.
        /// </summary>
        public const int NoCollideSprinkle = 16;
        /// <summary>
        /// Index of layer 'Editor'.
        /// </summary>
        public const int Editor = 17;
        /// <summary>
        /// Index of layer 'FillColor'.
        /// </summary>
        public const int FillColor = 18;
        /// <summary>
        /// Index of layer 'Gem'.
        /// </summary>
        public const int Gem = 19;
        /// <summary>
        /// Index of layer 'BrushPreview'.
        /// </summary>
        public const int BrushPreview = 20;
        /// <summary>
        /// Index of layer 'GroundCheck'.
        /// </summary>
        public const int GroundCheck = 21;
        public const int MiniDecorPreview = 22;
        /// <summary>
        /// Bitmask of layer 'Default'.
        /// </summary>
        public const int DefaultMask = 1 << 0;
        /// <summary>
        /// Bitmask of layer 'TransparentFX'.
        /// </summary>
        public const int TransparentFXMask = 1 << 1;
        /// <summary>
        /// Bitmask of layer 'Ignore Raycast'.
        /// </summary>
        public const int Ignore_RaycastMask = 1 << 2;
        /// <summary>
        /// Bitmask of layer 'Bakery'.
        /// </summary>
        public const int BakeryMask = 1 << 3;
        /// <summary>
        /// Bitmask of layer 'Water'.
        /// </summary>
        public const int WaterMask = 1 << 4;
        /// <summary>
        /// Bitmask of layer 'UI'.
        /// </summary>
        public const int UIMask = 1 << 5;
        /// <summary>
        /// Bitmask of layer 'Gameplay'.
        /// </summary>
        public const int GameplayMask = 1 << 8;
        /// <summary>
        /// Bitmask of layer 'Effect'.
        /// </summary>
        public const int EffectMask = 1 << 9;
        /// <summary>
        /// Bitmask of layer 'Background'.
        /// </summary>
        public const int BackgroundMask = 1 << 10;
        /// <summary>
        /// Bitmask of layer 'Pastry'.
        /// </summary>
        public const int PastryMask = 1 << 11;
        /// <summary>
        /// Bitmask of layer 'Shop'.
        /// </summary>
        public const int ShopMask = 1 << 12;
        /// <summary>
        /// Bitmask of layer 'BrushTexture'.
        /// </summary>
        public const int BrushTextureMask = 1 << 13;
        /// <summary>
        /// Bitmask of layer 'SpinklesPreview'.
        /// </summary>
        public const int SpinklesPreviewMask = 1 << 14;
        /// <summary>
        /// Bitmask of layer 'CakeSurface'.
        /// </summary>
        public const int CakeSurfaceMask = 1 << 15;
        /// <summary>
        /// Bitmask of layer 'NoCollideSprinkle'.
        /// </summary>
        public const int NoCollideSprinkleMask = 1 << 16;
        /// <summary>
        /// Bitmask of layer 'Editor'.
        /// </summary>
        public const int EditorMask = 1 << 17;
        /// <summary>
        /// Bitmask of layer 'FillColor'.
        /// </summary>
        public const int FillColorMask = 1 << 18;
        /// <summary>
        /// Bitmask of layer 'Gem'.
        /// </summary>
        public const int GemMask = 1 << 19;
        /// <summary>
        /// Bitmask of layer 'BrushPreview'.
        /// </summary>
        public const int BrushPreviewMask = 1 << 20;
        /// <summary>
        /// Bitmask of layer 'GroundCheck'.
        /// </summary>
        public const int GroundCheckMask = 1 << 21;
        public const int MiniDecorPreviewMask = 1 << 22;
    }

    public static class SceneNames
    {
        public const string INVALID_SCENE = "InvalidScene";
        public static readonly string[] ScenesNameArray = {
            "GameManager",
            "Loading",
            "Gameplay",
            "Home",
            "Rate",
            "Exit",
            "GlobalUI",
            "Shop",
            "Settings",
            "Result",
            "UIBakery",
            "GiveCake",
            "ChestRoom",
            "Gift",
            "UnlockItem",
            "Gallery"
        };
        /// <summary>
        /// Convert from enum to string
        /// </summary>
        public static string GetSceneName(SceneID scene) {
              int index = (int)scene;
              if(index > 0 && index < ScenesNameArray.Length) {
                  return ScenesNameArray[index];
              } else {
                  return INVALID_SCENE;
              }
        }
    }

    public static class ExtentionHelpers {
        /// <summary>
        /// Shortcut to change enum to string
        /// </summary>
        public static string GetName(this SceneID scene) {
              return SceneNames.GetSceneName(scene);
        }
    }
}

