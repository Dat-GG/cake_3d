
using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Nama
{
	public class SceneShield : MonoBehaviour
	{
		[SerializeField] private Image image;
		private Tweener _tween;

		private void Awake()
		{
			image.color = new Color(0, 0, 0, 0);
		}

		public void Hide()
		{
			_tween?.Kill();
			_tween = image.DOFade(0, 0.5f);
			_tween.onComplete = () =>
			{
				_tween = null;
				gameObject.SetActive(false);
			};
		}

		protected void Show(float opacity = 0.7f, Action onComplete = null)
		{
			gameObject.SetActive(true);
			_tween?.Kill();
			_tween = image.DOFade(opacity, 0.5f).OnComplete(() =>
			{
				_tween = null;
				onComplete?.Invoke();
			});
		}

		public void Show()
		{
			gameObject.SetActive(true);
			_tween?.Kill();
			_tween = null;
			var color = image.color;
			color.a = 0.0f;
			image.color = color;
			_tween = image.DOFade(0.7f, 0.5f).OnComplete(() => _tween = null);
		}
	}
}
