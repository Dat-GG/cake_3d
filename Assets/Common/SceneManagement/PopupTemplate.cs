
using UnityEngine;

namespace Nama
{
	class PopupTemplate : Popup
	{
		public void Close()
		{
			SceneManager.Instance.ClosePopup();
		}
	}
}
