﻿using UnityEngine;

public class Rotate : MonoBehaviour
{
    [SerializeField] Vector3 speed;

    void Update()
    {
        transform.localEulerAngles += speed * Time.smoothDeltaTime;
    }
}