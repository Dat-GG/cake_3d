﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nama
{
	public class AutoScreenshot : MonoBehaviour
	{
#if UNITY_EDITOR
		// Start is called before the first frame update
		void Start()
		{
			try
			{
				System.IO.Directory.CreateDirectory("Preview");
			}
			catch { }
			StartCoroutine(AutoScreenShot());
		}

		static bool goNext = true;
		IEnumerator AutoScreenShot()
		{
			yield return 0;
			ScreenCapture.CaptureScreenshot("Preview/Level_" + Profile.Instance.Level.ToString("000") + ".png");
			if (!goNext)
			{
				yield break;
			}
			Profile.Instance.Level++;
			if (Profile.Instance.Level == AssetManager.LevelList.Count)
			{
				goNext = false;
			}
			SceneManager.Instance.ReloadScene(SceneID.Gameplay);
		}
#endif
	}
}