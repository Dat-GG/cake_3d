
using System;
using System.Reflection;
using UnityEngine;

#if UNITY_IOS && !UNITY_EDITOR
using System.Runtime.InteropServices;
#endif

namespace Nama
{
	public class Utils : Singleton<Utils>
	{
#if UNITY_EDITOR
		// Nothing
#elif UNITY_IOS
		[DllImport("__Internal")] static extern void vibrate(int level);
		[DllImport("__Internal")] static extern bool isVibrationCustomizable();
#elif UNITY_ANDROID
		private AndroidJavaObject activityContext;
		private AndroidJavaObject plugin;
#endif

		bool vibrationCustomizable = false;

		enum VibrationLevel
		{
			Flash,
			Light,
			Medium,
			Heavy,
		}

		public static Color ColorFromUint(uint hex)
		{
			var b = (hex >> 0) & 0xff;
			var g = (hex >> 8) & 0xff;
			var r = (hex >> 16) & 0xff;
			return new Color(r / 255.0f, g / 255.0f, b / 255.0f);
		}

		private void Awake()
		{
#if UNITY_EDITOR
			// Nothing
#elif UNITY_ANDROID
			using (AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
			{
				activityContext = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
			}

			using (AndroidJavaClass pluginClass = new AndroidJavaClass("com.dunghn94.utilslibrary.Utils"))
			{
				plugin = pluginClass.CallStatic<AndroidJavaObject>("createInstance", activityContext);
			}

			using (var version = new AndroidJavaClass("android.os.Build$VERSION"))
			{
				vibrationCustomizable = version.GetStatic<int>("SDK_INT") >= 26;
			}
#elif UNITY_IOS
			vibrationCustomizable = isVibrationCustomizable();
#endif
		}

		public void VibrateFlash()
		{
			if (Preference.Instance.VibrationOn && vibrationCustomizable)
			{
				Vibrate(VibrationLevel.Flash);
			}
		}
		
		public void VibrateLight()
		{
			if (Preference.Instance.VibrationOn && vibrationCustomizable)
			{
				Vibrate(VibrationLevel.Light);
			}
		}

		public void VibrateHeavy()
		{
			if (!Preference.Instance.VibrationOn)
			{
				return;
			}
			if (vibrationCustomizable)
			{
				Vibrate(VibrationLevel.Heavy);
			}
			else
			{
				Handheld.Vibrate();
			}
		}

		public void VibrateMedium()
		{
			if (Preference.Instance.VibrationOn && vibrationCustomizable)
			{
				Vibrate(VibrationLevel.Medium);
			}
		}


		void Vibrate(VibrationLevel level)
		{

#if UNITY_EDITOR
#elif UNITY_ANDROID
			int miliseconds = 0, amplitude = 0;
			switch (level)
			{
				case VibrationLevel.Flash:
					miliseconds = 10;
					amplitude = 40;
					break;

				case VibrationLevel.Light:
					miliseconds = 20;
					amplitude = 40;
					break;

				case VibrationLevel.Medium:
					miliseconds = 40;
					amplitude = 100;
					break;

				case VibrationLevel.Heavy:
					miliseconds = 80;
					amplitude = 255;
					break;
			}
			plugin.Call("Vibrate", miliseconds, amplitude);
#elif UNITY_IOS
			vibrate((int)level);
#endif
		}


		public static bool NonUIHold()
		{
#if UNITY_EDITOR
			if (!Input.GetMouseButton(0) || UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
			{
				return false;
			}
#else
			if (Input.touches.Length <= 0 ||
				Input.touches[0].phase == TouchPhase.Canceled ||
				Input.touches[0].phase == TouchPhase.Ended ||
				UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(Input.touches[0].fingerId))
			{
				return false;
			}
#endif
			return true;
		}

		public static bool NonUITapped()
		{
#if UNITY_EDITOR
			if (!Input.GetMouseButtonDown(0) || UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
			{
				return false;
			}
#else
			if (Input.touches.Length <= 0 ||
				Input.touches[0].phase != TouchPhase.Began ||
				UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(Input.touches[0].fingerId))
			{
				return false;
			}
#endif
			return true;
		}

		public static bool TouchReleased()
		{
#if UNITY_EDITOR
			return Input.GetMouseButtonUp(0);
#else
			return
				Input.touches.Length > 0 &&
				(Input.touches[0].phase == TouchPhase.Ended ||
				Input.touches[0].phase == TouchPhase.Canceled);
#endif
		}

		public static Vector2 TouchPosition()
		{
#if UNITY_EDITOR
			return Input.mousePosition;
#else
			return Input.touches[0].position;
#endif
		}

		public static bool IsAppInstalled(string bundleId)
		{
#if UNITY_EDITOR
			return false;
#elif UNITY_ANDROID
			if (bundleId == null || bundleId == "")
			{
				return false;
			}
			bool installed = false;
			AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject curActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaObject packageManager = curActivity.Call<AndroidJavaObject>("getPackageManager");
			AndroidJavaObject launchIntent = null;
			try
			{
				launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", bundleId);
				installed = launchIntent != null;
			}
			catch (System.Exception e)
			{
				installed = false;
			}
			return installed;
#else
			return false;
#endif
		}

		public static float RandomCurveValue(AnimationCurve curve)
		{
			return curve.Evaluate(UnityEngine.Random.Range(
				curve.keys[0].time,
				curve.keys[curve.keys.Length - 1].time));
		}
	}

#if UNITY_EDITOR
	public static class EditorUtils
	{
		public static T GetCopyOf<T>(this Component comp, T other) where T : Component
		{
			Type type = comp.GetType();
			if (type != other.GetType()) return null; // type mis-match
			BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
			PropertyInfo[] pinfos = type.GetProperties(flags);
			foreach (var pinfo in pinfos)
			{
				if (pinfo.CanWrite)
				{
					try
					{
						pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
					}
					catch { } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
				}
			}
			FieldInfo[] finfos = type.GetFields(flags);
			foreach (var finfo in finfos)
			{
				finfo.SetValue(comp, finfo.GetValue(other));
			}
			return comp as T;
		}

		public static T AddComponent<T>(this GameObject go, T toAdd) where T : Component
		{
			return go.AddComponent<T>().GetCopyOf(toAdd) as T;
		}
	}
#endif
}
