using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Build.Reporting;
#endif
using UnityEngine;
using Nama;

public static class NamaUtils
{

	#region static

	public static T GetRandomElement<T>(this IEnumerable<T> list)
	{
		List<T> _list = list.GetRandomElements(1);
		if (_list.IsNullOrEmpty()) return default(T);
		return _list[0];
	}
	public static List<T> GetRandomElements<T>(this IEnumerable<T> list, int elementsCount)
	{
		elementsCount = Mathf.Clamp(elementsCount, 0, list.Count());
		return list.OrderBy(arg => System.Guid.NewGuid()).Take(elementsCount).ToList();
	}

	public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
	{
		if (enumerable == null)
		{
			return true;
		}
		/* If this is a list, use the Count property for efficiency. 
         * The Count property is O(1) while IEnumerable.Count() is O(N). */
		var collection = enumerable as ICollection<T>;
		if (collection != null)
		{
			return collection.Count < 1;
		}
		return !enumerable.Any();
	}

	// texture process
	static string GALLERY_PATH = Application.persistentDataPath + "/";
	public static void CreateTextureToGallery(Texture2D tex, string levelName)
	{
		var path = GALLERY_PATH + levelName + ".png";
		var bytes = tex.EncodeToPNG();
		File.WriteAllBytes(path, bytes);
	}

	public static bool ExistsTexture(string levelName)
	{
		var path = GALLERY_PATH + levelName + ".png";
		return File.Exists(path);
	}

	public static Texture2D BakeTexture(RenderTexture texture, Vector2Int size)
	{
		Texture2D texture2d = new Texture2D(size.x, size.y, TextureFormat.ARGB32, false);
		texture2d.filterMode = FilterMode.Point;
		RenderTexture.active = texture;
		texture2d.ReadPixels(new Rect(0, 0, size.x, size.y), 0, 0);
		texture2d.Apply();
		return texture2d;
	}
	public static Texture2D LoadTexture(string levelName)
	{
		var path = GALLERY_PATH + levelName + ".png";
		var fileData = File.ReadAllBytes(path);
		var tex = new Texture2D(256, 256);
		tex.LoadImage(fileData);
		return tex;
	}

	public static Sprite ConvertTextureToSprite(Texture2D tex)
	{
		return Sprite.Create(tex, new Rect(0, 0, 256, 256), new Vector2(0.5f, 0.5f));
	}

	/// <summary>
	/// Add or edit key frame
	/// </summary>
	/// <param name="curve"></param>
	/// <param name="key"></param>
	/// <returns>
	/// Return 0 on add new key, 1 when edit existed key
	/// </returns>
	public static int AddOrEditKeyFrame(this AnimationCurve curve, Keyframe key)
	{
		if (curve.AddKey(key) == -1)
		{
			for (int i = 0; i < curve.length; i++)
			{
				if (curve.keys[i].time == key.time)
				{
					//curve.keys[i] = key;
					Keyframe[] keys = curve.keys;
					keys[i] = key;
					curve.keys = keys;
					return 1;
				}
			}
		}
		return 0;
	}

	public static bool RandomPercentage(float percent)
	{
		float _rdm = Random.Range(0f, 99.99f);
		return _rdm < percent;
	}
	public static double ToRadians(this double val)
	{
		return (Mathf.PI / 180) * val;
	}
	public static float ToRadians(this float val)
	{
		return (Mathf.PI / 180) * val;
	}

#if UNITY_EDITOR
	[MenuItem("Tools/Clear Data")]
	public static void ClearUserData()
	{
		PlayerPrefs.DeleteAll();
		File.Delete(Application.persistentDataPath + "/save.dat");
	}

	static string[] SCENES = FindEnabledEditorScenes();

	static string APP_NAME = Application.productName;
	static string TARGET_DIR = "Build";

	[MenuItem("Tools/Build Android")]
	static void PerformAndroidBuild()
	{
		UnityEngine.Debug.ClearDeveloperConsole();
		if (Directory.Exists(TARGET_DIR + "/Android/" + APP_NAME))
		{
			System.IO.DirectoryInfo di = new DirectoryInfo(TARGET_DIR + "/Android/" + APP_NAME);

			foreach (FileInfo file in di.GetFiles())
			{
				file.Delete();
			}
			foreach (DirectoryInfo dir in di.GetDirectories())
			{
				dir.Delete(true);
			}
			Directory.Delete(TARGET_DIR + "/Android/" + APP_NAME);
		}
		GenericBuild(SCENES, TARGET_DIR + "/Android/", BuildTarget.Android, BuildOptions.AcceptExternalModificationsToPlayer);
	}

	private static string[] FindEnabledEditorScenes()
	{
		List<string> EditorScenes = new List<string>();
		foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
		{
			if (!scene.enabled)
				continue;
			EditorScenes.Add(scene.path);
		}
		return EditorScenes.ToArray();
	}

	static void GenericBuild(string[] scenes, string target_dir, BuildTarget build_target, BuildOptions build_options)
	{
		EditorUserBuildSettings.SwitchActiveBuildTarget(build_target);
		var res = BuildPipeline.BuildPlayer(scenes, target_dir, build_target, build_options);
		if (res.summary.result == BuildResult.Failed)
		{
			throw new System.Exception("BuildPlayer failure: " + res);
		}

		if (res.summary.result == BuildResult.Succeeded)
		{
			UnityEngine.Debug.LogError("BuildPlayer Succeeded");
			System.Diagnostics.Process process = new System.Diagnostics.Process();
			System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
			startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;
			startInfo.FileName = "CMD.exe";
			startInfo.Arguments = "/K cd Build/Android&build";
			process.StartInfo = startInfo;
			process.Start();
		}
	}

	[MenuItem("Window/Convert level to StrokeManual")]
	static void ConvertLevelToStrokeManual()
	{
		UnityEngine.Debug.ClearDeveloperConsole();

		var levelList = Nama.AssetManager.LoadCsv("LevelsShort");
		foreach(var l in levelList)
		{
			var level = PrefabUtility.LoadPrefabContents($"Assets/Resources/{l}.prefab");
			//var level = Resources.Load<Level>(l);

			if(level == null)
			{
				Debug.LogError($"Missing level {l}");
				return;
			}
			
			var surface = level.GetComponentInChildren<CakeSurface>();
			if(surface == null)
			{
				Debug.LogError($"Missing surface {l}");
				return;
			}
			
			surface.StrokeAutoToStrokeManual();
			PrefabUtility.SaveAsPrefabAssetAndConnect(level, $"Assets/Resources/LevelsShort/{l}.prefab", InteractionMode.UserAction);
			PrefabUtility.UnloadPrefabContents(level);
		}
	}

	[MenuItem("Tools/Valid LevelList")]
	public static void ValidLevelList()
	{
		if (Selection.activeObject == null)
		{
			Debug.LogError("Select level list first!");
			return;
		}
		var path = Selection.activeObject.name;
		var list = AssetManager.LoadCsv(path);
		if (list == null || list.Count <= 0)
		{
			Debug.LogError("Level list nay khong dung, select level list khac di.");
			return;
		}
		Debug.ClearDeveloperConsole();
		var missingCount = 0;
		var validCount = 0;
		foreach (var levelPath in list)
		{
			var levelPrefab = Resources.Load<GameObject>(levelPath);
			if (levelPrefab == null)
			{
				Debug.LogError($"Level: {levelPath} is missing!");
				missingCount++;
			}
			validCount++;

		}

		if (missingCount <= 0)
		{
			Debug.LogError($"Level list \"{path}\" co {validCount} level ngon.");
		}
	}
#endif

	public static void SetLayerRecursively(this GameObject go, int layerNumber)
	{
		foreach (Transform trans in go.GetComponentsInChildren<Transform>(true))
		{
			trans.gameObject.layer = layerNumber;
		}
	}

	#endregion
}
