﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateX : MonoBehaviour
{
    [SerializeField] float speed;
    float angle = 0;

    void Update()
    {
        float dt = Time.smoothDeltaTime;
        angle += speed * dt;
        if (angle > 360)
        {
            angle -= 360;
        }
        transform.localEulerAngles = new Vector3(angle, 0, 0);
    }
}
