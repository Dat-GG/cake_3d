using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using UnityEngine;

namespace Nama
{
	internal class Profile : Singleton<Profile>
	{

		[Serializable]
		private class UserData
		{
			// Soft currency
			public int nCoins;

			// Level
			public int level = 1;
			public int vipLevel = 1;

			// Activated tutorial
			public bool[] tutorialActived = new bool[5];
			public bool fillTutorialActived;
			public bool strokeManualSelectOptionTutorialActived = false;
			public bool doneTutorialActived;

			// First open time
			public string firstTime;

			// first run anim button shop
			public bool firstShowButtonShop = true;

			// Current ball skin
			public string characterSkin = string.Empty;

			// Owned ball skins
			public List<string> characterSkins = new List<string>(10);

			// Key
			public int keyAmount;
			public int playCount;
			public int freestyleCount;
			public int freeDrawCount;

			// gallery own
			public List<string> galleryCakes = new List<string>(10);
			public int themeLevel;
			public string lastTimeClaimed = "";


			// Open gift progress
			public int giftProgress;
			public int giftOpenCount;

			public int buyCharCount;

			public int buyStorageCount;
			public int openChestCount;
			public int nStars;
			public List<string> cakeShapes;

			//-------- Keep save ------------------------
			// Owned Options
			public List<int> sprinkles = new List<int>();
			public List<int> strokes = new List<int>();
			public List<int> fills = new List<int>();

			// Owned pet skins
			public string petSkin = string.Empty;
			public List<string> petSkins = new List<string>(10);
			public int petBuyCount;

			//current storage skin
			public string storageSkin = string.Empty;

			// owned storage skin
			public List<string> storageSkins = new List<string>(10);

			// Owned glaze skins
			public string glazeSkin = string.Empty;
			public List<string> glazeSkins = new List<string>(10);
			public int glazeBuyCount;

			public int freestyleChoiceCakeCount = 0;

			// first run anim button shop
			public bool shopNotified;
			public bool collectionNotified;
			public bool upgradeNotified;
			public bool showTipButton;
			public bool showUpgradeButton;
			public bool showShopButton;
			public bool showGalleryButton;
			public int countShowUpgradeBtn;
			public int interstitialShowCount;
			public int firstTwoTimeGoStore;
			public float rateUnlockedItem;

			//Upgrade_bakery
			public int upgradeItemCount;
			public int upgradeWallCount;
			public int upgradeFloorCount;

			//Tip
			public int tipCount;
			public int tipMoney;

			//Day_in_game
			public int dayCount;

			//DailyReward
			public int availableReward;
			public int lastReward;
			public bool checkClaimDailyReward;
			public string lastRewardTime;
			public string debugTime;

			//DailyQuest
			public int dailyQuestReward;
			public int moneyCurrentSpendOnShop;
			public int moneyCurrentEarn;
			public int upgradeBakeryCurrentTime;
			public int playVipLevelCurrentTime;
			public int useMirrorGlazeCurrentTime;
			public int litTheCandlesCurrentTime;
			public int completeQ1;
			public int completeQ2;
			public int completeQ3;
			public int completeQ4;
			public int completeQ5;
			public int completeQ6;
			public bool dailyQuestNotified;
			public bool showDailyQuestButton;
			public string dailyTaskDate;
			public UserData()
			{
				fillTutorialActived = false;
				doneTutorialActived = false;
				playCount = 0;
				freeDrawCount = 0;
				giftOpenCount = 0;
				buyCharCount = 0;
				openChestCount = 0;
				collectionNotified = false;
				interstitialShowCount = 0;
				tipMoney = 100;
				dailyQuestReward = 50;
				checkClaimDailyReward = false;
			}
		}

		private UserData _data;
		private bool _vip;

		public bool VIP
		{
			get => _vip;
			set
			{
				if (_vip == value)
				{
					return;
				}

				_vip = value;
				EventManager.Instance.Annouce(EventType.VipChanged);
			}
		}

		public bool DailyQuestNotified
		{
			get => _data?.dailyQuestNotified ?? false;
			set
			{
				if (_data == null) return;
				_data.dailyQuestNotified = value;
				RequestSave();
			}
		}

		public bool CheckClaimDailyReward
		{
			get => _data?.checkClaimDailyReward ?? false;
			set
			{
				if (_data == null) return;
				_data.checkClaimDailyReward = value;
				RequestSave();
			}
		}

		public int LastReward
		{
			get => _data?.lastReward ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.lastReward = value;
				RequestSave();
			}
		}

		public int AvailableReward
		{
			get => _data?.availableReward ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.availableReward = value;
				RequestSave();
			}
		}

		public TimeSpan DebugTime
		{
			get
			{
				try
				{
					return _data != null ? TimeSpan.Parse(_data.debugTime) : TimeSpan.MinValue;
				}
				catch
				{
					return TimeSpan.MinValue;
				}
			}
			set { }
		}

		public DateTime LastRewardTime
		{
			get
			{
				try
				{
					return _data != null ? DateTime.Parse(_data.lastRewardTime) : DateTime.Now;
				}
				catch
				{
					return DateTime.Now;
				}
			}
			set
			{
				_data.lastRewardTime = value.ToString(CultureInfo.InvariantCulture);
				RequestSave();
			}
		}

		public DateTime DailyTaskDate
		{
			get
			{
				try
				{
					return _data != null ? DateTime.Parse(_data.dailyTaskDate) : DateTime.Now;
				}
				catch
				{
					return DateTime.Now;
				}
			}
			set
			{
				_data.dailyTaskDate = value.ToString(CultureInfo.InvariantCulture);
				RequestSave();
			}
		}

		public int CompleteQ1
		{
			get => _data?.completeQ1 ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.completeQ1 = value;
				RequestSave();
			}
		}

		public int CompleteQ2
		{
			get => _data?.completeQ2 ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.completeQ2 = value;
				RequestSave();
			}
		}

		public int CompleteQ3
		{
			get => _data?.completeQ3 ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.completeQ3 = value;
				RequestSave();
			}
		}

		public int CompleteQ4
		{
			get => _data?.completeQ4 ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.completeQ4 = value;
				RequestSave();
			}
		}

		public int CompleteQ5
		{
			get => _data?.completeQ5 ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.completeQ5 = value;
				RequestSave();
			}
		}

		public int CompleteQ6
		{
			get => _data?.completeQ6 ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.completeQ6 = value;
				RequestSave();
			}
		}

		public int MoneyCurrentEarn
		{
			get => _data?.moneyCurrentEarn ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.moneyCurrentEarn = value;
				RequestSave();
			}
		}

		public int MoneyCurrentSpendOnShop
		{
			get => _data?.moneyCurrentSpendOnShop ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.moneyCurrentSpendOnShop = value;
				RequestSave();
			}
		}

		public int UpgradeBakeryCurrentTime
		{
			get => _data?.upgradeBakeryCurrentTime ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.upgradeBakeryCurrentTime = value;
				RequestSave();
			}
		}

		public int PlayVipLevelCurrentTime
		{
			get => _data?.playVipLevelCurrentTime ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.playVipLevelCurrentTime = value;
				RequestSave();
			}
		}

		public int UseMirrorGlazeCurrentTime
		{
			get => _data?.useMirrorGlazeCurrentTime ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.useMirrorGlazeCurrentTime = value;
				RequestSave();
			}
		}

		public int LitTheCandlesCurrentTime
		{
			get => _data?.litTheCandlesCurrentTime ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.litTheCandlesCurrentTime = value;
				RequestSave();
			}
		}

		public int DailyQuestReward
		{
			get => _data?.dailyQuestReward ?? 50;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.dailyQuestReward = value;
				RequestSave();
			}
		}

		private void Awake()
		{
			Initialize();
		}

		private void Initialize()
		{
			LoadLocal();
		}

		private const string Passphase = "Nama1234";
		private const string SaveFile = "/save.dat";

		public int CoinAmount
		{
			get => _data?.nCoins ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.nCoins = value;
				if (_data.nCoins < value)
				{
					SoundManager.Instance.PlaySFX("coin_collect");
				}

				EventManager.Instance.Annouce(EventType.CoinAmountChanged);
				RequestSave();
			}
		}

		public bool FillTutorialActived
		{
			get => _data?.fillTutorialActived ?? false;
			set
			{
				if (_data != null) _data.fillTutorialActived = value;
			}
		}

		public bool DoneTutorialActived
		{
			get => _data != null ? _data.doneTutorialActived : false;
			set
			{
				if (_data != null) _data.doneTutorialActived = value;
			}
		}

		public bool StrokeManualSelectOptionTutorialActived
		{
			get => _data?.strokeManualSelectOptionTutorialActived ?? false;
			set
			{
				if (_data != null) _data.strokeManualSelectOptionTutorialActived = value;
			}
		}


		public int InterstitialShowCount
		{
			get => _data?.interstitialShowCount ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.interstitialShowCount = value;
				RequestSave();
			}
		}

		public int KeyAmount
		{
			get => _data != null ? _data.keyAmount : 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.keyAmount = value;
				if (_data.keyAmount < value)
				{
					SoundManager.Instance.PlaySFX("coin_collect");
				}

				RequestSave();
			}
		}

		public int PetBuyCount
		{
			get => _data != null ? _data.petBuyCount : 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.petBuyCount = value;
				RequestSave();
			}
		}

		public int OpenChestCount
		{
			get => _data?.openChestCount ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.openChestCount = value;
				RequestSave();
			}
		}

		public int PastryBagAmount => _data != null ? _data.characterSkins.Count : 1;

		public int SprinkleBottleAmount => _data != null ? _data.storageSkins.Count : 1;

		public int PetAmount => _data != null ? _data.petSkins.Count : 1;

		public int GlazeAmount => _data != null ? _data.glazeSkins.Count : 1;


		public int FirstTwoTimeGoStore
		{
			get => _data?.firstTwoTimeGoStore ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.firstTwoTimeGoStore = value;
				RequestSave();
			}
		}

		public int TipCount
		{
			get => _data?.tipCount ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.tipCount = value;
				RequestSave();
			}
		}

		public int TipMoney
		{
			get => _data?.tipMoney ?? 100;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.tipMoney = value;
				RequestSave();
			}
		}

		public int CountShowUpgradeBtn
		{
			get => _data?.countShowUpgradeBtn ?? 0;

			set
			{
				if (_data == null)
				{
					return;
				}

				_data.countShowUpgradeBtn = value;
				RequestSave();
			}
		}

		public int UpgradeDecoCount
		{
			get => _data?.upgradeItemCount ?? 0;

			set
			{
				if (_data == null)
				{
					return;
				}

				_data.upgradeItemCount = value;
				RequestSave();
			}
		}

		public int UpgradeWallCount
		{
			get => _data?.upgradeWallCount ?? 0;

			set
			{
				if (_data == null)
				{
					return;
				}

				_data.upgradeWallCount = value;
				RequestSave();
			}
		}

		public int UpgradeFloorCount
		{
			get => _data?.upgradeFloorCount ?? 0;

			set
			{
				if (_data == null)
				{
					return;
				}

				_data.upgradeFloorCount = value;
				RequestSave();
			}
		}

		public int DayCount
		{
			get => _data != null ? _data.dayCount : 0;

			set
			{
				if (_data == null)
				{
					return;
				}

				_data.dayCount = value;
				RequestSave();
			}
		}

		public bool ShopNotified
		{
			get => _data?.shopNotified ?? false;
			set
			{
				if (_data == null) return;
				_data.shopNotified = value;
				RequestSave();
			}
		}

		public bool ShowTipButton
		{
			get => _data?.showTipButton ?? false;
			set
			{
				if (_data == null) return;
				_data.showTipButton = value;
				RequestSave();
			}
		}

		public bool ShowShopButton
		{
			get => _data?.showShopButton ?? false;
			set
			{
				if (_data == null) return;
				_data.showShopButton = value;
				RequestSave();
			}
		}

		public bool ShowUpgradeButton
		{
			get => _data?.showUpgradeButton ?? false;
			set
			{
				if (_data == null) return;
				_data.showUpgradeButton = value;
				RequestSave();
			}
		}

		public bool ShowGalleryButton
		{
			get => _data?.showGalleryButton ?? false;
			set
			{
				if (_data == null) return;
				_data.showGalleryButton = value;
				RequestSave();
			}
		}

		public bool ShowDailyQuestButton
		{
			get => _data?.showDailyQuestButton ?? false;
			set
			{
				if (_data == null) return;
				_data.showDailyQuestButton = value;
				RequestSave();
			}
		}

		public bool UpgradeNotified
		{
			get => _data?.upgradeNotified ?? false;
			set
			{
				if (_data == null) return;
				_data.upgradeNotified = value;
				RequestSave();
			}
		}

		public int PetPrice
		{
			get
			{
				var petPrices = Config.Instance.CostData.ToolShop.PetCosts;
				var price = petPrices[Mathf.Min(PetBuyCount, petPrices.Count - 1)];
				return price;
			}
		}

		public int GlazeBuyCount
		{
			get => _data?.glazeBuyCount ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.glazeBuyCount = value;
				RequestSave();
			}
		}

		public int GlazePrice
		{
			get
			{
				var glazePrices = Config.Instance.CostData.ToolShop.GlazeCosts;
				var price = glazePrices[Mathf.Min(GlazeBuyCount, glazePrices.Count - 1)];
				return price;
			}
		}

		public int GiftProgress
		{
			get => _data?.giftProgress ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.giftProgress = value;
				RequestSave();
			}
		}

		public bool PetOwned(string petSkin)
		{
			return _data?.petSkins != null && _data.petSkins.Any(s => s.Contains(petSkin));
		}

		public List<PetData> GetAvailablePets()
		{
			return PetManager.Instance.Pets.Where(skin => !PetOwned(skin.id)).ToList();
		}

		public bool SprinkleBottleOwned(string bottleId)
		{
			return _data != null && _data.storageSkins.Any(s => s.Contains(bottleId));
		}

		public List<StorageSkin> GetAvailableSprinkleBottles()
		{
			var availableSkins = new List<StorageSkin>();

			for (var i = 0; i < 6; i++)
			{
				if (SprinkleBottleOwned(ShopManager.Instance.SprinkleBottles[i].id))
				{
					continue;
				}

				availableSkins.Add(ShopManager.Instance.SprinkleBottles[i]);
			}

			if (availableSkins.Count > 0) return availableSkins;
			for (var i = 6; i < ShopManager.Instance.SprinkleBottles.Count; i++)
			{
				if (SprinkleBottleOwned(ShopManager.Instance.SprinkleBottles[i].id))
				{
					continue;
				}

				availableSkins.Add(ShopManager.Instance.SprinkleBottles[i]);
			}

			return availableSkins;
		}

		public int GiftOpenCount
		{
			get => _data?.giftOpenCount ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.giftOpenCount = value;
				RequestSave();
			}
		}

		public int PlayCount
		{
			get => _data?.playCount ?? 0;
			set
			{
				if (_data == null) return;
				_data.playCount = value;
				RequestSave();
			}
		}

		public int Level
		{
			get => _data?.level ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.level = value;
				RequestSave();
			}
		}

		public int VipLevel
		{
			get => _data?.vipLevel ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.vipLevel = value;
				RequestSave();
			}
		}

		public bool[] TutorialActived
		{
			get => _data != null ? _data.tutorialActived : new bool[5];
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.tutorialActived = value;
				RequestSave();
			}
		}

		public int TotalPlay => _data != null ? _data.playCount + _data.freestyleCount + _data.freeDrawCount : 0;

		public int BuyCharCount
		{
			get => _data?.buyCharCount ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.buyCharCount = value;
				RequestSave();
			}
		}

		public string CharacterSkinName
		{
			get => _data != null ? _data.characterSkin : string.Empty;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.characterSkin = value;
				UpdateCharacterSkin();
				EventManager.Instance.Annouce(EventType.CharacterSkinChanged);
				RequestSave();
			}
		}

		public int SprinkleBottleBuyCount
		{
			get => _data?.buyStorageCount ?? 0;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.buyStorageCount = value;
				RequestSave();
			}
		}

		public int SprinkleBottlePrice
		{
			get
			{
				var bottleCosts = Config.Instance.CostData.ToolShop.BottleCosts;
				var price = bottleCosts[Mathf.Min(SprinkleBottleBuyCount, bottleCosts.Count - 1)];
				return price;
			}
		}

		public bool PastryBagOwned(string skinId)
		{
			return _data != null && _data.characterSkins.Any(s => s.Contains(skinId));
		}

		public int PastryBagPrice
		{
			get
			{
				var pastryBagCosts = Config.Instance.CostData.ToolShop.PastryBagCosts;
				var price = pastryBagCosts[Mathf.Min(BuyCharCount, pastryBagCosts.Count - 1)];
				return price;
			}
		}

		public List<CharacterSkin> GetAvailablePastryBags()
		{
			var availableSkins = new List<CharacterSkin>();

			for (var i = 0; i < 6; i++)
			{
				if (PastryBagOwned(ShopManager.Instance.PastryBags[i].id))
				{
					continue;
				}

				availableSkins.Add(ShopManager.Instance.PastryBags[i]);
			}

			if (availableSkins.Count > 0) return availableSkins;
			for (var i = 6; i < ShopManager.Instance.PastryBags.Count; i++)
			{
				if (PastryBagOwned(ShopManager.Instance.PastryBags[i].id)) continue;
				availableSkins.Add(ShopManager.Instance.PastryBags[i]);
			}

			return availableSkins;
		}

		private List<CharacterSkin> GetAvailableSkins()
		{
			return ShopManager.Instance.PastryBags.Where(skin => !CharacterSkinOwned(skin.id)).ToList();
		}

		public List<GlazeData> GetAvailableGlazes()
		{
			var availableSkins = new List<GlazeData>();

			for (var i = 0; i < 6; i++)
			{
				if (GlazeSkinOwned(GlazeManager.Instance.Glazes[i].id))
				{
					continue;
				}

				availableSkins.Add(GlazeManager.Instance.Glazes[i]);
			}

			if (availableSkins.Count > 0) return availableSkins;
			for (var i = 6; i < GlazeManager.Instance.Glazes.Count; i++)
			{
				if (GlazeSkinOwned(GlazeManager.Instance.Glazes[i].id))
				{
					continue;
				}

				availableSkins.Add(GlazeManager.Instance.Glazes[i]);
			}

			return availableSkins;
		}

		public bool GlazeSkinOwned(string glazeSkin)
		{
			return _data != null && _data.glazeSkins.Any(s => s.Contains(glazeSkin));
		}

		public CharacterSkin RandomizeRewardedSkin()
		{
			var skins = GetAvailableSkins();
			if (skins.Count <= 0) return null;
			var skin = skins[UnityEngine.Random.Range(0, skins.Count)];
			RequestSave();
			return skin;
		}

		private void UpdateCharacterSkin()
		{
			if (_data == null) return;
			ShopManager.Instance.GetPastryBag(_data.characterSkin);
		}

		public void AddCharacterSkin(string characterSkin)
		{
			if (_data == null || CharacterSkinOwned(characterSkin))
			{
				return;
			}

			_data.characterSkins.Add(characterSkin);
			RequestSave();
		}

		public bool CharacterSkinOwned(string characterSkin)
		{
			if (_data == null)
			{
				return false;
			}

			foreach (string s in _data.characterSkins)
			{
				if (s.Contains(characterSkin))
				{
					return true;
				}
			}

			return false;
		}

		public int CharacterSkinAmount
		{
			get => _data != null ? _data.characterSkins.Count : 1;
		}

		public int CharacterPrice
		{
			get
			{
				int price = Config.SkinPrices[Mathf.Min(BuyCharCount, Config.SkinPrices.Length - 1)];
				return price;
			}
		}

		public List<GiftItem> GetAvaibleGifts()
		{
			var skins = new List<GiftItem>();
			var characterSkins = GetAvailablePastryBags();
			var sprinkleBottles = GetAvailableSprinkleBottles();
			var glazes = GetAvailableGlazes();

			foreach (var character in characterSkins)
			{
				skins.Add(new GiftItem(GiftItemType.IcingBag, character.id));
			}

			foreach (var storage in sprinkleBottles)
			{
				skins.Add(new GiftItem(GiftItemType.SprinkleCup, storage.id));
			}
			foreach (var glaze in glazes)
			{
				skins.Add(new GiftItem(GiftItemType.GlazeCup, glaze.id));
			}

			return skins;
		}

		public CharacterSkin PastryBag { get; private set; }
		public PetData Pet { get; private set; }
		public GlazeData GlazeSkin { get; private set; }


		void UpdatePet()
		{
			Pet = _data == null ? null : PetManager.Instance.GetPet(_data.petSkin);
		}

		public string PetName
		{
			get => _data != null ? _data.petSkin : string.Empty;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.petSkin = value;
				UpdatePet();
				RequestSave();
			}
		}

		public void AddPet(string pet)
		{
			if (_data == null || PetOwned(pet))
			{
				return;
			}

			_data.petSkins.Add(pet);
			Analytics.Instance.LogEvent("pet_owned_" + pet);
			RequestSave();
		}

		public void AddSprinkleBottle(string bottleId)
		{
			if (_data == null || SprinkleBottleOwned(bottleId))
			{
				return;
			}

			_data.storageSkins.Add(bottleId);
			Analytics.Instance.LogEvent("sprinkles_owned_" + bottleId);
			RequestSave();
		}

		public string SprinkleBottleSkinName
		{
			get => _data != null ? _data.storageSkin : string.Empty;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.storageSkin = value;
				UpdateSprinkleBottle();
				RequestSave();
			}
		}

		void UpdateSprinkleBottle()
		{
			if (_data == null)
			{
				SprinkleBottle =
					ShopManager.Instance.SprinkleBottles.Count > 0 ? ShopManager.Instance.SprinkleBottles[0] : null;
			}

			SprinkleBottle = ShopManager.Instance.GetSprinkleBottle(_data.storageSkin);
		}

		public StorageSkin SprinkleBottle { get; private set; }

		public void AddPastryBag(string skinId)
		{
			if (_data == null || PastryBagOwned(skinId))
			{
				return;
			}

			_data.characterSkins.Add(skinId);
			Analytics.Instance.LogEvent("icing_owned_" + skinId);
			RequestSave();
		}

		public string PastryBagName
		{
			get => _data != null ? _data.characterSkin : string.Empty;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.characterSkin = value;
				UpdatePastryBag();
				RequestSave();
			}
		}

		private void UpdatePastryBag()
		{
			if (_data == null)
			{
				PastryBag =
					ShopManager.Instance.PastryBags.Count > 0 ? ShopManager.Instance.PastryBags[0] : null;
			}

			PastryBag = ShopManager.Instance.GetPastryBag(_data.characterSkin);
		}

		public void AddGlazeSkin(string glazeSkin)
		{
			if (_data == null || GlazeSkinOwned(glazeSkin))
			{
				return;
			}

			_data.glazeSkins.Add(glazeSkin);
			Analytics.Instance.LogEvent("glaze_owned_" + glazeSkin);
			RequestSave();
		}

		public string GlazeName
		{
			get => _data != null ? _data.glazeSkin : string.Empty;
			set
			{
				if (_data == null)
				{
					return;
				}

				_data.glazeSkin = value;
				UpdateGlaze();
				RequestSave();
			}
		}

		private void UpdateGlaze()
		{
			if (_data == null)
			{
				GlazeSkin =
					GlazeManager.Instance.Glazes.Count > 0 ? GlazeManager.Instance.Glazes[0] : null;
			}

			GlazeSkin = GlazeManager.Instance.GetGlazeSkin(_data.glazeSkin);
		}

		public void CollectGift(GiftItem giftData)
		{
			switch (giftData.type)
			{
				case GiftItemType.Gem:
					CoinAmount += giftData.gem;
					break;
				case GiftItemType.Pet:
					AddPet(giftData.id);
					PetName = giftData.id;
					break;
				case GiftItemType.SprinkleCup:
					AddSprinkleBottle(giftData.id);
					SprinkleBottleSkinName = giftData.id;
					break;
				case GiftItemType.IcingBag:
					AddPastryBag(giftData.id);
					PastryBagName = giftData.id;
					break;
				case GiftItemType.GlazeCup:
					AddGlazeSkin(giftData.id);
					GlazeName = giftData.id;
					break;
				case GiftItemType.Random:
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public DateTime FirstOpenTime
		{
			get
			{
				try
				{
					return _data != null ? DateTime.Parse(_data.firstTime) : DateTime.Now;
				}
				catch
				{
					return DateTime.Now;
				}
			}
		}

		private void LoadLocal()
		{
			try
			{
				TextReader tr = new StreamReader(Application.persistentDataPath + SaveFile);
				var encryptedJson = tr.ReadToEnd();
				tr.Close();

				var json = Security.Decrypt(encryptedJson, Passphase);
				_data = JsonUtility.FromJson<UserData>(json);
			}
			catch
			{
				// ignored
			}

			if (_data == null)
			{
				_data = new UserData
				{
					firstShowButtonShop = true,
					firstTime = DateTime.Now.ToString(CultureInfo.InvariantCulture)
				};
				RequestSave();
			}

			if (_data.playCount == 0 && Statistics.Instance.PlayCount > 0)
			{
				// Convert old save
				_data.playCount = Statistics.Instance.PlayCount;
			}

			if (_data.level <= 0)
			{
				_data.level = 1;
			}

			if (_data.vipLevel <= 0)
			{
				_data.vipLevel = 1;
			}

			_data.characterSkins ??= new List<string>(10);
			_data.storageSkins ??= new List<string>(10);

			if (_data.tutorialActived == null || _data.tutorialActived.Length < 5)
			{
				_data.tutorialActived = new bool[5];
			}

			_data.galleryCakes ??= new List<string>(10);

			if (ShopManager.Instance.PastryBags != null)
			{
				var skins = ShopManager.Instance.PastryBags;
				if (_data.characterSkins.Count == 0 &&
					skins.Count > 0)
				{
					_data.characterSkins.Add(skins[0].id);
				}
			}

			if (ShopManager.Instance.SprinkleBottles != null)
			{
				var storageSkins = ShopManager.Instance.SprinkleBottles;
				if (_data.storageSkins.Count == 0 && storageSkins.Count > 0)
				{
					_data.storageSkins.Add(storageSkins[0].id);
				}
			}

			if (GlazeManager.Instance.Glazes != null)
			{
				var glazeSkins = GlazeManager.Instance.Glazes;
				if (_data.glazeSkins.Count == 0 && glazeSkins.Count > 0)
				{
					_data.glazeSkins.Add(glazeSkins[0].id);
				}
			}

			if (string.IsNullOrEmpty(_data.characterSkin) ||
				!ShopManager.Instance.HasPastryBag(_data.characterSkin))
			{
				if (_data.characterSkins.Count > 0)
				{
					_data.characterSkin = _data.characterSkins[0];
				}
			}

			if (string.IsNullOrEmpty(_data.storageSkin) ||
				!ShopManager.Instance.HasSprinkleBottle(_data.storageSkin))
			{
				if (_data.storageSkins.Count > 0)
				{
					_data.storageSkin = _data.storageSkins[0];
				}
			}

			if (string.IsNullOrEmpty(_data.glazeSkin) ||
				!GlazeManager.Instance.HasGlazeSkin(_data.glazeSkin))
			{
				if (_data.glazeSkins.Count > 0)
				{
					_data.glazeSkin = _data.glazeSkins[0];
				}
			}

			if (string.IsNullOrEmpty(_data.petSkin) ||
				!PetManager.Instance.HasPet(_data.petSkin))
			{
				if (_data.petSkins.Count > 0)
				{
					_data.petSkin = _data.petSkins[0];
				}
			}
			
			if (string.IsNullOrEmpty(_data.dailyTaskDate))
			{
				DailyTaskDate = DateTime.Now;
			}

			_data.cakeShapes ??= new List<string>(10);

			UpdatePastryBag();
			UpdateSprinkleBottle();
			UpdatePet();
			UpdateGlaze();
		}
		public void SaveGallery(Texture2D texture, string cakeName)
		{
			if (_data.galleryCakes.Any(item => item.Equals(cakeName))) return;
			var bytes = texture.EncodeToPNG();
			File.WriteAllBytes($"{Application.persistentDataPath}/{cakeName}.png", bytes);
			_data.galleryCakes.Add(cakeName);
			RequestSave();
		}

		public List<string> GalleryCakes => _data?.galleryCakes;

		public bool Vip { get; internal set; } = false;

		private bool _modifed;

		public void RequestSave()
		{
			_modifed = true;
		}

		private void Update()
		{
			if (!_modifed) return;
			_modifed = false;
			SaveLocal();
		}

		private void SaveLocal()
		{
			try
			{
				var json = JsonUtility.ToJson(_data);
				var encryptedJson = Security.Encrypt(json, Passphase);

				TextWriter tw = new StreamWriter(Application.persistentDataPath + SaveFile);
				tw.Write(encryptedJson);
				tw.Close();
			}
			catch
			{
				// ignored
			}
		}

		#region ReviewReward

		public GiftItem rewardGift;

		#endregion
	}
}
