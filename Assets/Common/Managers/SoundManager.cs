using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

[RequireComponent(typeof(AudioSource))]
public class SoundManager : Nama.Singleton<SoundManager>
{
	private AudioSource sfxPlayer;
	private AudioSource musicPlayer;
	private string currentMusicKey = string.Empty;
	private AudioClip music;

	LinkedList<AudioSource> pitchedSfxPlayers = new LinkedList<AudioSource>();

	[SerializeField] private List<string> list;

	private Dictionary<string, AudioClip> dict = new Dictionary<string, AudioClip>();

	AudioClip LoadAudioClip(string key)
	{
		try
		{
			var clip = Resources.Load<AudioClip>("Audio/" + key);
			if (clip != null)
			{
				dict.Add(key, clip);
			}
			return clip;
		}
		catch
		{
			return null;
		}
	}

	AudioClip GetAudioClip(string key)
	{
		AudioClip clip;
		if (dict.TryGetValue(key, out clip))
		{
			return clip;
		}

		clip = LoadAudioClip(key);
		return clip;
	}

	public void PlaySFX(string key)
	{
		if (!Nama.Preference.Instance.SfxOn)
		{
			return;
		}
		var clip = GetAudioClip(key);
		if (clip != null)
		{
			sfxPlayer.PlayOneShot(clip);
		}
	}

	public void PlaySFX(string key, float pitch)
	{
		if (!Nama.Preference.Instance.SfxOn)
		{
			return;
		}
		var clip = GetAudioClip(key);
		if (clip == null)
		{
			return;
		}
		AudioSource player;
		if (pitchedSfxPlayers.First == null)
		{
			player = gameObject.AddComponent<AudioSource>();
		}
		else
		{
			player = pitchedSfxPlayers.First.Value;
			pitchedSfxPlayers.RemoveFirst();
		}
		player.pitch = pitch;
		player.PlayOneShot(clip);
		DOVirtual.DelayedCall(clip.length, () => { pitchedSfxPlayers.AddLast(player); });
	}

	public bool MusicPlaying { get { return !string.IsNullOrEmpty(currentMusicKey); } }

	public void PlayMusic(string key, bool loop = false, float delay = 0)
	{
		if (!Nama.Preference.Instance.MusicOn)
		{
			return;
		}

		if (currentMusicKey.Equals(key))
		{
			return;
		}
		currentMusicKey = key;
		// Play music logic here

		var clip = GetAudioClip(key);
		if (clip != null)
		{
			musicPlayer.PlayOneShot(clip);
			musicPlayer.PlayDelayed(delay);
			musicPlayer.loop = loop;
		}
	}

	public void StopMusic()
	{
		currentMusicKey = string.Empty;
		musicPlayer.Stop();
	}

	public void ResumeMusic()
	{
		if (!Nama.Preference.Instance.MusicOn ||
			string.IsNullOrEmpty(currentMusicKey) ||
			musicPlayer.isPlaying)
		{
			return;
		}
		musicPlayer.Play();
	}

	public void SetLoopMusic(bool value)
	{
		musicPlayer.loop = value;
	}

	private void Awake()
	{
		AudioSource[] audioSources = GetComponents<AudioSource>();
		if (audioSources.Length < 2)
		{
			Array.Resize(ref audioSources, 2);
			for (int i = 0; i < 2; i++)
			{
				audioSources[i] = gameObject.AddComponent<AudioSource>();
			}
		}
		sfxPlayer = audioSources[0];
		musicPlayer = audioSources[1];

		if (list != null)
		{
			foreach (var key in list)
			{
				LoadAudioClip(key);
			}
		}
	}
}