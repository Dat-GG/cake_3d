using UnityEngine;

namespace Nama
{

	internal class Analytics : Singleton<Analytics>
	{
		public void LogEvent(string eventName)
		{

#if UNITY_EDITOR || DEBUG_ENABLED
			Debug.Log("LogEvent: " + eventName);
#endif
		}

		public void LogEvent(string eventName, string paramName, string paramValue)
		{
#if UNITY_EDITOR || DEBUG_ENABLED
			Debug.Log($"LogEvent: {eventName}, {paramName}={paramValue}");
#endif
		}


		public void LogEvent(string eventName, string paramName, float paramValue)
		{
#if UNITY_EDITOR || DEBUG_ENABLED
			Debug.Log($"LogEvent: {eventName}, {paramName}={paramValue}");
#endif
		}
	}
}