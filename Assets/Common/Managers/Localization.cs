﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

namespace Nama
{
	public class Localization : Singleton<Localization>
	{
		private bool Initialized = false;
		private Dictionary<string, string[]> dict = new Dictionary<string, string[]>();
		private int startData = 2;
		private const string LANGUAGE_KEY = "LANGUAGE_KEY";
		private const string LOCALIZATION_FILE = "Localization";
		private Language language = Language.English;
		static List<string> suggestList = new List<string>();

		public void Init(SystemLanguage systemLanguage)
		{
			var csv = Resources.Load<TextAsset>(LOCALIZATION_FILE).text;
			CSVReader.LoadFromString(csv,
				(lineIndex, content) =>
				{
					if (lineIndex == 1)
					{
						suggestList.Add(content[0]);
					}
					else if (lineIndex != 0)
					{
						for (int i = 0; i < content.Count - startData; i++)
						{

							if (!string.IsNullOrEmpty(content[i + 2]))
							{

								if (dict.ContainsKey(content[0]))
								{
									dict[content[0]][i] = content[i + startData];
								}
								else
								{
									dict.Add(content[0], new string[content.Count - 2]);
									dict[content[0]][i] = content[i + startData];
								}
							}
						}
					}
				});
			ChangeLanguage(systemLanguage);
			Initialized = true;
		}

		public string Get(string key)
		{
			if (!Initialized)
			{
				Init(Application.systemLanguage);
			}
			string[] value = null;
			bool result = dict.TryGetValue(key, out value);
			if (result)
			{
				if (!string.IsNullOrEmpty(value[(int)language]))
					return value[(int)language];
			}
			return "";
		}

		public void ChangeLanguage(SystemLanguage systemLanguage)
		{
			var count = Enum.GetNames(typeof(Language)).Length;
			for (int i = 0; i < count; i++)
			{
				if (systemLanguage.ToString().Contains(((Language)i).ToString()))
				{
					SetLanguage((Language)i);
				}
			}
		}

		static bool initSuggest = false;
		static void InitSuggest()
		{
			var csv = Resources.Load<TextAsset>(LOCALIZATION_FILE).text;
			CSVReader.LoadFromString(csv,
				(lineIndex, content) =>
				{
#if UNITY_EDITOR
					if (lineIndex > 1)
					{
						suggestList.Add(content[0]);
					}
#endif
				});
			initSuggest = true;
		}

		public static List<string> GetSuggest(string key)
		{
			if (!initSuggest)
			{
				InitSuggest();
			}
			var result = new List<string>(5);
			for (int i = 0; i < suggestList.Count; i++)
			{
				if (suggestList[i].ToLower().Contains(key.ToLower()))
				{
					result.Add(suggestList[i]);
				}
			}
			return result;
		}

		void SetLanguage(Language _language)
		{
			this.language = _language;
			PlayerPrefs.SetInt(LANGUAGE_KEY, (int)_language);
		}
	}

	enum Language
	{
		English,
		Vietnamese,
		Portuguese,
		Russian,
		German,
		Thai,
		Korean,
		French,
		ChineseSimplified,
		ChineseTraditional,
		Japanese,
		Spanish,
		Arabic
	}
}