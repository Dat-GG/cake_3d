﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Nama {
	public class UILocalization : MonoBehaviour
	{
		public string key = "";
		private Text text;
		private void OnEnable()
		{
			Set();
			EventManager.Instance.Subscribe(EventType.OnChangeLanguage, Set);
		}

		private void OnDisable()
		{
			var eventManager = EventManager.Instance;
			if(eventManager != null)
			{
				EventManager.Instance.Unsubscribe(EventType.OnChangeLanguage, Set);
			}
		}

		void Set(object data = null)
		{
			if (text == null)
			{
				text = GetComponent<Text>();
			}
			if (text != null)
			{
				if (!string.IsNullOrEmpty(key))
				{
					text.text = Localization.Instance.Get(key);
				}
			}
		}
	}
}