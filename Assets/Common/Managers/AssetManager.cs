using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Nama
{
	internal class AssetManager : Singleton<AssetManager>
	{
		private static List<string> _levels;

		private static readonly string[] LevelListName = {
			"LevelsMulti",
			"LevelsNM"
		};

		public static void ReloadLevelList()
		{
			var index = Config.Instance.NewMaterial ? 1 : 0;
			_levels = LoadCsv(LevelListName[index]);
		}

		public static List<string> LevelList
		{
			get
			{
				if (_levels == null)
				{
					ReloadLevelList();
				}
				return _levels;
			}
		}

		[SerializeField] private Guideline guideline;
		public Guideline NewGuideline(Transform parent, Polyline poly, float surfaceRadius)
		{
			var instance = Instantiate(guideline, parent);
			instance.Init(poly, surfaceRadius);
			instance.gameObject.SetActive(false);
			return instance;
		}

		[SerializeField] private StrokeDisplay strokeDisplay;
		public StrokeDisplay NewStrokeDisplay(Transform parent)
		{
			return Instantiate(strokeDisplay, parent);
		}

		[SerializeField] private Material cakeMaterial;
		public Material CakeMaterial => cakeMaterial;
		[SerializeField] private Material cakeGlitterMaterial;
		public Material CakeGlitterMaterial => cakeGlitterMaterial;

		[SerializeField] private CakeRenderer cakeRenderer;
		public CakeRenderer CakeRenderer => cakeRenderer;

		[SerializeField] private MeshRenderer fillHint;
		public MeshRenderer FillHint => fillHint;

		[SerializeField] private ProgressList progressList;
		public ProgressList ProgressList => progressList;

		[SerializeField] private Material mirrorGlazeMaterial;
		public Material MirrorGlazeMaterial => mirrorGlazeMaterial;

		private void Awake()
		{
#if UNITY_EDITOR
			if (guideline == null)
			{
				guideline = AssetDatabase.LoadAssetAtPath<Guideline>(
					"Assets/Game/Gameplay/Guideline.prefab");
			}
			if (strokeDisplay == null)
			{
				strokeDisplay = AssetDatabase.LoadAssetAtPath<StrokeDisplay>(
					"Assets/Game/Gameplay/StrokeDisplay.prefab");
			}
			if (cakeMaterial == null)
			{
				cakeMaterial = AssetDatabase.LoadAssetAtPath<Material>(
					"Assets/Game/Models/cake.mat");
			}
			if (cakeRenderer == null)
			{
				cakeRenderer = AssetDatabase.LoadAssetAtPath<CakeRenderer>(
					"Assets/Game/Gameplay/CakeRenderer.prefab");
			}
			if (fillHint == null)
			{
				fillHint = AssetDatabase.LoadAssetAtPath<MeshRenderer>(
					"Assets/_FillColors/FillHint.prefab");
			}

			if(progressList == null)
			{
				progressList = AssetDatabase.LoadAssetAtPath<ProgressList>(
					"Assets/Game/RewardProgress/ProgressList.prefab");
			}
#endif
		}

		private static readonly int[] VipLevelIndices =
		{
			2, 6, 10, 14, 18, 22, 26, 30, 34, 38, 42
		};
		
		public static readonly string[] VipClients =
		{
			"VIP",
			"Vampire",
			"VIP2",
			"Bride",
			"VIP",
			"BirthDayBoy",
			"VIP2",
			"Vampire",
			"BirthDayBoy",
			"Bride",
			"Vampire",
		};

		private static readonly string[] VipLevelNames =
		{
			"LevelsShort/tuan_circle_big_011_vip",
			"LevelsMulti/vam_bee_01_vip",
			"LevelsMulti/multi_tuan_11_vip",
			"LevelsMulti/multi_tuan_12_vip",
			"LevelsMulti/multi_tuan_13_vip",
			"LevelsShort/mkt_hoa_02a_vip",
			"LevelsMulti/multi_tuan_10_vip",
			"LevelsMulti/vam_bee_03_vip",
			"LevelsMulti/tuan_mirror_05_vip",
			"LevelsMulti/multi_tuan_15_vip",
			"LevelsMulti/vam_bee_04_vip",
		};

		public static Level GetLevel()
		{
			string levelName;
			var index = Profile.Instance.VipLevel - 1;
			if (index >= 0 && index < VipLevelIndices.Length && VipLevelIndices[index] == Profile.Instance.Level)
			{
				levelName = VipLevelNames[index];
			}
			else
			{
				var levels = LevelList;
				levelName = levels[(Profile.Instance.Level-1) % levels.Count];
			}
			var prefab = Resources.Load<Level>(levelName);
			return prefab;
		}

		private static BrushTexture _brushTexture;
		public static BrushTexture BrushTexture
		{
			get
			{
				if (_brushTexture != null)
				{
					return _brushTexture;
				}
				var prefab = Resources.Load<BrushTexture>("BrushTexture");
				_brushTexture = Instantiate(prefab);
				_brushTexture.gameObject.SetActive(false);
				_brushTexture.transform.localPosition = new Vector3(0, 1000, 0);
				_brushTexture.gameObject.hideFlags = HideFlags.HideAndDontSave;
				return _brushTexture;
			}
		}

		private GameObject[] _sprinklesPreviewPresetArray;
		public GameObject GetSprinklesPreviewPreset(int preset)
		{
			_sprinklesPreviewPresetArray ??= Resources.LoadAll<GameObject>("SprinklesPreviewPreset");
			return preset >= _sprinklesPreviewPresetArray.Length ? null : Instantiate(_sprinklesPreviewPresetArray[preset]);
		}

		public static List<string> LoadCsv(string csvFile)
		{
			var csv = Resources.Load<TextAsset>(csvFile).text;
			var columns = new Dictionary<string, int>();
			var list = new List<string>(100);
			CSVReader.LoadFromString(csv,
				(lineIndex, content) =>
				{
					if (lineIndex == 0)
					{
						for (var i = 0; i < content.Count; i++)
						{
							if (!string.IsNullOrEmpty(content[i]))
								columns.Add(content[i], i);
						}
					}
					else
					{
						list.Add(content[columns["Level"]]);
					}
				});
			return list;
		}
		public static GameObject GetGlaze(string id)
		{
			var prefab = Resources.Load<GameObject>("GlazeSkins/" + id);
			return Instantiate(prefab);
		}
		
		public static PastrySkin GetPastryBag(string name)
		{
			var prefab = Resources.Load<PastrySkin>("PastrySkins/" + name);
			return Instantiate(prefab);
		}
		
		public static Bottle GetBottle(string id)
		{
			var bottle = Resources.Load<Bottle>("BottleSkins/" + id);
			return Instantiate(bottle);
		}

		private static Material _glitterStrokeMaterial;
		public static Material GetGlitterStrokeMaterial()
		{
			if (_glitterStrokeMaterial == null)
			{
				_glitterStrokeMaterial = Resources.Load<Material>("glitterStroke");
			}
			return _glitterStrokeMaterial;
		}

		private static Material _glitterFillMaterial;
		public static Material GetGlitterFillMaterial()
		{
			if (_glitterFillMaterial == null)
			{
				_glitterFillMaterial = Resources.Load<Material>("glitterFill");
			}
			return _glitterFillMaterial;
		}

		public ProgressData CurrentProgress(int level)
		{
			return progressList.list.FirstOrDefault(p => p.levelStart <= level && level <= p.levelEnd);
		}

		public string ClientName { get; set; } = "";
	}
}