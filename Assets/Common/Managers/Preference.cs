using UnityEngine;

namespace Nama
{
	class Preference : Singleton<Preference>
	{
		const string OPTION_SFX = "OPTION_SFX";
		const string OPTION_MUSIC = "OPTION_MUSIC";
		const string OPTION_VIBRATION = "OPTION_VIBRATION";

		private void Awake()
		{
			sfxOn = PlayerPrefs.GetInt(OPTION_SFX, 1) > 0;
			vibrationOn = PlayerPrefs.GetInt(OPTION_VIBRATION, 1) > 0;
		}

		bool sfxOn = false;
		public bool SfxOn
		{
			get { return sfxOn; }
			set
			{
				sfxOn = value;
				PlayerPrefs.SetInt(OPTION_SFX, sfxOn ? 1 : 0);
			}
		}

		bool musicOn = true;
		public bool MusicOn
		{
			get { return musicOn; }
			set
			{
				musicOn = value;
				PlayerPrefs.SetInt(OPTION_MUSIC, musicOn ? 1 : 0);
			}
		}

		bool vibrationOn = true;
		public bool VibrationOn
		{
			get { return vibrationOn; }
			set
			{
				vibrationOn = value;
				PlayerPrefs.SetInt(OPTION_VIBRATION, vibrationOn ? 1 : 0);
			}
		}
	}
}
