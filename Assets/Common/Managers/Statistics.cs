using UnityEngine;

namespace Nama
{
	internal class Statistics : Singleton<Statistics>
	{
		public static float PaintRangeX => 3;
		public static float PaintRangeZ => 3;
		const string RATING_REQUEST_SHOWN = "RatingRequestShown";
		const string PLAY_COUNT = "PlayCount";
		const string KEY_COUNT = "KeyCount";
		private const string KEY_LEVEL_COLLECTED = "KeyCollected";
		const string FIRST_OPEN = "FirstOpen";

		private void Awake()
		{
			_ratingRequestShown = PlayerPrefs.GetInt(RATING_REQUEST_SHOWN, 0) > 0;
			_playCount = PlayerPrefs.GetInt(PLAY_COUNT, 0);
			_keyCount = PlayerPrefs.GetInt(KEY_COUNT, 0);
			_firstOpen = PlayerPrefs.GetInt(FIRST_OPEN, 0) <= 0;
		}

		private bool _ratingRequestShown;
		public bool RatingRequestShown
		{
			get => _ratingRequestShown;
			set
			{
				_ratingRequestShown = value;
				PlayerPrefs.SetInt(RATING_REQUEST_SHOWN, _ratingRequestShown ? 1 : 0);
			}
		}

		private int _playCount;
		public int PlayCount
		{
			get => _playCount;
			set
			{
				_playCount = value;
				PlayerPrefs.SetInt(PLAY_COUNT, _playCount);
			}
		}

		private int _keyCount = 0;
		public int KeyCount
		{
			get => _keyCount;
			set
			{
				_keyCount = value;
				PlayerPrefs.SetInt(KEY_COUNT, _keyCount);
			}
		}

		public static bool IsKeyCollected(int level)
		{
			return PlayerPrefs.GetInt(KEY_LEVEL_COLLECTED + level, 0) == 1;
		}

		public static void CollectKey(int level)
		{
			PlayerPrefs.SetInt(KEY_LEVEL_COLLECTED + level, 1);
		}

		private bool _firstOpen = true;
		public bool FirstOpen
		{
			get
			{
				var value = _firstOpen;
				if (!_firstOpen) return value;
				_firstOpen = false;
				PlayerPrefs.SetInt(FIRST_OPEN, 1);
				return value;
			}
		}
	}
}
