﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nama
{
	public class NamaDebug : MonoBehaviour
	{
		[SerializeField] bool alwayOpen;
		[SerializeField]
		TextAnchor[] password = new TextAnchor[]
		{
			TextAnchor.UpperLeft,
			TextAnchor.UpperRight,
			TextAnchor.UpperRight,
			TextAnchor.LowerRight,
			TextAnchor.LowerRight,
			TextAnchor.LowerRight,
			TextAnchor.LowerLeft,
			TextAnchor.LowerLeft,
			TextAnchor.LowerLeft,
			TextAnchor.LowerLeft,
		};
		[SerializeField] GameObject[] debugObjects;

		TextAnchor area;
		int currentIndex = 0;
		bool opened = false;
		float deltaTime = 0.0f;
		static readonly string DEBUG_KEY = "DEBUG_KEY";

		private void Start()
		{
			var open = PlayerPrefs.GetInt(DEBUG_KEY, -1) > 0;
			SetActive(alwayOpen || open);
		}

		void SetActive(bool active)
		{
			foreach (var o in debugObjects)
			{
				o.SetActive(active);
			}
		}

		private void ToggleOpen()
		{
			opened = !opened;
			PlayerPrefs.SetInt(DEBUG_KEY, opened ? 1 : -1) ;
			SetActive(opened);
		}

		private void Update()
		{
			if (Input.GetMouseButtonDown(0))
			{
				area = GetArea(Input.mousePosition);
				if (area == password[currentIndex])
				{
					currentIndex++;
					if (currentIndex >= password.Length)
					{
						ToggleOpen();
						currentIndex = 0;
					}
				}
				else
				{
					currentIndex = 0;
				}
			}

			deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
		}

		public void Restart(int i)
		{
			Profile.Instance.Level += i;
			SceneManager.Instance.ReloadScene(SceneID.Gameplay);
		}

		TextAnchor GetArea(Vector3 position)
		{
			TextAnchor area;
			if (position.x < Screen.width / 2f)
			{
				if (position.y > Screen.height / 2f)
				{
					area = TextAnchor.UpperLeft;
				}
				else
				{
					area = TextAnchor.LowerLeft;
				}
			}
			else
			{
				if (position.y > Screen.height / 2f)
				{
					area = TextAnchor.UpperRight;
				}
				else
				{
					area = TextAnchor.LowerRight;
				}
			}
			return area;
		}
	}
}