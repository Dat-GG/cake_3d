using Tabtale.TTPlugins;
using UnityEngine;

namespace Nama
{
	internal class GameManager : Singleton<GameManager>
	{
		public static bool IsLoadedGameplay { get; set; }

#if UNITY_EDITOR
		public static bool Initialized { get; set; }
#endif
		private void Awake()
		{
			Application.targetFrameRate = 60;
			Input.multiTouchEnabled = false;
			TTPCore.Setup();
#if UNITY_EDITOR
			Initialized = true;
#endif
			SceneManager.Instance.OpenScene(SceneID.Loading);
			SceneManager.Instance.OpenScene(Profile.Instance.Level == 1
				? SceneID.Gameplay
				: SceneID.UIBakery);
		}
	}
}
