using System.Collections.Generic;
using UnityEngine;

namespace Nama
{
	class Config : Singleton<Config>
	{
		public static float DrawSpeed = 3.0f;
		public static float FallbackDistance = 1.0f;
		public static bool DrawVibrate = true;
		public const float CharacterDelay = 0.3f;
		public static float SprinkleDelay = 0.3f;
		public const float SWIPE_SPEED = 1.315f;
		public const int SPECIAL_COIN_EANRED = 50;
		public static readonly string EXPERIMENT_NAME = "Test_211015";
		public static int EXPERIMENT_COUNT = 2;
		public static float  MAX_DRAW_SPEED_1 = 3.8f; // Toc do gioi han 1, don vi la unit/second
		public static float  MAX_DRAW_SPEED_2 = 9.7f; // Toc do gioi han 2, don vi la unit/second
		public static float  DRAW_SPEED_POINT = 9f; // Neu toc do lon hon gia tri nay thi gioi han se la MAX_DRAW_SPEED_2 va nguoc lai
		public static float MIN_LENGTH_FACTOR = 1.2f;
		public static float MAX_LENGTH_FACTOR = 6.6f;
		public static float DRAW_SPEED_MULTIPLY = 0.55f; // He so van toc so voi quang duong
		public static float DELAY_TIME = 0.25f; // Thoi gian delay ve tu do neu chon sai mau
		// Camera slow cho stroke manual
		public static float MIN_STROKE_WIDTH = 1f; // Neu do rong cua net ve ngan hon 1 thi su dung SHORT_STROKE_SPEED
		public static float NEXT_STROKE_SPEED = 1.5f; // Toc do di chuyen co ban camera khi sang net ve moi
		public static float NEXT_STROKE_SPEED_FACTOR = 0.6f; // Toc do di chuyen camera khi sang net ve moi tinh theo khoang cach
		public static float LONG_STROKE_SPEED = 2f; // Toc do camera di theo net but dai
		public static float SHORT_STROKE_SPEED = 0.1f; // Toc do camera di theo net but dai
		// Magnet sprinkle
		public static float MAGNET_RADIUS = 0.55f; // Ban kinh snap
		public static float CORRECT_RADIUS = 0.15f; // Neu trong ban kinh nay se duoc tinh la dung
		public static float LOSE_CONTROL_FORCE = 1.0f; // Luc vuot khien cho net ve bi lech ra ngoai
		public static float MAGNET_FORCE = 0.25f; // Luc hut khi o trong pham vi MAGNET_RADIUS

		public static readonly int[] SkinPrices = new int[] { 500, 1000, 1500, 2000, 2500, 3000 };
		public static readonly int[] ThemePrices = new int[] { 300, 500, 1000, 1500, 2000, 2500 };
		public static readonly int IdleEarningMin = 1;
		public static readonly int IdleEarningMax = 5;
		public float InterstitialCappingTime { get; private set; } = 60f;
		public float FirstInterstitialCappingTime { get; private set; } = 30f;
		public float InterstitialRewardedVideoCappingTime { get; private set; } = 20f;
		public bool InterstitialAsRewadedVideo { get; private set; } = false;
		public int GamesForInterstitial { get; private set; } = 10;
		public int GamesForRating { get; private set; } = 20;
		public bool BannerEnabled { get; private set; } = false;

		public float DelayDisplaySkipButtonTime { get; private set; } = 1f;

		public CostData CostData { get; private set; } = CostData.Default;
		public InComeData InComeData { get; private set; } = InComeData.Default;

		public int Experiment { get; set; } = 0;
		public bool BonusAndSpecialOption => false;
		public bool DisableProgress => true;
		public bool StrokeManualEnabled { get; private set; } = true; // Apply
		public bool StepAdsEnable { get; private set; } = true; // Apply
		public bool MagnetEnable { get; private set; } = true; // Apply

		public bool NewMaterial => false;
	}
}
