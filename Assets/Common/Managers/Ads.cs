using System;
using System.Collections.Generic;
using Tabtale.TTPlugins;

namespace Nama
{
	internal enum RewardedVideoState
	{
		Failed,
		Watched
	}

	internal class Ads : Singleton<Ads>
	{
		private static readonly Dictionary<string, object> EventParameters = new Dictionary<string, object>();

		internal static void ShowInterstitial(Action onFinish, string place)
		{
			TTPInterstitials.Show(place, onFinish);
		}

		internal static void ShowRewardedVideo(string place, Action<RewardedVideoState> callback)
		{
			TTPRewardedAds.Show(place, ok => { callback(ok ? RewardedVideoState.Watched : RewardedVideoState.Failed); });
		}

		internal static void SendRvImpression(string location)
		{
			var adReady = TTPRewardedAds.IsReady() ? 1 : 0;
			var paramseters = new Dictionary<string, object>
			{
				{"location", location},
				{"adReady", adReady}
			};
			if (Gameplay.PreviewLevel != null && Gameplay.PreviewLevel.CakeType == CakeType.Vip)
			{
				paramseters.Add("lastVipMissionID", Profile.Instance.VipLevel - 1);
			}
			else
			{
				paramseters.Add("lastMissionID", Profile.Instance.Level - 1);
			}
			SendCustomFirebaseEvent("rvImpression", paramseters);
		}

		internal static void SendRvClicked(string location)
		{
			var adReady = TTPRewardedAds.IsReady() ? 1 : 0;
			var paramseters = new Dictionary<string, object>
			{
				{"location", location},
				{"adReady", adReady}
			};
			if (Gameplay.PreviewLevel != null && Gameplay.PreviewLevel.CakeType == CakeType.Vip)
			{
				paramseters.Add("lastVipMissionID", Profile.Instance.VipLevel - 1);
			}
			else
			{
				paramseters.Add("lastMissionID", Profile.Instance.Level - 1);
			}
			SendCustomFirebaseEvent("rvClicked", paramseters);
		}

		internal static void SendRvWatched(string location)
		{
			var paramseters = new Dictionary<string, object> {{"location", location}};
			if (Gameplay.PreviewLevel != null && Gameplay.PreviewLevel.CakeType == CakeType.Vip)
			{
				paramseters.Add("lastVipMissionID", Profile.Instance.VipLevel - 1);
			}
			else
			{
				paramseters.Add("lastMissionID", Profile.Instance.Level - 1);
			}
			SendCustomFirebaseEvent("rvWatched", paramseters);
		}

		internal static void SendCustomFirebaseEvent(string name, IDictionary<string, object> parameters)
		{
			TTPAnalytics.LogEvent(AnalyticsTargets.ANALYTICS_TARGET_FIREBASE, name, parameters, false);
		}

		internal static void SendMissionStart()
		{
			EventParameters.Clear();
			EventParameters.Add("missionStartedType", UIResult.Replayed ? "replay" : "new");
			EventParameters.Add("CoinBalance", Profile.Instance.CoinAmount);

			if (Gameplay.Level != null && Gameplay.Level.CakeType == CakeType.Vip)
			{
				EventParameters.Add("lastMissionID", Profile.Instance.VipLevel);
				SendCustomFirebaseEvent("VIPLevelStarted", EventParameters);
			}
			else
			{
				TTPGameProgression.FirebaseEvents.MissionStarted(Profile.Instance.Level, EventParameters);
			}
		}

		internal static void SendMissionCompleted()
		{
			EventParameters.Clear();
			EventParameters.Add("CoinBalance", Profile.Instance.CoinAmount);
			EventParameters.Add("missionStartedType", UIResult.Replayed ? "replay" : "new");
			
			if (Gameplay.Level != null && Gameplay.Level.CakeType == CakeType.Vip)
			{
				EventParameters.Add("lastMissionID", Profile.Instance.VipLevel);
				SendCustomFirebaseEvent("VIPLevelCompleted", EventParameters);
			}
			else
			{
				EventParameters.Add("lastMissionID", Profile.Instance.Level);
				TTPGameProgression.FirebaseEvents.MissionComplete(EventParameters);
			}
		}
	}
}