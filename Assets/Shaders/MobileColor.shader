Shader "Mobile/Color" {
	Properties{
		_Color("Main Color", COLOR) = (1,1,1,1)
	}

	SubShader{
		Tags { "RenderType" = "Opaque" }
		LOD 150

		CGPROGRAM
		#pragma surface surf Lambert noforwardadd

		float4 _Color;

		struct Input {
			float placeHolder;
		};

		void surf(Input IN, inout SurfaceOutput o) {
			o.Albedo = _Color.rgb;
			o.Alpha = _Color.a;
		}
		ENDCG
	}

	Fallback "Mobile/VertexLit"
}