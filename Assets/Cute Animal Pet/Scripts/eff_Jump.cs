﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eff_Jump : MonoBehaviour {

	// Use this for initialization
	void Start () {
        this.GetComponent<Animation>().Play();
	}
	
	// Update is called once per frame
	void Update () {
        if (this.GetComponent<Animation>().isPlaying == false)
        {
            DestroyImmediate(this.gameObject);
        }

	}
}
