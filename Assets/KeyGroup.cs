﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nama
{
	public class KeyGroup : MonoBehaviour
	{
		[SerializeField] private Camera cam;
		public Vector3 KeyTarget
		{
			get
			{
				Vector3 point = new Vector3();
				var screenPoint = keyArray[Mathf.Clamp(Profile.Instance.KeyAmount, 0, 2)].GetComponent<RectTransform>().position;
				point = cam.ScreenToWorldPoint(new Vector3(screenPoint.x, screenPoint.y, cam.nearClipPlane + 8));
				return point;
			}
		}

		[SerializeField] Animator[] keyArray;

		public void UpdateKey(bool add)
		{
			var keyAmount = Profile.Instance.KeyAmount + Gameplay.Instance.KeyAmount;
			if (add)
			{
				keyArray[Mathf.Clamp(keyAmount - 1, 0, 2)].SetTrigger("collect");
				return;
			}

			for (int i = 0; i < keyArray.Length; i++)
			{
				if (i < keyAmount)
				{
					keyArray[i].SetTrigger("has_key");
				}
				else
				{
					keyArray[i].SetTrigger("empty");
				}
			}
		}
	}
}