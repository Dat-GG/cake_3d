﻿
Shader "Custom/ShadowColorUnlit"
{
	Properties{
		_Color("Main Color", COLOR) = (1,1,1,1)
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" }
		Pass
		{
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

			sampler2D_float _ShadowTex;
			float4x4 _ShadowMatrix;
			float _ShadowBias;
			float _OneMinusShadowStrength;
			float4 _Color;

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 shadowCoords : TEXCOORD0;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);

				float3 worldPos = mul(unity_ObjectToWorld, float4(v.vertex.xyz, 1.0)).xyz;
				o.shadowCoords = mul(_ShadowMatrix, float4(worldPos, 1.0));
				return o;
			}

			fixed4 frag(v2f IN) : SV_Target
			{
#if SHADER_API_GLES || SHADER_API_GLES3
				float lightDepth = tex2D(_ShadowTex, IN.shadowCoords).r;
#else
				float lightDepth = 1.0 - tex2D(_ShadowTex, IN.shadowCoords).r;
#endif
				float shadow = (IN.shadowCoords.z - _ShadowBias) < lightDepth ? 1.0 : _OneMinusShadowStrength;
				return float4(_Color.xyz * shadow, _Color.a);
			}
			ENDCG
		}
	}
}
