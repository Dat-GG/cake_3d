﻿


Shader "Custom/ShadowColorAlpha" {
	Properties{
		_Color("Main Color", COLOR) = (1,1,1,1)
	}
	SubShader{
		Tags { "RenderType" = "Transparent" }
		LOD 200

		CGPROGRAM
		#pragma surface surf Lambert noforwardadd vertex:vert alpha

		sampler2D_float _ShadowTex;
		float4x4 _ShadowMatrix;
		float _ShadowBias;
		float _OneMinusShadowStrength;

		float4 _Color;

		struct Input {
			float4 shadowCoords;
		};

		void vert(inout appdata_full v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input, o);
			float3 worldPos = mul(unity_ObjectToWorld, float4(v.vertex.xyz, 1.0)).xyz;
			o.shadowCoords = mul(_ShadowMatrix, float4(worldPos, 1.0));
		}

		void surf(Input IN, inout SurfaceOutput o)
		{
#if SHADER_API_GLES || SHADER_API_GLES3
			float lightDepth = tex2D(_ShadowTex, IN.shadowCoords).r;
#else
			float lightDepth = 1.0 - tex2D(_ShadowTex, IN.shadowCoords).r;
#endif
			float shadow = (IN.shadowCoords.z - _ShadowBias) < lightDepth ? 1.0 : _OneMinusShadowStrength;
			o.Albedo = _Color.rgb * shadow;
			o.Alpha = _Color.a;
		}
		ENDCG
	}
}
