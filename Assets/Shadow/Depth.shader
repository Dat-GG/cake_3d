// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Depth"
{
	Properties {
	}

	SubShader {
		Tags { "RenderType"="Opaque" }
		Pass {
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#include "UnityCG.cginc"
		struct v2f {
			float4 pos : SV_POSITION;
			float depth : TEXCOORD0;
		};
		v2f vert( appdata_base v ) {
			v2f o;
			UNITY_SETUP_INSTANCE_ID(v);
			o.pos = UnityObjectToClipPos(v.vertex);
			o.depth = 1 - (COMPUTE_DEPTH_01);
			return o;
		}
		fixed4 frag(v2f i) : SV_Target {
			return float4(
				(i.depth * 1),
				0,
				0,
				0);
		}
		ENDCG
		}
	}
}