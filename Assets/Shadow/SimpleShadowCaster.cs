﻿
using UnityEngine;

[ExecuteInEditMode]
public class SimpleShadowCaster : MonoBehaviour
{
	[SerializeField] [Range(0, 1)] float shadowStrength = 0.5f;
	[SerializeField] Shader shader;

	private Camera cam;
	static RenderTexture depthTarget;

	private void OnEnable()
	{
		UpdateResources();
	}

	private void OnValidate()
	{
		UpdateResources();
	}

	private void UpdateResources()
	{
		if (cam == null)
		{
			cam = GetComponent<Camera>();
			cam.SetReplacementShader(shader, "");
		}

		int targetSize = 1024;
		if (depthTarget == null || depthTarget.width != targetSize)
		{
			depthTarget = new RenderTexture(
				targetSize,
				targetSize / 2,
				0,
				RenderTextureFormat.ARGB32,
				RenderTextureReadWrite.Linear);
			depthTarget.wrapMode = TextureWrapMode.Clamp;
			depthTarget.filterMode = FilterMode.Bilinear;
			depthTarget.autoGenerateMips = false;
			depthTarget.useMipMap = false;
		}
		if (cam.targetTexture != depthTarget)
		{
			cam.targetTexture = depthTarget;
		}
	}

	static Matrix4x4 bias = new Matrix4x4()
	{
		m00 = 0.5f,
		m01 = 0,
		m02 = 0,
		m03 = 0.5f,
		m10 = 0,
		m11 = 0.5f,
		m12 = 0,
		m13 = 0.5f,
		m20 = 0,
		m21 = 0,
		m22 = 0.5f,
		m23 = 0.5f,
		m30 = 0,
		m31 = 0,
		m32 = 0,
		m33 = 1,
	};

	private void OnPostRender()
	{
		if (enabled)
		{
			Matrix4x4 view = cam.worldToCameraMatrix;
			Matrix4x4 proj = cam.projectionMatrix;
			Matrix4x4 mtx = bias * proj * view;

			Shader.SetGlobalMatrix("_ShadowMatrix", mtx);
			Shader.SetGlobalTexture("_ShadowTex", depthTarget);
			Shader.SetGlobalFloat("_ShadowStrength", shadowStrength / 2);
		}
	}
}
